// -*- c++ -*-
//------------------------------------------------------------------------------
//                               FileSaveDialog.h
//------------------------------------------------------------------------------
// $Id: FileSaveDialog.h,v 1.1 2007/12/02 00:31:24 vlg Exp $
//------------------------------------------------------------------------------
//  Copyright (c) 2006 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version
//  2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------
#ifndef FILE_SAVE_DIALOG_H
#define FILE_SAVE_DIALOG_H

#include <gtkmm/filechooserdialog.h>
#include <gtkmm/fileselection.h>

#include "Granule-main.h"
#include "Granule.h"

class FileSaveDialog
{
public:
	FileSaveDialog (const Glib::ustring& title_,
					Gtk::Widget*         parent_,
					const Glib::ustring& filter_name_    = "",
					const Glib::ustring& filter_pattern_ = "");

	~FileSaveDialog ();

	Glib::ustring get_filename     () const;
	void          set_current_name (const Glib::ustring& name_);

	int  run  ();
	void show ();
	void hide ();

private:

#ifdef OBSOLETE
	Gtk::FileSelection*     m_dialog;
#else
	Gtk::FileChooserDialog* m_dialog;
#endif
};

//------------------------------------------------------------------------------
// Inline functions
//------------------------------------------------------------------------------
inline
FileSaveDialog::
~FileSaveDialog ()
{
	if (m_dialog) {
		delete m_dialog;
		m_dialog = NULL;
	}
}

inline Glib::ustring 
FileSaveDialog::
get_filename () const
{
	return (m_dialog->get_filename ());
}

inline void
FileSaveDialog::
set_current_name (const Glib::ustring& name_)
{
#ifdef OBSOLETE
	m_dialog->set_filename (name_);
#else
	m_dialog->set_current_name (name_);
#endif
}

inline int
FileSaveDialog::
run ()
{
	return (m_dialog->run ());
}
	
inline void
FileSaveDialog::
hide ()
{
	m_dialog->hide ();
}

inline void
FileSaveDialog::
show ()
{
	m_dialog->show ();
}



#endif /* FILE_SAVE_DIALOG_H */
