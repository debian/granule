// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: TextColorsDB.cpp,v 1.2 2008/01/16 22:13:11 vlg Exp $
//------------------------------------------------------------------------------
//                            TextColorsDB.cpp
//------------------------------------------------------------------------------
//  Copyright (c) 2007 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date   : Oct 7 2007
//
//------------------------------------------------------------------------------

#include "TextColorsDB.h"

#include "Granule.h"
#include "ColorsSelectorUI.h"

using ASSA::IniFile;

/**-----------------------------------------------------------------------------
 *	Implementation of TextColorsDB Model.
 **-----------------------------------------------------------------------------
 */
void
TextColorsDB::
init_global_defaults ()
{
	m_text_fg_color   = "black";
	m_text_bg_color   = "white";
	m_separator_color = "pink";
	m_frame_color     = "white";
}

void
TextColorsDB::
init_deck_defaults ()
{
	m_text_fg_color   = "#FFD700";  // gold
	m_text_bg_color   = "black";
	m_separator_color = "#FFC0CB";	// ping
	m_frame_color     = "#A5A5A5";  // dark shade of grey
}

/**********************************************
 * Load fonts configuration settings from XML *
 **********************************************/
void
TextColorsDB::
load_from_xml_config (xmlDocPtr parser_, std::string& error_msg_)
{
	xmlChar* result;

	result = Granule::get_node_by_xpath (parser_, "/deck/appearance/@enabled");

	if (result == NULL) {
		return;
	}
	xmlFree (result);

	/*****************
	 * Foreground    *
	 *****************/
	result = Granule::get_node_by_xpath (parser_, 
									 "/deck/appearance/colors/@text_fgcolor");
	if (result == NULL) {
		return;
	}
	set_text_fg_color ((const char*) result);
	xmlFree (result);

	/*****************
	 * Background    *
	 *****************/
	result = Granule::get_node_by_xpath (parser_, 
									 "/deck/appearance/colors/@text_bgcolor");
	if (result == NULL) {
		return;
	}
	set_text_bg_color ((const char*) result);
	xmlFree (result);

	/*****************
	 * Separator     *
	 *****************/
	result = Granule::get_node_by_xpath (parser_, 
									 "/deck/appearance/colors/@sep_color");
	if (result == NULL) {
		return;
	}
	set_separator_color ((const char*) result);
	xmlFree (result);

	/*****************
	 * Frame         *
	 *****************/
	result = Granule::get_node_by_xpath (parser_, 
									 "/deck/appearance/colors/@frame_color");
	if (result == NULL) {
		return;
	}
	set_frame_color ((const char*) result);
	xmlFree (result);
}

void
TextColorsDB::
save_to_xml_config (xmlTextWriterPtr writer_)
{
	int ret = 0;

	// Start <colors> section
	//
	ret = xmlTextWriterStartElement (writer_, BAD_CAST "colors");

	ret = xmlTextWriterWriteAttribute (writer_, BAD_CAST "text_fgcolor",
						   BAD_CAST m_text_fg_color.c_str ());

	ret = xmlTextWriterWriteAttribute (writer_, BAD_CAST "text_bgcolor",
						   BAD_CAST m_text_bg_color.c_str ());

	ret = xmlTextWriterWriteAttribute (writer_, BAD_CAST "sep_color",
						   BAD_CAST m_separator_color.c_str ());

	ret = xmlTextWriterWriteAttribute (writer_, BAD_CAST "frame_color",
						   BAD_CAST m_frame_color.c_str ());

	// Close </colors>
	//
	ret = xmlTextWriterEndElement (writer_);	// @colors
}

/***************************************************
 * Load fonts configuration settings from INI file *
 ***************************************************
 *
 * [Default]
 *    ...
 *    text_bgcolor="white"
 *    text_fgcolor="black"
 *    sep_color="pink"
 *    frame_color="white"
 *    ...
 *
 ***************************************************
 */
void
TextColorsDB::
load_from_ini_config (ASSA::IniFile* inifile_)
{
	Glib::ustring ustr;

	if (inifile_ == NULL) {
		return;
	}

	ustr = inifile_->get_value ("Default", "text_bgcolor");
	if (! ustr.empty ()) {
		set_text_bg_color (ustr);
	}

	ustr = inifile_->get_value ("Default", "text_fgcolor");
	if (! ustr.empty ()) {
		set_text_fg_color (ustr);
	}

	ustr = inifile_->get_value ("Default", "sep_color");
	if (! ustr.empty ()) {
		set_separator_color (ustr);
	}

	ustr = inifile_->get_value ("Default", "frame_color");
	if (! ustr.empty ()) {
		set_frame_color (ustr);
	}
}

void
TextColorsDB::
save_to_ini_config (ASSA::IniFile* inifile_)
{
    inifile_->set_pair("Default", 
					   IniFile::tuple_type("text_fgcolor", m_text_fg_color));

    inifile_->set_pair("Default", 
					   IniFile::tuple_type("text_bgcolor", m_text_bg_color));

    inifile_->set_pair("Default", 
					   IniFile::tuple_type("sep_color", m_separator_color));

    inifile_->set_pair("Default", 
					   IniFile::tuple_type("frame_color", m_frame_color));
}

/** Update View with data from the Model
 */
void
TextColorsDB::
update_view (ColorsSelectorUI*& view_)
{
	view_->set_text (TXTFGCLR, get_text_fg_color ());
	view_->set_text (TXTBGCLR, get_text_bg_color ());
	view_->set_text (SEPCLR,   get_separator_color ());
	view_->set_text (FRMCLR,   get_frame_color ());
}

/** Store data from View to the Model.
 */
void
TextColorsDB::
fetch_from_view (ColorsSelectorUI*& view_)
{
	set_text_fg_color   (view_->get_text (TXTFGCLR));
	set_text_bg_color   (view_->get_text (TXTBGCLR));
	set_separator_color (view_->get_text (SEPCLR));
	set_frame_color     (view_->get_text (FRMCLR));
}
