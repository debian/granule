// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: DeckView.h,v 1.41 2008/06/02 03:03:54 vlg Exp $
//------------------------------------------------------------------------------
//                            DeckView.h
//------------------------------------------------------------------------------
//  Copyright (c) Vladislav Grinchenko 
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date   : Fri Feb  6 23:37:11 EST 2004
//
//------------------------------------------------------------------------------
#ifndef DECK_VIEWER_H
#define DECK_VIEWER_H

#ifdef HAVE_CONFIG_H
#    include "config.h"
#endif

#include <gtkmm/messagedialog.h>
#include <gtkmm/entry.h>
#include <gtkmm/label.h>
#include <gtkmm/frame.h>
#include <gtkmm/button.h>
#include <gtkmm/box.h>
#include <gtkmm/treeview.h>
#include <gtkmm/liststore.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/uimanager.h>
#include <gtkmm/toolbar.h>

#ifdef IS_HILDON
#   include <hildonmm.h>
#endif

class VDeck;
class ButtonWithImageLabel;

//------------------------------------------------------------------------------
// Class DeckView
//------------------------------------------------------------------------------
//
#ifdef IS_HILDON 
class DeckView : public Hildon::Window
#else
class DeckView : public Gtk::Window
#endif
{
public:
	DeckView (VDeck& deck_, int selected_row_);
	~DeckView ();

	/** Simulate re-entrant event loop. Also, hide/show parent
	 *  window and reposition accordingly.
	 */
	gint run (Gtk::Window& parent_);

	/** We block [x] delete action and reroute it to <Close>
	 */
	bool on_delete_window_clicked (GdkEventAny* event_);

	/** Has the Deck been modified so that it might invalidate
	 *	the iterators? Deleting a card and moving card(s) up/down
	 *  might invalidated external iterators. 
	 *	Editing/Saving, however, does not.
	 */
	bool is_modified () const { return m_modified; }

	/** Get a hold of the Deck
	 */
	VDeck& get_deck () const { return m_deck; }

	/** Return true if user changed Appearance settings with
	 *	DeckInfo dialog.
	 */
	bool appearance_changed () const { return m_repaint_player; }

	/** User selected a row in the cards view list.
	 */
	void on_card_selected ();

	/** User double-clicked on a row in the cards view list.
	 */
	bool on_button_press_event (GdkEventButton* event_);

	/** Called in response to pressing <Close> button
	 */
	void on_close_clicked ();

	/** Called when the user clicks on the column header to 
	 *	sort the list of words.
	 */
	void on_column_sorted ();

	/** Sort Front column
	 */
	int on_sort_compare_front (const Gtk::TreeModel::iterator& a_, 
							   const Gtk::TreeModel::iterator& b_)
	{
		return (sort_compare_impl (a_, b_, 0));
	}

	/** Sort Back column
	 */
	int on_sort_compare_back (const Gtk::TreeModel::iterator& a_, 
							  const Gtk::TreeModel::iterator& b_)
	{
		return (sort_compare_impl (a_, b_, 1));
	}

	// -*- Sidebar action button callbacks -*-
	void on_add_clicked         ();
	void on_edit_clicked        ();
	void on_autofill_clicked    ();
	void on_delete_clicked      ();
	void on_more_clicked        ();
	void on_card_up_clicked     ();
	void on_card_down_clicked   ();
	void on_add_card_to_cardbox ();
	void on_save_deck_clicked   ();

	/// [Enter] key brings up edit dialog
	bool on_key_tapped (GdkEventKey* event_);

	void add_new_card (Card* card_);

	/// Return number of cards in the view
	int  get_deck_size () const;

private:
	/** Column sorting implementation
	 */
	int sort_compare_impl (const Gtk::TreeModel::iterator& a_, 
						   const Gtk::TreeModel::iterator& b_,
						   int column_);

	void set_cards_count ();
	void set_win_name ();

	/** De-activate action buttons depending on the type 
	    of the underlying VDeck object.
	*/
	void set_sensitivity ();

	void autoscroll_up   ();
	void autoscroll_down ();

#ifdef IS_HILDON
	Gtk::VBox* get_vbox () { return m_vbox; }
#endif

private:
	typedef Gtk::TreeModel::Children children_t;
	typedef children_t::iterator chiter_t;
	typedef std::vector<Gtk::TreeModel::Path> pathlist_t;
	typedef std::vector<Gtk::TreeModel::Path>::iterator pathlist_iterator_t;
	typedef std::vector<CardRef*> cardlist_t;

	struct ModelColumns : public Gtk::TreeModelColumnRecord
	{
		Gtk::TreeModelColumn<Glib::ustring> m_front_row;
		Gtk::TreeModelColumn<Glib::ustring> m_back_row;
		Gtk::TreeModelColumn<VCard*>        m_card;    // Hidden Card reference.
		
		ModelColumns () { add (m_front_row); add (m_back_row); add (m_card); }
	};

	const ModelColumns   m_columns;
	Gtk::TreeView        m_tree_view;
	Gtk::ScrolledWindow* m_scrollwin;

	/// TreeModel
	Glib::RefPtr<Gtk::ListStore> m_list_store_ref; 

    /// Selection in current TreeView
	Glib::RefPtr<Gtk::TreeSelection> m_tree_sel_ref; 

private:
	Glib::RefPtr<Gtk::UIManager>   m_uimanager;
	Glib::RefPtr<Gtk::ActionGroup> m_actgroup;

	/** Control buttons
	 */
	Gtk::Widget* m_add_button;
	Gtk::Widget* m_edit_button;
	Gtk::Widget* m_autofill_button;
	Gtk::Widget* m_delete_button;
	Gtk::Widget* m_card_up_button;
	Gtk::Widget* m_card_down_button;
	Gtk::Widget* m_add_card_to_cardbox;
	Gtk::Widget* m_more_button;
	Gtk::Widget* m_save_deck_button;

	ButtonWithImageLabel* m_close_button;

	Gtk::Label*  m_label_count;

	VDeck&       m_deck;
	int          m_row_idx;		    // The row selected in the model
	bool         m_modified;	    // Has Deck been modified to invalidate?
	bool         m_repaint_player;	// Has Appearance prefs been modified?

	Gtk::VBox*   m_vbox;
};

inline int
DeckView::
get_deck_size () const
{
	return (m_deck.size ());
}

#endif /* DECK_VIEWER_H */
