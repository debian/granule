// -*- c++ -*-
//------------------------------------------------------------------------------
//                         CardViewHelpDialog.cpp
//------------------------------------------------------------------------------
// $Id: CardViewHelpDialog.cpp,v 1.7 2007/06/18 02:30:43 vlg Exp $
//
//  Copyright (c) 2005 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//
//  Date: Jun 8, 2005
//------------------------------------------------------------------------------

#include <iostream>

#include <gtkmm/buttonbox.h>
#include <gtkmm/box.h>
#include <gtkmm/main.h>
#include <gtkmm/separator.h>

#include "CardViewHelpDialog.h"

CardViewHelpDialog::
CardViewHelpDialog (Gtk::Window& parent_)
{  
	trace_with_mask("CardViewHelpDialog::CardViewHelpDialog",GUITRACE);

	m_dialog = this;
	m_dialog->set_size_request (433, 530);
	m_dialog->add (m_vbox);

	m_dialog->set_title("Pang Markup Help");
	m_dialog->set_resizable(true);
	m_dialog->set_modal (true);

#ifndef IS_HILDON
	m_dialog->set_transient_for (parent_);
#endif

#ifdef GLIBMM_PROPERTIES_ENABLED
	m_dialog->property_window_position().set_value(Gtk::WIN_POS_CENTER_ON_PARENT);
#else
	m_dialog->set_property ("window_position", Gtk::WIN_POS_CENTER_ON_PARENT);
#endif

	m_vbox.set_homogeneous(false);
	m_vbox.set_spacing(0);

	m_close_button   = Gtk::manage (new Gtk::Button(Gtk::StockID("gtk-close")));
	m_scrolledwindow = Gtk::manage (new Gtk::ScrolledWindow());

	m_textview = Gtk::manage (new Gtk::TextView());

/** A bug either in Gtk+ or in Gtkmm on nokia 800 
 *  doesn't allocate the default buffer.
 */
#ifdef IS_HILDON
	m_textview = Gtk::manage (new Gtk::TextView ());
	m_textbuf_ref = Gtk::TextBuffer::create();
	m_textview->set_buffer (m_textbuf_ref);
#endif

	m_close_button->set_flags  (Gtk::CAN_FOCUS);
	m_close_button->set_flags  (Gtk::CAN_DEFAULT);
	m_close_button->set_relief (Gtk::RELIEF_NORMAL);

	m_textview->set_flags (Gtk::CAN_FOCUS);
	m_textview->set_border_width (3);
	m_textview->set_editable (false);
	m_textview->set_cursor_visible (true);
	m_textview->set_pixels_above_lines (2);
	m_textview->set_pixels_below_lines (2);
	m_textview->set_pixels_inside_wrap (2);
	m_textview->set_left_margin (4);
	m_textview->set_right_margin (4);
	m_textview->set_indent (0);
	m_textview->set_wrap_mode (Gtk::WRAP_WORD);
	m_textview->set_justification (Gtk::JUSTIFY_LEFT);

#ifdef IS_HILDON
	create_tags (m_textbuf_ref);
	insert_text (m_textbuf_ref);
#else
	Glib::RefPtr<Gtk::TextBuffer> ref_textbuf = m_textview->get_buffer();
	create_tags (ref_textbuf);
	insert_text (ref_textbuf);
#endif

	m_scrolledwindow->set_flags (Gtk::CAN_FOCUS);
	m_scrolledwindow->set_shadow_type (Gtk::SHADOW_NONE);
	m_scrolledwindow->set_policy (Gtk::POLICY_AUTOMATIC, Gtk::POLICY_ALWAYS);

#ifdef GLIBMM_PROPERTIES_ENABLED
	m_scrolledwindow->
		property_window_placement().set_value(Gtk::CORNER_TOP_LEFT);
#else
	m_scrolledwindow->
		set_property ("window_placement", Gtk::CORNER_TOP_LEFT);
#endif
	m_scrolledwindow->add(*m_textview);

	m_vbox.set_homogeneous(false);
	m_vbox.set_spacing(0);
	m_vbox.pack_start(*m_scrolledwindow,  Gtk::PACK_EXPAND_WIDGET, 2);

	Gtk::HSeparator* hseparator = manage (new Gtk::HSeparator);
    Gtk::HButtonBox* hbuttonbox = manage (new Gtk::HButtonBox);
    hbuttonbox->set_homogeneous ();
    hbuttonbox->set_spacing (1);
	hbuttonbox->set_layout (Gtk::BUTTONBOX_END);
	hbuttonbox->pack_end (*m_close_button, false, false, 1);

	m_vbox.pack_end (*hbuttonbox, false, true, 0);
	m_vbox.pack_end (*hseparator, false, true, 4);

	m_close_button->signal_clicked ().connect (
		sigc::mem_fun (*this, &CardViewHelpDialog::on_close_clicked));

	show_all_children ();
}

void
CardViewHelpDialog::
create_tags (Glib::RefPtr<Gtk::TextBuffer>& textbuf_)
{
	Glib::RefPtr<Gtk::TextBuffer::Tag> refTag;

	refTag = textbuf_->create_tag("bold");
#ifdef GLIBMM_PROPERTIES_ENABLED
	refTag->property_weight().set_value(Pango::WEIGHT_BOLD);
#else
	refTag->set_property ("weight", Pango::WEIGHT_BOLD);
#endif

	refTag = textbuf_->create_tag("monospace");
#ifdef GLIBMM_PROPERTIES_ENABLED
	refTag->property_scale().set_value(Pango::SCALE_X_SMALL);
	refTag->property_family().set_value("monospace");
	refTag->property_foreground().set_value("brown");
#else
//	refTag->set_property ("scale", Pango::SCALE_X_SMALL);
	refTag->set_property ("family", Glib::ustring ("monospace"));
	refTag->set_property ("foreground", Glib::ustring ("brown"));
#endif

	refTag = textbuf_->create_tag("heading");
#ifdef GLIBMM_PROPERTIES_ENABLED
	refTag->property_weight().set_value(Pango::WEIGHT_BOLD);
	refTag->property_size().set_value(15 * Pango::SCALE);
	refTag->property_foreground().set_value("blue");
#else
	refTag->set_property ("weight", Pango::WEIGHT_BOLD);
	refTag->set_property ("size", (15 * Pango::SCALE));
	refTag->set_property ("foreground", Glib::ustring ("blue"));
#endif

	refTag = textbuf_->create_tag("option_desc");
#ifdef GLIBMM_PROPERTIES_ENABLED
    refTag->property_wrap_mode().set_value(Gtk::WRAP_WORD);
    refTag->property_indent().set_value(5);
    refTag->property_left_margin().set_value(40);
    refTag->property_right_margin().set_value(2);
#else
    refTag->set_property ("wrap_mode", Gtk::WRAP_WORD);
    refTag->set_property ("indent", 5);
    refTag->set_property ("left_margin", 40);
    refTag->set_property ("right_margin", 2);
#endif
}

void
CardViewHelpDialog::
insert_text (Glib::RefPtr<Gtk::TextBuffer>& buf_)
{
	Gtk::TextBuffer::iterator iter = buf_->get_iter_at_offset (0);
	
	iter = buf_->insert_with_tag (iter, "CardView Dialog\n", "heading");

	iter = buf_->insert (iter, "You can add a new card or edit existing ");
	iter = buf_->insert (iter, "cards with CardView dialog. ");
	iter = buf_->insert (iter, "Anywhere in the text entry window you ");
	iter = buf_->insert (iter, "can use Pango Markup Language to spruce up ");
	iter = buf_->insert (iter, "the visual representation of the information ");
	iter = buf_->insert (iter, "you try to learn. ");
	iter = buf_->insert (iter, "Below is the summary of Pango Markup.\n\n");

	iter = buf_->insert_with_tag (iter, "Pango Text Attribute Markup Language\n", 
								  "heading");

	iter = buf_->insert (iter, "Frequently, you want to display some text ");
	iter = buf_->insert (iter, "to the user with attributes applied to ");
	iter = buf_->insert (iter, "part of the text (for example, you might ");
	iter = buf_->insert (iter, "want bold or italicized words).\n\n");

	iter = buf_->insert (iter, "Pango provides a small markup language for");
	iter = buf_->insert (iter, "that. This simple example illustrates a possibile way to greately exhance the visual appeal of your flashcards:\n\n");

	iter = buf_->insert_with_tag (iter, "<span foreground=\"blue\" size=\"x-large\">Blue text</span> is <i>cool</i>!\n\n", "monospace");

	iter = buf_->insert_with_tag (iter, "<span></span> Tag Attributes:\n", 
								  "heading");

	iter = buf_->insert_with_tag (iter, "font_desc\n", "bold");
	iter = buf_->insert_with_tag (iter, "A font description string, such as \"Sans Italic 12\"; note that any other span attributes will override this description. So if you have \"Sans Italic\" and also a style=\"normal\" attribute, you will get Sans normal, not italic.\n", "option_desc");


	iter = buf_->insert_with_tag (iter, "font_family\n", "bold");
	iter = buf_->insert_with_tag (iter, "A font family name.\n", "option_desc");

	iter = buf_->insert_with_tag (iter, "face\n", "bold");
	iter = buf_->insert_with_tag (iter, "Synonym for font_family.\n", "option_desc");

	iter = buf_->insert_with_tag (iter, "size\n", "bold");
	iter = buf_->insert_with_tag (iter, "Font size in 1024ths of a point, or one of the absolute sizes 'xx-small', 'x-small', 'small', 'medium', 'large', 'x-large', 'xx-large', or one of the relative sizes 'smaller' or 'larger'. If you want to specify a absolute size, it's usually easier to take advantage of the ability to specify a partial font description using 'font_desc'; you can use font_desc='12.5' rather than size='12800'.\n", "option_desc");

	iter = buf_->insert_with_tag (iter, "style\n", "bold");
	iter = buf_->insert_with_tag (iter, "One of 'normal', 'oblique', 'italic'.\n", "option_desc");

	iter = buf_->insert_with_tag (iter, "weight\n", "bold");
	iter = buf_->insert_with_tag (iter, "One of 'ultralight', 'light', 'normal', 'bold', 'ultrabold', 'heavy', or a numeric weight.\n", "option_desc");

	iter = buf_->insert_with_tag (iter, "variant\n", "bold");
	iter = buf_->insert_with_tag (iter, "'normal' or 'smallcaps'.\n", "option_desc");

	iter = buf_->insert_with_tag (iter, "stretch\n", "bold");
	iter = buf_->insert_with_tag (iter, "One of 'ultracondensed', 'extracondensed', 'condensed', 'semicondensed', 'normal', 'semiexpanded', 'expanded', 'extraexpanded', 'ultraexpanded'.\n", "option_desc");

	iter = buf_->insert_with_tag (iter, "foreground\n", "bold");
	iter = buf_->insert_with_tag (iter, "An RGB color specification such as '#00FF00' or a color name such as 'red'.\n", "option_desc");

	iter = buf_->insert_with_tag (iter, "background\n", "bold");
	iter = buf_->insert_with_tag (iter, "An RGB color specification such as '#00FF00' or a color name such as 'red'.\n", "option_desc");

	iter = buf_->insert_with_tag (iter, "underline\n", "bold");
	iter = buf_->insert_with_tag (iter, "One of 'single', 'double', 'low', 'none'.\n", "option_desc");

	iter = buf_->insert_with_tag (iter, "underline_col\n", "bold");
	iter = buf_->insert_with_tag (iter, "The color of underlines; an RGB color specification such as '#00FF00' or a color name such as 'red'.\n", "option_desc");

	iter = buf_->insert_with_tag (iter, "rise\n", "bold");
	iter = buf_->insert_with_tag (iter, "Vertical displacement, in 10000ths of an em. Can be negative for subscript, positive for superscript..\n", "option_desc");

	iter = buf_->insert_with_tag (iter, "strikethrough\n", "bold");
	iter = buf_->insert_with_tag (iter, "'true' or 'false' whether to strike through the text.\n", "option_desc");

	iter = buf_->insert_with_tag (iter, "strikethrough_color\n", "bold");
	iter = buf_->insert_with_tag (iter, "The color of strikethrough lines; an RGB color specification such as '#00FF00' or a color name such as 'red'.\n", "option_desc");

	iter = buf_->insert_with_tag (iter, "fallback\n", "bold");
	iter = buf_->insert_with_tag (iter, "'true' or 'false' whether to enable fallback. If disabled, then characters will only be used from the closest matching font on the system. No fallback will be done to other fonts on the system that might contain the characters in the text. Fallback is enabled by default. Most applications should not disable fallback.\n", "option_desc");

	iter = buf_->insert_with_tag (iter, "lang\n", "bold");
	iter = buf_->insert_with_tag (iter, "A language code, indicating the text language.\n\n", "option_desc");
	iter = buf_->insert (iter, "The following convenience tags similar to HTML markup language are provided. The tags can be nested.\n\n");

	iter = buf_->insert_with_tag (iter, "Convenience tags:\n\n", "heading");

	iter = buf_->insert_with_tag (iter, "<b></b>\n", "bold");
	iter = buf_->insert_with_tag (iter, "Bold\n", "option_desc");

	iter = buf_->insert_with_tag (iter, "<big></big>\n", "bold");
	iter = buf_->insert_with_tag (iter, "Makes font relatively larger\n", 
								  "option_desc");

	iter = buf_->insert_with_tag (iter, "<i></i>\n", "bold");
	iter = buf_->insert_with_tag (iter, "Italic\n", "option_desc");

	iter = buf_->insert_with_tag (iter, "<s></s>\n", "bold");
	iter = buf_->insert_with_tag (iter, "Strikethrough\n", "option_desc");

	iter = buf_->insert_with_tag (iter, "<sub></sub>\n", "bold");
	iter = buf_->insert_with_tag (iter, "Subscript\n", "option_desc");

	iter = buf_->insert_with_tag (iter, "<sup></sup>\n", "bold");
	iter = buf_->insert_with_tag (iter, "Superscript\n", "option_desc");

	iter = buf_->insert_with_tag (iter, "<small></small>\n", "bold");
	iter = buf_->insert_with_tag (iter, "Makes font relatively smaller\n", 
								  "option_desc");

	iter = buf_->insert_with_tag (iter, "<tt></tt>\n", "bold");
	iter = buf_->insert_with_tag (iter, "Monospace font\n", "option_desc");

	iter = buf_->insert_with_tag (iter, "<u></u>\n", "bold");
	iter = buf_->insert_with_tag (iter, "Underline\n", "option_desc");
}
