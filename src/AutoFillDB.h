// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: AutoFillDB.h,v 1.2 2007/11/03 03:22:31 vlg Exp $
//------------------------------------------------------------------------------
//                            AutoFillDB.h
//------------------------------------------------------------------------------
//  Copyright (c) Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date   : Sun Oct 28 2007
//
//------------------------------------------------------------------------------
#ifndef AUTOFILL_DB_H
#define AUTOFILL_DB_H

#include "Granule-main.h"

#include <glibmm/ustring.h>

class CardAutoFill;
class Card;

/**-----------------------------------------------------------------------------
 *	AutoFillDB is the Model of the settings for Card AutoFill dialog.
 **-----------------------------------------------------------------------------
 */
class AutoFillDB
{  
public:
    AutoFillDB () { init_global_defaults (); }

	void init_global_defaults ();

	/** Text alignment controls (From Deck.h)
	 */
    Glib::ustring get_asf_pattern  () const { return m_asf_pattern;  }
    Glib::ustring get_back_pattern () const { return m_back_pattern; }
    Glib::ustring get_asb_pattern  () const { return m_asb_pattern;  }

    void set_asf_pattern  (const Glib::ustring& v_) { m_asf_pattern  = v_; }
    void set_back_pattern (const Glib::ustring& v_) { m_back_pattern = v_; }
    void set_asb_pattern  (const Glib::ustring& v_) { m_asb_pattern  = v_; }

	bool is_enabled  () const  { return m_enabled; }
	void set_enabled (bool v_) { m_enabled = v_;   }

	/// Update View with data from Model
	void update_view (CardAutoFill& view_);

	/** Apply patterns to a Card. 
	 *  This method is called by DeckView::on_autofill_clicked ()
	 */
	void apply_patterns (Card* card_, int seqnum_);

	/// Store View values in Model
	void fetch_from_view (CardAutoFill& view_);

private:
	Glib::ustring m_asf_pattern;
	Glib::ustring m_back_pattern;
	Glib::ustring m_asb_pattern;

	bool m_enabled;
};

#endif /* AUTOFILL_DB */
