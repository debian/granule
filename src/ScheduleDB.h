// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: ScheduleDB.h,v 1.6 2008/02/22 18:52:45 vlg Exp $
//------------------------------------------------------------------------------
//                            ScheduleDB.h
//------------------------------------------------------------------------------
//  Copyright (c) 2007 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date   : Mon Sep 17 2007
//
//------------------------------------------------------------------------------

#ifndef SHEDULE_DB_H
#define SHEDULE_DB_H

#include "Granule-main.h"
#include <assa/IniFile.h>
#include "ScheduleUI.h"

#include <libxml/tree.h>		// xmlDocPtr
#include <libxml/xmlwriter.h>	// xmlTextWriterPtr

#include <string>
using std::string;

/**
 * @class ScheduleDB
 *
 *	ScheduleDB is the Model (a persistent storage manager of data)
 *  for the Schedule Preferences.
 *  Internall all schedule-related infromation is stored in days resolution.
 *
 *  For each box in the range of configured capacity, the interval
 *  (in days) is calculated based on EFactor and Quality of 2.5 (easiest).
 *  
 *  Let's say if a card is on its 5th repetition with its EFactor=2.08,
 *  then its next interval would be 124 days. It would be then added
 *  for repetition to the Box #6 that has interval of 230 days 
 *  (box #5 has its interval set to 92 days).
 */
class ScheduleDB
{  
public:
	typedef enum {
		DAY, WEEK, MONTH, YEAR
	} DWMY;

	typedef std::vector<ScheduleUI::interval_type> collection_type;
	typedef collection_type::iterator collection_iter;

	static const float EF_MIN;
	static const float EF_MAX;
	static const int   BOX_ONE_STEP;
	static const int   BOX_TWO_STEP;
	static const int   DAYS_IN_YEAR;
	static const int   DAYS_IN_MONTH;
	static const int   DAYS_IN_WEEK;

public:
	/// Constructor. Set to default schedule.
    ScheduleDB () { reset (); }

	/// Reset the schedule to the default
	void reset ();

	/** Load CardDeck from persisten storage (XML file).
	 *  @param parser_ XML document parser.
	 *  @param error_msg_ On error, return verbal description of the error.
	 *  @return true on success; false on error
	 */
    bool load_from_xml_config (xmlDocPtr parser_, string& error_msg_);
	
	/** Save CardDeck to persistent storage (XML file).
	 *  @param writer_ XML document writer.
	 */
    void save_to_xml_config (xmlTextWriterPtr writer_);

	/** Change the number of boxes in the CardFile. 
	 *  For the changes to take effect, a call to rebuild_shedule()
	 *  should follow.
	 *  @param ns_ New capacity to set.
	 *  @return true if ok; false if ns_ is out of valid range.
	 */
	bool set_capacity (size_t ns_);

	/// Return number of boxes.
	size_t capacity () const { return m_capacity; }

	/** Change the EFactor.
	 *  For the changes to take effect, a call to rebuild_shedule()
	 *  should follow.
	 *  @param efactor_ E-Factor to set.
	 *  @return true if ok; false if efactor_ is out of valid range.
	 */
	bool set_efactor  (float efactor_);

	/// Return active EFactor
	float efactor () const { return m_efactor; }

	/// Recalculate schedule based on new Capacity and EFactor.
	void rebuild_schedule ();

	/// Update View with data from Model
	void update_view (ScheduleUI& view_);
	
	/// Store View configuration to Model
	void fetch_from_view (ScheduleUI& view_);

	/** Format interval of the Box index_ in displayable form
	 *  by breaking up total days to years/months/weeks/days.
	 *
	 *  @param index_    Box number
	 *  @param interval_ Returns interval in days
	 *  @param years_    Returns complete years in interval.
	 *  @param months_   Returns complete months in interval.
	 *  @param weeks_    Returns complete weeks in interval.
	 *  @param days_     Returns remaining days in interval.
	 *  @return true is formatting worked; false otherwise
	 */
	bool format_interval (int index_,
						  ScheduleUI::interval_type& interval_,
						  string& years_,
						  string& months_,
						  string& weeks_,
						  string& days);

	/** Find next box for the card based on its new
	 *  scheduling interval.
	 */
	int find_next_box (int interval_);

	/// @return Interval in days for CardBox index_
	ScheduleUI::interval_type get_box_interval (guint index_);	

public:
	/** Calculate coefficient based on quality of answer.
	 */
	static float ef_coefficient (int quality_);

    /** Calculate EF(n+1) according to SM2 algorithm.
	 *  
	 *  @param efactor_   EF(n)
	 *  @param quality_   Quality of answer in range [0; 5]
	 */
	static float ef_next (float efactor_, int quality_);

private:
	bool fill_view_row (int index_, ScheduleUI::ViewRowData& vrow_);

private:
    /** SM-2 algorithm E-Factor. 
	 *  The value is in the range [1.3; 2.5] (most difficult to the easiest).
	 */
	float m_efactor;

	/** Number of boxes. Default is 5.
	 */
	size_t m_capacity;

	/** The distance (in days) between first and second box, I(1) = 1.
	 */
	ScheduleUI::interval_type m_interval_1;

	/** The distance (in days) between second and third box, I(2) = 5.
	 */
	ScheduleUI::interval_type m_interval_2;

	/** The collection of Reviews. 
	 *  It holds (years/months/weeks/days) information for each box.
	 */
	collection_type m_box_intervals;
};

inline void
ScheduleDB::
reset () 
{
	m_efactor    = 2.22;		// My favorite for learning languages.
	m_capacity   = CARDBOXES_MIN;
	m_interval_1 = BOX_ONE_STEP;
	m_interval_2 = BOX_TWO_STEP;

	rebuild_schedule (); 
}

inline ScheduleUI::interval_type
ScheduleDB::
get_box_interval (guint index_)
{
	if (index_ < 0 || index_ >= m_box_intervals.size ()) {
		return 0;
	}
	return m_box_intervals [index_];
}


#endif /* SHEDULE_DB */
