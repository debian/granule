// -*- c++ -*-
//------------------------------------------------------------------------------
//                              CardView.cpp
//------------------------------------------------------------------------------
// $Id: CardView.cpp,v 1.66 2008/08/19 02:52:46 vlg Exp $
//
//  Copyright (c) Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//
// Date: Feb 15, 2004
//------------------------------------------------------------------------------

#include <gtkmmconfig.h>
#include <gtkmm/image.h>
#include <gtkmm/label.h>
#include <gtkmm/box.h>
#include <gtkmm/alignment.h>
#include <gtkmm/button.h>
#include <gtkmm/buttonbox.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/table.h>
#include <gtkmm/messagedialog.h>
#include <gtkmm/paned.h>

#include "Granule.h"
#include "MainWindow.h"
#include "GrappConf.h"
#include "CardView.h"
#include "DeckView.h"
#include "Card.h"
#include "CardRef.h"
#include "EditControls.h"
#include "AutoFillDB.h"
#include "FileOpenDialog.h"
#include "ButtonWithImageLabel.h"

#include "Intern.h"				/* always last */

using sigc::bind;
using sigc::mem_fun;
using Gtk::manage;

#define SET_CV_LABEL(l) \
	l->set_alignment  (0.5,0.5);  \
	l->set_padding    (0,0);      \
	l->set_justify    (Gtk::JUSTIFY_LEFT); \
	l->set_line_wrap  (false);    \
	l->set_use_markup (false);    \
	l->set_selectable (false);   

#define SET_CV_TEXT(t) \
	t->set_flags (Gtk::CAN_FOCUS);   \
	t->set_border_width (3);         \
	t->set_editable (true);          \
	t->set_cursor_visible (true);    \
	t->set_pixels_above_lines (3);   \
	t->set_pixels_below_lines (0);   \
	t->set_pixels_inside_wrap (0);   \
	t->set_left_margin  (4);         \
	t->set_right_margin (4);         \
	t->set_indent (0);               \
	t->set_wrap_mode (Gtk::WRAP_WORD);         \
	t->set_justification (Gtk::JUSTIFY_LEFT);  \
	t->set_accepts_tab (false);

static void 
set_cv_scrollwin (Gtk::ScrolledWindow& win_, Gtk::Widget& widget_)
{
	win_.set_flags       (Gtk::CAN_FOCUS);
	win_.set_shadow_type (Gtk::SHADOW_NONE);
	win_.set_policy      (Gtk::POLICY_ALWAYS, Gtk::POLICY_ALWAYS); 
#ifdef GLIBMM_PROPERTIES_ENABLED 
	win_.property_window_placement ().set_value (Gtk::CORNER_TOP_LEFT);
#else
	win_.set_property ("window_placement", Gtk::CORNER_TOP_LEFT);
#endif 

/** Defeat Hildons' default theme and provide
 *  our own visual border around Text Entry
 */
#ifdef IS_HILDON

	Gtk::Frame* f = manage (new Gtk::Frame ());
	f->set_border_width (1);
    f->set_shadow_type  (Gtk::SHADOW_ETCHED_OUT);
	f->set_label_align  (0, 0);
	f->add (widget_);
	win_.add (*f);
#else
	win_.add (widget_);
#endif
}

/**
 * Enclose Gtk::TextView in a visible box. Some default themes (hildon)
 * blend the surrounding, so it is hard to see where the editable
 * region lies.
 */
static Gtk::Widget*
set_text_border (Gtk::Widget* widget_)
{
#ifdef IS_HILDON
	Gtk::Frame* frame = manage (new Gtk::Frame ());
	frame->set_border_width (1);
    frame->set_shadow_type  (Gtk::SHADOW_ETCHED_OUT);
	frame->set_label_align  (0, 0);
	frame->add (*widget_);
	return (frame);
#else
	return (widget_);
#endif
}


/*------------------------------------------------------------------------------
** CardView class implementation
**------------------------------------------------------------------------------
*/
CardView::
CardView (VCard* card_) 
	: 
	m_card          (card_),
	m_syntax_error  (false),
	m_keep_open     (false),
	m_response      (Gtk::RESPONSE_OK),

	m_cancel_button (NULL),
	m_add_n_close_button (NULL),
	m_add_button    (NULL),
	m_count_button  (NULL),
	m_help_button   (NULL),

	m_front_text    (NULL),
	m_img_text      (NULL),
	m_back_text     (NULL),
	m_example_text  (NULL),
	m_example_img_text (NULL),
	m_asf_text      (NULL),
	m_asb_text      (NULL),

	m_edit_controls (NULL),
	m_help_dialog   (NULL),
	m_cards_count   (0)
{
	trace_with_mask("CardView::CardView(VCard&)",GUITRACE);

	Gtk::Alignment* bt_alignment;
	Gtk::Alignment* ft_alignment;
	Gtk::Alignment* et_alignment;

	Gtk::Alignment* asf_alignment;
	Gtk::Alignment* asb_alignment;
	Gtk::Alignment* img_alignment;
	Gtk::Alignment* example_img_alignment;

	Gtk::ScrolledWindow* front_scrollwin;
	Gtk::ScrolledWindow* backtext_scrollwin;
	Gtk::ScrolledWindow* example_scrollwin;
	Gtk::ScrolledWindow* asf_scrollwin;
	Gtk::ScrolledWindow* asb_scrollwin;
	Gtk::ScrolledWindow* img_scrollwin;
	Gtk::ScrolledWindow* example_img_scrollwin;

	Gtk::Label* back_label;
	Gtk::Label* example_label;
	Gtk::Label* front_label;
	Gtk::Label* asf_label;
	Gtk::Label* asb_label;
	Gtk::Label* img_label;
	Gtk::Label* example_img_label;

	Gtk::VBox* vbox_frontlabel;
	Gtk::VBox* vbox_backlabel;
	Gtk::VBox* vbox_examplelabel;

	Gtk::HBox* hbox_asf;
	Gtk::HBox* hbox_asb;
	Gtk::HBox* hbox_img;
	Gtk::HBox* hbox_example_img;

	Gtk::Table* upper_table;
	Gtk::Table* middle_table;
	Gtk::Table* lower_table;

	Gtk::VPaned* paned_1;		/* Holds upper_table and paned_2       */
	Gtk::VPaned* paned_2;		/* Holds middle_table and lower_table */

	Gtk::VBox*  vbox_all;

	/** The dialog setup
	 */
	set_title ("Card View");

#ifdef IS_HILDON
//	set_size_request (672, 396);
#else
	set_modal (true);  // very important - we're not protected from multi-clicks

#ifdef IS_PDA
	set_resizable (false);
	Gdk::Geometry dp_geometry =	{ 240, 320,	240, 320, 240, 320,-1, -1, -1, -1};
	set_geometry_hints (*this, dp_geometry, 
				Gdk::HINT_MIN_SIZE | Gdk::HINT_MAX_SIZE | Gdk::HINT_BASE_SIZE);
#else // DESKTOP
	set_resizable (true);
	set_icon_from_file (GRAPPDATDIR "/pixmaps/cardview_32x32.png");
	Gdk::Rectangle geom = CONFIG->get_card_view_geometry ();
	set_size_request (geom.get_width (), geom.get_height ());
//	set_transient_for (*MAINWIN);
#endif // (!IS_PDA)

#endif	// (!IS_HILDON)

	/** <Clear> button - OBSOLETE -
	 */
	m_help_button = manage (new ButtonWithImageLabel (
							 _("<u>H</u>elp"), "gtk-help", Gtk::IconSize (4)));
	m_help_button->set_alt_accelerator_key (GDK_h, get_accel_group ());

	/** <Cancel> Button
	 */
	m_cancel_button = manage (new ButtonWithImageLabel (
					   _("<u>C</u>ancel"), "gtk-cancel", Gtk::IconSize (4)));
	m_cancel_button->set_alt_accelerator_key (GDK_c, get_accel_group ());

	/** <Count> pseudo-button
	 */
	m_count_button  = manage (new Gtk::Button ());

	/** <Add> Button
	 */
	m_add_button = manage (new ButtonWithImageLabel (
								_("Add"), "gtk-add", Gtk::IconSize (4)));
	m_add_button->set_alt_accelerator_key (GDK_n, get_accel_group ());

	/** <Add'n'Close> Button
	 */
	m_add_n_close_button = manage (new ButtonWithImageLabel (
								_("Ok"), "gtk-apply", Gtk::IconSize (4)));
	m_add_n_close_button->set_alt_accelerator_key (GDK_Return, 
												   get_accel_group ());

	/** <Browse Img> Buttons
	 */
	m_browse_img_button = manage (new Gtk::Button (_("Browse")));
	m_browse_example_img_button = manage (new Gtk::Button (_("Browse")));

	/**********************************************************************
	 *  Text                                                              *
	 **********************************************************************/
	m_front_text       = manage (new Gtk::TextView);
	front_scrollwin    = manage (new Gtk::ScrolledWindow);
	front_label        = manage (new Gtk::Label (_("Front")));
	ft_alignment       = manage (new Gtk::Alignment (0.5, 0.5, 0, 0));
	vbox_frontlabel    = manage (new Gtk::VBox (false, 0));

	m_back_text        = manage (new Gtk::TextView);
	backtext_scrollwin = manage (new Gtk::ScrolledWindow);
	back_label         = manage (new Gtk::Label (_("Back")));
	bt_alignment       = manage (new Gtk::Alignment (0.5, 0.5, 0, 0));
	vbox_backlabel     = manage (new Gtk::VBox (false, 0));

	m_example_text     = manage (new Gtk::TextView);
	example_scrollwin  = manage (new Gtk::ScrolledWindow);
	example_label      = manage (new Gtk::Label (_("Example")));
	et_alignment       = manage (new Gtk::Alignment (0.5, 0.5, 0, 0));
	vbox_examplelabel  = manage (new Gtk::VBox (false, 0));

	m_asf_text     = manage (new Gtk::TextView);
	asf_scrollwin  = manage (new Gtk::ScrolledWindow);
	asf_label      = manage (new Gtk::Label (_("ASF")));
	asf_alignment  = manage (new Gtk::Alignment (0, 0.5, 0, 0));
	hbox_asf       = manage (new Gtk::HBox (false, 0));

	m_asb_text     = manage (new Gtk::TextView);
	asb_scrollwin  = manage (new Gtk::ScrolledWindow);
	asb_label      = manage (new Gtk::Label (_("ASB")));
	asb_alignment  = manage (new Gtk::Alignment (0, 0.5, 0, 0));
	hbox_asb       = manage (new Gtk::HBox (false, 0));

	m_img_text     = manage (new Gtk::TextView);
	img_scrollwin  = manage (new Gtk::ScrolledWindow);
	img_label      = manage (new Gtk::Label (_("IMG")));
	img_alignment  = manage (new Gtk::Alignment (0, 0.5, 0, 0));
	hbox_img       = manage (new Gtk::HBox (false, 0));

	m_example_img_text     = manage (new Gtk::TextView);
	example_img_scrollwin  = manage (new Gtk::ScrolledWindow);
	example_img_label      = manage (new Gtk::Label (_("IMG")));
	example_img_alignment  = manage (new Gtk::Alignment (0, 0.5, 0, 0));
	hbox_example_img       = manage (new Gtk::HBox (false, 0));

	upper_table  = manage (new Gtk::Table (2, 2, false));
	middle_table = manage (new Gtk::Table (3, 2, false));
	lower_table  = manage (new Gtk::Table (3, 2, false));

	paned_1        = manage (new Gtk::VPaned);
	paned_2        = manage (new Gtk::VPaned);

	vbox_all       = manage (new Gtk::VBox (false, 0));

	Pango::FontDescription text_font = CONFIG->fonts_db ().get_example_font ();
	m_front_text   ->modify_font (text_font);
	m_back_text    ->modify_font (text_font);
	m_example_text ->modify_font (text_font);
	m_asf_text     ->modify_font (text_font);
	m_asb_text     ->modify_font (text_font);
	m_img_text     ->modify_font (text_font);
	m_example_img_text->modify_font (text_font);

#ifdef IS_HILDON
	m_textbuf_ref [TV_FRONT] = Gtk::TextBuffer::create();
	m_front_text->set_buffer (m_textbuf_ref [TV_FRONT]);

	m_textbuf_ref [TV_BACK] = Gtk::TextBuffer::create();
	m_back_text->set_buffer (m_textbuf_ref [TV_BACK]);

	m_textbuf_ref [TV_EXAMPLE] = Gtk::TextBuffer::create();
	m_example_text->set_buffer (m_textbuf_ref [TV_EXAMPLE]);

	m_textbuf_ref [TV_ASF] = Gtk::TextBuffer::create();
	m_asf_text->set_buffer (m_textbuf_ref [TV_ASF]);

	m_textbuf_ref [TV_ASB] = Gtk::TextBuffer::create();
	m_asb_text->set_buffer (m_textbuf_ref [TV_ASB]);

	m_textbuf_ref [TV_IMG] = Gtk::TextBuffer::create();
	m_img_text->set_buffer (m_textbuf_ref [TV_IMG]);

	m_textbuf_ref [TV_EIMG] = Gtk::TextBuffer::create();
	m_example_img_text->set_buffer (m_textbuf_ref [TV_EIMG]);
#endif

	/**----------------**
	 **  Edit Controls **
	 **----------------**
	 */
	m_edit_controls = manage(new EditControls(m_front_text->get_buffer()));
	m_edit_controls->unset_flags (Gtk::CAN_FOCUS);
	vbox_all->pack_start (*m_edit_controls, Gtk::PACK_SHRINK, 0);

	/**********************************************************************
	 ** Setup buttons                                                    **
	 **********************************************************************/
	
	/** [Help] Button
	 */
	m_help_button->unset_flags (Gtk::CAN_FOCUS); // Take help off the chain

	/** [Count] Label should be invisible
	 */
	m_count_button->set_focus_on_click (false);
	m_count_button->unset_flags (Gtk::CAN_FOCUS);
	m_count_button->set_relief (Gtk::RELIEF_NONE);
	m_count_button->set_use_stock (false);
	m_count_button->set_use_underline (false);

	/**--------------**
	 **  Front Text  **
	 **--------------**
	 */
	SET_CV_TEXT(m_front_text);
	set_cv_scrollwin(*front_scrollwin, *m_front_text);

	SET_CV_LABEL(front_label);
	Granule::rotate_label (front_label, 90.0, "Q:");
	ft_alignment->add (*front_label);
	vbox_frontlabel->pack_start (*ft_alignment, Gtk::PACK_SHRINK, 0);
	
	/**--------------**
	 **   Back Text  **
	 **--------------**
	 */
	SET_CV_TEXT(m_back_text);
	set_cv_scrollwin (*backtext_scrollwin, *m_back_text);

	SET_CV_LABEL(back_label);
	Granule::rotate_label (back_label, 90.0, "A:");
	bt_alignment->add (*back_label);
	vbox_backlabel->pack_start (*bt_alignment, Gtk::PACK_SHRINK, 0);

	/**----------------**
	 **  Example Text  **
	 **----------------**
	 */
	SET_CV_TEXT(m_example_text);
	set_cv_scrollwin (*example_scrollwin, *m_example_text);

	SET_CV_LABEL(example_label);
	Granule::rotate_label (example_label, 90.0, "E:");
	vbox_examplelabel->pack_start (*example_label, Gtk::PACK_SHRINK, 0);

	/**--------------------------**
	 **  AltSpell Text for Front **
	 **--------------------------**
	 */
	SET_CV_TEXT(m_asf_text);
	SET_CV_LABEL(asf_label);
	asf_alignment->add (*asf_label);

	hbox_asf->pack_start (*asf_alignment, Gtk::PACK_SHRINK, 0);
	hbox_asf->pack_start (*(set_text_border (m_asf_text)), 
						  Gtk::PACK_EXPAND_WIDGET, 0);
	hbox_asf->unset_flags(Gtk::CAN_FOCUS);

	/**--------------------------**
	 **  AltSpell Text for Back  **
	 **--------------------------**
	 */
	SET_CV_TEXT(m_asb_text);
	SET_CV_LABEL(asb_label);
	asb_alignment->add (*asb_label);

	hbox_asb->pack_start (*asb_alignment, Gtk::PACK_SHRINK, 0);
	hbox_asb->pack_start (*(set_text_border (m_asb_text)), 
						  Gtk::PACK_EXPAND_WIDGET, 0);
	hbox_asb->unset_flags(Gtk::CAN_FOCUS);

	/**--------------------------**
	 **  IMG Text                **
	 **--------------------------**
	 */
	SET_CV_TEXT(m_img_text);
	m_img_text->set_wrap_mode (Gtk::WRAP_NONE);
	SET_CV_LABEL(img_label);
	img_alignment->add (*img_label);

	hbox_img->pack_start (*img_alignment, Gtk::PACK_SHRINK, 0);
	hbox_img->pack_start (*(set_text_border (m_img_text)), 
						  Gtk::PACK_EXPAND_WIDGET, 0);
	hbox_img->pack_start (*m_browse_img_button, Gtk::PACK_SHRINK, 3);

	hbox_img->unset_flags(Gtk::CAN_FOCUS);

	/**--------------------------**
	 **  Example IMG Text        **
	 **--------------------------**
	 */
	SET_CV_TEXT(m_example_img_text);
	m_example_img_text->set_wrap_mode (Gtk::WRAP_NONE);
	SET_CV_LABEL(example_img_label);
	example_img_alignment->add (*example_img_label);

	hbox_example_img->pack_start (*example_img_alignment, Gtk::PACK_SHRINK, 0);
	hbox_example_img->pack_start (*(set_text_border (m_example_img_text)), 
								  Gtk::PACK_EXPAND_WIDGET, 0);
	hbox_example_img->pack_start (*m_browse_example_img_button, 
								  Gtk::PACK_SHRINK, 3);

	hbox_example_img->unset_flags(Gtk::CAN_FOCUS);

	/********************************************************************
	 * Hookup callbacks                                                 *
	 ********************************************************************/

	m_front_text->signal_grab_focus ().connect (
		bind<Gtk::TextView*> (mem_fun (*m_edit_controls, 
									   &EditControls::on_focus_changed),
							  m_front_text));

	m_back_text->signal_grab_focus ().connect (
		bind<Gtk::TextView*> (mem_fun (*m_edit_controls, 
									   &EditControls::on_focus_changed),
							  m_back_text));

	m_example_text->signal_grab_focus ().connect (
		bind<Gtk::TextView*> (mem_fun (*m_edit_controls, 
									   &EditControls::on_focus_changed),
							  m_example_text));

	m_asf_text->signal_grab_focus ().connect (
		bind<Gtk::TextView*> (mem_fun (*m_edit_controls, 
									   &EditControls::on_focus_changed),
							  m_asf_text));
	m_asb_text->signal_grab_focus ().connect (
		bind<Gtk::TextView*> (mem_fun (*m_edit_controls, 
									   &EditControls::on_focus_changed),
							  m_asb_text));

	m_img_text->signal_grab_focus ().connect (
		bind<Gtk::TextView*> (mem_fun (*m_edit_controls, 
									   &EditControls::on_focus_changed),
							  m_img_text));

	m_example_img_text->signal_grab_focus ().connect (
		bind<Gtk::TextView*> (mem_fun (*m_edit_controls, 
									   &EditControls::on_focus_changed),
							  m_example_img_text));

 	signal_delete_event ().connect (
 		sigc::mem_fun (*this, &CardView::on_delete_window_clicked));

	/*********************************************************************
	 *  Table holds elements together:
	 *
	 *    0                1                  2
	 *  0 .----------------+------------------.
	 *    | vbox(label)    |  scrollwin(text) | Front
	 *  1 `----------------+------------------'
	 *
	 *    --------------------------[=]-------- <-- pane
	 *
     *    0                1                  2
	 *  0 .----------------+------------------.
	 *    |                |  scrollwin(img ) | Front IMG
	 *  1 `----------------+------------------'
	 *    |                |  scrollwin(text) | Front Alt Spell
	 *  2 `----------------+------------------'
	 *    | vbox(label)    |  scrollwin(text) | Back 
	 *  3 `----------------+------------------'
	 *
	 *    --------------------------[=]-------- <-- pane
	 *
	 *    0                1                  2
	 *  0 .----------------+------------------.
	 *    | vbox(label)    |  scrollwin(text) | Example
	 *  1 `----------------+------------------'
	 *    |                |  scrollwin(img ) | Example IMG
	 *  2 `----------------+------------------'
	 *    |                |  scrollwin(text) | Back Alt Spell
	 *  3 `----------------+------------------'
	 *
	 *********************************************************************/

	upper_table->set_border_width (4);
	upper_table->set_row_spacings (2);
	upper_table->set_col_spacings (2);
	upper_table->set_name ("CVElemsTableFront");
	
	middle_table->set_border_width (4);
	middle_table->set_row_spacings (2);
	middle_table->set_col_spacings (2);
	middle_table->set_name ("CVElemsTableBack");
	
	lower_table->set_border_width (4);
	lower_table->set_row_spacings (2);
	lower_table->set_col_spacings (2);
	lower_table->set_name ("CVElemsTableExample");
	
	/*-----------------------------------------------------------------------*/
	/*                               y-beg, y-end, x-beg, x-end              */
	/*-----------------------------------------------------------------------*/
	upper_table->attach (*vbox_frontlabel, 0, 1, 0, 1, 
						   Gtk::FILL, Gtk::FILL, 0, 0);

	upper_table->attach (*front_scrollwin, 1, 2, 0, 1, 
						   Gtk::FILL|Gtk::EXPAND, Gtk::FILL|Gtk::EXPAND, 0, 0);

	/*=======================================================================*/
	middle_table->attach (*hbox_img, 1, 2, 0, 1, 
						   Gtk::FILL|Gtk::EXPAND, Gtk::SHRINK, 0, 0);

	middle_table->attach (*hbox_asf, 1, 2, 1, 2, 
						   Gtk::FILL|Gtk::EXPAND, Gtk::SHRINK, 0, 0);

	/*-----------------------------------------------------------------------*/
	middle_table->attach (*vbox_backlabel, 0, 1, 2, 3,
						   Gtk::FILL, Gtk::FILL, 0, 0);

	middle_table->attach (*backtext_scrollwin, 1, 2, 2, 3, 
						   Gtk::FILL|Gtk::EXPAND, Gtk::FILL|Gtk::EXPAND, 0, 0);

	/*=======================================================================*/
	lower_table->attach (*vbox_examplelabel, 0, 1, 0, 1, 
						   Gtk::FILL, Gtk::EXPAND|Gtk::FILL, 0, 0);

	lower_table->attach (*example_scrollwin, 1, 2, 0, 1, 
						   Gtk::FILL|Gtk::EXPAND, Gtk::FILL|Gtk::EXPAND, 0, 0);

	/*-----------------------------------------------------------------------*/
	lower_table->attach (*hbox_example_img, 1, 2, 1, 2, 
						   Gtk::FILL|Gtk::EXPAND, Gtk::SHRINK, 0, 0);

	lower_table->attach (*hbox_asb, 1, 2, 2, 3, 
						   Gtk::FILL|Gtk::EXPAND, Gtk::SHRINK, 0, 0);
	/*=======================================================================*/

	/** Pane 1 holds top elems_table and Pane 2.
	 *	Pane 2 holds both middle and the bottom elems_table.
	 */
	paned_2->pack1 (*middle_table, false, false);
	paned_2->pack2 (*lower_table, true,  false);

	paned_1->pack1 (*upper_table, true,  false);
	paned_1->pack2 (*paned_2,       true,  false);

    /** Both PDAs and Maemo have a narrow screen and thus requires
	 *  smart packing.
	 */
#if defined(IS_HILDON) || defined (IS_PDA)
	Gtk::ScrolledWindow* scrollw = manage (new Gtk::ScrolledWindow);
	set_cv_scrollwin (*scrollw, *paned_1);
	vbox_all->pack_start (*scrollw);
#else                                      // Regular desktop 
	vbox_all->pack_start (*paned_1);
#endif

	/**--------------------------------------------------**
	 ** Pack [Help] [Cancel][+ Add][Add-n-Close] Buttons **
	 **--------------------------------------------------**
	 */
	Gtk::HBox* hbbox = manage (new Gtk::HBox(false, 0));
	Gtk::HSeparator* hseparator = manage (new Gtk::HSeparator);

#if !defined(IS_PDA)
	Gtk::HButtonBox*  hbuttonbox = manage (new Gtk::HButtonBox);

    hbuttonbox->set_homogeneous (false);
    hbuttonbox->set_spacing (0);
	hbuttonbox->set_layout (Gtk::BUTTONBOX_END);
	hbuttonbox->set_name ("CardView_CtrlButtonBox");

	hbuttonbox->pack_end (*m_count_button,       Gtk::PACK_SHRINK, 0);
	hbuttonbox->pack_end (*m_cancel_button,      Gtk::PACK_SHRINK, 0);
	hbuttonbox->pack_end (*m_add_button,         Gtk::PACK_SHRINK, 0);
	hbuttonbox->pack_end (*m_add_n_close_button, Gtk::PACK_SHRINK, 0);

	hbbox->pack_start (*m_help_button, Gtk::PACK_SHRINK, 0);
	hbbox->pack_start (*hbuttonbox);
#else
	hbbox->pack_start (*m_help_button,        Gtk::PACK_SHRINK, 0);
	hbbox->pack_end   (*m_add_button,         Gtk::PACK_SHRINK, 0);
	hbbox->pack_end   (*m_add_n_close_button, Gtk::PACK_SHRINK, 0);
	hbbox->pack_end   (*m_cancel_button,      Gtk::PACK_SHRINK, 0);
	hbbox->pack_end   (*m_count_button,       Gtk::PACK_SHRINK, 0);
#endif	

	vbox_all->pack_start (*hseparator, Gtk::PACK_SHRINK, 0);
	vbox_all->pack_start (*hbbox,      Gtk::PACK_SHRINK, 0);

	add (*vbox_all);

	/**---------------**
	 ** Setup signals **
	 **---------------**
	 */
	m_help_button->signal_clicked ().connect (
		mem_fun (*this, &CardView::on_help_clicked));

	m_cancel_button->signal_clicked ().connect (
		mem_fun (*this, &CardView::on_cancel_clicked));

	m_add_button->signal_clicked ().connect (
		mem_fun (*this, &CardView::on_add_clicked));

	m_add_n_close_button->signal_clicked ().connect (
		mem_fun (*this, &CardView::on_addnclose_clicked));

	m_browse_img_button->signal_clicked ().connect (
		bind<int>(mem_fun (*this,&CardView::on_browse_img_clicked), TV_IMG));

	m_browse_example_img_button->signal_clicked ().connect (
		bind<int>(mem_fun(*this, &CardView::on_browse_img_clicked),	TV_EIMG));

    signal_size_allocate ().connect (
		mem_fun (*this, &CardView::size_allocate_cb));

	/**------------------**
	 ** Fill up the form **
	 **------------------**
	 */
	if (m_card != NULL) 
	{
		m_front_text   ->get_buffer ()->set_text (m_card->get_question ());
		m_back_text    ->get_buffer ()->set_text (m_card->get_answer   ());
		m_example_text ->get_buffer ()->set_text (m_card->get_example  ());

		m_asf_text->get_buffer ()->set_text (m_card->get_alt_spelling (ASF));
		m_asb_text->get_buffer ()->set_text (m_card->get_alt_spelling (ASB));

		m_img_text->get_buffer ()->set_text (m_card->get_image_path ());
		m_example_img_text->get_buffer ()->set_text (
			m_card->get_example_image_path ());
	}

	/** Set Widget names - very helpful with debugging!
	 */
	m_front_text        ->set_name ("FrontText");
	m_back_text         ->set_name ("BackText");
	m_example_text      ->set_name ("ExampleText");
	m_asf_text          ->set_name ("AltSpellFrontText");
	m_asb_text          ->set_name ("AltSpellBackText");
	m_help_button       ->set_name ("HelpButton");
	m_cancel_button     ->set_name ("CancelButton");
	m_add_button        ->set_name ("AddButton");
	m_add_n_close_button->set_name ("AddNCloseButton");

	vbox_all->set_name ("CardView_VBoxAll");

	show_all_children ();
}

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//                              Callbacks
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

/**
 * Called by the following methods to edit an existing card:
 *
 *    - DeckPlayer::on_vcard_edit_clicked ()
 *
 *    - DeckView::on_edit_clicked ()
 */
int
CardView::
run_in_edit_mode (Gtk::Window& parent_, bool disable_cancel_)
{
	trace_with_mask("CardView::run_in_edit_mode",GUITRACE);

	m_syntax_error = false;
	m_add_button->set_sensitive (false);
	if (disable_cancel_) {
		m_cancel_button->set_sensitive (false);
	}

	take_snapshot ();

	GRANULE->move_child_to_parent_position (parent_, *this);

	show ();
	present ();
	gtk_main ();

	GRANULE->move_parent_to_child_position (parent_, *this);

	return (m_syntax_error ? Gtk::RESPONSE_REJECT : m_response);
}

/**
 * Handling the very first card is different from the rest.
 *
 * The first card is created by the DeckView.
 *
 * If card creation is cancelled by the user, 
 * we are responsible for memory deallocation.
 *
 * The rest of the cards are created here.
 *
 * Called by:
 *    
 *   - DeckView::on_add_clicked ()
 *
 * Possible values returned by run():
 *
 *    <Cancel> - Gtk::RESPONSE_CANCEL : cancel dialog
 *    <Add>    - Gtk::RESPONSE_APPLY  : add new card and keep dialog open
 *    <Ok>     - Gtk::RESPONSE_OK     : add new card and close dialog
 **/
int
CardView::
run_in_add_mode (DeckView& deckview_, AutoFillDB& autofill_db_)
{
	trace_with_mask("CardView::run_in_add_mode",GUITRACE);

	m_syntax_error = false;

	refresh_cards_count (deckview_);
	apply_patterns (autofill_db_, deckview_.get_deck_size ()+1);

	/** Show the dialog for the first card.
	 */
	GRANULE->move_child_to_parent_position (deckview_, *this);

	while (true) 
	{
		show ();
		present ();
		gtk_main ();

		DL ((GRAPP,"Caught syntax error : RESPONSE_REJECT\n"));
		if (m_response == Gtk::RESPONSE_CANCEL) 
		{
			DL ((GRAPP,"Dialog was cancelled - RESPONSE_CANCEL\n"));
			delete m_card;
			goto done;
		}

		if (m_syntax_error == false) 
		{
			break;
		}
	}

	deckview_.add_new_card (dynamic_cast<Card*>(m_card));

	while (m_keep_open) 
	{
		DL ((GRAPP,"Looping ...\n"));
		m_card = new Card (dynamic_cast<Deck&>(deckview_.get_deck ()));
		
		m_syntax_error = false;
		refresh_cards_count (deckview_);
		apply_patterns (autofill_db_, deckview_.get_deck_size ()+1);

		show ();
		present ();
		gtk_main ();

		if (m_syntax_error) {
			continue;			// give the user a chance to fix errors
		}

		if (m_response == Gtk::RESPONSE_CANCEL) {
			delete m_card;
			break;
		}

		deckview_.add_new_card (dynamic_cast<Card*>(m_card));
	}

  done:
	GRANULE->move_parent_to_child_position (deckview_, *this);
	return (m_response);
}

void
CardView::
on_add_clicked  ()
{
	trace_with_mask("CardView::on_add_clicked",GUITRACE);

	on_addnclose_clicked ();
	if (!m_syntax_error) {
		clear_dialog ();
	}
	m_keep_open = true;
	m_response = Gtk::RESPONSE_APPLY;
}

void
CardView::
on_addnclose_clicked  ()
{
	trace_with_mask("CardView::on_addnclose_clicked",GUITRACE);

	Glib::ustring tmp;
	Glib::ustring text = m_front_text->get_buffer ()->get_text ();

	m_syntax_error = false;
	m_response = Gtk::RESPONSE_OK;


	if (text.length () == 0) 
	{
		Gtk::MessageDialog err0 (
	_("<b>Front</b> field <span foreground=\"red\">IS EMPTY</span>!\n\n"
	  "This is an illegal syntax.\n"
	  "You must fill it with (unique) text.\n"),
	true, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_CLOSE);
		err0.run ();
		m_syntax_error = true;
		return;
	}

	if (!Granule::check_markup (text)) 
	{
		Gtk::MessageDialog err1 (
			_("<b>Front</b> field contains illegal markup tag(s)!"),
			true, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_CLOSE);
		err1.run ();
		m_syntax_error = true;
		return;
	}

	if (!Granule::check_markup (m_back_text->get_buffer ()->get_text ())) 
	{
		Gtk::MessageDialog err2 (
			_("<b>Back</b> field contains illegal markup tag(s)!"),
			true, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_CLOSE);
		err2.run ();
		m_syntax_error = true;
		return;
	}

	if (!Granule::check_markup (m_example_text->get_buffer ()->get_text ())) 
	{
		Gtk::MessageDialog err3 (
			_("<b>Example</b> field contains illegal markup tag(s)!"),
			true, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_CLOSE);
		err3.run ();
		m_syntax_error = true;
		return;
	}
	
	text = m_img_text->get_buffer ()->get_text ();
	if (text.length () > 0) {
		if (!Granule::test_file_exists (text, m_card->get_deck_path (), tmp))
		{
			Gtk::MessageDialog err4 (
				_("<b>Front IMG</b> field contains non-existent filename!"),
				true, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_CLOSE);
			err4.run ();
			m_syntax_error = true;
			return;
		}
	}

	text = m_example_img_text->get_buffer ()->get_text ();
	if (text.length () > 0) {
		if (!Granule::test_file_exists (text, m_card->get_deck_path (), tmp))
		{
			Gtk::MessageDialog err5 (
				_("<b>Example IMG</b> field contains non-existent filename!"),
				true, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_CLOSE);
			err5.run ();
			m_syntax_error = true;
			return;
		}
	}

	/** Transfer data from the widget to the Card object
	 */
	if (m_card != NULL && text_modified ()) 
	{
		m_card->set_question (m_front_text  ->get_buffer ()->get_text ());
		m_card->set_answer   (m_back_text   ->get_buffer ()->get_text ());
		m_card->set_example  (m_example_text->get_buffer ()->get_text ());

		m_card->set_alt_spelling (ASF, m_asf_text->get_buffer()->get_text ());
		m_card->set_alt_spelling (ASB, m_asb_text->get_buffer()->get_text ());

		m_card->set_image_path (m_img_text->get_buffer ()->get_text ());

		m_card->set_example_image_path (
			m_example_img_text->get_buffer ()->get_text ());
	}

	m_keep_open = false;
	m_front_text->grab_focus ();

	hide ();
	gtk_main_quit ();
}

/**
 * @return true - top other handlers from being invoked for the event.
 */
bool
CardView::
on_delete_window_clicked (GdkEventAny* /*event_*/)
{
    trace_with_mask("CardView::on_delete_window_clicked",GUITRACE);

	on_cancel_clicked ();
    return true;  
}

void
CardView::
on_cancel_clicked  ()
{
	trace_with_mask("CardView::on_cancel_clicked",GUITRACE);
	
	m_response = Gtk::RESPONSE_CANCEL;
	hide ();
	gtk_main_quit ();
}

/** 
 *  Fend off clicking Esc key on the keyboard.
 *
 *	This often happens when I want to enter ~ (tilda) key, and instead
 *  press Esc. The effect is to close the editing window which leads to
 *  the loss of all data already entered. The only way to close dialog
 *	is to click on Cancel button.
 *
 *  @param event_ Event data structure.
*/
bool
CardView::
on_key_press_event (GdkEventKey* event_)
{
	trace_with_mask("CardView::on_key_press_event",GEOM);
	DL((GEOM,"key_pressed = %d\n", event_->keyval));

	/*= Trap and disable ESC key event.
	 *	The values for all keysyms are found 
	 *	in $prefix/gtk-2.0/gdk/gdkkeysyms.h
	 */
	if (event_->keyval == GDK_Escape) { 
		return false;
	}

    /** Keep processing keystroke
	 */
#ifdef IS_HILDON
#ifdef GLIBMM_VFUNCS_ENABLED
	return Bin::on_key_press_event (event_);
#else
	return false;   // don't know how to handle for IT2007: HILDON_DEPRICATED
#endif
#else
	return Window::on_key_press_event (event_);
#endif
}

/** 
 * A card has 'image' and 'image_path' properties:
 *
 *  + 'image_path' (RO) is absolute path of the Deck the card came from.
 *  + 'image' (RW) is the relative path + filename.
 *
 * We show 'image' property below its corresponding m_img_text entry widget.
 * If user clicks on <Browse>, the file chooser changes to 'image_path/image'.
 *
 * When user clicks on <Browse>, we take the new image pathname, and 
 * calcuate its path relative to the 'image_path' (deck path), and
 * set the result to m_img_text. 
 *
 * The 'image' part is saved in the deck XML file as an attribute of
 * the 'front' tag.
 */
void
CardView::
on_browse_img_clicked  (int type_)
{
	trace_with_mask("CardView::on_browse_img_clicked",GUITRACE);

	PathMode path_mode = m_card->image_path_mode ();

	DL ((GRAPP,"Image path mode: %s\n",
		 (path_mode == PATH_RELATIVE ? "relative" : "absolute")));

	FileOpenDialog f (_("Select Image File"), this);

	string selection;
	Glib::ustring path;
	
	/**
	 * Set up initial path to guide FileOpenDialog
	 * to the right place in the filesystem.
	 */
	if (type_ == TV_IMG) {
		path= m_img_text->get_buffer ()->get_text ();
	}
	else {
		path = m_example_img_text->get_buffer ()->get_text ();
	}

	if (!path.empty ()) 
	{
		selection = Granule::filename_from_utf8 (path);
		if (path_mode == PATH_RELATIVE) 
		{
			if (!g_path_is_absolute (selection.c_str ())) {
				path = m_card->get_deck_path () + G_DIR_SEPARATOR_S + path;
			}
		}
		else 
		{
            if (g_path_is_absolute (selection.c_str ())) {
                path = selection;
            }
            else { 
                path = Granule::calculate_absolute_path (
                    m_card->get_deck_path (), selection);
            }
		}
		DL ((GRAPP,"Set initial pathname to \"%s\"\n", path.c_str ()));
		f.set_filename (path);
	}

	DL ((GRAPP,"calling FileOpenDialog (\"%s\")\n", path.c_str ()));

    if (f.run () != Gtk::RESPONSE_OK)
	{
		return;
	}

	selection = Granule::filename_from_utf8 (f.get_filename ());

	if (selection.length () == 0) {
		const Glib::ustring msg ("The filepath \"" + f.get_filename () +
								 " contains characters not representable " +
								 "in the encoding of the local filesystem.");
		Gtk::MessageDialog error_dialog (*this, msg, false, 
										 Gtk::MESSAGE_ERROR, 
										 Gtk::BUTTONS_OK, 
										 true);
		error_dialog.run();
		return;
	}

	if (path_mode == PATH_RELATIVE) 
	{
		DL ((GRAPP,"Deck absolute path: '%s'\n", 
			 m_card->get_deck_path ().c_str ()));

		string rp = Granule::calculate_relative_path (
			m_card->get_deck_path (), selection);

		DL ((GRAPP,"Relative path calculated: '%s'\n", rp.c_str ()));

		if (type_ == TV_IMG) {
			m_img_text->get_buffer ()->set_text (rp);
		}
		else {
			m_example_img_text->get_buffer ()->set_text (rp);
		}
	}
	else {
		if (type_ == TV_IMG) {
			m_img_text->get_buffer ()->set_text (selection);
		}
		else {
			m_example_img_text->get_buffer ()->set_text (selection);
		}
	}
}

void
CardView::
take_snapshot ()
{
	trace_with_mask("CardView::take_snapshot",GUITRACE);

	m_snapshot_front    = m_front_text   ->get_buffer ()->get_text ();
	m_snapshot_back     = m_back_text    ->get_buffer ()->get_text ();
	m_snapshot_example  = m_example_text ->get_buffer ()->get_text ();
	m_snapshot_asf      = m_asf_text     ->get_buffer ()->get_text ();
	m_snapshot_asb      = m_asb_text     ->get_buffer ()->get_text ();
	m_snapshot_img      = m_img_text     ->get_buffer ()->get_text ();
	m_snapshot_example_img = m_example_img_text->get_buffer ()->get_text ();
}

bool
CardView::
text_modified ()
{
	bool result;

	result = 
		m_snapshot_front  != m_front_text    ->get_buffer ()->get_text () ||
		m_snapshot_back    != m_back_text    ->get_buffer ()->get_text () ||
		m_snapshot_example != m_example_text ->get_buffer ()->get_text () ||
		m_snapshot_asf     != m_asf_text     ->get_buffer ()->get_text () ||
		m_snapshot_asb     != m_asb_text     ->get_buffer ()->get_text () ||
		m_snapshot_img     != m_img_text     ->get_buffer ()->get_text () ||
		m_snapshot_example_img != m_example_img_text->get_buffer ()->get_text();

	DL ((GRAPP,"Card is %smarked as modified.\n", (result ? "":"not ")));

	return result;
}

void
CardView::
on_help_clicked  ()
{
	trace_with_mask("CardView::on_help_clicked",GUITRACE);

	if (m_help_dialog == NULL) {
		m_help_dialog = new CardViewHelpDialog (*this);
	}

	m_help_dialog->show ();
	m_help_dialog->present ();
}

void
CardView::
clear_dialog ()
{
	m_front_text  ->get_buffer ()->set_text ("");
	m_back_text   ->get_buffer ()->set_text ("");
	m_example_text->get_buffer ()->set_text ("");
	m_asf_text    ->get_buffer ()->set_text ("");
	m_asb_text    ->get_buffer ()->set_text ("");
	m_img_text    ->get_buffer ()->set_text ("");
	m_example_img_text->get_buffer ()->set_text ("");

	m_snapshot_front    = "";
	m_snapshot_back     = "";
	m_snapshot_example  = "";
	m_snapshot_asf      = "";
	m_snapshot_asb      = "";
	m_snapshot_img      = "";
	m_snapshot_example_img = "";
}

void
CardView::
size_allocate_cb (Gtk::Allocation& allocation_)
{
    trace_with_mask("CardView::size_allocate_cb",GEOM);

	CONFIG->set_card_view_geometry (allocation_.get_width (),
									allocation_.get_height ());
}

void
CardView::
refresh_cards_count (DeckView& deckview_)
{
	std::ostringstream os;
	m_cards_count = deckview_.get_deck_size ();

#ifdef IS_PDA
	os << m_cards_count;
#else
	os << m_cards_count << " cards";
#endif
	m_count_button->set_label (os.str ());
}

/** Apply patterns to a newly created Card
 */
void
CardView::
apply_patterns (AutoFillDB& autofill_db_, int seqnum_)
{
	static char buffer [1024];
	Glib::ustring pattern;

	if (!autofill_db_.is_enabled () || seqnum_ < 1) {
		return;
	}

	pattern = autofill_db_.get_back_pattern ();
	if (pattern.size ()) {
		snprintf (buffer, sizeof(buffer)-1, pattern.c_str (), seqnum_);
		m_back_text->get_buffer ()->set_text (buffer);
	}

	pattern = autofill_db_.get_asf_pattern ();
	if (pattern.size ()) {
		snprintf (buffer, sizeof(buffer)-1, pattern.c_str (), seqnum_);
		m_asf_text->get_buffer ()->set_text (buffer);
	}

	pattern = autofill_db_.get_asb_pattern ();
	if (pattern.size ()) {
		snprintf (buffer, sizeof(buffer)-1, pattern.c_str (), seqnum_);
		m_asb_text->get_buffer ()->set_text (buffer);
	}
}
