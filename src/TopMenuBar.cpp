// -*- c++ -*-
//------------------------------------------------------------------------------
//                              TopMenuBar.cpp
//------------------------------------------------------------------------------
// $Id: TopMenuBar.cpp,v 1.34 2008/06/16 02:36:53 vlg Exp $
//------------------------------------------------------------------------------
//  Copyright (c) 2004,2006-2007 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version
//  2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------
// Created: 01/03/2004
//------------------------------------------------------------------------------

#include <gtkmm/stock.h>

#include <assa/Assure.h>

#include "Granule.h"
#include "TopMenuBar.h"
#include "MainWindow.h"
#include "DeckManager.h"
#include "GrappConf.h"

#include "Intern.h"

using namespace ASSA;
using sigc::bind;
using sigc::mem_fun;

void
TopMenuBar::
create ()
{
    trace("TopMenuBar::create");
    using namespace Gtk;

	if (m_created) {
		return;
	}

    m_menu_bar = manage (new MenuBar ());
    m_open_recent_menu = manage (new Menu ());
    {
		using namespace Menu_Helpers;

		/**==================================================================**
		 *  [File] Menu                                                       *
		 **==================================================================**/
		m_file_menu = manage (new Menu ());
		MenuList& f = m_file_menu->items ();

		f.push_back (MenuElem (_("_Preferences"), 
							   mem_fun (m_mw, &MainWindow::pref_cb)));
		m_preferences = &f.back ();

#ifndef IS_HILDON
		f.push_back (SeparatorElem ());
		f.push_back (MenuElem (_("_Quit"), 
							   mem_fun (m_mw, &MainWindow::exit_cb)));
		m_exit = &f.back ();
#endif

		/**==================================================================**
		 *  [CardFile] Menu                                                   *
		 **==================================================================**/
		m_cardfile_menu = manage (new Menu ());
		MenuList& cf = m_cardfile_menu->items ();

		cf.push_back (MenuElem (_("_New"),
								mem_fun (m_mw, &MainWindow::new_cb)));
		m_new = &cf.back ();
		cf.push_back (MenuElem (_("_Open"), 
								mem_fun (m_mw, &MainWindow::open_cb)));
		m_open = &cf.back ();
		refill_history_list ();
		cf.push_back (MenuElem (_("Open _Recent"), 
								*m_open_recent_menu));
		m_open_recent = &cf.back ();
		cf.push_back (SeparatorElem ()); 
		cf.push_back (MenuElem (_("_Save"), 
								mem_fun (m_mw, &MainWindow::save_cb)));
		m_save = &cf.back ();

		cf.push_back (MenuElem (_("Save _As"), 
								mem_fun(m_mw, &MainWindow::save_as_cb)));
		m_save_as = &cf.back ();

		cf.push_back (SeparatorElem ());
		cf.push_back (MenuElem(_("_Close"), 
							   mem_fun(m_mw, &MainWindow::close_cb)));
		m_close = &cf.back ();

		/*--------------------------------*/
		cf.push_back (SeparatorElem ());
		/*--------------------------------*/

		/**------------------------------**
		 *  [CardFile]->[Export] SubMenu  *
		 **------------------------------**/
		m_export_menu = manage (new Menu ());
		MenuList& expt = m_export_menu->items ();

		expt.push_back (MenuElem (_("Sound _Bites"),
								  mem_fun(m_mw, &MainWindow::export_sound_cb)));

		cf.push_back (MenuElem (_("_Export"), *m_export_menu));
		m_export = &cf.back ();

		/**----------------------------**
		 *  [CardFile]->[Play] SubMenu  *
		 **----------------------------**/
		m_play_menu = manage (new Menu ());
		MenuList& cfp = m_play_menu->items ();

		for (int i = 0; i < CARDBOXES_MIN; i++) {
			std::ostringstream os;
			os << "Card box _" << (i+1);
			cfp.push_back (MenuElem (os.str (), 
				  bind<int>(mem_fun(m_mw, &MainWindow::play_in_box), i)));
		}

		cf.push_back (MenuElem (_("_Play in"), 
								*m_play_menu));
		m_play_in = &cf.back ();

		/*--------------------------------*/
		cf.push_back (SeparatorElem ());
		/*--------------------------------*/

		cf.push_back (MenuElem (_("_Preferences"), 
							   mem_fun (m_mw, &MainWindow::cardbox_prefs_cb)));
		m_cb_preferences = &cf.back ();

		/**==================================================================**
		 *  [Deck] Menu                                                       *
		 **==================================================================**/
		m_deck_menu = manage (new Menu ());
		MenuList& d = m_deck_menu->items ();

		d.push_back (MenuElem (_("_New"), 
							   mem_fun(m_dm, &DeckManager::new_deck_cb)));
		m_deck_new = &d.back ();
		d.push_back (MenuElem (_("_Open"),
							   mem_fun (m_dm, &DeckManager::open_deck_cb)));
		m_deck_open = &d.back ();
		d.push_back (MenuElem (_("_Insert from Deck"),
							   mem_fun (m_dm, &DeckManager::insert_deck_cb)));
		m_deck_insert = &d.back ();

		d.push_back (MenuElem (_("I_mport from CSV"),
							   mem_fun (m_dm, &DeckManager::import_deck_cb)));
		m_deck_import = &d.back ();

		d.push_back (MenuElem (_("E_xport to CSV"),
							   mem_fun (m_dm, &DeckManager::export_deck_cb)));
		m_deck_export = &d.back ();

		d.push_back (SeparatorElem ()); 

		d.push_back (MenuElem (_("_Save"),
							   mem_fun(m_dm, &DeckManager::save_deck_cb)));
		m_deck_save = &d.back ();

		d.push_back (MenuElem (_("Save _As"),
							   mem_fun(m_dm,&DeckManager::save_as_deck_cb)));
		m_deck_save_as = &d.back ();

		d.push_back (MenuElem (_("Save All"),
							   mem_fun(m_dm,&DeckManager::save_all_deck_cb)));
		m_deck_save_all = &d.back ();

		d.push_back (SeparatorElem ()); 

		d.push_back (MenuElem (_("_Play/Edit"),
							   mem_fun (m_dm, &DeckManager::play_deck_cb)));
		m_deck_play = &d.back ();

		/**------------------------------**
		 *  [Deck]->[Add_To_Box] SubMenu  *
		 **------------------------------**/
		m_add_to_box_menu = manage (new Menu ());
		MenuList& a2bp = m_add_to_box_menu->items ();

		for (int j = 0; j < CARDBOXES_MIN; j++) {
			std::ostringstream os;
			os << "Card box _" << (j + 1);
			a2bp.push_back (MenuElem (os.str (), 
						   bind<int>(mem_fun(m_dm, 
									 &DeckManager::add_deck_to_box), j)));
		}
		d.push_back (MenuElem (_("Add to "), 
							   *m_add_to_box_menu));
		m_add_to_box = &d.back ();

		/*------------------------------------------------------------*/
		d.push_back (MenuElem (_("_Unselect"),
							   mem_fun (m_mw, &MainWindow::unselect_deck_cb)));
		m_deck_unselect = &d.back ();

		/**-----------------------------------------------------------*/
		Menu* help_menu = manage (new Menu ());
        MenuList& help_list = help_menu->items ();

        help_list.push_back (MenuElem (_("_About"),
                                       mem_fun (m_mw, &MainWindow::about_cb)));

		/**
		 * Assemble backwards
		 */
#ifdef IS_HILDON	
		MenuList& list_bar = m_mw.get_menu ()->items ();
#else
		MenuList& list_bar = m_menu_bar->items ();
#endif
		
		list_bar.push_back (MenuElem (_("_File"),
									  Gtk::AccelKey("<control>f"), 
									  *m_file_menu));

		list_bar.push_back (MenuElem (_("_CardFile"), 
									  Gtk::AccelKey("<control>c"), 
									  *m_cardfile_menu));

		list_bar.push_back (MenuElem (_("_Deck"), 
									  Gtk::AccelKey("<control>d"), 
									  *m_deck_menu));

		list_bar.push_back (MenuElem (_("_Help"), 
									  Gtk::AccelKey("<control>h"), *help_menu));
		list_bar.back ().set_right_justified ();

#ifdef IS_HILDON
		list_bar.push_back (MenuElem (_("Quit"), 
							mem_fun (m_mw, &MainWindow::exit_cb)));
		m_exit = &f.back ();
#endif

		m_created = true;
    }
}

void
TopMenuBar::
add_history ()
{
    trace("TopMenuBar::add_history");

    /* Add whatever file Config has open to its history list.
	 */
    string nm = CONFIG->add_document_history ();
    if (nm.size () == 0) {
		return;
    }
    refill_history_list ();
}

void
TopMenuBar::
refill_history_list ()
{
    trace("TopMenuBar::refill_history_list");

    /* Clear the list and refill it with history elements
	 */
    guint position = 0;
    Gtk::Menu_Helpers::MenuList& file_list = m_open_recent_menu->items ();
    file_list.clear ();

	std::string underscored;
    const GrappConf::DHList& dh_list = CONFIG->get_document_history ();
    GrappConf::DHList_CIter cit = dh_list.begin ();

	while (cit != dh_list.end ()) {
		if ((*cit).find ("_") == std::string::npos) {
			underscored = (*cit);
		}
		else {
			std::string::const_iterator pos = (*cit).begin ();
			underscored = "";
			while (pos != (*cit).end ()) {
				if ((*pos) == '_') { underscored += "__"; }
				else { underscored += (*pos); }
				pos++;
			}
		}
		file_list.push_back (
			Gtk::Menu_Helpers::MenuElem (
				underscored,
				bind<guint> (
					mem_fun (m_mw, &MainWindow::open_recent_cb), position)));
		position++;
		cit++;
    }
}

MenuBar*
TopMenuBar::
get_menu_bar ()
{
    if (m_menu_bar == NULL) {
		DL((ASSA::ASSAERR, 
			"TopMenuBar::get_menu_bar() : m_menu_bar uninitialized!\n"));
		Assure_exit (false);
    }
    return m_menu_bar;
}

void
TopMenuBar::
set_sensitivity (MWState state_)
{
    trace("TopMenuBar::set_sensitivity");

	if (! is_created ()) {
		return;
	}

	m_mw.dump_mw_state ();

    switch (state_) 
    {
    case Start:
		m_new        ->set_sensitive (true ); // new
		m_open       ->set_sensitive (true ); // open
		m_open_recent->set_sensitive (true ); // open recent
		m_save       ->set_sensitive (false); // save
		m_save_as    ->set_sensitive (false); // save as
		m_export     ->set_sensitive (false); // export
		m_close      ->set_sensitive (false); // close
		m_exit       ->set_sensitive (true ); // exit
		m_play_in    ->set_sensitive (false); // play in CardBox
		m_cb_preferences->set_sensitive (false); // CardBox prefs

		break;

    case Dirty:
		m_new        ->set_sensitive (false); // new
		m_open       ->set_sensitive (false); // open
		m_open_recent->set_sensitive (false); // open recent
		m_save       ->set_sensitive (true ); // save
		m_save_as    ->set_sensitive (true ); // save as
		m_export     ->set_sensitive (true ); // export
		m_close      ->set_sensitive (true ); // close
		m_exit       ->set_sensitive (true ); // exit
		m_play_in    ->set_sensitive (true ); // play in CardBox
		m_cb_preferences->set_sensitive (true); // CardBox prefs

		break;

    case Clean:
		m_new        ->set_sensitive (false); // new
		m_open       ->set_sensitive (false); // open
		m_open_recent->set_sensitive (false); // open recent
		m_save       ->set_sensitive (false); // save
		m_save_as    ->set_sensitive (true ); // save as
		m_export     ->set_sensitive (true ); // export
		m_close      ->set_sensitive (true ); // close
		m_exit       ->set_sensitive (true ); // exit
		m_play_in    ->set_sensitive (true ); // play in CardBox
		m_cb_preferences->set_sensitive (true); // CardBox prefs

		break;

	case Finish:
        /* do-nothing */
		break;
    }
}

void
TopMenuBar::
set_deck_sensitivity (bool is_deck_selected_, size_t num_open_decks_)
{
    trace("TopMenuBar::set_deck_sensitivity");

	if (!is_created ()) {
		return;
	}

	if (is_deck_selected_) 
	{
		m_deck_new      ->set_sensitive (false);
		m_deck_open     ->set_sensitive (false);
		m_deck_insert   ->set_sensitive (true );
		m_deck_import   ->set_sensitive (true );
		m_deck_export   ->set_sensitive (true );
		m_deck_save     ->set_sensitive (true );
		m_deck_save_as  ->set_sensitive (true );
		m_deck_save_all ->set_sensitive (false);
		m_deck_play     ->set_sensitive (true );
		m_add_to_box    ->set_sensitive (true );
		m_deck_unselect ->set_sensitive (true );
	}
	else {
		m_deck_new      ->set_sensitive (true );
		m_deck_open     ->set_sensitive (true );
		m_deck_insert   ->set_sensitive (false);
		m_deck_import   ->set_sensitive (false);
		m_deck_export   ->set_sensitive (false);
		m_deck_save     ->set_sensitive (false);
		m_deck_save_as  ->set_sensitive (false);
		m_deck_save_all ->set_sensitive ((num_open_decks_ > 0));
		m_deck_play     ->set_sensitive (false);
		m_add_to_box    ->set_sensitive (false);
		m_deck_unselect ->set_sensitive (false);
	}
}

