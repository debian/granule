// -*- c++ -*-
//------------------------------------------------------------------------------
//                              Deck.cpp
//------------------------------------------------------------------------------
// $Id: Deck.cpp,v 1.82 2008/08/19 02:52:46 vlg Exp $
//
//  Copyright (c) Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//
// Date: Jan 8, 2004
//------------------------------------------------------------------------------

#include <fstream>
using std::ofstream;
using std::ifstream;

#include <glib.h>
#include <glib/gstdio.h>

#if !defined (IS_WIN32)
#  include <libgen.h>               // basename(3), dirname(3)
#  include <fcntl.h>
#endif

#include <assa/CommonUtils.h>
#include <assa/Fork.h>
#include <assa/TimeVal.h>
#include <assa/AutoPtr.h>

#include "Granule.h"
#include "Granule-main.h"
#include "DeckManager.h"
#include "Deck.h"
#include "Card.h"
#include "MainWindow.h"
#include "GrappConf.h"
#include "CSVImportDialog.h"
#include "CSVExportDialog.h"

#include "Intern.h"

/** Compare real cards by their IDs
 */
class Compare_Cards 
{
public:
	int operator () (VCard* const& vc1_, VCard* const& vc2_)
	{
		Card* cr1 = dynamic_cast<Card*> (vc1_);
		Card* cr2 = dynamic_cast<Card*> (vc2_);
		Assure_exit (cr1 && cr2);
		return (cr1->get_id () < cr2->get_id ());
	}
};	

FILE* Deck::s_pipe_stream = NULL;

void
Deck::
init_internals ()
{
	m_snd_enabled       = false;
	m_snd_path_mode     = PATH_ABSOLUTE;
	m_pics_path_mode    = PATH_RELATIVE;
	m_side_selection    = FRONT;
	m_custom_appearance = false;
}

/** C'tor for existing deck
 */
Deck::
Deck (DeckEditStatus status_) 
	: 
	m_audio_playback_pipe (NULL),
	m_status              (status_), 
	m_needs_renaming      (false),
	m_side_selection      (FRONT)
{
	trace_with_mask("Deck::Deck",GUITRACE|DECK);

	init_internals ();

	m_active_snd_path = Granule::filename_to_utf8 (
		CONFIG->get_snd_archive_path ());

	m_text_colors_db.init_deck_defaults ();
	m_fonts_db.init_deck_defaults ();
	m_appearance.init_deck_defaults ();
}

/** C'tor for the new deck
 */
Deck::
Deck (const string& fname_, DeckEditStatus status_) 
	:
	m_fname  (fname_), 
	m_status (status_),
	m_needs_renaming ((status_ == NEW_DECK)),
	m_desc   ("Created on "),
	m_custom_appearance (false)
{
	trace_with_mask("Deck::Deck(untitled)",GUITRACE|DECK);

	init_internals ();

	m_author = Granule::locale_to_utf8 (CONFIG->get_user_name ());
	m_active_snd_path = 
		Granule::filename_to_utf8 (CONFIG->get_snd_archive_path ());

	/** It is very important to have valid absolute path name
	 *  for a newly created Deck.
	 */
	m_deck_path = CONFIG->get_filesel_path ();
	if (!Glib::path_is_absolute (m_deck_path)) {
		m_deck_path = GRANULE->get_current_working_dir ();
	}

	m_text_colors_db.init_deck_defaults ();
	m_fonts_db.init_deck_defaults ();
	m_appearance.init_deck_defaults ();

	{
		ASSA::TimeVal today = ASSA::TimeVal::gettimeofday ();
		m_desc += Granule::locale_to_utf8 (today.fmtString ("%c"));
	}

	dump_deck_status ();
}

int
Deck::
load (const string& fname_, DeckAction action_)
{
	trace_with_mask("Deck::load",GUITRACE|DECK);
	DL((DECK|APP,"Loading \"%s\" ...\n", fname_.c_str ()));

	string error_msg;
	xmlParserCtxtPtr context;
	xmlDocPtr parser;
	int ret;

	context = xmlCreateFileParserCtxt (fname_.c_str ()); // new

	if (!context) {
		DL((GRAPP,"Failed to parse \"%s\"\n", fname_.c_str ()));
		DECKMGR->on_deck_loaded (this, LOAD_FAILED);
		xmlFreeParserCtxt (context);
		return -1;
	}
	// Initialize context
	context->linenumbers = 1;
	context->validate = 1;
	
	//Tell the validity context about the callbacks:
	//(These are only called if validation is on - see above)
	//
	// context->vctxt.error = &callback_validity_error;
	// context->vctxt.warning = &callback_validity_warning;
	
	context->replaceEntities = 1;

	// Parse the document
	xmlParseDocument (context);

	if(!context->wellFormed) {
		xmlFreeParserCtxt (context); 
		return -1;
	}
	if (context->errNo != 0) {
		xmlFreeParserCtxt (context); 
		return -1;
	}
	parser = context->myDoc;
	xmlFreeParserCtxt (context); // not needed anymore?
	
	if (action_ == INSTALL_DECK) {
		m_fname = fname_;
		m_deck_path = GRANULE->get_dirname (fname_.c_str ());
	}

	DL((GRAPP,"Parsing the deck ...\n"));
	m_status = CLEAN_DECK;

	ret = parse_xml_tree (parser, error_msg);
	xmlFreeDoc (parser);

	if (ret < 0) {
		DECKMGR->on_deck_loaded (this, LOAD_FAILED);
		Gtk::MessageDialog emsg (error_msg, MESSAGE_ERROR);
		emsg.run ();
		return -1;
	}

	/** Setting side selection should come before callback to DECKMGR
	 */
	set_side_selection (CONFIG->get_flipside_history (get_name ()));

	if (action_ == INSTALL_DECK) {
		DECKMGR->on_deck_loaded (this, LOAD_OK);
	}

	dump_deck_status ();
	return 0;
}

/**
 * This method is where all of the XML parsing actually
 * takes place.
 */
int
Deck::
parse_xml_tree (xmlDocPtr parser_, std::string& error_msg_)
{
	trace_with_mask("Deck::parse_xml_tree",GUITRACE|DECK);

	char     buffer [1024];
	char     xpath [64];
	xmlChar* result = NULL;
	Card*    card = NULL;
	
	std::string deck_dirname = GRANULE->get_dirname (m_fname.c_str ());

	std::ostringstream os;
	int illegal_count = 0;

	result = Granule::get_node_by_xpath (parser_, "/deck/author");
	if (result) {
		m_author = (const char*) result;
		xmlFree (result);
	}

	result = Granule::get_node_by_xpath (parser_, "/deck/description");
	if (result) {
		m_desc = (const char*) result;
		xmlFree (result);
	}

	result = Granule::get_node_by_xpath (parser_, "/deck/sound_path");
	if (result) {
		DL ((APP,"/deck/sound_path = \"%s\"\n", (const char*) result));
		if (strlen ((const char*) result) != 0) {
			m_snd_path = Granule::locale_from_utf8 ((const char*) result);
			m_active_snd_path = m_snd_path;
		}
		xmlFree (result);
	}

	result = Granule::get_node_by_xpath (parser_, "/deck/sound_path/@relative");
	if (result)
	{
		DL ((APP,"sound_path relative=\"%s\"\n", (char*)result));
		if (!strcmp ((const char*) result, "yes")) {
			snd_path_mode (PATH_RELATIVE);
			m_active_snd_path = Granule::calculate_absolute_path (
				m_deck_path, m_snd_path);
		}
		else {
			snd_path_mode (PATH_ABSOLUTE);
		}
	}

	DL ((APP,"Final m_active_snd_path \"%s\" (%s)\n", 
		 m_active_snd_path.c_str (), 
		 (snd_path_mode () == PATH_RELATIVE ? "Relative" : "Absolute")));
	
	/** Test the path to see if it is valid.
	 */
	test_active_snd_path ();

	/** Fetch pics_path if present. Default is relative.
	 */
	result = Granule::get_node_by_xpath (parser_, "/deck/pics_path/@relative");
	if (result) {
		if (!strcmp ((const char*) result, "yes")) {
			m_pics_path_mode = PATH_RELATIVE;
		}
		else {
			m_pics_path_mode = PATH_ABSOLUTE;
		}
		xmlFree (result);
	}

	/**-------------------------------------------------------------------------
	 *  NOTE: The order is *important* - it must follow granule.dtd -
	 *        fonts colors ...
	 **-------------------------------------------------------------------------
	 */
	result = Granule::get_node_by_xpath (parser_, "/deck/appearance/@enabled");

	if (result)
	{
		/** Optional appearance settings are enabled
		 */
		if (!strcmp ((const char*) result, "yes")) {
			set_custom_appearance (true);

			// Load font settings (optional) 
			m_fonts_db.load_from_xml_config (parser_, error_msg_);

			// Load text colors settings (optional)
			m_text_colors_db.load_from_xml_config (parser_, error_msg_);

			// Load text appearance settings (optional) 
			m_appearance.load_from_xml_config (parser_, error_msg_);
		}
		xmlFree (result);
	}

	dump ();

	/**********************
	 *   Load the cards   *
	 **********************/
	for (int cards_counter = 1; ; cards_counter++) 
	{
		sprintf (xpath, "/deck/card[%d]", cards_counter);

		result = Granule::get_node_by_xpath (parser_, xpath);
		if (result == NULL) {
			break;
		}
		/** Detected new card
		 */
		card = new Card (*this);

		//-----------------------------------------------------
		sprintf (xpath, "/deck/card[%d]/@id", cards_counter);
		result = Granule::get_node_by_xpath (parser_, xpath);
		if (result == NULL) {
			os << "id is missing in card # " << cards_counter
			   << "\nof the deck " << m_fname;
			error_msg_ = os.str ();
			return -1;
		}
		card->set_id (::atol ((const char*) result+1));
		xmlFree (result);

		//-----------------------------------------------------
		sprintf (xpath, "/deck/card[%d]/front", cards_counter);
		result = Granule::get_node_by_xpath (parser_, xpath);
		if (result == NULL) {
			illegal_count++;
			sprintf (buffer, "<span foreground=\"red\" size=\"large\" "
					 "background=\"black\">Missing entry %d</span>",
					 illegal_count);
			card->set_question (buffer);
			mark_as_modified ();
		}
		else {
			card->set_question ((const char*) result);
			xmlFree (result);
		}

		//-----------------------------------------------------
		sprintf (xpath, "/deck/card[%d]/front/@alt_spelling", cards_counter);
		result = Granule::get_node_by_xpath (parser_, xpath);
		if (result != NULL) {
			card->set_alt_spelling (ASF, (const char*) result);
			xmlFree (result);
		}

		//-----------------------------------------------------
		sprintf (xpath, "/deck/card[%d]/front/@image", cards_counter);
		result = Granule::get_node_by_xpath (parser_, xpath);
		if (result != NULL) {
			card->set_image_path ((const char*) result);
			xmlFree (result);
		}

		//-----------------------------------------------------
		sprintf (xpath, "/deck/card[%d]/back", cards_counter);
		result = Granule::get_node_by_xpath (parser_, xpath);
		if (result != NULL) {	                              /* can be empty */
			card->set_answer ((const char*) result);
			xmlFree (result);
		}

		//-----------------------------------------------------
		sprintf (xpath, "/deck/card[%d]/back/@alt_spelling", cards_counter);
		result = Granule::get_node_by_xpath (parser_, xpath);
		if (result != NULL) {
			card->set_alt_spelling (ASB, (const char*) result);
			xmlFree (result);
		}

		/*----------------------------------------------------------
		 ** back_example node might be empty - thus non-critical.
		 */
		sprintf (xpath, "/deck/card[%d]/back_example", cards_counter);
		result = Granule::get_node_by_xpath (parser_, xpath);
		if (result != NULL) {                                 /* can be empty */
			card->set_example ((const char*) result);
			xmlFree (result);
		}

		sprintf (xpath, "/deck/card[%d]/back_example/@image", cards_counter);
		result = Granule::get_node_by_xpath (parser_, xpath);
		if (result != NULL) {
			card->set_example_image_path ((const char*) result);
			xmlFree (result);
		}

		card->mark_clean ();
		m_cards.push_back (card);
		card->dump ();
	}

	DL((GRAPP,"Processed %d cards\n", m_cards.size ()));
	return 0;
}

void 
Deck::
insert (const string& from_fname_)
{
	trace_with_mask("Deck::insert",GUITRACE|DECK);

	Card* cr_p = NULL;
	Card* cr_q = NULL;

	load (from_fname_, ADD_TO_DECK); 
	m_status = MODIFIED_DECK;

	/** Wack duplicates
	 */
	std::sort (m_cards.begin (), m_cards.end (), Compare_Cards ());
	cardlist_iterator p = m_cards.begin ();
	cardlist_iterator q = p + 1;

	while (q != m_cards.end ()) {
		cr_p = dynamic_cast<Card*> (*p);
		cr_q = dynamic_cast<Card*> (*q);
		Assure_exit (cr_p && cr_q);
		if (cr_p->get_id () == cr_q->get_id ()) {
			delete (*q);
			m_cards.erase (q);
			q = p;
		}
		else {
			p = q;
		}
		q++;
	}
}

/*------------------------------------------------------------------------------
 * import_from_csv ()
 *
 *  Variations:
 *
 *    1: [ ]
 *    2: [ ];[ ]
 *    3: [ ];[ ];[_________]
 *    4: [ ];[ ];[ ];[ ]; ... ;[______]
 *
 *    and much more
 *------------------------------------------------------------------------------
 */
bool
Deck::
import_from_csv (const string& file_)
{
	trace_with_mask("Deck::import_from_csv",GUITRACE|DECK);

	const int size = 1024;
    static char inbuf [size];
	static char failed_fname [size];

	int ret;
	std::vector<Glib::ustring> tokens;
	ParserState state;
	bool bracket_flag;

	Glib::ustring instream;
	Glib::ustring question;
	Glib::ustring answer;
	Glib::ustring example;

	std::ifstream source;	
	int ok_count = 0;      

	std::ofstream ffailed;		// Log input that fails to parse for the user
	int failed_count = 0;       // to deal with later 

	ASSA::TimeVal t (ASSA::TimeVal::gettimeofday ());
	long card_id = t.sec ();

	CSVImportDialog impd;

	if (impd.run () != Gtk::RESPONSE_APPLY) {
		return false;
	}
	impd.hide ();

	source.open (file_.c_str (), std::ios::in);

	if (!source.good ()) {
		Gtk::MessageDialog emsg (
			_("Failed to open import file.\nCheck file permissions."),
			Gtk::MESSAGE_ERROR);
        emsg.run ();
		return false;
    }

	sprintf (failed_fname, "%s.failed", file_.c_str ());
	ffailed.open (failed_fname, std::ios::out);

	DL ((GRAPP,"separator = %s\n", impd.get_separator ()));
	DL ((GRAPP,"doubld_separator = %s\n", impd.get_double_separator ()));

	state = ST_BEGIN;
	bracket_flag = false;

	while (source) {
		DL ((GRAPP,"-------------- next line --------------------\n"));
        source.getline (inbuf, size, '\n');

        DL((GRAPP,"Input: [%s]\n", inbuf));
		if (::strlen (inbuf) == 0 && !bracket_flag) {
			continue;
		}

		DL ((GRAPP, "state = %s\n", get_state_name (state)));

		if (impd.is_windoz_linebreak () && inbuf [strlen (inbuf)-1] == 0x0D) {
			inbuf [strlen (inbuf)-1] = '\0';
		}

		instream = inbuf;
		ret = split_tokens (instream,
							tokens, 
							impd.get_separator ()[0], 
							bracket_flag);

		/****************************
		 * CASE: 0 separators found *
		 ***************************/
		if (ret == 0) {
			if (tokens[0][0] == '"') {
				open_double_quote (bracket_flag);
				change_state (state);              // => ST_1ST_FIELD; 
			}
			switch (state) {
			case ST_BEGIN:
				question = tokens [0]; 
				add_new_card (question, answer, example, card_id, ok_count);
				break;
			case ST_1ST_FIELD:
				if (tokens[0][0] != '"') {
					question += "\n";
				}
				question += tokens [0]; 
				break;
			case ST_2ND_FIELD:
				answer += bracket_flag ? "\n" : "";
				answer += tokens [0];
				break;
			case ST_3RD_FIELD:
				example += bracket_flag ? "\n" : "";
				example += tokens [0];
				break;
			}

			if (state > ST_BEGIN) {
				if (tokens[0][tokens[0].size()-1] == '"') {
					close_double_quote (bracket_flag);
				}
				if (!bracket_flag) {
					add_new_card (question, answer, example, card_id, ok_count);
					reset_state (state);
				}
			}
		}
		/***************************
		 * CASE: 1 separator found *
		 **************************/
		else if (ret == 1) {   
			if (bracket_flag) {
				if (tokens[0][tokens[0].size()-1] != '"') {
					DL((ASSA::ASSAERR,"Exception 11: bad input [%s]\n",inbuf));
					ffailed << "--------------------------\n"
							<< "question: " << question << "\n"
							<< "answer  : " << answer   << "\n"
							<< "example : " << example  << std::endl;
					reset_state (state);
					failed_count++;
					continue;
				}
				switch (state) {
				case ST_1ST_FIELD:
					question += bracket_flag ? "\n" : "";
					question += tokens [0]; 
					DL ((GRAPP,"Completed parsing Question:\n%s\n",
						 question.c_str ()));
					change_state (state);     // => ST_2ND_FIELD;
					break;
				case ST_2ND_FIELD:
					answer += bracket_flag ? "\n" : "";
					answer += tokens [0];
					change_state (state);     // => ST_3RD_FIELD;
					break;
				case ST_3RD_FIELD:
					DL((ASSA::ASSAERR, "Exception 12: bad input [%s]\n",inbuf));
					ffailed << "--------------------------\n"
							<< "question: " << question << "\n"
							<< "answer  : " << answer   << "\n"
							<< "example : " << example  << std::endl;
					reset_state (state);
					failed_count++;
					continue;
					break;
				}
				close_double_quote (bracket_flag);
			}
			else { // bracket not open
				if (state > ST_1ST_FIELD) {
					DL((ASSA::ASSAERR, "Exception 13: bad input [%s]\n",inbuf));
					ffailed << "--------------------------\n"
							<< "question: " << question << "\n"
							<< "answer  : " << answer   << "\n"
							<< "example : " << example  << std::endl;
					reset_state (state);
					failed_count++;
					continue;
				}
				question += tokens [0];
				state = ST_2ND_FIELD;          // => ST_2ND_FIELD;
			}

			if (tokens[1][0] == '"') {
				open_double_quote (bracket_flag);
			}

			switch (state) {
			case ST_2ND_FIELD:
				answer += tokens [1];
				if (tokens[1][tokens[1].size()-1] == '"') {
					close_double_quote (bracket_flag);
				}
				break;
			case ST_3RD_FIELD:
				example += tokens [1];
				if (tokens[1][tokens[1].size()-1] == '"') {
					close_double_quote (bracket_flag);
				}
				break;
			}

			if (!bracket_flag) {
				add_new_card (question, answer, example, card_id, ok_count);
				reset_state (state);
			}
		}
		/***************************
		 * CASE: 2 separators fond *
		 **************************/
		else {  
			if (bracket_flag) {
				if (tokens[0][tokens[0].size ()-1] == '"') {
					if (state != ST_1ST_FIELD) {
						DL((ASSA::ASSAERR,"Exception 21: bad input [%s]\n",
							inbuf));
						ffailed << "--------------------------\n"
								<< "question: " << question << "\n"
								<< "answer  : " << answer   << "\n"
								<< "example : " << example  << std::endl;
						reset_state (state);
						failed_count++;
						continue;
					}
					question += bracket_flag ? "\n" : "";
					question += tokens [0]; 
					close_double_quote (bracket_flag);
				}
			}
			else {	// bracket not opened
				change_state (state);                  // => ST_1ND_FIELD;
				question = tokens [0];
			}
			change_state (state);                      // => ST_2ND_FIELD;

			// Second field is always defined
			answer = tokens[1];
			change_state (state);                      // => ST_3RD_FIELD;

			// Third field can span multiline
			example = tokens[2];
			if (tokens[2][0] == '"') {
				open_double_quote (bracket_flag);
			}
			if (tokens[2][tokens[2].size ()-1] == '"') {
				close_double_quote (bracket_flag);
			}
			if (!bracket_flag) {				// finished record parsing
				add_new_card (question, answer, example, card_id, ok_count);
				change_state (state);           // => ST_BEGIN;
			}
		}
	} // while

	source.close ();
	ffailed.close ();

	if (failed_count == 0) {
		::unlink (failed_fname);
		sprintf (inbuf, "Successful records parsed: %d\n", ok_count);
	}
	else {
		sprintf (inbuf, "Successful records parsed: %d\n"
				 "Failed records: %d\n"
				 "Examine failed log file\n\"%s\"\n"
				 "for syntax errors",
				 ok_count, failed_count, failed_fname);
	}
	Gtk::MessageDialog im (inbuf, Gtk::MESSAGE_INFO);
	im.run ();

	return true;
}

int
Deck::
split_tokens (const Glib::ustring& instream_, 
			  std::vector<Glib::ustring>& tokens_, 
			  char separator_,
			  bool bracket_flag_)
{
    trace_with_mask("Deck::split_tokens",GUITRACE);

	u_int pos_start = 0;
	u_int pos_end = 0;
	bool bracket_open = bracket_flag_;

	tokens_.clear ();

	for (int i = 0; i < 3; i++) 
	{
		while (pos_end != instream_.size ()) 
		{
			DL ((CSVPARSER,"pos_start = '%c', pos_end = '%c'\n",
				 instream_[pos_start], instream_[pos_end]));

			if (instream_[pos_end] == (gunichar) separator_ && 
				! bracket_open) 
			{
				DL ((CSVPARSER,"copying [%s]\n", 
					 instream_.substr (pos_start, pos_end-pos_start).c_str ()));
				tokens_.push_back (
					instream_.substr (pos_start, pos_end-pos_start));
				pos_end++;
				pos_start = pos_end;
				DL ((CSVPARSER,"-- next token --\n"));
				break;
			}
			if (instream_[pos_end] == '"') {
				bracket_open = !bracket_open;
			}
			pos_end++;
		}

		if (pos_end == instream_.size ()) {
			break;
		}
	}
	
	tokens_.push_back (instream_.substr (pos_start, pos_end));

	if      (tokens_.size () == 1) { DL ((GRAPP,"Case #0 (1 token)\n"));  }
	else if (tokens_.size () == 2) { DL ((GRAPP,"Case #1 (2 tokens)\n")); }
	else if (tokens_.size () == 3) { DL ((GRAPP,"Case #2 (3 tokens)\n")); }

	for (u_int i = 0; i < tokens_.size (); i++) {
		DL ((GRAPP,"token [%d] = [%s]\n", i, tokens_[i].c_str ()));
	}

	return (tokens_.size () - 1);
}

void
Deck::
add_new_card (Glib::ustring& q_, 
			  Glib::ustring& a_, 
			  Glib::ustring& e_,
			  long& card_id_,
			  int& ok_count_)
{
	Card* card = new Card (*this);
	card->set_id (card_id_++);

	/** Trim and replace escape characters as necessary 
	 */
	trim_escaped_quotes (q_);
	trim_escaped_quotes (a_);
	trim_escaped_quotes (e_);

	card->set_question (q_);
	card->set_answer   (a_);
	card->set_example  (e_);

	m_cards.push_back (card);
	ok_count_++;
	DL ((GRAPP,"*** record accepted\n"));

	q_ = a_ = e_ = "";			// cleanup
}

void
Deck::
trim_escaped_quotes (Glib::ustring& s_)
{
    static const Glib::ustring escape_char  ("\"\"");
    static const Glib::ustring escape_char2 ("\\\"");

    Glib::ustring::size_type idx;
    size_t last;

    if (s_[0] == '"') {
        s_.replace (0, 1, "");
    }
    last = s_.size ()-1;

    if (s_[last] == '"') {
        s_.replace (last, 1, "");
    }

    while ((idx = s_.find (escape_char)) != Glib::ustring::npos) {
        s_.replace (idx, 1, "");
    }

    while ((idx = s_.find (escape_char2)) != Glib::ustring::npos) {
        s_.replace (idx, 1, "");
    }

}

void
Deck::
change_state (ParserState& state_)
{
	ParserState old_state = state_;

	switch (old_state) {
	case ST_BEGIN:     state_ = ST_1ST_FIELD; break;
	case ST_1ST_FIELD: state_ = ST_2ND_FIELD; break;
	case ST_2ND_FIELD: state_ = ST_3RD_FIELD; break;
	case ST_3RD_FIELD: state_ = ST_BEGIN;     break;
	}
	DL ((GRAPP,"state change %s -> %s\n",
		 get_state_name (old_state),
		 get_state_name (state_)));
}

void
Deck::
reset_state (ParserState& state_)
{
	DL ((GRAPP,"state change %s -> ST_BEGIN\n",get_state_name (state_)));
	state_ = ST_BEGIN;
}

/*------------------------------------------------------------------------------
 * export_to_csv()
 *------------------------------------------------------------------------------
 */
bool
Deck::
export_to_csv (const string& file_)
{
	trace_with_mask("Deck::export_to_csv",GUITRACE|DECK);

	std::ofstream sink;
	int ok_count = 0;      

	CSVExportDialog expd;
	if (expd.run () != Gtk::RESPONSE_APPLY) {
		return false;
	}
	expd.hide ();

	sink.open (file_.c_str (), std::ios::out);

	if (!sink.good ()) {
		Gtk::MessageDialog emsg (
			_("Failed to open export file.\nCheck file permissions."),
			Gtk::MESSAGE_ERROR);
        emsg.run ();
		return false;
    }

	Glib::ustring sep (expd.get_separator ());
	gunichar eol = '\n';
	gunichar dbq = '\"';

	Glib::ustring field;
	VDeck::cardlist_const_iterator i = m_cards.begin ();
	
	while (i != m_cards.end ()) 
	{
		/** question
		 */
		field = (*i)->get_question ();
		if (field.find (sep) != Glib::ustring::npos ||
			field.find (eol) != Glib::ustring::npos ||
			field.find (dbq) != Glib::ustring::npos)
		{
			escape_double_quotes (field);
			sink << "\"" << field.raw () << "\"";
		}
		else {
			sink << field.raw ();
		}
		sink << sep;

		/** answer
		 */
		field = (*i)->get_answer ();
		if (field.find (sep) != Glib::ustring::npos ||
			field.find (eol) != Glib::ustring::npos ||
			field.find (dbq) != Glib::ustring::npos)
		{
			escape_double_quotes (field);
			sink << "\"" << field.raw () << "\"";
		}
		else {
			sink << field.raw ();
		}

		/** example
		 */
		if (expd.with_example ()) 
		{
			sink << sep;
			field = (*i)->get_example ();
			if (field.find (sep) != Glib::ustring::npos ||
				field.find (eol) != Glib::ustring::npos ||
				field.find (dbq) != Glib::ustring::npos)
			{
				escape_double_quotes (field);
				sink << "\"" << field.raw () << "\"";
			}
			else {
				sink << field.raw ();
			}
		}
		sink << std::endl;
		i++;
		ok_count++;
	}
	sink.close ();

	char msg [64];
	sprintf (msg, "Saved %d CSV records", ok_count);
	Gtk::MessageDialog im (msg, Gtk::MESSAGE_INFO);
	im.run ();

	return true;
}

void
Deck::
escape_double_quotes (Glib::ustring& input_)
{
	Glib::ustring output;
	u_int i;

	for (i = 0; i < input_.size (); i++) {
		if (input_[i] == '"') {
			output.append ("\\");
			output.append ("\"");
		}
		else {
			output.append (1, input_[i]);
		}
	}
	input_ = output;
}

/*------------------------------------------------------------------------------
 * erase()
 *------------------------------------------------------------------------------
 */
bool
Deck::
erase (VCard* vcard_)
{
	trace_with_mask("Deck::erase",GUITRACE|DECK);

	cardlist_iterator pos;
	Assure_exit (vcard_);

	pos = find (m_cards.begin (), m_cards.end (), vcard_);
	if (pos != m_cards.end ()) {
		m_cards.erase (pos);
		m_status = MODIFIED_DECK;
		dump_deck_status ();
		return true;
	}
	DL((GRAPP, "Card (%d) cannot be deleted (not found)\n", vcard_->get_id ()));
	return false;
}

int
Deck::
save_as (const string& fname_)
{
	trace_with_mask("Deck::save_as",GUITRACE|DECK);
	string fname_ext = fname_;

	/** If extension is missing, add it.
	 */
	string ext (DECK_FILE_EXT);
    if (fname_ext.find (ext) == string::npos) {
		fname_ext += ext;
    }
	if (Glib::file_test (fname_ext, Glib::FILE_TEST_IS_REGULAR)) {
		string msg = "File \n" + fname_ext
			+ "\nalready exists.\nWould you like to delete it first?";
		Gtk::MessageDialog qm (msg, false,
							   Gtk::MESSAGE_QUESTION, Gtk::BUTTONS_NONE,
							   true);
		qm.add_button (Gtk::Stock::NO,  Gtk::RESPONSE_NO);
		qm.add_button (Gtk::Stock::YES, Gtk::RESPONSE_YES);
		if (qm.run () == Gtk::RESPONSE_NO) {
			return -1;
		}
		::unlink (fname_ext.c_str ());
	}

	if (needs_renaming ()) {
		m_fname = fname_ext;
		save ();
		CONFIG->set_flipside_history (m_fname, FRONT);
	}
	else {
		string tmp = m_fname;
		m_fname = fname_ext;
		save ();
		m_fname = tmp;

	}
	m_deck_path = GRANULE->get_dirname (m_fname.c_str ());

	return 0;
}

int
Deck::
save ()
{
	trace_with_mask("Deck::save",GUITRACE|DECK);

	static const char dtd_url [] = "http://granule.sourceforge.net/granule.dtd";

	int ret = 0;
	xmlTextWriterPtr writer;
	Glib::ustring tmp;

	DL ((GRAPP,"Saving Deck as \"%s\"\n", m_fname.c_str ()));

	g_remove (m_fname.c_str ());

	writer = xmlNewTextWriterFilename (m_fname.c_str (), 0);
	if (writer == NULL) {
		DL ((GRAPP,"Failed to save Deck \"%s\"\n", m_fname.c_str ()));
		return -1;
	}

	ret = xmlTextWriterStartDocument (writer, NULL, "UTF-8", NULL);
	if (ret < 0) {
		xmlFreeTextWriter (writer);
		return -1;
	}

	/** 
	 * We have to disable indent prior to StartDTD call and 
	 * enable it back again before EndDTD call to generate expected
	 * indentation of XML file. Oddly enough, xmlTextWriterWriteDTD()
	 * doesn't do the right thing itself.
	 */
	xmlTextWriterSetIndent (writer, 0);
	xmlTextWriterStartDTD  (writer, BAD_CAST "deck", NULL, BAD_CAST dtd_url);
	xmlTextWriterSetIndent (writer, 1);
	xmlTextWriterEndDTD    (writer);

	xmlTextWriterSetIndentString (writer, (xmlChar*) "  ");

	ret = xmlTextWriterStartElement (writer, BAD_CAST "deck");
	ret = xmlTextWriterWriteElement (writer, BAD_CAST "author", 
									 BAD_CAST m_author.c_str ());
	ret = xmlTextWriterWriteElement (writer, BAD_CAST "description", 
									 BAD_CAST m_desc.c_str ());

	/** If Sound Path is relative, then store only relative
	 *  portion.
	 */
	ret = xmlTextWriterStartElement (writer, BAD_CAST "sound_path");

	if (m_snd_path_mode == PATH_RELATIVE) 
	{
		ret = xmlTextWriterWriteAttribute (writer, 
										   BAD_CAST "relative", BAD_CAST "yes");
	}
	else {
		ret = xmlTextWriterWriteAttribute (writer, 
										   BAD_CAST "relative", BAD_CAST "no");

	}

	if (!m_snd_path.empty ()) {
		ret = xmlTextWriterWriteString (writer, 
					 BAD_CAST Granule::locale_to_utf8 (m_snd_path).c_str ());
	}

	ret = xmlTextWriterEndElement (writer);	// @sound_path

	/** Record (optional) pics path relativeness.
	 */
	ret = xmlTextWriterStartElement (writer, BAD_CAST "pics_path");
	ret = xmlTextWriterWriteAttribute (writer, 
									   BAD_CAST "relative", 
									   pics_path_mode () == PATH_RELATIVE ?
									   BAD_CAST "yes" : BAD_CAST "no");
	ret = xmlTextWriterEndElement (writer);	// @pics_path

    /** Save Appearance settings (text fonts and alignments).
	 */
	ret = xmlTextWriterStartElement (writer, BAD_CAST "appearance");

	if (with_custom_appearance ()) 
	{
		ret = xmlTextWriterWriteAttribute (writer, 
										   BAD_CAST "enabled", BAD_CAST "yes");
		m_fonts_db.save_to_xml_config (writer);
		m_text_colors_db.save_to_xml_config (writer);
		m_appearance.save_to_xml_config (writer);
	}
	else {
		ret = xmlTextWriterWriteAttribute (writer, 
										   BAD_CAST "enabled", BAD_CAST "no");
	}

	ret = xmlTextWriterEndElement (writer);	// @appearance

	/** Save all cards
	 */
	cardlist_const_iterator citer = m_cards.begin ();

	while (citer != m_cards.end ()) 
	{
		/**---------------------------------------------------------------------
		 * @start card
		 */
		ret = xmlTextWriterStartElement (writer, BAD_CAST "card");

		/**---------------------------------------------------------------------
		 */
		ret = xmlTextWriterWriteAttribute (writer, BAD_CAST "id",
							   BAD_CAST (*citer)->get_id_str ().c_str ());
		/**---------------------------------------------------------------------
		 * @start front
		 */
		ret = xmlTextWriterStartElement (writer, BAD_CAST "front");	

		tmp = (*citer)->get_alt_spelling (ASF);
		if (tmp.length () > 0) {
			ret = xmlTextWriterWriteAttribute (writer, BAD_CAST "alt_spelling",
											   BAD_CAST tmp.c_str ());
		}
		
		/** If path is nonempty I save the path.
		 *  The pics path mode dictates how I should record it -
		 *  either relative to the Deck path or absolute. If path is
		 *  out of compliance, convert it on a fly - the end result
		 *  should be uniform across the board.
		 */
		tmp = (*citer)->get_image_path ();

		if (tmp.length () > 0) 
		{
			string path = Granule::filename_from_utf8 (tmp);
			if (pics_path_mode () == PATH_RELATIVE) 
			{
				string relpath;				
				if (g_path_is_absolute (path.c_str ()))
				{
					relpath = Granule::calculate_relative_path (
						m_deck_path, path);
					ret = xmlTextWriterWriteAttribute (
						writer, BAD_CAST "image", BAD_CAST relpath.c_str ());
				}
				else {
					ret = xmlTextWriterWriteAttribute (
						writer, BAD_CAST "image", BAD_CAST tmp.c_str ());
				}
			}
			else {	/* PATH_ABSOLUTE */
				string abspath;
				if (!g_path_is_absolute (path.c_str ()))
				{
					abspath = Granule::calculate_absolute_path (
						m_deck_path, path);
					ret = xmlTextWriterWriteAttribute (
						writer, BAD_CAST "image", BAD_CAST abspath.c_str ());
				}
				else {
					ret = xmlTextWriterWriteAttribute (
						writer, BAD_CAST "image", BAD_CAST tmp.c_str ());
				}		   
			}
		}

		ret = xmlTextWriterWriteString (writer,
								 BAD_CAST (*citer)->get_question ().c_str ());

		ret = xmlTextWriterEndElement (writer);
		
		/* @end front
		**----------------------------------------------------------------------
		** @start back
		*/
		ret = xmlTextWriterStartElement (writer, BAD_CAST "back"); 

		tmp = (*citer)->get_alt_spelling (ASB);
		if (tmp.length () > 0) {
			ret = xmlTextWriterWriteAttribute (writer, BAD_CAST "alt_spelling",
											   BAD_CAST tmp.c_str ());
		}
		ret = xmlTextWriterWriteString (writer,
								 BAD_CAST (*citer)->get_answer ().c_str ());
		ret = xmlTextWriterEndElement (writer);

		/* @end back
		**----------------------------------------------------------------------
		** @start back_example
		*/
		ret = xmlTextWriterStartElement (writer, BAD_CAST "back_example"); 

		/** If path is nonempty I save the path.
		 *  The pics path mode dictates how I should record it -
		 *  either relative to the Deck path or absolute. If path is
		 *  out of compliance, convert it on a fly - the end result
		 *  should be uniform across the board.
		 */
		tmp = (*citer)->get_example_image_path ();

		if (tmp.length () > 0) 
		{
			string path = Granule::filename_from_utf8 (tmp);
			if (pics_path_mode () == PATH_RELATIVE) 
			{
				string relpath;				
				if (g_path_is_absolute (path.c_str ()))
				{
					relpath = Granule::calculate_relative_path (
						m_deck_path, path);
					ret = xmlTextWriterWriteAttribute (
						writer, BAD_CAST "image", BAD_CAST relpath.c_str ());
				}
				else {
					ret = xmlTextWriterWriteAttribute (
						writer, BAD_CAST "image", BAD_CAST tmp.c_str ());
				}
			}
			else {	/* PATH_ABSOLUTE */
				string abspath;
				if (!g_path_is_absolute (path.c_str ()))
				{
					abspath = Granule::calculate_absolute_path (
						m_deck_path, path);
					ret = xmlTextWriterWriteAttribute (
						writer, BAD_CAST "image", BAD_CAST abspath.c_str ());
				}
				else {
					ret = xmlTextWriterWriteAttribute (
						writer, BAD_CAST "image", BAD_CAST tmp.c_str ());
				}		   
			}
		}

		ret = xmlTextWriterWriteString (writer,
								 BAD_CAST (*citer)->get_example ().c_str ());
		ret = xmlTextWriterEndElement (writer);

		/* @end back
		**----------------------------------------------------------------------
		*/

		ret = xmlTextWriterEndElement (writer);	// @end card
		/* 
		** @end card
		**----------------------------------------------------------------------
		*/
		citer++;
	}

	/** Close <deck> and write out the file
	 */
	xmlTextWriterEndElement (writer); // @end deck
	xmlTextWriterEndDocument (writer);
	xmlTextWriterFlush (writer);
	xmlFreeTextWriter (writer);	      // clenup

	m_status = CLEAN_DECK;
	m_needs_renaming = false;

	/** Clear up all cards
	 */
	cardlist_iterator iter = m_cards.begin ();
	while (iter != m_cards.end ()) {
		(*iter)->mark_clean ();
		iter++;
	}

	dump_deck_status ();
	return (ret);
}

void
Deck::
get_progress (float& percent_done_, std::string& msg_, int idx_)
{
	trace_with_mask("Deck::get_progress",GUITRACE|DECK);

	std::ostringstream os;

	os << (idx_+1) << "/" << m_cards.size ();
	msg_ = os.str ();

	if (m_cards.size () <= 1) {
		percent_done_ = 0;
	}
	else {
		percent_done_ = (idx_ * 1.0) / (m_cards.size () - 1);
	}
}

VCard* 
Deck::
find_card_by_id (long id_)
{
	cardlist_iterator iter = m_cards.begin ();
	while (iter != m_cards.end ()) {
		if ((*iter)->get_id () == id_) {
			return (*iter);
		}
		iter++;
	}
	return (NULL);
}

/** 
 *	Deck information consists of two parts: the header (More... dialog)
 *  and a list of cards. Either could have been modified by the user.
 *  In the first case, the status would be MODIFIED_DECK; in the second,
 *  however, the status would be CLEAN_DECK. We double check here to 
 *  see if any of the cards have been edited (if new card were added 
 *  or deleted, the status would be MODIFIED_DECK.
 */
DeckEditStatus
Deck::
get_status ()
{	
	if (m_status == CLEAN_DECK) {
		dump_deck_status ();
		cardlist_const_iterator citer = m_cards.begin ();
		while (citer != m_cards.end ()) {
			if ((*citer)->is_dirty ()) {
				m_status = MODIFIED_DECK;
				break;
			}
			citer++;
		}
	}
	dump_deck_status ();
	return m_status;
}

void
Deck::
dump () const 
{
	DL((DECK, "fname = \"%d\"\n", m_fname.c_str ()));
	DL((DECK, "author = \"%s\"\n", m_author.c_str ()));
	DL((DECK, "description = \"%s\"\n", m_desc.c_str ()));
}

void
Deck::
dump_deck_status () const
{
	static const char* names [] = { "NEW_DECK", "INVALID_DECK", 
								   "MODIFIED_DECK", "CLEAN_DECK" };

	DL((APP, "Deck \"%s\"\n", get_name ().c_str ()));
	DL((APP, "Status = <%s>\n", names [m_status]));
}

void
Deck::
swap_with_next (VCard* vcard_)
{
	cardlist_iterator p;
	cardlist_iterator n;
	p = m_cards.begin ();
	while (p != m_cards.end ()) {
		if (vcard_ == *p) {
			n = p+1;
			VCard* next_card = *n;
			m_cards.erase (n);
			m_cards.insert (p, next_card);
			m_status = MODIFIED_DECK;
			break;
		}
		p++;
	}
}

void
Deck::
swap_with_prev (VCard* vcard_)
{
	cardlist_iterator p;
	cardlist_iterator n;
	p = m_cards.begin ();
	while (p != m_cards.end ()) {
		if (vcard_ == *p) {
			n = p-1;
			VCard* prev_card = *p;
			m_cards.erase (p);
			m_cards.insert (n, prev_card);
			m_status = MODIFIED_DECK;
			break;
		}
		p++;
	}
}

/** Called by the DeckInfo
 */
void
Deck::
set_snd_path (const string& spath_)
{
	trace_with_mask("Deck::set_snd_path",GUITRACE|DECK);

	m_snd_path = Granule::locale_from_utf8 (spath_.c_str ());

	DL ((APP,"Testing new snd_path \"%s\"\n", m_snd_path.c_str ()));

	if (m_snd_path.length () > 0) 
	{
		if (m_snd_path_mode == PATH_RELATIVE) {
			m_active_snd_path = m_deck_path + ASSA_DIR_SEPARATOR_S + m_snd_path;
		}
		else {
			m_active_snd_path = m_snd_path;			
		}
	}
	else {
		m_active_snd_path = 
			Granule::filename_to_utf8 (CONFIG->get_snd_archive_path ());
	}

	test_active_snd_path ();

	DL ((APP,"Updated active_snd_path \"%s\"\n", m_active_snd_path.c_str ()));
}

/** An empty sound path in the Deck file indicates that 
 *  the Sound Settings from the application Preferences should 
 *  be used instead.
 */
void
Deck::
test_active_snd_path ()
{
	trace_with_mask("Deck::test_active_snd_path",GUITRACE|DECK);

	if (m_active_snd_path.size () > 0) 
	{
		struct stat st;
		if (::stat (m_active_snd_path.c_str (), &st) != 0 || 
			!S_ISDIR (st.st_mode)) 
		{
			DL((APP,"Running with sound disabled.\n"));
			DL((APP,"Path \"%s\" is invalid\n", m_active_snd_path.c_str ())); 
			m_snd_enabled = false;
			return;
		}
	}

	DL((APP,"Running with sound enabled.\n"));
	DL((APP,"sound_path=\"%s\"\n", m_active_snd_path.c_str ()));
	m_snd_enabled = true;
}

/** Rules for playable name are as follows:
 *
 *  <ol>
 *   <li> A word can be preceeded by "to " to indicate that it is a verb. </li>
 *
 *   <li> All multiword expressions are ignored. </li>
 *
 *	 <li> The WAV file is assumed to be stored in the directory
 *     with the name of its first letter, i.e. "granule" will be
 *     stored in the directory "/path/to/dictionary/g/", and thus 
 *     the full name would be "/path/to/dictionary/g/granule.wav".
 *   </li>
 *
 *	@return Playable name on success; an empty string on error.
 */
void
Deck::
play_card (SideSelection side_, Deck::cardlist_iterator& iter_)
{
	trace_with_mask("Deck::play_card",GUITRACE|DECK);

	const char* cmd = NULL;
	static char execmdbuf [PATH_MAX];

	std::vector<string> v;

	string sound_fname;
	string cmd_args;
	string file_ext;
	string w;

	if (!m_snd_enabled) {
		DL ((GRAPP,"Sound is disabled!\n"));
		return;
	}

	if (Deck::s_pipe_stream != NULL) {
		DL ((GRAPP, "Audio playback is already in progress!\n"));
		return;
	}

	DL ((GRAPP,"Playing card audio for %s side.\n", 
		 (side_ == FRONT ? "Front" : "Back")));

	w = Granule::locale_from_utf8 (
		(*iter_)->get_alt_spelling (side_ == FRONT ? ASF : ASB));

	if (w.length () == 0) 
	{
		w = Granule::locale_from_utf8 ((*iter_)->get_question ());
		if (w.length () == 0) {
			DL ((GRAPP,"Conversion for FRONT failed (say, UTF-8 to ASCII).\n"));
			return;
		}

		ASSA::Utils::split (w.c_str (), v);
		if (v.size () > 1) 
		{
			if (v[0] != "to") {
				DL ((GRAPP,"Hmm, v[0] != \"to\"!\n"));
				return;
			}
			w = v[1];
		}
	}
	
	cmd = CONFIG->get_snd_player_cmd ();

	sound_fname  = m_active_snd_path + G_DIR_SEPARATOR_S;
	sound_fname += w[0];
	sound_fname += G_DIR_SEPARATOR_S + w; 
	
	cmd_args = CONFIG->get_snd_player_args ();

/** @todo
 *   This would not work any more because the type of the file 
 *   (and thus the whole name) is specified in the command line argument.
 *   But, without this check, what follows might cause a core dump.
 *   So, you need to fix it somehow!
 *
 *	struct stat st;
 *	if (::stat (sound_fname.c_str (), &st) != 0 || !S_ISREG (st.st_mode)) {
 *		DL((APP,"Can't stat sound file \"%s\"\n", sound_fname.c_str ()));
 *		return;
 *	}
 */
	DL((APP,"snd filename=\"%s\"\n", sound_fname.c_str ()));

	/** Arguments composition common to all.
	 */
	snprintf (execmdbuf, PATH_MAX, cmd_args.c_str (), sound_fname.c_str ());

    /**======================================================================**
	 * Win32
	 * -----
	 * If sound player command is an empty string, then use
	 * build-in system PlaySound() 
	 *   (FIXME: test it if it really works. Looks like you still need
	 *           the command arguments format to add the sound extension)
	 *      
	 * Otherwise, use the external command, which happens by default to be
	 * granule-audio-player which plays all popular formats regardless.
	 *
	 **======================================================================**/
#if defined (IS_WIN32)
	int n; 
 	if (cmd != NULL && strlen (cmd) != 0) 
 	{
		n = snprintf (execmdbuf, PATH_MAX, "\"%s\" ", cmd);

		/** Surrond command argument by double-quotes - otherwise
		 *  win32 would garble file path.
		 */
		sound_fname = "\"" + sound_fname + "\"";
		snprintf (execmdbuf+n, PATH_MAX-n, 
				  cmd_args.c_str(), sound_fname.c_str());

		DL((GRAPP,"WIN32: system(\"%s\")\n", execmdbuf));
		g_spawn_command_line_async (execmdbuf, NULL);
	}
	else {
		DL((GRAPP,"WIN32: PlaySound(\"%s\")\n", execmdbuf));
		PlaySound (execmdbuf, NULL, SND_FILENAME| SND_ASYNC);
	}

    /**======================================================================**
	 * Hildon
	 * ------
	 * We use DPS chip with no impact on main CPU to play mp3 files 
	 * with GStreamer command-line tool (you need to install 
	 *
	 *   gst-launch filesrc location=/path/to/your.mp3 ! dspmp3sink
	 *
	 * 
	 * The expected output from gst-launch is:
	 *
	 *   Setting pipeline to PAUSED ...
	 *   Pipeline is PRELOADED ...
	 *   Setting pipeline to PLAYING ...
	 *   New clock: DSPClock
	 *   Got EOS from element "pipeline0".
	 *   Execution ended after 56467864151 ns.
	 *   Setting pipeline to PAUSED ...
	 *   Setting pipeline to READY ...
	 *   Setting pipeline to NULL ...
	 *   FREEING pipeline ...
	 *
	 **======================================================================**
	 * NOTE: New Gtk 2.4 FileChooserDialog changes disposition mask for SIGCHLD.
	 *       We have to preserve it - otherwise, we would crash and burn.
	 *       COLLECT_STATUS option does that for us here.
	 **======================================================================**/
// #elif defined (IS_HILDON)
// 	if (cmd != NULL && strlen (cmd) != 0) 
// 	{
//  		DL((APP,"HILDON: fork_exec(\"%s %s\")\n", cmd, execmdbuf));
//  		n = ASSA::Fork::fork_exec (cmd, execmdbuf, ASSA::Fork::COLLECT_STATUS);
//  		DL((GRAPP,"%s on playing audio clip.\n",
// 			(n == 0 ? "Success" : (n==1 ? "Failed" : "Failed with wait(2)"))));
// 	}

	/**======================================================================**
	 * POSIX (IS_DESKTOP)                                                     *
	 * ------------------                                                     *
	 * For now, we squarely rely on external command.                         *
	 *                                                                        *
	 * @todo For the next release,  evaluate GStreamer framework.             *
	 *                                                                        *
	 **======================================================================**/
#else
	if (cmd != NULL && strlen (cmd) != 0) 
	{
		if (! GRANULE->async_adapter_enabled ()) {
			GRANULE->set_async_events_adapter ();
		}

		w = cmd + string (" ") + execmdbuf;
		ASSA::AutoPtr<ASSA::Pipe> pipe (new ASSA::Pipe);

		DL ((GRAPP,"POSIX: exec pipe(\"%s\")\n", w.c_str ()));

		Deck::s_pipe_stream = pipe->open (w.c_str (), "r");
		if (Deck::s_pipe_stream == NULL) {
			DL ((GRAPP,"POSIX: Pipe::open(\"%s\") failed!\n", w.c_str ()));
		}
		else {
			int val;
			int fd = pipe->fd ();
			val = ::fcntl (fd, F_GETFL, 0);
			val |= O_NONBLOCK;
			::fcntl (fd, F_SETFL, val);
			REACTOR->registerIOHandler (this, fd, ASSA::READ_EVENT);
			m_audio_playback_pipe = pipe.release ();
			
		}
	}
#endif
}

/** Drain the pipe stream.
 */
int
Deck::
handle_read (int fd_)
{
	trace_with_mask("Deck::handle_read",GUITRACE);

	int ret = 0;
	static char buf [1024];

	for (;;) {
		if ((ret = ::read (fd_, buf, 1024)) < 0) {
			if (errno == EAGAIN) {
				break;
			}
			else {
				DL ((GRAPP,"read() error from audio playback pipe : %s\n",
					 strerror (errno)));
				return -1;
			}
		}
		
		else if (ret > 0) { 
			buf [ret] = '\0';
			DL ((GRAPP,"GStreamer: %s\n", buf));
		}
		else { /* == 0 end-of-stream */
			return -1;
		}
	}
	
	return 0;
}

int
Deck::
handle_close (int fd_)
{
	trace_with_mask("Deck::handle_close",GUITRACE);

	if (m_audio_playback_pipe != NULL) {
		delete m_audio_playback_pipe;
		m_audio_playback_pipe = NULL;
		Deck::s_pipe_stream = NULL;
	}

	return 0;
}

//------------------------------------------------------------------------------
// LEFTOVERS FROM HEAVY DEBUGGING OF THE PAST
//------------------------------------------------------------------------------
#if 0
void
Deck::
traverse_xml_tree (const xmlpp::Node* node_)
{
	trace_with_mask("Deck::traverse_xml_tree",GUITRACE);

	string nodename;
	if (node_) {
		nodename = node_->get_name ();
	}
	const xmlpp::TextNode* node_text;
	node_text = dynamic_cast<const xmlpp::TextNode*> (node_);

	/** Ignore indentation if XML file is indented.
		I wonder how it would work with UTF-8.
	 */
	if (node_text && node_text->is_white_space ()) {
		return;
	}

	DL((DECK,"%sPath = \"%s\"\n", 
		m_indent.c_str(), node_->get_path ().c_str ()));
	
	/** We cast node pointer to all possible types and then 
	 *  determine which one we are dealing with by elimination.
	 */
	const xmlpp::ContentNode* node_content;
	node_content = dynamic_cast<const xmlpp::ContentNode*> (node_);

	const xmlpp::CommentNode* node_comment;
	node_comment = dynamic_cast<const xmlpp::CommentNode*> (node_);

	const xmlpp::Element* node_element;
	node_element = dynamic_cast<const xmlpp::Element*> (node_);

	/** If either Content or Element, indent and print out its name.
	 */
	if (!node_text && !node_comment && !nodename.empty ()) {
		DL((DECK,"%sNode name = %s\n", 
			m_indent.c_str(), nodename.c_str ()));
	}

	/** Find the right type
	 */
	if (node_text) {
		DL((DECK,"%sType = TEXT_NODE\n", m_indent.c_str()));
		DL((DECK,"%stext = \"%s\"\n", m_indent.c_str(), 
			node_text->get_content ().c_str ()));

		if (m_element_node_name == "front_encoding") {
			m_front_encoding = node_text->get_content ();
		}
		else if (m_element_node_name == "back_encoding") {
			m_back_encoding = node_text->get_content ();
		}
		else if (m_element_node_name == "card") {
			m_current_card = new Card;
			m_cards.push_back (m_current_card);
		}
		else if (m_element_node_name == "front") {
			m_current_card->set_question (node_text->get_content ());
		}
		else if (m_element_node_name == "back") {
			m_current_card->set_answer (node_text->get_content ());
		}
		else if (m_element_node_name == "back_example") {
			m_current_card->set_example (node_text->get_content ());
		}
		else if (m_element_node_name == "") {
			= node_text->get_content ();
		}

	}
	else if (node_comment) {
		DL((DECK,"%sType = COMMENT_NODE\n", m_indent.c_str()));
		DL((DECK,"%scomment = \"%s\"\n", m_indent.c_str(), 
			node_comment->get_content ().c_str ()));
	}
	else if (node_content) {
		DL((DECK,"%sType = CONTENT_NODE\n", m_indent.c_str()));
		DL((DECK,"%scontent = \"%s\"\n", m_indent.c_str(), 
			node_content->get_content ().c_str ()));
	}
	else if (node_element) {
		DL((DECK,"%sType = ELEMENT_NODE\n", m_indent.c_str()));
		DL((DECK,"%sline = \"%d\"\n", m_indent.c_str(), 
			node_element->get_line ()));     // line() is for Elements

		/** Print attributes of the element
		 */
		const xmlpp::Element::AttributeList& 
			attributes = node_element->get_attributes ();
		const xmlpp::Attribute* attr;
		xmlpp::Element::AttributeList::const_iterator iter = attributes.begin();
		while (iter != attributes.end ()) {
			attr = *iter;
			DL((DECK,"%sType = ATTRIBUTE_NODE\n", m_indent.c_str()));
			DL((DECK,"%sAttribute: %s = \"%s\"\n", m_indent.c_str(),
				attr->get_name ().c_str (), attr->get_value ().c_str ()));
			iter++;
		}
		attr = node_element->get_attribute ("id");
		if (attr) {
			DL((DECK,"%sID found = %s\n", m_indent.c_str(), 
				attr->get_value().c_str()));
			if (m_element_node_name == "card") {
				std::string value = attr->get_value ();
				m_current_card->set_id (atol (value.substr (1, value.size ())));
				m_element_node_name = "":
			}

		}
	}

	/** If not Content, recurse through the children
	 */
	m_element_node_name = "";
	if (!node_content) {
		xmlpp::Node::NodeList list = node_->get_children ();
		xmlpp::Node::NodeList::iterator iter = list.begin ();
		while (iter != list.end ()) {
			m_indent += "  ";
			traverse_xml_tree (*iter);
			iter++;
		}
	}
	m_indent = "";
}
#endif
