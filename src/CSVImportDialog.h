// -*- c++ -*-
//------------------------------------------------------------------------------
//                            CSVImportDialog.h
//------------------------------------------------------------------------------
// $Id: CSVImportDialog.h,v 1.2 2006/09/21 01:25:20 vlg Exp $
//------------------------------------------------------------------------------
//  Copyright (c) 2005 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version
//  2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------
// 05/14/2005 VLG  Created
//------------------------------------------------------------------------------
#ifndef CSV_IMPORT_DIALOG_H
#define CSV_IMPORT_DIALOG_H

#include <gtkmm/dialog.h>
#include <gtkmm/button.h>
#include <gtkmm/checkbutton.h>

#include "Granule-main.h"

class CSVImportDialog : public Gtk::Dialog
{  
public:
	CSVImportDialog();

	/// Do we break on LF or CR+LF?
	bool is_windoz_linebreak () const { 
		return m_windoz_crlf->get_active ();
	}

	/// Return separator selected
	const char* get_separator () const;

	/// Return double separator
	const char* get_double_separator () const;

	/** If true, then replace each pair of adjacent separators with one.
	 */
	bool consume_adjacent_separators () const { 
		return m_shrink_seps->get_active (); 
	}

	/** callbacks
	 */
	void on_unix_lf_checked     ();
	void on_windoz_crlf_checked ();

	void on_space_sep_checked  ();
	void on_tab_sep_checked    ();
	void on_colon_sep_checked  ();
	void on_semicolon_sep_checked  ();
	void on_comma_sep_checked  ();

private:
	Gtk::Button*      m_cancel_button;
	Gtk::Button*      m_import_button;

	Gtk::CheckButton* m_unix_lf;
	Gtk::CheckButton* m_windoz_crlf;

	Gtk::CheckButton* m_space_sep;
	Gtk::CheckButton* m_tab_sep;
	Gtk::CheckButton* m_colon_sep;
	Gtk::CheckButton* m_semicolon_sep;
	Gtk::CheckButton* m_comma_sep;
	Gtk::CheckButton* m_shrink_seps;

	CSVSeparator m_sep_selection;
	bool         m_recursive_call;
};

#endif
