// -*- c++ -*-
//------------------------------------------------------------------------------
//                            InputDialog.h
//------------------------------------------------------------------------------
// $Id: InputDialog.h,v 1.1 2007/12/02 00:31:24 vlg Exp $
//------------------------------------------------------------------------------
//  Copyright (c) 2006 by Vladislav Grinchenko 
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date   : Mon Dec 25 15:53:51 EST 2006
//
//------------------------------------------------------------------------------
#ifndef INPUT_DIALOG_H
#define INPUT_DIALOG_H

#include <gtkmm/messagedialog.h>
#include <gtkmm/entry.h>


/* @class InputDialog

   A standard question dialog and two buttons, 'OK' and 'Cancel',
   with additional input entry field positioned below the the message.

   The run() method returns Gtk::RESPONSE_OK if 'OK' button is pressed
   and Gtk::RESPONSE_CANCEL if 'Cancel' button is pressed.

   @param message_ - Message to display
   @param value_   - Initial value of the entry widget.
*/
class InputDialog : public Gtk::MessageDialog
{
public:
	InputDialog (const Glib::ustring& message_, const Glib::ustring& value_);

	Glib::ustring get_entry_value () const;

private:
	Gtk::Entry* m_local_entry;
};

inline
Glib::ustring 
InputDialog::
get_entry_value () const 
{ 
	if (m_local_entry != NULL) {
		return (m_local_entry->get_text ());
	}
}

#endif /* INPUT_DIALOG */
