// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: DialogWithCheckbutton.h,v 1.1 2007/12/02 00:31:24 vlg Exp $
//------------------------------------------------------------------------------
//                            DialogWithCheckbutton.h
//------------------------------------------------------------------------------
//  Copyright (c) 2006 by Vladislav Grinchenko 
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date   : Sat Aug 26 19:20:12 EDT 2006
//
//------------------------------------------------------------------------------
#ifndef DIALOG_WITH_CHECKBUTTON_H
#define DIALOG_WITH_CHECKBUTTON_H

#include <gtkmm/checkbutton.h>
#include <gtkmm/messagedialog.h>

/* @class DialogWithCheckbutton

   A standard question dialog and two buttons, 'Yes' and 'No',
   with additional checkbutton framed with the text.
*/
class DialogWithCheckbutton : public Gtk::MessageDialog
{
public:
	DialogWithCheckbutton (const Glib::ustring& message_, 
							 const Glib::ustring& checkbutton_message_, 
							 bool activate_checkbutton_ = false,
							 bool use_markup_ = false);

	bool get_checkbutton_status () const { return m_checkbutton->get_active ();}

private:
	Gtk::CheckButton* m_checkbutton;
};

#endif /* DIALOG_WITH_CHECKBUTTON_H */
