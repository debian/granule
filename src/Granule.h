// -*- c++ -*-
// Generated by assa-genesis
//------------------------------------------------------------------------------
// $Id: Granule.h,v 1.36 2008/08/16 03:13:09 vlg Exp $
//------------------------------------------------------------------------------
//                            Granule.h
//------------------------------------------------------------------------------
//  Copyright (c) 2004,2005 by Vladislav Grinchenko 
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date   : Wed Dec 31 23:18:34 2003
//
//------------------------------------------------------------------------------
#ifndef Granule_H
#define Granule_H

#include <assa/GenServer.h>
#include <assa/Singleton.h>
#include <assa/TimeVal.h>
#include <assa/Assure.h>

#include <libxml/parser.h>
#include <libxml/xmlIO.h>

#include <string>
using std::string;
#include <stack>

#include <gtk/gtkcontainer.h>
#include <gtkmm.h>

#ifdef IS_HILDON
#  include <hildonmm.h>
#endif

#include "DeckManager.h"

#ifdef HAVE_CONFIG_H
#    include "config.h"
#endif

class MainWindow;

/******************************************************************************
 * Class Granule - an application shell                                       *
 ******************************************************************************/

class Granule :
    public virtual sigc::trackable,
    public ASSA::GenServer,
    public ASSA::Singleton<Granule>
{
public:
    Granule ();
    ~Granule ();

	virtual void init (int* argc_, char* argv_[], const char* help_);

    virtual void init_service      ();
    virtual void process_events    ();
	virtual void fatal_signal_hook ();
	virtual int  handle_timeout (ASSA::TimerId tid_);

	void set_async_events_adapter (bool enable_ = true);
	bool async_adapter_enabled  () const { return m_async_adapter_enabled; }
    bool timer_cb ();

	bool dump_cardboxes_enabled () const { return (m_dump_cardboxes == "yes"); }

	MainWindow*  get_main_window ();

	/** Overload XML parser default entity loader.
	 */
    static xmlParserInputPtr 
	my_xml_entity_loader (const char*      url_, 
						  const char*      id_, 
						  xmlParserCtxtPtr ctxt_);
public: 
    /** Common utilities used everywhere
	 *
	 * @{ start
	 */
	static gchar*        strip_pango_markup     (const char* src_);
	static void          remove_common_prefixes (gchar*& src_);
	static Glib::ustring trim_multiline (const Glib::ustring& text_);

	/** Search DOM XML parser tree for the first element that
		matches the xpath_.
	*/
	static xmlChar* get_node_by_xpath (xmlDocPtr parser_, const char* xpath_);

	/** Validate pango markup
		@return true on success; false if entry is invalid
	*/
	static bool check_markup (const Glib::ustring& str_);

	/** Find 'Add' and 'Remove' buttons of the GtkFileChooserDialog
	 *  and hide its labels, leaving icons only. This helps with
	 *  portrait orientation of file-related dialogs in IS_PDA mode.
	 *
	 *  @param container_  gobj() of Gtk::FileChooserDialog
	 *  @param counter_    Should be set to 0 by the caller. Helps to 
	 *                     abort the search earlier.
	 */
	static void hide_fcd_gtk_labels (GtkContainer* container_, int& counter_);

	/** Rotate Gtk::Label text. Older versions of Gtk+ didn't have this
	 *  ability implemented. Also, on some platforms 
	 *  (OpenEmedded in portait mode), use abbreviated text instead.
	 */
	static void rotate_label (Gtk::Label* label_, 
							  double angle_, 
							  const char* alt_text_);

	/** Wrapper for Gdk equivalent that takes care of disabled
	 *  exception handling on some platforms (nokia800).
	 */
	static Glib::RefPtr<Gdk::Pixbuf> 
	create_from_file (const string& filename_, int width_= -1, int height_= -1);

	static string locale_from_utf8 (const Glib::ustring& utf8_string_);
	static Glib::ustring locale_to_utf8 (const string& opsys_string_);

	static Glib::ustring filename_to_utf8 (const string& opsys_string_);
	static string filename_from_utf8 (const Glib::ustring& utf8_string);

	/** Take an absolute path and absolute file name 
	 *  and calculate resultant file name in relation to the absolute path.
	 */
	static string calculate_relative_path (const string& abs_pname_,
										   const string& abs_fname_);

	/** Take an absolute path to the directory
	 *  and a relative file name and calculate resultant absolute file name.
	 */
	static string calculate_absolute_path (const string& abs_pname_,
										   const string& rel_fname_);
	/** Save parent position.
	 *  If child was moved, restore parent to new coordinates.
	 */
	static void move_child_to_parent_position (Gtk::Window& parent_, 
											   Gtk::Window& child_);

	static void move_parent_to_child_position (Gtk::Window& parent_, 
											   Gtk::Window& child_);

	/** Memory-safe wrapper for basename(3C).
	 */
	static string get_basename (const char* path_);

	/** Memory-safe wrapper for dirname(3C).
	 */
	static string get_dirname  (const char* path_);

	/** Get current working directory (getcwd) wrapper.
	 */
	static string get_current_working_dir ();

	/** Resolve pixmap path. This depends on compiler '--prefix' value.
	 */
	static string find_pixmap_path (const char* pixmap_);

	/** Printable quality answers.
	 */
	static const char* quality_answer_str (AnswerQuality qa_);

	/** Test to see if file exists. Filename can be given as 
	 *  relative to the basename.
	 *
	 * @param fname_     Either relative or absolute filename to test.
	 * @param basepath_  If fname_ is relative, use basepath_ to form 
	 *                   fully-qualified pathname.
	 * @param file_path_ The path tested (and returned).
	 */
	static bool test_file_exists (const Glib::ustring& fname_,
								  const Glib::ustring& basepath_,
								  Glib::ustring& file_path_);

	/* }@ end
	 **/

private:
	void dump_package_envs ();

private:
	ASSA::TimeVal    m_timeout;

	/** Options */
    string           m_gtk_options;
	long             m_secs_in_day;
	long             m_secs_in_week;
	string           m_dump_cardboxes;

	MainWindow*      m_main_window;

    Gtk::Main*       m_kit;
	sigc::connection m_tconn;

	bool             m_async_adapter_enabled;  // True if enabled
	ASSA::TimerId    m_async_timer;	 // Disable async adapter when expires

    static xmlExternalEntityLoader m_default_entity_loader;

#ifdef IS_HILDON
	Glib::RefPtr<Hildon::Program> m_hildon_program;
#endif
};

/******************************************************************************
 * Useful definitions                                                         *
 ******************************************************************************/

#define GRANULE    Granule::get_instance()

#define REACTOR    GRANULE->get_reactor()
#define MAINWIN    GRANULE->get_main_window()
#define DECKMGR    MAINWIN->get_deck_manager()
#define DECKLIST   MAINWIN->get_deck_list()
#define CARDBOX    MAINWIN->get_cardbox()
#define TOPMENUBAR MAINWIN->get_top_menubar()

inline MainWindow* 
Granule::
get_main_window () 
{ 
	if (m_main_window == NULL) 
	{
		DL((ASSA::ASSAERR,"Assure Aborted False Expression!\n"));
		DL((ASSA::ASSAERR,"Error on line %d in file %s\n",
			__LINE__,__FILE__));
		::raise( SIGTERM );
	}
	return m_main_window; 
}

#endif // Granule_H

