// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: TextAlignmentsUI.h,v 1.2 2008/08/16 03:13:09 vlg Exp $
//------------------------------------------------------------------------------
//                          TextAlignmentsUI.h
//------------------------------------------------------------------------------
//  Copyright (c) Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date   : Wed Nov 1 2006
//
//------------------------------------------------------------------------------
#ifndef MY_TEXT_ALIGNMENT_H
#define MY_TEXT_ALIGNMENT_H

#include <vector>
#include <string>

#include <gtkmm/dialog.h>
#include <gtkmm/frame.h>
#include <gtkmm/alignment.h>
#include <gtkmm/box.h>
#include <gtkmm/checkbutton.h>
#include <gtkmm/combobox.h>
#include <gtkmm/entry.h>
#include <gtkmm/liststore.h>
#include <gtkmm/treemodel.h>

class TextAlignmentsUI
{
public:
	typedef std::vector<std::string>     value_list_t;
	typedef value_list_t::iterator       vlist_iter_t;
	typedef value_list_t::const_iterator vlist_citer_t;

public:
	TextAlignmentsUI (const char*   title_,
					  value_list_t& x_align_list_,
					  value_list_t& y_align_list_,
					  value_list_t& paragraph_list_,
					  value_list_t& img_position_list_,
					  bool          show_img_position_ = true);

	Gtk::ComboBox* x_align_combo      () { return m_x_align_combo;      }
	Gtk::ComboBox* y_align_combo      () { return m_y_align_combo;      }
	Gtk::Entry*    x_padding_entry    () { return m_x_padding_entry;    }
	Gtk::Entry*    y_padding_entry    () { return m_y_padding_entry;    }
	Gtk::ComboBox* paragraph_combo    () { return m_paragraph_combo;    }
	Gtk::ComboBox* img_position_combo () { return m_img_position_combo; }

	Gtk::Frame*    frame () { return m_frame; }

	static value_list_t make_align_x_list      ();
	static value_list_t make_align_y_list      ();
	static value_list_t make_paragraph_list    ();
	static value_list_t make_img_position_list ();

private:
	Gtk::Frame*      m_frame;

	Gtk::ComboBox*   m_x_align_combo;
	Gtk::ComboBox*   m_y_align_combo;
	Gtk::Entry*      m_x_padding_entry;
	Gtk::Entry*      m_y_padding_entry;
	Gtk::ComboBox*   m_paragraph_combo;
	Gtk::ComboBox*   m_img_position_combo;

private:
	/** Alignment Enum
	 */
	class AlignmentType_Model : public Gtk::TreeModel::ColumnRecord
	{
	public:
		AlignmentType_Model () { add (m_value); }

		Gtk::TreeModelColumn<Glib::ustring> m_value;
	};

	AlignmentType_Model           m_falign_x_col;
	AlignmentType_Model           m_falign_y_col;

	Glib::RefPtr<Gtk::ListStore>  m_falign_x_tree_model;
	Glib::RefPtr<Gtk::ListStore>  m_falign_y_tree_model;

private:
	/** Paragraph justification
	 */
	class JustifyType_Model : public Gtk::TreeModel::ColumnRecord
	{
	public:
		JustifyType_Model () { add (m_value); }

		Gtk::TreeModelColumn<Glib::ustring> m_value;
	};

	JustifyType_Model             m_justify_col;
	Glib::RefPtr<Gtk::ListStore>  m_justify_tree_model;

private:
	/** IMG Alignment
	 */
	class ImgAlignmentType_Model : public Gtk::TreeModel::ColumnRecord
	{
	public:
		ImgAlignmentType_Model () { add (m_value); }

		Gtk::TreeModelColumn<Glib::ustring> m_value;
	};

	ImgAlignmentType_Model        m_img_position_col;
	Glib::RefPtr<Gtk::ListStore>  m_img_position_tree_model;
};



#endif /* MY_TEXT_ALIGNMENTS_H */
