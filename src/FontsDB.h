// -*- c++ -*-
//------------------------------------------------------------------------------
//                            FontsDB.h
//------------------------------------------------------------------------------
// $Id: FontsDB.h,v 1.1 2007/12/02 00:31:24 vlg Exp $
//------------------------------------------------------------------------------
//  Copyright (c) 2007 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date   : Mon Sep 17 2007
//
//------------------------------------------------------------------------------
#ifndef FONTS_DB_H
#define FONTS_DB_H

#include "Granule-main.h"

#include <assa/IniFile.h>

#include <string>
#include <libxml/tree.h>		// xmlDocPtr
#include <libxml/xmlwriter.h>	// xmlTextWriterPtr

#include <pangomm/fontdescription.h>

class FontsSelectorUI;

/**-----------------------------------------------------------------------------
 *	FontsDB is the Model of the Fonts Selector MVC pattern.
 *  It it a persistent storage manager of data.
 **-----------------------------------------------------------------------------
 */
class FontsDB
{  
public:
    FontsDB () { init_global_defaults (); }

	void init_global_defaults ();
	void init_deck_defaults   ();

	/** Persisten storage with XML (Deck) file.
	 */
    void load_from_xml_config  (xmlDocPtr parser_, std::string& error_msg_);
    void save_to_xml_config    (xmlTextWriterPtr writer);

	/** Persistent storage with INI file (application config).
	 */
    void load_from_ini_config  (ASSA::IniFile* ini_filep_);
    void save_to_ini_config    (ASSA::IniFile* ini_filep_);

	/** Text alignment controls (From Deck.h)
	 */
    Pango::FontDescription& get_question_font () { return m_question_font_desc;}
    Pango::FontDescription& get_answer_font   () { return m_answer_font_desc;  }
    Pango::FontDescription& get_example_font  () { return m_example_font_desc; }
    Pango::FontDescription& get_input_font    () { return m_input_font_desc;   }
    Pango::FontDescription& get_app_font      () { return m_app_font_desc;     }

    void set_question_font (const Glib::ustring& font_);
    void set_answer_font   (const Glib::ustring& font_);
    void set_example_font  (const Glib::ustring& font_);
    void set_input_font    (const Glib::ustring& font_);
    void set_app_font      (const Glib::ustring& font_);

	/// Update View with data from Model
	void update_view (FontsSelectorUI*& view_);

	/// Store View values in Model
	void fetch_from_view (FontsSelectorUI*& view_);

private:
	Pango::FontDescription  m_question_font_desc;
    Pango::FontDescription  m_answer_font_desc;
    Pango::FontDescription  m_example_font_desc;
    Pango::FontDescription  m_input_font_desc;
    Pango::FontDescription  m_app_font_desc;
};

/**--------------------
 **** Set a property **
 **--------------------
 */
inline void 
FontsDB::set_question_font (const Glib::ustring& font_)
{
    m_question_font_desc = Pango::FontDescription (font_);
}

inline void 
FontsDB::set_answer_font (const Glib::ustring& font_)
{
    m_answer_font_desc = Pango::FontDescription (font_);
}

inline void 
FontsDB::set_example_font (const Glib::ustring& font_)
{
    m_example_font_desc = Pango::FontDescription (font_);
}

inline void 
FontsDB::set_input_font (const Glib::ustring& font_)
{
    m_input_font_desc = Pango::FontDescription (font_);
}

inline void 
FontsDB::set_app_font (const Glib::ustring& font_)
{
    m_app_font_desc = Pango::FontDescription (font_);
}

/// ----------------------------------------------------------------------

#endif /* FONTS_DB */
