// -*- c++ -*-
//------------------------------------------------------------------------------
//                        FolderChooseDialog.cpp
//------------------------------------------------------------------------------
// $Id: FolderChooseDialog.cpp,v 1.1 2007/12/02 00:31:24 vlg Exp $
//------------------------------------------------------------------------------
//  Copyright (c) 2006 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version
//  2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------

#include "GrappConf.h"
#include "FolderChooseDialog.h"

/**
 *  From GTK+ Reference Manual:
 *
 *  GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER -
 *
 *    Indicates an Open mode for selecting folders. 
 *    The file chooser will let the user pick an existing folder.
 */

FolderChooseDialog::
FolderChooseDialog (const Glib::ustring& title_, Gtk::Widget* parent_)
{
#ifdef OBSOLETE

	m_dialog = new Gtk::FileSelection (title_);
	m_dialog->set_filename (CONFIG->get_filesel_path ());
	m_dialog->get_file_list ()->get_parent ()->hide ();

#else

    m_dialog = new Gtk::FileChooserDialog (title_, 
									   Gtk::FILE_CHOOSER_ACTION_SELECT_FOLDER);
	int counter = 0;
	Granule::hide_fcd_gtk_labels (GTK_CONTAINER (m_dialog->gobj ()), counter);

    m_dialog->add_button (Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
    m_dialog->add_button ("Select",           Gtk::RESPONSE_OK);
    
    m_dialog->set_current_folder (CONFIG->get_filesel_path () + 
								  G_DIR_SEPARATOR_S);
    m_dialog->set_local_only ();
#endif

#ifdef IS_HILDON
//    m_dialog->set_transient_for (*HILDONAPPWIN);
    m_dialog->set_transient_for ((Gtk::Window&)*parent_);
#else
    m_dialog->set_transient_for ((Gtk::Window&)*parent_);
#endif

}

