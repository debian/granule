// -*- c++ -*-
//------------------------------------------------------------------------------
//                              VDeck.h
//------------------------------------------------------------------------------
// $Id: VDeck.h,v 1.11 2007/11/26 03:58:32 vlg Exp $
//
//  Copyright (c) 2004,2006 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//
// Date: Apr 24 2004
//------------------------------------------------------------------------------
#ifndef VDECK_H
#define VDECK_H

#include <cstdio>
#include <string>
#include <vector>

#include <glibmm/ustring.h>

#include <libxml/encoding.h>
#include <libxml/xmlwriter.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>
#include <libxml/parserInternals.h>

#include "Granule-main.h"

class VCard;

class VDeck
{
public:
	typedef std::vector<VCard*> cardlist_type;
	typedef cardlist_type::const_iterator cardlist_const_iterator;
	typedef cardlist_type::iterator cardlist_iterator;

public:
	VDeck () : m_side_selection (FRONT) { /* no-op */ }
	virtual ~VDeck ();

	virtual int  load    (const string& fname_)  { return -1;  }
	virtual int  save    ()                      { return  0;  }
	virtual int  save_as (const string& fname_)  { return  0;  }

	virtual cardlist_const_iterator begin () const = 0;
	virtual cardlist_const_iterator end   () const = 0;

	virtual cardlist_iterator begin () = 0;
	virtual cardlist_iterator end   () = 0;

	virtual size_t size  () const = 0;
	virtual bool   empty () const = 0;

	virtual void push_back (VCard* card_)            = 0;
	virtual void erase     (cardlist_iterator iter_) = 0;
	virtual bool erase     (VCard* card_)            = 0;

	virtual void play_card (SideSelection side_,
							VDeck::cardlist_iterator& iter_) = 0;

	virtual const string&  get_name () const = 0;

	virtual int get_index () const { return -1; }

	/** Report percent of the cards already processed. For Deck, its
		size is constant and its current card idx is changing.
		For CardDeck, its current card idx is always 0 and its size
		is decrementing. A special case is CardDeck #1 and #5. In both
		cases the answer that makes the card go to the end of the deck
		rather then to the next/previous deck, doesn't change the size
		of the deck as well as the idx.

		@param pct_  [out] Percentage cards already viewed.
		@param msg_  [out] Current/Total ratio message.
		@param idx_  [in ] Index of the current card (if applicable).
	*/
	virtual void get_progress (float& pct_, std::string& msg_, int idx_) = 0;

	/// Reset progress parameters (size/index)
	virtual void reset_progress () { /* no-op */ }

	/** Does Deck dictate its own custom appearance?
	 *  This is only true for *real* decks.
	 */
	virtual bool with_custom_appearance () const { return false; }

	/** Side selection indicates whether to show the Front or the Back
		of each card in the Deck. This works for both the Deck as well as
		CardDeck.
	*/
	SideSelection get_side_selection () const;
	void set_side_selection (SideSelection ss_);

protected:
	SideSelection m_side_selection; /* Indicates which side to show*/
};

inline 
VDeck::
~VDeck ()
{
	trace_with_mask("VDeck::~VDeck",GUITRACE|DECK);
}

inline SideSelection
VDeck::
get_side_selection () const
{ 
	return m_side_selection;
}

inline void
VDeck::
set_side_selection (SideSelection ss_) 
{ 
	m_side_selection = ss_;
}

#endif /* VDECK_H */
