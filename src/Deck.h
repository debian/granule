// -*- c++ -*-
//------------------------------------------------------------------------------
//                              Deck.h
//------------------------------------------------------------------------------
// $Id: Deck.h,v 1.45 2008/08/04 02:54:31 vlg Exp $
//
//  Copyright (c) Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//
// Date: Jan 8, 2004
//------------------------------------------------------------------------------
#ifndef DECK_H
#define DECK_H

#include "VDeck.h"
#include "AppearanceDB.h"
#include "FontsDB.h"
#include "TextColorsDB.h"

#include <assa/Pipe.h>
#include <assa/EventHandler.h>

#include <libxml/tree.h>		// xmlDocPtr

typedef enum _ParserState 
{
	ST_BEGIN,
	ST_1ST_FIELD,
	ST_2ND_FIELD,
	ST_3RD_FIELD
} ParserState;

class Card;

/**-----------------------------------------------------------------------------
 *  Class Deck
 *------------------------------------------------------------------------------
 */
class Deck : public VDeck, public ASSA::EventHandler
{
public:
	Deck (DeckEditStatus status_);
	Deck (const string& fname_, DeckEditStatus status_);
	~Deck ();

	virtual int  load (const string& fname_, DeckAction = INSTALL_DECK);

	/** Handle and discard input from external 
	 *  audio playback command.
	 */
	virtual int handle_read  (int fd_);

	/** Handle audio playback pipe close event.
	 */
    virtual int handle_close (int fd_);

	void         insert (const string& from_file_);
	bool         import_from_csv (const string& file_);
	bool         export_to_csv   (const string& file_);

	virtual int  save ();
	virtual int  save_as (const string& fname_);

	virtual cardlist_const_iterator begin () const { return m_cards.begin (); }
	virtual cardlist_const_iterator end   () const { return m_cards.end   (); }

	virtual cardlist_iterator begin () { return m_cards.begin (); }
	virtual cardlist_iterator end   () { return m_cards.end   (); }

	virtual size_t size  () const { return m_cards.size ();  }
	virtual bool   empty () const { return m_cards.empty (); }

	virtual void push_back (VCard* card_) { m_cards.push_back (card_); }
	virtual void erase (cardlist_iterator iter_) { m_cards.erase (iter_); }
	virtual bool erase (VCard* card_);

	void swap_with_next (VCard* vcard_);
	void swap_with_prev (VCard* vcard_);

	virtual const string& get_name () const { return m_fname; }
	const string& get_deck_path () const { return m_deck_path; }

	virtual void get_progress (float& pct_done_, std::string& msg_, int idx_);
	virtual void play_card (SideSelection s_, Deck::cardlist_iterator& iter_);

	VCard*         find_card_by_id (long id_);
	DeckEditStatus get_status ();

	const Glib::ustring& get_author () const { return m_author; }
	const Glib::ustring& get_desc   () const { return m_desc;   }

	void set_author (const Glib::ustring& s_) { m_author = s_; }
	void set_desc   (const Glib::ustring& s_) { m_desc = s_; }

	const string& get_snd_path   () const { return m_snd_path;       }
	PathMode      snd_path_mode  () const { return m_snd_path_mode;  }
	PathMode      pics_path_mode () const { return m_pics_path_mode; }

	void set_snd_path   (const string& spath_);
	void snd_path_mode  (PathMode mode_) { m_snd_path_mode = mode_; }
	void pics_path_mode (PathMode mode_) { m_pics_path_mode = mode_; }

	/** Test to see if active_snd_path is a valid path.
	 */
	void test_active_snd_path ();

	/** Is audio playback active?
	 */
	static bool audio_playback_active () { return s_pipe_stream != NULL; }

	/** Utility functions
	 */
	void mark_as_modified     ()       { m_status = MODIFIED_DECK; }
	bool needs_renaming       () const { return m_needs_renaming;  }

	DeckEditStatus get_status () const { return m_status; }

	void set_custom_appearance (bool v_ = true);
	virtual bool with_custom_appearance() const;

	TextColorsDB& text_colors_db () { return m_text_colors_db; }
	FontsDB&      fonts_db       () { return m_fonts_db;   }
	AppearanceDB& app_db         () { return m_appearance; }

	void dump () const;
	void dump_deck_status () const;

private:
	/** An audio playback pipe. We listen on it to detect
	 *  the end of the playback.
	 */
	static FILE* s_pipe_stream;

	ASSA::Pipe* m_audio_playback_pipe;

private:
	void init_internals ();

	/** Parse the Deck file card by card.
	 *  @return 0 on success; -1 on error with error_ explaining the reason.
	 */
	int  parse_xml_tree (xmlDocPtr parser_, std::string& error_);

	void reset_snd_setup ();

	int split_tokens (const Glib::ustring& instream_, 
					  std::vector<Glib::ustring>& tokens_, 
					  char separator_,
					  bool bracket_flag_);

	void trim_escaped_quotes (Glib::ustring& s_);
	void escape_double_quotes (Glib::ustring& input_);
	void add_new_card (Glib::ustring& q_, 
					   Glib::ustring& a_, 
					   Glib::ustring& e_,
					   long& card_id_,
					   int& ok_count_);

	void open_double_quote (bool& flag);
	void close_double_quote (bool& flag);

	const char* get_state_name (ParserState state_);

	void change_state (ParserState& state_);
	void reset_state (ParserState& state_);

private:
	string         m_fname;		// full XML file path name
	string         m_deck_path;	// path to the deck
	cardlist_type  m_cards;
	string         m_indent;	// for pretty printing

	DeckEditStatus m_status;
	bool           m_needs_renaming;

	Glib::ustring  m_author;
	Glib::ustring  m_desc;
	SideSelection  m_side_selection; // Tells which side to play next time

	/** Active sound path is used to construct the name of the 
	 *  sound bite file for playing it through the sound card.
	 */
	string m_active_snd_path; 

	/** True if m_active_snd_path holds a valid path
	 */
	bool m_snd_enabled;

	/** The sound path associated with the Deck. This is an alternative
	 *  sound path if the user wished to overwrite the default one from
	 *  application preferences.
	 *
	 *  It is accessible via DeckInfo dialog and is recorded in
	 *  XML .dkf file. When empty, it is assumed disabled.
	 */
	string m_snd_path; 

	/** If sound path is present, it might be recorded relative
	 *  to the Deck path itself. This comes handy when Deck is distributed
	 *  with its audio clips.
	 */
	PathMode m_snd_path_mode;

	/** If pic path is present, it might be recorded relative
	 *  to the Deck path itself. This comes handy when Deck is distributed
	 *  with its pictures.
	 */
	PathMode m_pics_path_mode;

	/** Does Deck define its own appearance?
	 */
	bool m_custom_appearance;

	/** Text Colors database is the data model.
	 */
	TextColorsDB m_text_colors_db;

	/** Fonts database is the data model.
	 */
	FontsDB m_fonts_db;

	/** Text appearance database is the data model.
	 */
	AppearanceDB m_appearance;
};

/**-----------------------------------------------------------------------------
 *  Implementation
 *------------------------------------------------------------------------------
 */
inline 
Deck::~Deck ()
{
	trace_with_mask("Deck::~Deck",GUITRACE|DECK);
	dump_deck_status ();
}

inline void
Deck::open_double_quote (bool& flag)
{
	DL ((GRAPP,"double_bracket_flag: <true>\n"));
	flag = true;
}

inline void
Deck::close_double_quote (bool& flag)
{
	DL ((GRAPP,"double_bracket_flag: <false>\n"));
	flag = false;
}

inline const char*
Deck::get_state_name (ParserState state_)
{
	return (state_ == ST_BEGIN     ? "ST_BEGIN" :
			state_ == ST_1ST_FIELD ? "ST_1ST_FIELD" :
			state_ == ST_2ND_FIELD ? "ST_2ND_FIELD" : "ST_3RD_FIELD");
}

inline void
Deck::set_custom_appearance (bool v_)
{ 
	m_custom_appearance = v_; 
	DL ((GRAPP,"custom_appearance is %s\n", 
		 m_custom_appearance ? "enabled" : "disabled"));
}

inline bool 
Deck::with_custom_appearance() const 
{ 
	return m_custom_appearance;
}

#endif /* DECK_H */
