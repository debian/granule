// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: CardBox.cpp,v 1.81 2008/08/19 02:52:46 vlg Exp $
//------------------------------------------------------------------------------
//                            CardBox.cpp
//------------------------------------------------------------------------------
//  Copyright (c) 2004-2007 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date   : Sat Apr 17 23:37:11 EDT 2004
//
//------------------------------------------------------------------------------

#include <limits.h>				// PATH_MAX
#include <string.h>				// strcpy(3)

#if !defined (IS_WIN32)
#  include <libgen.h>				// basename(3), dirname(3)
#endif

#include <gtkmm/table.h>
#include <gtkmm/eventbox.h>
#include <gtkmm/label.h>
#include <gtkmm/box.h>
#include <gtkmm/checkbutton.h>

#include <assa/AutoPtr.h>
using ASSA::AutoPtr;

#include "Granule.h"
#include "CardBox.h"
#include "GrappConf.h"
#include "MainWindow.h"
#include "CardRef.h"
#include "Deck.h"
#include "DialogWithCheckbutton.h"

#include "Intern.h"

/*=-----------------------------------------------------------------------------
 *   CardBox::DrawerUI methods
 *=-----------------------------------------------------------------------------
 */
CardBox::DrawerUI::
DrawerUI (pixbuf_ref_type drawer_empty_, pixbuf_ref_type grey_light_)
	: 
	m_expired_count (0), 
	m_showed_exp_dialog (false)
{
	m_sched_light = manage (new Gtk::Image);
	m_sched_count = manage (new Gtk::Label);
	m_sched_vbox  = manage (new Gtk::VBox (false, 0));
	m_box_pic     = manage (new Gtk::Image);
	m_total_count_label = manage (new Gtk::Label);

	m_sched_light->set (grey_light_);
	m_sched_light->set_alignment (0.5,0.5);
	m_sched_light->set_padding   (0,2);

	m_sched_count->set_alignment (0.5,0.5);
	m_sched_count->set_padding   (0,0);
	m_sched_count->set_justify   (Gtk::JUSTIFY_CENTER);
	m_sched_count->set_line_wrap (false);
	m_sched_count->set_text      ("0");

	m_sched_vbox->pack_start (*m_sched_light, Gtk::PACK_SHRINK, 2);
	m_sched_vbox->pack_start (*m_sched_count, Gtk::PACK_SHRINK, 0);

	m_box_pic->set (drawer_empty_);
	m_box_pic->set_alignment (0.5,0.5);
	m_box_pic->set_padding   (2,2);
		
	m_total_count_label->set_alignment (0,0.5);
	m_total_count_label->set_padding   (8,0);
	m_total_count_label->set_justify   (Gtk::JUSTIFY_RIGHT);
	m_total_count_label->set_line_wrap (false);

#if defined (IS_PDA)
	m_total_count_label->set_text (_("0"));
#else
	m_total_count_label->set_text (_("0 cards"));
#endif
}

void
CardBox::DrawerUI::
reset_counts () 
{
	trace_with_mask("CardBox::DrawerUI::reset_counts",GUITRACE);

	m_sched_count->set_text ("0");
	m_expired_count = 0;
	m_showed_exp_dialog = false;

#ifdef IS_PDA
	m_total_count_label->set_text ("0");
#else
	m_total_count_label->set_text ("0 cards");
#endif
}

/*=-----------------------------------------------------------------------------
 * CardBox class methods
 *=-----------------------------------------------------------------------------
 */
#if defined(IS_PDA) && (GTKMM_MAJOR_VERSION == 2 && GTKMM_MINOR_VERSION >= 6)
#  define CB_SHRINK_ICONS ,34,39,false
#else
#  define CB_SHRINK_ICONS
#endif

/**
 * Construct CardBox object.
 *
 * We always maintain the presence of 5 CardDeckUIs in the visible
 * frame.
 */
CardBox::
CardBox (MainWindow& mwin_) : m_mwin (mwin_)
{
	trace_with_mask("CardBox::CardBox",GUITRACE);

	Glib::ustring pixpath;
	int i;

	for (i = 0; i < CARDBOXES_MIN; i++) {
		m_card_deck.push_back (new CardDeck);
	}

	m_drawer_empty = Granule::create_from_file(
		Granule::find_pixmap_path ("drawer_empty.png") CB_SHRINK_ICONS);
	m_drawer_one   = Granule::create_from_file(
		Granule::find_pixmap_path ("drawer_one.png")   CB_SHRINK_ICONS);
	m_drawer_many  = Granule::create_from_file(
		Granule::find_pixmap_path ("drawer_many.png")  CB_SHRINK_ICONS);

	m_sched_green  = Granule::create_from_file(
		Granule::find_pixmap_path ("sched_green.png"));
	m_sched_grey   = Granule::create_from_file(
		Granule::find_pixmap_path ("sched_grey.png"));
	m_sched_red    = Granule::create_from_file(
		Granule::find_pixmap_path ("sched_red.png"));
	m_sched_yellow = Granule::create_from_file(
		Granule::find_pixmap_path ("sched_yellow.png"));

	for (i = 0; i < CARDBOXES_MAX; i++) 
	{
		m_drawer_ui.push_back (DrawerUI (m_drawer_empty, m_sched_grey));
	}

	Gtk::Table* cbt = manage (new Gtk::Table (CARDBOXES_MAX, 3, false));
	
	cbt->set_row_spacings(1);
	cbt->set_col_spacings(1);

	/** Also enable card box icon as DND destination.
	 */
	std::list<Gtk::TargetEntry> list_targets;
	list_targets.push_back (Gtk::TargetEntry ("STRING"));
	list_targets.push_back (Gtk::TargetEntry ("text/plain"));

	for (i = 0; i < CARDBOXES_MAX; i++) 
	{
		m_drawer_ui [i].m_evtbox = manage (new Gtk::EventBox);
		m_drawer_ui [i].m_evtbox->add (*(m_drawer_ui [i].m_box_pic));
		m_drawer_ui [i].m_evtbox->set_events (Gdk::BUTTON_PRESS_MASK);

		m_drawer_ui [i].m_evtbox->signal_button_press_event ().connect (
			sigc::bind<int>(sigc::mem_fun(*this, &CardBox::play_in_box_cb), i));

		m_drawer_ui [i].m_evtbox->drag_dest_set (list_targets);
		m_drawer_ui [i].m_evtbox->signal_drag_data_received ().connect (
			sigc::bind(sigc::mem_fun (*this, &CardBox::on_deck_dropped_cb), i));

		cbt->attach (*m_drawer_ui [i].m_evtbox, 
					 1, 2, i, i+1, Gtk::FILL, Gtk::FILL, 0,0);
	}

	for (i = 0; i < CARDBOXES_MAX; i++) {
		cbt->attach (*m_drawer_ui[i].m_total_count_label, 
					 2,3,i,i+1, 
					 Gtk::FILL,
					 Gtk::AttachOptions(), 
					 0,0);
	}

	for (i = 0; i < CARDBOXES_MAX; i++) {
		cbt->attach (*m_drawer_ui [i].m_sched_vbox, 0,1,i,i+1, 
					 Gtk::FILL, Gtk::FILL,4,0);
	}

	Gtk::ScrolledWindow* scrollw = manage (new Gtk::ScrolledWindow);

	scrollw->set_flags       (Gtk::CAN_FOCUS);
    scrollw->set_shadow_type (Gtk::SHADOW_NONE);
    scrollw->set_policy      (Gtk::POLICY_NEVER , Gtk::POLICY_ALWAYS);

	scrollw->add (*cbt);
  	pack_start (*scrollw, Gtk::PACK_EXPAND_WIDGET, 4);

	show_all ();
}

void
CardBox::
mask_out_unused_boxes ()
{
	trace_with_mask("CardBox::mask_out_unused_boxes",GUITRACE);


	u_int i = m_sched_db.capacity ();
	for (; i < CARDBOXES_MAX && i < m_drawer_ui.size (); i++) 
	{
		m_drawer_ui [i].m_sched_vbox->hide ();
		m_drawer_ui [i].m_evtbox->hide ();
		m_drawer_ui [i].m_total_count_label->hide ();
	}
}

void
CardBox::
expose_hidden_box (int idx_)
{
	if ((u_int) idx_ < m_drawer_ui.size ()) 
	{
		m_drawer_ui [idx_].m_sched_vbox->show ();
		m_drawer_ui [idx_].m_evtbox->show ();
		m_drawer_ui [idx_].m_total_count_label->show ();
	}
}

void
CardBox::
update_count (int idx_)
{
	std::ostringstream os;

#ifdef IS_PDA
	os << m_card_deck [idx_]->size ();
#else
	os << m_card_deck [idx_]->size () << " cards";
#endif

	m_drawer_ui [idx_].m_total_count_label->set_text (os.str ());
	m_drawer_ui [idx_].m_box_pic->clear ();	

	if (m_card_deck [idx_]->size () == 0) {
		m_drawer_ui [idx_].m_box_pic->set (m_drawer_empty);
	}
	else if (m_card_deck [idx_]->size () == 1) {
		m_drawer_ui [idx_].m_box_pic->set (m_drawer_one);
	}
	else {
		m_drawer_ui [idx_].m_box_pic->set (m_drawer_many);
	}
}

void
CardBox::
new_file ()
{
	trace_with_mask("CardBox::new_file",GUITRACE);

	m_fname = UNKNOWN_FNAME;
	CONFIG->set_proj_name (m_fname);
	m_sched_db.reset ();

	for (int i = 0; i < CARDBOXES_MIN; i++) {
		m_card_deck [i]->set_name (i);
		m_deck_player.push_back (new DeckPlayer (*(m_card_deck [i])));
	}

	mask_out_unused_boxes ();
	m_mwin.set_mw_name ();
}

int
CardBox::
load (const string& fname_)
{
	trace_with_mask("CardBox::load",GUITRACE);

	int ret = 0;
	string error_msg;
	xmlParserCtxtPtr context;
	xmlDocPtr parser;

	/*= Test to see if file exists.
	 */
    struct stat file_stat;
    if (stat (fname_.c_str (), &file_stat) < 0 ||
		S_ISREG (file_stat.st_mode) == false)           // invalid project file
	{
		error_msg = "Invalid CardBox file:\n";
		error_msg += fname_;
		Gtk::MessageDialog md_error (error_msg, false, Gtk::MESSAGE_ERROR);
		md_error.run ();
		return -1;
    }

    CONFIG->set_proj_name (fname_);

	context = xmlCreateFileParserCtxt (fname_.c_str ()); // new
	if (!context) {
		DL ((GRAPP,"Failed to create file context.\n"));
		xmlFreeParserCtxt (context);
		return -1;
	}
	// Initialize context
	context->linenumbers = 1;
	context->validate = 1;
	context->replaceEntities = 1;

	// Parse the document
	xmlParseDocument (context);

	if(!context->wellFormed || context->errNo != 0) {
		error_msg = "File is not well-formed:\n";
		error_msg += fname_;
		Gtk::MessageDialog md_error (error_msg, false, Gtk::MESSAGE_ERROR);
		md_error.run ();
		xmlFreeParserCtxt (context); 
		return -1;
	}

	parser = context->myDoc;
	xmlFreeParserCtxt (context); // not needed anymore?

	DL((GRAPP,"Parsing the card file ...\n"));

	if (parse_xml_tree (parser, error_msg) == 0) {
		m_fname = fname_;
		m_mwin.set_mw_name ();
		CONFIG->set_filesel_path (fname_);
	}
	else {
 		string msg (_("Failed to parse file\n"));
 		msg += fname_ + _("\nReason: invalid file syntax/DTD");
 		Gtk::MessageDialog e (msg, false, MESSAGE_ERROR);
 		e.run ();
		DL ((ASSA::ASSAERR, "Failed to parse file: \"%s\"\n", 
			 error_msg.c_str ()));
		ret = -1;
	}

	xmlFreeDoc (parser);
	return ret;
}

int
CardBox::
parse_xml_tree (const xmlDocPtr parser_, string error_msg_)
{
	trace_with_mask("CardBox::parse_xml_tree",GUITRACE);

	char     total_count [32];
	char     xpath [64];
	xmlChar* result;

	string   attr_node;
	long     card_id;
	string   abs_path;			// absolute deck path
	string   deck_path;			// deck path as recorded in CDF file
	int      ccount;			// card counter
	int      cloaded;			// count of cards loaded
	CardRef* card_ref;
	bool     reversed;			// Is it a backward-oriented card?
	string   errmsg;
	u_int    j;

	Glib::RefPtr<Gdk::Pixbuf> exp_light;
	DeckPlayer* deck_player;

	/** Load the schedule database first.
	 */
	if (m_sched_db.load_from_xml_config (parser_, errmsg) == false) {
		DL ((GRAPP,"Invalid schedule DTD format. Reason : \"%s\"\n", 
			 errmsg.c_str ()));
	}

	/** If capacity of the CardFile is greater then 
	 *  CARDBOXES_MIN (5), create additional CardDecks.
	 */
	j = m_sched_db.capacity () - CARDBOXES_MIN;
	DL ((GRAPP,"Adding %d additional CardDeck%s\n", j, (j==1 ? ".":"s.")));

	if (j > 0) 
	{
		for (; j > 0; j--) {
			m_card_deck.push_back (new CardDeck);
		}
		for (j = CARDBOXES_MIN; j < m_sched_db.capacity (); j++) {
			expose_hidden_box (j);
		}
	}
	DL ((GRAPP,"m_card_deck.size () = %d\n", m_card_deck.size ()));
		
	for (u_int i = 0; i < m_sched_db.capacity (); i++) 
	{
		DL((GRAPP,"Filling Card Deck #%d ...\n", i+1));
		m_card_deck [i]->set_name (i);

		/** Parse carddeck side 
		 */
		sprintf(xpath, "/cardfile/carddeck[%d]/@side", i+1);
		result = Granule::get_node_by_xpath (parser_, xpath);
		if (result != NULL) {
			if (!strcmp ((const char*) result, "back")) {
				m_card_deck [i]->set_side_selection (BACK);
				DL ((GRAPP,"CardDeck[%d] selected side set to BACK\n", i+1));
			}
			xmlFree (result);
		}

		cloaded = 0;
		for (ccount = 1; ccount < MAX_CARDS; ccount++) 
		{
			DL((GRAPP,"Parsing Card # %d/%d ...\n", i+1, ccount));

			sprintf(xpath, "/cardfile/carddeck[%d]/card[%d]", i+1, ccount);
			result = Granule::get_node_by_xpath (parser_, xpath);
			if (result == NULL) {
				break;
			}
			xmlFree (result);

			/** Parse card's ID
			 */
			sprintf (xpath, "/cardfile/carddeck[%d]/card[%d]/@id", i+1, ccount);
			result = Granule::get_node_by_xpath (parser_, xpath);
			if (result == NULL) {
				error_msg_  = "Failed to retrieve card id for ";
				error_msg_ += xpath;
				return -1;
			}
			attr_node = (const char*) result;
			card_id = ::atol ((attr_node.substr (1)).c_str ());
			xmlFree (result);

			/** Test to see if this reference of a reversed card.
			 */
			reversed = false;
			sprintf (xpath, "/cardfile/carddeck[%d]/card[%d]/@reverse", 
					 i+1, ccount);
			result = Granule::get_node_by_xpath (parser_, xpath);
			if (result != NULL) {
				reversed = true;
				xmlFree (result);
			}

			/** Parse the expiration time of the card (expdate tag). If
			 *  tag is not found, then we are dealing with an older file.
			 */
			ASSA::TimeVal exp_time;
			sprintf (xpath, "/cardfile/carddeck[%d]/card[%d]/expdate", 
					 i+1, ccount);
			result = Granule::get_node_by_xpath (parser_, xpath);
			if (result == NULL) {
				exp_time = CONFIG->get_load_time (); // card expires today
			}
			else {
				exp_time.sec (::atol ((const char*) result));
				xmlFree (result);
			}

			/** Parse the source Deck path of the card.
			 */
			sprintf (xpath, "/cardfile/carddeck[%d]/card[%d]/fromdeck", 
					 i+1, ccount);
			result = Granule::get_node_by_xpath (parser_, xpath);
			if (result == NULL) {
				return -1;
			}
			deck_path = Granule::locale_from_utf8 ((const char*) result);
			xmlFree (result);

			if (deck_path[0] != ASSA_DIR_SEPARATOR) {
				abs_path = GRANULE->calculate_absolute_path (
					CONFIG->get_proj_path (), deck_path);
			}
			else {
				abs_path = deck_path;
			}

			DL((GRAPP,"Card parsed ID=%ld,\n path = \"%s\"\n", 
				card_id, deck_path.c_str ()));

			/** Look up the card in the list of Decks by (ID, PATH) pair.
			 *  If Deck is found, it is loaded (if it is not already in the
			 *  memory) and a newly created CardRef object is returned.
			 */
			card_ref = DECKMGR->lookup_card (card_id, abs_path);
			if (card_ref != NULL) 
			{
				DL((GRAPP,"Card (ID=%ld) found\n", card_id));
				card_ref->set_expiration (exp_time);
				if (card_ref->is_expired ()) {
					m_drawer_ui [i].add_one ();
				}

				card_ref->set_reversed (reversed);
				card_ref->load_from_xml_config (parser_, i, ccount);

				m_card_deck [i]->push_back (card_ref);
				cloaded++;
			}
		} // for (each card) 

		DL((GRAPP,"Processed %d cards\n", m_card_deck [i]->size ()));
		m_card_deck [i]->sort_by_expiration_time ();
		sprintf (total_count, "%d cards", cloaded);
		update_count (i);
		exp_light = set_expiration_light (i);
		deck_player = new DeckPlayer (*m_card_deck [i]);
		deck_player->set_light (exp_light);
		m_deck_player.push_back (deck_player);

	} // for (each card box)

	return 0;
}
	
int
CardBox::
save ()
{
	trace_with_mask("CardBox::save",GUITRACE);

	static const char dtd_url[] = "http://granule.sourceforge.net/cardfile.dtd";

	const std::string proj_path (CONFIG->get_proj_path ());

	VDeck::cardlist_iterator citer;
	CardRef* card_ref = NULL;
	xmlTextWriterPtr writer;
	int ret = 0;

	if (m_fname.length () == 0) {
		return ret;
	}

	DL((GRAPP,"Saving to \"%s\"\n", m_fname.c_str ()));
	::unlink (m_fname.c_str ());

	writer = xmlNewTextWriterFilename (m_fname.c_str (), 0);
	if (writer == NULL) {
		return -1;
	}

	ret = xmlTextWriterStartDocument (writer, NULL, "UTF-8", NULL);
	if (ret < 0) {
		xmlFreeTextWriter (writer);
		return -1;
	}

	xmlTextWriterSetIndent(writer, 0);
	xmlTextWriterStartDTD (writer, BAD_CAST "cardfile", NULL, BAD_CAST dtd_url);
	xmlTextWriterSetIndent(writer, 1);
	xmlTextWriterEndDTD   (writer);

	/*= @start cardfile
	 */
	ret = xmlTextWriterStartElement (writer, BAD_CAST "cardfile");

	/*= Write out schedule
	 */
	m_sched_db.save_to_xml_config (writer);

	/*= Write out decks
	 */
	for (u_int i = 0; i < m_sched_db.capacity (); i++) 
	{
		ret = xmlTextWriterStartElement (writer, BAD_CAST "carddeck");
		ret = xmlTextWriterWriteAttribute (writer, BAD_CAST "side",
		   (m_card_deck [i]->get_side_selection () == FRONT ? 
			BAD_CAST "front" : BAD_CAST "back"));

		citer = m_card_deck [i]->begin ();

		while (citer != m_card_deck [i]->end ()) 
		{
			if ((card_ref = dynamic_cast<CardRef*> (*citer)) != NULL) {
				ret = card_ref->save_to_xml_config (writer, proj_path);
			}
			citer++;
		}
		ret = xmlTextWriterEndElement (writer);	// carddeck
	}

	xmlTextWriterEndElement (writer); // @end cardfile
	xmlFreeTextWriter (writer);	      // cleanup

	return ret;
}

int
CardBox::
save_as (const string& fname_)
{
	trace_with_mask("CardBox::save_as",GUITRACE);

	/** If extension is not given, add it to the filepath.
	 */
	m_fname = fname_;
    string ext (CARD_FILE_EXT);
    if (m_fname.find (ext) == string::npos) {
		m_fname += ext;
    }

	if (Glib::file_test (fname_, Glib::FILE_TEST_IS_REGULAR)) 
	{
		string msg = N_("File \n") + fname_ 
			+ _("\nalready exists.\nWould you like to delete it first?");

		Gtk::MessageDialog qm (msg, 
							   false,
							   Gtk::MESSAGE_QUESTION, 
							   Gtk::BUTTONS_NONE,
							   true);
		qm.add_button (Gtk::Stock::NO,  Gtk::RESPONSE_NO );
		qm.add_button (Gtk::Stock::YES, Gtk::RESPONSE_YES);

		if (qm.run () == Gtk::RESPONSE_NO) {
			return -1;
		}
		::unlink (fname_.c_str ());
	}

	CONFIG->set_proj_name (fname_);
	save ();

	return 0;
}

void
CardBox::
close ()
{
	trace_with_mask("CardBox::close",GUITRACE);

	u_int i;

	for (i = 0; i < CARDBOXES_MIN; i++) 
	{
		DL ((DECK,"Resetting CardDeck #%d\n", i+1));
		m_card_deck [i]->clear ();

		m_drawer_ui [i].reset_counts ();
		m_drawer_ui [i].m_box_pic->clear ();
		m_drawer_ui [i].m_box_pic->set (m_drawer_empty);
		m_drawer_ui [i].m_sched_light->set (m_sched_grey);

		delete m_deck_player [i];
	}
	DL ((DECK,"Done.\n"));

	std::vector<CardDeck*>::iterator itr_b = m_card_deck.begin () + i;

	for (; i < m_sched_db.capacity (); i++) 
	{
		DL ((DECK,"Removing CardDeck #%d\n", i+1));
		delete m_card_deck [i];

		m_drawer_ui [i].reset_counts ();
		m_drawer_ui [i].m_box_pic->clear ();
		m_drawer_ui [i].m_box_pic->set (m_drawer_empty);
		m_drawer_ui [i].m_sched_light->set (m_sched_grey);

		delete m_deck_player [i];
	}

	m_card_deck.erase (itr_b, m_card_deck.end ());
	m_deck_player.clear ();

	m_sched_db.reset ();
	mask_out_unused_boxes ();

	m_fname = "";				// No cardbox is loaded any more

	DL ((GRAPP,"m_card_deck.size () =%d, m_cdplayer.size () = %d\n",
		 m_card_deck.size (), m_deck_player.size ()));
}

void
CardBox::
play_deck (int idx_)
{
	trace_with_mask("CardBox::play_deck",GUITRACE);
	
	if (idx_ < 0 || idx_ > CARDBOXES_MAX-1) {
		Gtk::MessageDialog w (_("Trying to access index out of range!"), 
							  false, MESSAGE_ERROR);
		w.run ();
	}

	if (m_card_deck [idx_]->size ()) 
	{
		m_card_deck [idx_]->reset_progress ();
		m_deck_player [idx_]->update_geometry ();

		m_deck_player [idx_]->run (*MAINWIN);  // Start DeckPlayer

		update_count (idx_);	// User might have deleted some cards.
	}
	else {
		Gtk::MessageDialog w (_("Card Deck you have\nselected is empty."), 
							  false, MESSAGE_WARNING);
		w.run ();
	}
}

/** 
 * Add all cards from_deck_ to the CardDeck number idx_.
 * This method is called from DeckMgr::add_deck_to_box (id) callback.
 */
void
CardBox::
add_deck_to_box (int idx_, Deck* from_deck_)
{
	trace_with_mask("CardBox::add_deck_to_box",GUITRACE);

	bool with_reverse;

	Assure_exit ((u_int) idx_ < m_sched_db.capacity ());
	Assure_exit (from_deck_);

	if (m_fname.empty ()) {
		Gtk::MessageDialog em (_("First, open or create\na Card Box file."), 
							   false, Gtk::MESSAGE_ERROR);
		em.run ();
		return;
	}

	{
		std::ostringstream os;

		os << "Do you want to proceed with adding Deck\n" 
		   << "\"" << from_deck_->get_name () << "\"\n"
		   << " to CardBox " << (idx_ + 1) << " ?";

		DialogWithCheckbutton qm (_(os.str ().c_str ()),
						(_("If enabled, this option makes adding a card \n"
						"from the Deck to a CardBox insert two references -\n"
						"one with the front/back and another with \n"
						"the opposite back/front orientation.")));

		if (qm.run () == Gtk::RESPONSE_NO) {
			return;
		}
		with_reverse = qm.get_checkbutton_status ();
	}

	DL((GRAPP,"Adding Deck \"%s\" to CardBox # %d\n", 
		from_deck_->get_name ().c_str (), idx_));

	/** Now, proceede with adding
	 */
	VDeck::cardlist_const_iterator iter = from_deck_->begin ();

	while (iter != from_deck_->end ()) 
	{
		AutoPtr<CardRef> card_ref (
			new CardRef (**iter, *from_deck_, sched_db ().efactor ()));

		card_ref->set_expiration_in_days (sched_db ().get_box_interval (idx_));
		card_ref->set_reversed (false);

		if (!find_in_all_carddecks (card_ref.get ())) {
			m_card_deck [idx_]->push_back (card_ref.release ());
		}

		if (with_reverse) 
		{
			AutoPtr<CardRef> rev_card_ref (
				new CardRef (**iter, *from_deck_, sched_db ().efactor()));

			rev_card_ref->set_expiration_in_days (
				sched_db ().get_box_interval (idx_));

			rev_card_ref->set_reversed (true);
			if (!find_in_all_carddecks (rev_card_ref.get ())) {
				m_card_deck [idx_]->push_back (rev_card_ref.release ());
			}
		}

		iter++;
	}

	m_card_deck [idx_]->reset_progress ();
	update_count (idx_);
	m_deck_player [idx_]->reset ();
	mark_as_modified ();
	mask_out_unused_boxes ();
}

/**
 * Add card(s) from DeckView to the CardBox. Adding of multiple cards
 * is allowed as of v.1.2.2.
 *
 * This function can be called both from DeckView to add selected card(s) 
 * to CardBox #1, or from DeckList to add an entire deck either with
 * drag-n-drop operation or with `insert' user action from the right-click 
 * popup menu of the DeckList.
 *
 * Either way, if `remove duplicates' option is activated in Preferences,
 * we ensure the uniqueness of the cards across all CardDecks. The policy
 * is that if a card is already present in any of the CardDecks, then it won't
 * be inserted again.
 *
 * This function assumes the ownership of CardRef objects passed on to
 * it by the caller via card_ref_ list argument.
 *
 * @param card_ref_ New card reference to add. 
 * @return 0 on success; -1 if CardFile is not open.
 */
int
CardBox::
add_cards_to_box (std::vector<CardRef*>& cardref_list_)
{
	trace_with_mask("CardBox::add_card_to_box",GUITRACE);

	bool with_reverse;

	if (m_fname.empty ()) {
		Gtk::MessageDialog em (_("First, open or create a Card File."), 
							   false, Gtk::MESSAGE_ERROR);
		em.run ();
		return -1;
	}

	{
		DialogWithCheckbutton qm (_("Add card(s) to the CardBox?"),
					 (_("If enabled, this option makes adding a card \n"
						"from the Deck to a CardBox insert two references -\n"
						"one with the front/back and another with \n"
						"the opposite back/front orientation.")));

		if (qm.run () == Gtk::RESPONSE_NO) {
			return -1;
		}
		with_reverse = qm.get_checkbutton_status ();
	}
	
	bool card_added = false;
	CardRef* card_ref;
	std::vector<CardRef*>::iterator iter = cardref_list_.begin ();

	while (iter != cardref_list_.end ()) 
	{
		card_ref = *iter;
		if (!find_in_all_carddecks (card_ref)) {
			card_ref->set_expiration (CONFIG->get_expiration (0));
			m_card_deck [0]->push_back (card_ref);
			card_added = true;
		}
		if (with_reverse) 
		{
			AutoPtr<CardRef> rev_card_ref (
				new CardRef (card_ref->get_card (), 
							 card_ref->get_deck (),
							 card_ref->get_efactor ()));

			rev_card_ref->set_reversed (true);
			if (!find_in_all_carddecks (rev_card_ref.get ())) {
				rev_card_ref->set_expiration (CONFIG->get_expiration (0));
				m_card_deck [0]->push_back (rev_card_ref.release ());
			}
		}
		if (!card_added) {
			delete card_ref;
		}

		card_added = false;
		iter++;
	}

	remove_all_duplicates (0);
	update_count (0);
	m_card_deck [0]->reset_progress ();
	m_deck_player [0]->reset ();
	mark_as_modified ();
	
	return 0;
}

/** 
 * Move the card from one Card Box to another.
 * This method is invoked by the DeckPlayer (know_answer_cb(), no_clue_cb())
 * when the user answered the card either right or wrong.
 */
void
CardBox::
move_card_to_box (VDeck::cardlist_iterator card_ref_, 
				  int from_deck_, 
				  AnswerQuality answer_quality_)
{
	trace_with_mask("CardBox::move_card_to_box",GUITRACE);

	u_int to_deck = 0;
	bool is_expired = (dynamic_cast<CardRef*> (*card_ref_))->is_expired ();

	/** Get hold of CardRef itself
	 */
	VCard* vcard = *card_ref_;

	/** First, remove the card from the old CardDeck.
	 */ 
	DL((GRAPP,"Removing card from the old CardDeck ...\n"));

	m_card_deck [from_deck_]->erase (card_ref_);
	update_count (from_deck_);

	if (is_expired) 
	{
		m_drawer_ui [from_deck_].m_expired_count--;
		if (m_drawer_ui [from_deck_].m_expired_count < 0) {
			m_drawer_ui [from_deck_].m_expired_count = 0;
		}

		set_expiration_light (from_deck_);

		if (m_drawer_ui [from_deck_].m_expired_count == 0) 
		{
			if (m_drawer_ui [from_deck_].m_showed_exp_dialog == false) 
			{
				Gtk::MessageDialog msg (_("You have learnt all\n"
										  "expired cards in this deck."), 
										false, MESSAGE_INFO);
				msg.run ();
				m_drawer_ui [from_deck_].m_showed_exp_dialog = true;
			}
		}
	}

	/** Reschedule card and figure out where it should go to.
	 */
	vcard->reschedule (answer_quality_);

	/** TODO: I don't remember how different answers are handled
	 */
	if (answer_quality_ == ANSWER_OK ||
		answer_quality_ == ANSWER_OK_HESITANT ||
		answer_quality_ == ANSWER_OK_DIFFICULT)
	{
		to_deck = m_sched_db.find_next_box (vcard->get_interval ());
	}
	else {
		to_deck = 0;
	}

	if (to_deck >= m_sched_db.capacity ()) {
		to_deck = m_sched_db.capacity () - 1;
	}

	/** Insert the CardRef in the back of the next CardDeck.
	 *  In case of the same CardDeck (the first deck for failed answer,
	 *  and the last deck for successful answer), the card goes back 
	 *  to the end of the CardDeck.
	 */
	DL((GRAPP,"%s; moving Card \"%s\" (interval=%d, Deck %d->%d)\n",
		GRANULE->quality_answer_str (answer_quality_),
		vcard->get_question ().c_str (), 
		vcard->get_interval (), from_deck_, to_deck));

	/** TODO: Perhaps I should insert in the right place instead.
	 */
	m_card_deck [to_deck]->push_back (vcard);

	if (GRANULE->dump_cardboxes_enabled ()) {
		for (u_int j = 0; j < m_sched_db.capacity (); j++) {
			m_card_deck [j]->dump_to_file ();
		}
	}

	if (to_deck != (u_int) from_deck_) {
		m_deck_player [from_deck_]->reset ();
		m_card_deck [to_deck]->reset_progress ();
	}

	m_deck_player [to_deck]->reset ();

	update_count (to_deck);
	Glib::RefPtr<Gdk::Pixbuf> light = set_expiration_light (to_deck);
	m_deck_player [to_deck]->set_light (light);

	mark_as_modified ();
}

bool 
CardBox::
play_in_box_cb (GdkEventButton* event, int idx_) 
{
	trace_with_mask("CardBox::play_in_box_cb",GUITRACE);

	if (m_fname.empty ()) {
		Gtk::MessageDialog emsg (_("First Open or Create\na New Card File!"), 
								 false, MESSAGE_ERROR);
		emsg.run ();
	}
	else {
		m_mwin.play_in_box (idx_);
		set_expiration_light (idx_);
	}
	return true;
}

/** Set the expiration light according to the ratio of expired
 *	cards to the total count:
 *  <pre>
 *      [  0%;  25% [ - grey; 
 *      [ 25%;  50% [ - green; 
 *      [ 50%;  75% [ - yellow;
 *      [ 75%; 100% ] - red.
 *  </pre>
 *
 *	@param idx_ Index of the CardDeck
 */
Glib::RefPtr<Gdk::Pixbuf>
CardBox::
set_expiration_light (int idx_) 
{
	trace_with_mask("CardBox::set_expiration_light",GUITRACE);

	std::ostringstream os;
	
	os << m_drawer_ui [idx_].m_expired_count;
	m_drawer_ui [idx_].m_sched_count->set_text (os.str ());
	
	if (m_card_deck [idx_]->size () == 0) {
		m_drawer_ui [idx_].m_sched_light->set (m_sched_grey);
		return m_sched_grey;
	}

	float x = (m_drawer_ui [idx_].m_expired_count * 100.0) /
		m_card_deck [idx_]->size();

	DL((GRAPP,"m_expired_count/deck size : %d/%d (x = %.2f)\n",
		m_drawer_ui [idx_].m_expired_count, m_card_deck[idx_]->size(), x)); 

	if (x >= 0 && x < 25.0) {                                  // grey
		m_drawer_ui [idx_].m_sched_light->set (m_sched_grey);
		return m_sched_grey;
	}
	else if (x >= 25.0 && x < 50.0) {                          // green
		m_drawer_ui [idx_].m_sched_light->set (m_sched_green);
		return m_sched_green;
	}
	else if (x >= 50.0 && x < 75.0) {                          // yellow
		m_drawer_ui [idx_].m_sched_light->set (m_sched_yellow);
		return m_sched_yellow;
	}
	// red
	m_drawer_ui [idx_].m_sched_light->set (m_sched_red);

	return m_sched_red;
}

void 
CardBox::
on_deck_dropped_cb (const Glib::RefPtr<Gdk::DragContext>& context_,
					int drop_x_coordinate_,
					int drop_y_coordinate_,
					const Gtk::SelectionData& selection_data_,
					guint type_id_,
					guint drop_timestamp_,
					int card_deck_index_)
{
	trace_with_mask("CardBox::on_deck_dropped_cd",GUITRACE);

	Deck* src_deck = NULL;
	
    /** Doesn't work - I get bogus pointer back! (investigate)
	    Deck* src_deck = (Deck*)(selection_data_.get_data ());
	*/
	src_deck = m_mwin.get_dnd_source ();
	add_deck_to_box (card_deck_index_, src_deck);
	context_->drag_finish (false, false, drop_timestamp_);
}

void
CardBox::
repaint_deck_players ()
{
	trace_with_mask("CardBox::repaint_deck_players",GUITRACE);

	deckplayer_iterator iter = m_deck_player.begin ();

	while (iter != m_deck_player.end ()) {
		(*iter)->repaint ();
		iter++;
	}
}

void
CardBox::
export_sound_bits (const std::string& path_)
{
	trace_with_mask("CardBox::export_sound_bits",GUITRACE);

	cardbox_iterator iter = m_card_deck.begin ();

	while (iter != m_card_deck.end ()) {
		(*iter)->export_sound_bits (path_);
		iter++;
	}
}

void
CardBox::
mark_as_modified () 
{ 
	m_mwin.go_state (Dirty); 
}


/**
 * Remove duplicate cards. The cards were just added to idx_ CardDeck.
 * If a card added already exists in some other CardDeck then remove it.
 */
void
CardBox::
remove_all_duplicates (int idx_)
{
	trace_with_mask("CardBox::remove_all_duplicates",GUITRACE);

	if (CONFIG->get_remove_duplicates ()) 
	{
		for (u_int idx = 0; idx < m_sched_db.capacity (); idx++) {
			m_card_deck [idx]->remove_duplicates ();
		}	
	}
}

bool
CardBox::
find_in_all_carddecks (CardRef* new_card_)
{
	trace_with_mask("CardBox::find_in_all_carddecks",GUITRACE);

	VDeck::cardlist_iterator iter;
	bool ret = false;
	CardRef* vcardp = NULL;

	DL ((GRAPP,"Searching for \"%s\" in all CardDecks.\n",
		 new_card_->get_question ().c_str ()));

	for (u_int idx = 0; idx < m_sched_db.capacity (); idx++) 
	{
		iter = m_card_deck [idx]->begin ();
		while (iter != m_card_deck [idx]->end ()) 
		{
			vcardp = dynamic_cast<CardRef*> (*iter);
			if (vcardp && 
				new_card_->get_id () == vcardp->get_id () &&
				new_card_->get_reversed () == vcardp->get_reversed ())
			{
				DL ((GRAPP, "Found matching card (\"%s\") - "
					 "exists in deck #%d.\n", 
					 vcardp->get_question ().c_str (), idx+1));
				ret = true;
				break;
			}
			iter++;
		}
	}

	return ret;
}

/** 
 *  The capacity (number of CardDecks) has been changed by the user
 *  via CardBox->Preferences. We need to grow or shrink the tray. 
 *  Either way we need to move cards around to their new location.
 */
void
CardBox::
adjust_cardbox_tray ()
{
	trace_with_mask("CardBox::adjust_cardbox_tray",GUITRACE);

	u_int diff;
	
	diff = m_sched_db.capacity () - m_card_deck.size ();

	/** Grow the tray
	 */
	if (diff > 0) 
	{	
		for (u_int i = m_card_deck.size () ; i < m_sched_db.capacity (); i++) 
		{
			m_card_deck.push_back (new CardDeck);
			m_card_deck [i]->set_name (i);
			m_deck_player.push_back (new DeckPlayer (*(m_card_deck [i])));
			expose_hidden_box (i);
		}
	}
	/*= Shrink the tray. 
	 */
	else if (diff < 0) 
	{
		u_int  counter = 0;
		size_t old_sz = m_card_deck.size ();
		size_t new_sz = m_sched_db.capacity ();
		int    target = new_sz - 1;

		/*= Move all cards from CardDecks that are slated
		 *  for removal to the last CardDeck of the new capacity.
		 */
		for (u_int j = new_sz; j < old_sz; j++) 
		{
			CardDeck::cardlist_const_iterator iter = m_card_deck [j]->begin ();
			while (iter != m_card_deck [j]->end ()) 
			{
				m_card_deck [target]->push_back (*iter);
				iter++, counter++;
			}
			delete m_card_deck [j];
			delete m_deck_player [j];
		}
		DL ((GRAPP, "Moved total of %d CardRefs to CardBox[%d]\n",
			 counter, target+1));

		/*= Remove excessive CardDecks, and paired DeckPlayers.
		 *  Hide extra CardDeckUIs.
		 */
		m_card_deck.erase   (m_card_deck.begin () + new_sz, 
							 m_card_deck.end ());
		m_deck_player.erase (m_deck_player.begin () + new_sz, 
							 m_deck_player.end ());
		mask_out_unused_boxes ();
	}
}

/** 
 * Redistribute CardRefs according to the new repetition schedule.
 */
void
CardBox::
normalize_cards ()
{
	trace_with_mask("CardBox::normalize_cards",GUITRACE);

	CardDeck::cardlist_iterator card_iter;
	VCard*    vcard;
	CardDeck* src_deck;
	CardDeck* dest_deck;

	u_int i;
	u_int dest;

	for (u_int src = 0; src < m_card_deck.size (); ++src) 
	{
		src_deck = m_card_deck [src];

		for (i = 0; i < src_deck->size (); ) 
		{
			card_iter = src_deck->begin () + i;
			if (card_iter == src_deck->end ()) {
				break;
			}
			vcard = *card_iter;
			dest = m_sched_db.find_next_box (vcard->get_interval ());
			if (dest != src) 
			{
				dest_deck = m_card_deck [dest];
				src_deck->erase (card_iter);
				dest_deck->push_back (vcard);
			}
			else {
				++i;
			}
		}
	}

	for (i = 0; i < m_card_deck.size (); ++i) 
	{
		update_count (i);
		m_card_deck [i]->reset_progress ();
		m_deck_player [i]->reset ();
	}

	mark_as_modified ();
}

void
CardBox::
adjust_efactor ()
{
	trace_with_mask("CardBox::adjust_efactor",GUITRACE);

	cardbox_iterator cbox_iter = m_card_deck.begin ();
	CardDeck::cardlist_iterator card_iter;
	CardRef* cref;

	while (cbox_iter != m_card_deck.end ()) 
	{
		card_iter = (*cbox_iter)->begin ();
		while (card_iter != (*cbox_iter)->end ()) 
		{
			cref = dynamic_cast<CardRef*>(*card_iter);
			cref->set_efactor (m_sched_db.efactor ());
			card_iter++;
		}
		cbox_iter++;
	}
}
