// -*- c++ -*-
//------------------------------------------------------------------------------
//                              CardDeck.h
//------------------------------------------------------------------------------
// $Id: CardDeck.h,v 1.24 2008/01/06 16:51:09 vlg Exp $
//
//  Copyright (c) 2004,2006 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//
// Date: Apr 24 2004
//------------------------------------------------------------------------------
#ifndef CARD_DECK_H
#define CARD_DECK_H

#include "CardRef.h"

using ASSA::ASSAERR;

class VCard;

/** @class Compare_CardRefs
 *
 * To remove duplicates we compare Card References by the IDs of 
 * the cards they point to. If two cards happen to have the same
 * ID (case of reversed *sticky* duplicates), we arbitrarily
 * nominate lefthand-side be smaller then righthand-side.
 */
class Compare_CardRefs 
{
public:
	int operator () (VCard* const& vc1_, VCard* const& vc2_)
	{
		CardRef* cr1 = dynamic_cast<CardRef*> (vc1_);
		CardRef* cr2 = dynamic_cast<CardRef*> (vc2_);
		Assure_exit (cr1 && cr2);
		if (cr1->get_card ().get_id () == cr2->get_card ().get_id ()) {
			return (false);
		}
		return (cr1->get_card ().get_id () < cr2->get_card ().get_id ());
	}
};

class Compare_ExpTime_CardRefs {
public:
	int operator () (VCard* const& vc1_, VCard* const& vc2_)
	{
		return ((*vc1_) < (*vc2_));
	}
};

/** 
 * @class CardDeck
 *
 * CardDeck represents each of the Card Boxes on the left of the MainWindow.
 * Each CardDeck holds a list of virtual cards, CardRef, each of which points
 * to the real Card in a real Deck.
 *
 * Because it is derived from VDeck, CardDeck can be played by DeckViewer.
 */

class CardDeck : public VDeck
{
public:
	CardDeck ();
	virtual ~CardDeck ();

	virtual cardlist_const_iterator begin () const { return m_cards.begin(); }
	virtual cardlist_const_iterator end   () const { return m_cards.end ();  }

	virtual cardlist_iterator begin () { return m_cards.begin (); }
	virtual cardlist_iterator end   () { return m_cards.end ();   }

	virtual size_t size  () const { return m_cards.size ();  }
	virtual bool   empty () const { return m_cards.empty (); }

	virtual void push_back (VCard* card_);

	/** Erase card from the VCard list only. Calling this method leaves 
	 *  the Deck intact.
	 */
	virtual void erase (cardlist_iterator iter_);

	/** Erase card from the VCard list only. Calling this method leaves
	 *	the Deck intact.
	 */
	virtual bool erase (VCard* card_);
		
	/** Remove all card references
	 */
	void clear ();

	/** Sort Card References based on their expiration time.
	 *  Those with oldest expiration dates go to the front of the list.
	 */
	void sort_by_expiration_time ();

	virtual SideSelection get_side_selection () { return m_side_selection; }
	virtual void set_side_selection (SideSelection s_) { m_side_selection=s_; }

	virtual void play_card (SideSelection s_, VDeck::cardlist_iterator& iter_);

	virtual const string&  get_name () const { return m_cdname; }
	virtual int  get_index () const { return m_index; }

	virtual void get_progress   (float& pct_done_, std::string& msg_, int idx_);
	virtual void reset_progress ();

	void set_name (int idx_);
	void remove_duplicates (); 

	/** Shuffle all subsets with equal expiration 
	 *  timestamp, preserving the general expiration order.
	 */
	void shuffle_subsets ();

	/** Dump the content of the deck to file (for troubleshooting).
	 */
	void dump_to_file ();

	/** Export sound bites to the path_ file.
	 */
	void export_sound_bits (const std::string& path_);

private:
    /// Number of the card box this deck represents
	string m_cdname;	

	/// CardDeck index 
	int m_index;

	/// A collection of cards
	cardlist_type  m_cards;

    /// The size at the moment when DeckPlayer started playing the Deck.
	u_int m_init_size;	

	/// How many cards has been already played by the DeckPlayer so far.
	u_int m_progress_idx;
};

/**-----------------------------------------------------------------------------
 * Inlined methods
 **-----------------------------------------------------------------------------
 */
inline
CardDeck::
CardDeck () 
	: 
	m_index (0),
	m_init_size (0), 
	m_progress_idx (0)
{
	trace_with_mask("CardDeck::CardDeck",GUITRACE|DECK);
}

inline
CardDeck::
~CardDeck ()
{
	trace_with_mask("CardDeck::~CardDeck",GUITRACE|DECK);
}

inline void 
CardDeck::
push_back (VCard* card_) 
{ 
	m_cards.push_back (card_); 
}

inline void 
CardDeck::
erase (cardlist_iterator iter_) 
{ 
	trace_with_mask("CardDeck::erase(iter)",GUITRACE|DECK);
	m_cards.erase (iter_); 
}

inline void 
CardDeck::
sort_by_expiration_time () 
{ 
	std::sort (m_cards.begin (), m_cards.end (), Compare_ExpTime_CardRefs ());
}

#endif /* CARD_DECK_H */
