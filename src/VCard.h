// -*- c++ -*-
//------------------------------------------------------------------------------
//                              VCard.h
//------------------------------------------------------------------------------
// $Id: VCard.h,v 1.13 2008/01/14 03:41:00 vlg Exp $
//
// Date: Apr 24 2004
//------------------------------------------------------------------------------
#ifndef VCARD_H
#define VCARD_H

#include <string>
using std::string;

#include <glibmm/ustring.h>
using Glib::ustring;

#include <assa/TimeVal.h>

#include "Granule-main.h"

class VCard 
{
public:
	VCard () : m_dirty (false), m_reversed (false) { /* no-op */ }
	virtual ~VCard () { /* no-op */ }

	virtual void set_question     (const ustring& q_) = 0;
	virtual void set_image_path   (const ustring& p_) = 0;
	virtual void set_answer       (const ustring& a_) = 0;
	virtual void set_example      (const ustring& e_) = 0;
	virtual void set_example_image_path   (const ustring& p_) = 0;
	virtual void set_alt_spelling (SideSelection side_,
								   const ustring& a_) = 0;

	virtual ustring  get_question     () const = 0;
	virtual ustring  get_image_path   () const = 0;
	virtual ustring  get_deck_path    () const = 0;
	virtual ustring  get_answer       () const = 0;
	virtual ustring  get_example      () const = 0;
	virtual ustring  get_example_image_path   () const = 0;
	virtual ustring  get_alt_spelling (SideSelection side_) const = 0;
	virtual long     get_id           () const = 0;

	virtual PathMode image_path_mode  () const = 0;

	void set_reversed (bool r_) { m_reversed = r_;   }
	bool get_reversed () const  { return m_reversed; }

	virtual void set_expiration (const ASSA::TimeVal&) { /* no-op */ }
	virtual ASSA::TimeVal get_expiration () const;

	void mark_dirty   () { m_dirty = true; }
	void mark_clean   () { m_dirty = false; }
	bool is_dirty     () const { return m_dirty; }
	string get_id_str ();
	
	/** For CardRef only, reschedule based on answer.
	 */
	virtual void reschedule (AnswerQuality answer_quality_) { /* no-op */ }

	/** CardRef returns scheduled interval.
	 */
	virtual int get_interval () const { return 0; }

	/**
	 * @param cw_ratio_ Success/Fail ratio expressed in numbers. 
	 * @return succcess ratio based on its statistics in percent form.
	 */
	virtual string get_success_ratio (string& cw_ratio_) const 
	{ 
		cw_ratio_ = "0/0"; 
		return "n/a"; 
	}

	virtual void dump () const = 0;

	/** Comparison by the expiration date
	 */
	bool operator<  (const VCard& rhs_);
	bool operator== (const VCard& rhs_);

protected:
	virtual bool less_then_expiration (const VCard& rhs_) = 0;
	virtual bool equal_expiration (const VCard& rhs_) = 0;

protected:
	/// Has the card being modified?
	bool m_dirty;

	/// Is this card backward-oriented?
	bool m_reversed;
};

inline bool
VCard::
operator< (const VCard& rhs_)
{
	return (less_then_expiration (rhs_));
}

inline bool
VCard::
operator== (const VCard& rhs_)
{
	return (equal_expiration (rhs_));
}

inline ASSA::TimeVal VCard::get_expiration () const
{
	return (ASSA::TimeVal (0));
}

#endif /* VCARD_H */
