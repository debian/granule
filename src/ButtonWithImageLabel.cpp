// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: ButtonWithImageLabel.cpp,v 1.2 2008/01/11 03:52:40 vlg Exp $
//------------------------------------------------------------------------------
//                      ButtonWithImageLabel.cpp
//------------------------------------------------------------------------------
//  Copyright (c) 2004 by Vladislav Grinchenko 
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date : Sat Apr 3 12:59:14 EST 2004
//
//------------------------------------------------------------------------------

#include "ButtonWithImageLabel.h"

void
ButtonWithImageLabel::
bil_setup ()
{
	/*= Set up button itself
	 */
	set_flags (Gtk::CAN_FOCUS);
	set_relief (Gtk::RELIEF_NORMAL);
	set_use_underline ();

	m_alignment = Gtk::manage (new Gtk::Alignment (0.5, 0.5, 0.0, 0.0));
	m_hbox = Gtk::manage (new Gtk::HBox (false, 2));

	m_label = manage (new Gtk::Label);

	m_label->set_alignment (0.5, 0.5);
	m_label->set_padding (0, 0);
	m_label->set_justify (Gtk::JUSTIFY_LEFT);
	m_label->set_use_markup (true);
	m_label->set_selectable (false);
}

void
ButtonWithImageLabel::
pack_components ()
{
	m_image->set_alignment (0.5, 0.5);
	m_image->set_padding (0, 0);
	
	m_hbox->pack_start (*m_image, Gtk::PACK_SHRINK, 0);
	m_hbox->pack_start (*m_label, Gtk::PACK_SHRINK, 0);

	m_alignment->add (*m_hbox);
	add (*m_alignment);
}

ButtonWithImageLabel::
ButtonWithImageLabel (const string& label_,
					  const Gtk::StockID& icon_id_, 
					  Gtk::IconSize icon_size_)
					  
{
	bil_setup ();

	m_image = manage (new Gtk::Image (icon_id_, icon_size_));
	m_label->set_markup (label_);

	pack_components ();
	show_all ();
}

ButtonWithImageLabel::
ButtonWithImageLabel (const string& label_,
					  const string& id_, 
					  Gtk::IconSize icon_size_)
{
	bil_setup ();

	m_image = manage (new Gtk::Image (Gtk::StockID (id_), icon_size_));
	m_label->set_markup (label_);

	pack_components ();
	show_all ();
}

void
ButtonWithImageLabel::
set_ctrl_accelerator_key (guint accel_key_, 
						  const Glib::RefPtr<Gtk::AccelGroup>& accel_group_)
{
	add_accelerator ("clicked", 
					 accel_group_, 
					 accel_key_,
					 Gdk::CONTROL_MASK, 
					 Gtk::ACCEL_VISIBLE);
}

void
ButtonWithImageLabel::
set_alt_accelerator_key (guint accel_key_, 
						 const Glib::RefPtr<Gtk::AccelGroup>& accel_group_)
{
	add_accelerator ("clicked", 
					 accel_group_, 
					 accel_key_,
					 Gdk::MOD1_MASK, 
					 Gtk::ACCEL_VISIBLE);
}
