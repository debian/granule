// -*- c++ -*-
//------------------------------------------------------------------------------
//                            PropertyBox.cpp
//------------------------------------------------------------------------------
// $Id: PropertyBox.cpp,v 1.3 2008/06/02 03:03:54 vlg Exp $
//------------------------------------------------------------------------------
//  Copyright (c) 2002-2004 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version
//  2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------
// Date: Sun May 30 10:16:48 EDT 2004
//------------------------------------------------------------------------------

#include "PropertyBox.h"

#include "Intern.h"

using Gtk::manage;
using sigc::mem_fun;

/*******************************************************************************
 Class member functions
*******************************************************************************/

PropertyBox::
PropertyBox (const std::string& title_) 
	:
	m_notebook (manage (new Gtk::Notebook)),
	m_property_changed (false)
{
	trace_with_mask("PropertyBox::PropertyBox",GUITRACE);

#ifdef IS_HILDON
	/** 
	 * Dialog box holds both vbox and action_area
	 */
	Gtk::VBox* dialog_box = manage (new Gtk::VBox (false, 2));
	add (*dialog_box);

    m_vbox = manage (new Gtk::VBox (false, 2));
	dialog_box->pack_start (*m_vbox, Gtk::PACK_EXPAND_WIDGET, 1);

	m_action_area = manage (new Gtk::HButtonBox);
    m_action_area->set_homogeneous ();
    m_action_area->set_spacing (8);
	m_action_area->set_layout (Gtk::BUTTONBOX_END);
	dialog_box->pack_start (*m_action_area, Gtk::PACK_SHRINK, 1);
#endif

#ifdef IS_HILDON
	set_size_request (672, 396);
#else
	set_size_request (-1,-1);
#endif

	set_title (title_.c_str ());

	/*= Setup buttons.
	 */
	m_close_button = manage (
		new ButtonWithImageLabel (_("<u>C</u>lose"), "gtk-close"));


	m_apply_button = manage (
		new ButtonWithImageLabel (_("<u>A</u>pply"), "gtk-apply"));

	m_close_button->set_alt_accelerator_key (GDK_c, get_accel_group ());
	m_apply_button->set_alt_accelerator_key (GDK_a, get_accel_group ());
	m_apply_button->set_sensitive (false);

	m_close_button->signal_clicked ().connect (
		mem_fun (*this, &PropertyBox::on_close_clicked));

	m_apply_button->signal_clicked ().connect (
		mem_fun (*this, &PropertyBox::on_apply_clicked));

	get_action_area ()->pack_start (*m_close_button);
	get_action_area ()->pack_start (*m_apply_button);

 	signal_delete_event ().connect (
 		sigc::mem_fun (*this, &PropertyBox::on_delete_window_clicked));

	m_notebook->set_show_border ();	// to draw a bevel around the pages
	m_notebook->set_scrollable ();  // set the tab label area scrollable

	get_vbox ()->pack_start (*m_notebook);

	show_all_children ();
}

gint
PropertyBox::
append_page (Gtk::Widget& child_, const std::string& tab_label_)
{
	trace_with_mask("PropertyBox::append_page",GUITRACE);
	m_notebook->pages ().push_back (
		Gtk::Notebook_Helpers::TabElem (child_, tab_label_));
	return 0;
}

/**
 * Make Apply button sensitive and remember that something 
 * has changed.
 */
void
PropertyBox::
changed ()
{
	trace_with_mask("PropertyBox::changed",GUITRACE);

	m_apply_button->set_sensitive (true);
	m_property_changed = true;
}

void
PropertyBox::
on_apply_clicked ()
{
	trace_with_mask("PropertyBox::on_apply_clicked",GUITRACE);

	if (m_property_changed) {
		apply_impl ();
		m_property_changed = false;
	}
	m_apply_button->set_sensitive (false);
}

void
PropertyBox::
on_close_clicked ()
{
	trace_with_mask("PropertyBox::on_close_clicked",GUITRACE);
	close_impl ();
	m_apply_button->set_sensitive (false);
}

/**
 * @return true - top other handlers from being invoked for the event.
 */
bool
PropertyBox::
on_delete_window_clicked (GdkEventAny* /*event_*/)
{
    trace_with_mask("PropertyBox::on_delete_window_clicked",GUITRACE);

	on_close_clicked ();
    return true;  
}
