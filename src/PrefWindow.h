// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: PrefWindow.h,v 1.14 2008/06/02 03:03:54 vlg Exp $
//------------------------------------------------------------------------------
//                            PrefWindow.h
//------------------------------------------------------------------------------
//  Copyright (c) 2004 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date   : Sun May 30 21:09:23 EDT 2004
//
//------------------------------------------------------------------------------
#ifndef PREF_WINDOW_H
#define PREF_WINDOW_H

#include <gtkmm/main.h>

#include "Granule-main.h"
#include "GrappConf.h"
#include "PropertyBox.h"
#include "GeneralPref.h"
#include "SchedulingPrefs.h"
#include "SoundPrefs.h"
#include "GeometryPrefs.h"
#include "AppearancePrefs.h"

class PrefWindow : public PropertyBox
{
public:
    PrefWindow (Gtk::Widget& parent_);
    ~PrefWindow();
    
    bool run ();

    virtual void apply_impl ();
    virtual void close_impl ();

    void changed_cb ();
    void ok_cb      ();
    void apply_cb   ();
    void cancel_cb  ();

private:
    void stop ();

private:
	GeneralPref      m_general_pref;
	AppearancePrefs  m_appearance_pref;
	SchedulingPrefs  m_scheduling_pref;
	SoundPrefs       m_sound_pref;
	GeometryPrefs    m_geometry_pref;

	bool m_ignore_callbacks;
	bool m_changed;
};

inline
PrefWindow::
~PrefWindow()
{
    trace_with_mask("PrefWindow::~PrefWindow",GUITRACE);
}

#endif /* PREF_WINDOW_H */
