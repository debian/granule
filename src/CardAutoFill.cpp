// -*- c++ -*-
//------------------------------------------------------------------------------
//                              CardAutoFill.cpp
//------------------------------------------------------------------------------
// $Id: CardAutoFill.cpp,v 1.7 2008/06/02 03:03:53 vlg Exp $
//
//  Copyright (c) Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//
// Date: Oct 24, 2007
//------------------------------------------------------------------------------

#include <gtkmmconfig.h>
#include <gtkmm/label.h>
#include <gtkmm/box.h>
#include <gtkmm/alignment.h>
#include <gtkmm/button.h>
#include <gtkmm/buttonbox.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/table.h>

#include "Granule.h"
#include "CardAutoFill.h"
#include "ButtonWithImageLabel.h"

#include "Intern.h"				/* always last */

using Gtk::manage;

#define SET_CV_LABEL(l) \
	l->set_alignment  (0.5,0.5);  \
	l->set_padding    (0,0);      \
	l->set_justify    (Gtk::JUSTIFY_LEFT); \
	l->set_line_wrap  (false);    \
	l->set_use_markup (false);    \
	l->set_selectable (false);   


static void 
set_cv_scrollwin (Gtk::ScrolledWindow& win_, Gtk::Widget& widget_)
{
	win_.set_flags       (Gtk::CAN_FOCUS);
	win_.set_shadow_type (Gtk::SHADOW_NONE);
	win_.set_policy      (Gtk::POLICY_ALWAYS, Gtk::POLICY_ALWAYS); 

#ifdef GLIBMM_PROPERTIES_ENABLED 
	win_.property_window_placement ().set_value (Gtk::CORNER_TOP_LEFT);
#else
	win_.set_property ("window_placement", Gtk::CORNER_TOP_LEFT);
#endif 

    /** Defeat Hildons' default theme and provide
	 *  our own visual border around Text Entry
	 */
#ifdef IS_HILDON
	Gtk::Frame* f = manage (new Gtk::Frame ());
	f->set_border_width (1);
    f->set_shadow_type  (Gtk::SHADOW_ETCHED_OUT);
	f->set_label_align  (0, 0);
	f->add (widget_);
	win_.add (*f);
#else
	win_.add (widget_);
#endif
}

/**
 * Enclose Gtk::TextView in a visible box. Some default themes (hildon)
 * blend the surrounding, so it is hard to see where the editable
 * region lies.
 */
static Gtk::Widget*
set_text_border (Gtk::Widget* widget_)
{
#ifdef IS_HILDON
	Gtk::Frame* frame = manage (new Gtk::Frame ());
	frame->set_border_width (1);
    frame->set_shadow_type  (Gtk::SHADOW_ETCHED_OUT);
	frame->set_label_align  (0, 0);
	frame->add (*widget_);
	return (frame);
#else
	return (widget_);
#endif
}


/*------------------------------------------------------------------------------
** CardAutoFill class implementation
**------------------------------------------------------------------------------
*/
CardAutoFill::
CardAutoFill ()
	: 
	m_response (Gtk::RESPONSE_OK),
	m_container (false, 0),
	m_table (3, 2, false)
{
	trace_with_mask("CardAutoFill::CardAutoFill",GUITRACE);

	/** Gtk::Window settings
	 */
	set_title ("Card Auto Fill");

	/** Dialog containers
	 */
	Gtk::VBox* vbox_all = Gtk::manage (new Gtk::VBox (false, 6));

#ifdef IS_HILDON
	/** 
	 * hildon-specific
	 */
#elif IS_PDA
    /** 
	 *  Make DeckPlayer expand to fill the whole screen. The screen size
	 *  is 240x320, but oddly enough, the WM still leave a few pixels
	 *  around. BTW, set_size_request() doesn't work - it makes the dialog
	 *  shorter then the MainWindow by the order of the titlebar height.
	 *  And, stay away from setting transient on parent.
	 */
	Gdk::Geometry dp_geometry =	{ 240, 320,	240, 320, 240, 320, -1, -1, -1, -1};
	set_geometry_hints (*this, dp_geometry, 
						Gdk::HINT_MIN_SIZE | 
						Gdk::HINT_MAX_SIZE | 
						Gdk::HINT_BASE_SIZE);

#else  // Desktop
    set_resizable (true);
	Gdk::Rectangle geom (0, 0, 493, 577);
	Gdk::Geometry answer_box_geometry =  { 
		400,  /* min_width  */    400,        /* min_height  */
		-1,	  /* max_width; */     -1,	      /* max_height  */
		493,  /* base_width */    577,        /* base_height */
		-1,	  /* width_inc  */     -1,	      /* height_inc  */
		-1,	  /* min_aspect (width/height) */
		-1	  /* max_aspect (width/height) */
	};
	set_geometry_hints (*this, 
						answer_box_geometry, 
						Gdk::HINT_MIN_SIZE | Gdk::HINT_BASE_SIZE);

#endif	// !(IS_PDA)

#if !defined(IS_HILDON)
	set_modal (true);
#endif

	/** Explanation text
	 */
	Gtk::Label* help_label = Gtk::manage (new Gtk::Label);

    help_label->set_alignment  (0.5,0.5);
    help_label->set_padding    (8, 8);
    help_label->set_justify    (Gtk::JUSTIFY_LEFT);
    help_label->set_line_wrap  (true);
    help_label->set_selectable (false);
    help_label->set_use_markup (true);

	help_label->set_markup (
		_("If enabled, a corresponding field in  <tt>CardView</tt>\n"
		  "dialog will be pre-filled with the pattern text.\n"
		  "\n"
		  "The only variable parameter of the pattern text\n"
		  "is the card number and it can be specified with\n"
		  "<span foreground=\"brown\">%d</span> parameter.\n"
		  "\n"
		  "The <b>Apply</b> button applies the patterns\n"
		  "to each existing card in the Deck. Empty patter\n"
		  "fields are ignored.\n"));

	Gtk::EventBox* help_evbox = manage (new Gtk::EventBox);
	help_evbox->add (*help_label);
	Gdk::Color color_white ("white");
	help_evbox->modify_bg (Gtk::STATE_NORMAL, color_white);

	m_container.pack_start (*help_evbox, Gtk::PACK_SHRINK, 10);

	Gtk::HSeparator* separator = Gtk::manage (new Gtk::HSeparator);
	m_container.pack_start (*separator, Gtk::PACK_EXPAND_WIDGET, 2);

	/** Frame that holds checkbutton
	 *
	 */
	m_enable_chkbutton.set_flags        (Gtk::CAN_FOCUS); 
    m_enable_chkbutton.set_relief       (Gtk::RELIEF_NORMAL); 
    m_enable_chkbutton.set_border_width (1); 
    m_enable_chkbutton.set_alignment    (0.1, 0);
    m_enable_chkbutton.set_mode         (true); // Draw [ ] Label
	m_enable_chkbutton.set_label (_("Enable auto-fill mode"));

	m_container.pack_start (m_enable_chkbutton);

	/** Frame holds text entries
	 */
	Gtk::Label* empty_label = Gtk::manage(new Gtk::Label);
    empty_label->set_alignment  (0.5,0.5);
    empty_label->set_padding    (0,0);
    empty_label->set_justify    (Gtk::JUSTIFY_LEFT);
    empty_label->set_line_wrap  (false);
    empty_label->set_use_markup (false);
    empty_label->set_selectable (false);

	m_frame.set_border_width (4);
	m_frame.set_shadow_type (Gtk::SHADOW_ETCHED_IN);
	m_frame.set_label_align  (1, 1);
	m_frame.set_label_widget (*empty_label);

	/** ASF text entry
	 */
	Gtk::Label*     asf_label;
	Gtk::Alignment* asf_alignment;

	asf_label = Gtk::manage (new Gtk::Label (_("ASF")));
	asf_alignment  = Gtk::manage (new Gtk::Alignment (0, 0.5, 0, 0));
	
	SET_CV_LABEL(asf_label);
	asf_alignment->add (*asf_label);

	/** Back text entry
	 */
	Gtk::Label*     back_label;
	Gtk::Alignment* back_alignment;

	back_label = Gtk::manage (new Gtk::Label (_("Back")));
	back_alignment  = Gtk::manage (new Gtk::Alignment (0, 0.5, 0, 0));
	
	SET_CV_LABEL(back_label);
	back_alignment->add (*back_label);

	/** ASB entry
	 */
	Gtk::Label*     asb_label;
	Gtk::Alignment* asb_alignment;

	asb_label = Gtk::manage (new Gtk::Label (_("ASB")));
	asb_alignment  = Gtk::manage (new Gtk::Alignment (0, 0.5, 0, 0));
	
	SET_CV_LABEL(asb_label);
	asb_alignment->add (*asb_label);

	/** Pack all into the table
	 */
	m_table.set_border_width (4);
	m_table.set_row_spacings (2);
	m_table.set_col_spacings (2);
	m_table.set_name ("Entries");

	/*************************************************************
	 *  Table holds elements together:
	 *
	 *    0                1                  2
	 *  0 .----------------+------------------.
	 *    | vbox(label)    | scrollwin(entry) | Front Alt Spell
	 *  1 '----------------+------------------'
	 *    | vbox(label)    | scrollwin(text)  | Back 
	 *  2 '----------------+------------------'
	 *    | vbox(label)    | scrollwin(text)  | Front Alt Spell
	 *  3 `----------------+------------------'
	 *
	 *************************************************************/

	/*-----------------------------------------------------------*/
	/*                               left, right, top, bottom    */
	/*-----------------------------------------------------------*/
	m_table.attach (*asf_alignment, 0, 1, 0, 1, 
					 Gtk::FILL, Gtk::FILL, 0, 0);

	m_table.attach (m_asf_entry, 1, 2, 0, 1, 
					 Gtk::FILL|Gtk::EXPAND, Gtk::FILL|Gtk::EXPAND, 0, 0);

	m_table.attach (*back_alignment, 0, 1, 1, 2,
					 Gtk::FILL, Gtk::FILL, 0, 0);

	m_table.attach (m_back_entry, 1, 2, 1, 2,
					 Gtk::FILL|Gtk::EXPAND, Gtk::FILL|Gtk::EXPAND, 0, 0);

	m_table.attach (*asb_alignment, 0, 1, 2, 3,
					 Gtk::FILL, Gtk::FILL, 0, 0);

	m_table.attach (m_asb_entry, 1, 2, 2, 3,
					 Gtk::FILL|Gtk::EXPAND, Gtk::FILL|Gtk::EXPAND, 0, 0);

	m_frame.add (m_table);
	m_container.pack_start (m_frame);

	Gtk::ScrolledWindow* scrollw = Gtk::manage (new Gtk::ScrolledWindow);
	set_cv_scrollwin (*scrollw, m_container);

	/**--------------------------------------------------------------------**
	 * Setup and pack control buttons:   [Apply] [Cancel] [OK]              *
	 **--------------------------------------------------------------------**/

	/** <Cancel>
	 */
	m_cancel_button = manage (
		new ButtonWithImageLabel (_("<u>C</u>ancel"), "gtk-cancel",
								  Gtk::IconSize (4)));
	m_cancel_button->set_alt_accelerator_key (GDK_c, get_accel_group ());
		
	/** <Apply>
	 */
	m_apply_button = manage (
		new ButtonWithImageLabel (_("<u>A</u>pply"), "gtk-apply",
								  Gtk::IconSize (4)));
	m_apply_button->set_alt_accelerator_key (GDK_a, get_accel_group ());
	
	/** <OK>
	 */
	m_ok_button = manage (
		new ButtonWithImageLabel (_("<u>O</u>k"), "gtk-ok",
								  Gtk::IconSize (4)));
	m_ok_button->set_alt_accelerator_key (GDK_o, get_accel_group ());
	
	/**--------------**
	 ** Pack Buttons **
	 **--------------**
	 */
	Gtk::HBox* hbbox = Gtk::manage(new Gtk::HBox(false, 0));
	Gtk::HSeparator* hseparator = manage (new Gtk::HSeparator);

#if !defined(IS_PDA)
	Gtk::HButtonBox* hbuttonbox = manage (new Gtk::HButtonBox);

    hbuttonbox->set_homogeneous (false);
    hbuttonbox->set_spacing (4);
	hbuttonbox->set_layout (Gtk::BUTTONBOX_END);
	hbuttonbox->set_name ("AutoFill_ButtonBox");

	hbuttonbox->pack_end (*m_apply_button,  Gtk::PACK_SHRINK, 0);
	hbuttonbox->pack_end (*m_cancel_button, Gtk::PACK_SHRINK, 0);
	hbuttonbox->pack_end (*m_ok_button,     Gtk::PACK_SHRINK, 0);

	hbbox->pack_start (*hbuttonbox);
#else
	hbbox->set_spacing (4);
	hbbox->pack_end (*m_apply_button,  Gtk::PACK_SHRINK, 0);
	hbbox->pack_end (*m_cancel_button, Gtk::PACK_SHRINK, 0);
	hbbox->pack_end (*m_ok_button,     Gtk::PACK_SHRINK, 0);
#endif	

	vbox_all->pack_start (*scrollw,    Gtk::PACK_EXPAND_WIDGET, 0);
	vbox_all->pack_start (*hseparator, Gtk::PACK_SHRINK, 0);
	vbox_all->pack_start (*hbbox,      Gtk::PACK_SHRINK, 0);

	add (*vbox_all);

    /** 
	 *  Both PDAs and Maemo have a narrow screen and thus requires
	 *  smart packing.
	 */

	/**---------------**
	 ** Setup signals **
	 **---------------**/
	m_enable_chkbutton.signal_toggled ().connect (
		mem_fun (*this, &CardAutoFill::on_enable_check_clicked));

	m_cancel_button->signal_clicked ().connect (
		mem_fun (*this, &CardAutoFill::on_cancel_clicked));

	m_apply_button->signal_clicked ().connect (
		mem_fun (*this, &CardAutoFill::on_apply_clicked));

	m_ok_button->signal_clicked ().connect (
		mem_fun (*this, &CardAutoFill::on_ok_clicked));

 	signal_delete_event ().connect (
 		sigc::mem_fun (*this, &CardAutoFill::on_delete_window_clicked));

	/** Arm the checkbutton
	 */
	mark_enabled (false);

	show_all ();
}

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//                              Callbacks
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

int
CardAutoFill::
run (Gtk::Window& parent_)
{
	trace_with_mask("CardAutoFill::run",GUITRACE);

	Granule::move_child_to_parent_position (parent_, *this);

	show ();
	present ();
	gtk_main ();

	Granule::move_parent_to_child_position (parent_, *this);

	return (m_response);
}

void
CardAutoFill::
on_apply_clicked  ()
{
	trace_with_mask("CardAutoFill::on_apply_clicked",GUITRACE);

	m_response = Gtk::RESPONSE_APPLY;
	hide ();
	gtk_main_quit ();
}

/**
 * @return true - top other handlers from being invoked for the event.
 */
bool
CardAutoFill::
on_delete_window_clicked (GdkEventAny* /*event_*/)
{
    trace_with_mask("CardAutoFill::on_delete_window_clicked",GUITRACE);

	on_cancel_clicked ();
    return true;  
}

void
CardAutoFill::
on_cancel_clicked  ()
{
	trace_with_mask("CardAutoFill::on_cancel_clicked",GUITRACE);
	
	m_response = Gtk::RESPONSE_CANCEL;
	hide ();
	gtk_main_quit ();
}

void
CardAutoFill::
on_ok_clicked  ()
{
	trace_with_mask("CardAutoFill::on_ok_clicked",GUITRACE);
	
	m_response = Gtk::RESPONSE_OK;
	hide ();
	gtk_main_quit ();
}

void
CardAutoFill::
on_enable_check_clicked  ()
{
	trace_with_mask("CardAutoFill::on_enable_check_clicked",GUITRACE);

	enable_input (m_enable_chkbutton.get_active ());
}

void
CardAutoFill::
enable_input (bool enabled_)
{
	if (enabled_) {
		m_frame.set_sensitive (true);
		m_apply_button->set_sensitive (true);
	}
	else {
		m_frame.set_sensitive (false);
		m_apply_button->set_sensitive (false);
	}
}
