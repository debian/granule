// -*- c++ -*-
//------------------------------------------------------------------------------
//                              GrappConf.h
//------------------------------------------------------------------------------
// $Id: GrappConf.h,v 1.20 2008/07/07 02:49:24 vlg Exp $
//------------------------------------------------------------------------------
//  Copyright (c) 2004-2006 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version
//  2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------
// 01/03/2004 VLG  Created
//------------------------------------------------------------------------------
#ifndef GRAPPCONF_H
#define GRAPPCONF_H

#include <string>
#include <deque>
using std::deque;
using std::string;

#include <pangomm/fontdescription.h>

#include <gdkmm/rectangle.h>

#include <assa/Singleton.h>
#include <assa/TimeVal.h>
#include <assa/IniFile.h>
class ASSA::IniFile;

#include "Granule-main.h"

#include "AppearanceDB.h"
#include "FontsDB.h"
#include "TextColorsDB.h"
#include "AutoFillDB.h"

//------------------------------------------------------------------------------
// Record when to review cards in box I (I=DAYS/WEEKS)
//------------------------------------------------------------------------------
struct SchedReview 
{
    string m_days;			// number of days
    string m_weeks;			// number of weeks
    string m_hours;
    string m_mins;

    SchedReview () : m_days ("0"), m_weeks ("0"), m_hours ("0"), m_mins ("0")
	{ /* no-op */ }

    ASSA::TimeVal m_tv;		// time in seconds

    string days  () const { return m_days;  }
    string weeks () const { return m_weeks; }
    string hours () const { return m_hours; }
    string mins  () const { return m_mins;  }

    void   days  (const string& d_) { m_days  = d_; }
    void   weeks (const string& w_) { m_weeks = w_; }
    void   hours (const string& h_) { m_hours = h_; }
    void   mins  (const string& m_) { m_mins  = m_; }

    void   set_delay (const ASSA::TimeVal& load_time_,
					  int secs_in_day_, 
					  int secs_in_week_);
    void   dump (int idx_);
};

//------------------------------------------------------------------------------
// GrappConf - persistent storage via INI file.
//------------------------------------------------------------------------------
class GrappConf : public ASSA::Singleton<GrappConf>
{
public:
    typedef deque<string> DHList; // document history list
    typedef DHList::iterator DHList_Iter;
    typedef DHList::const_iterator DHList_CIter;

public:
    GrappConf ();
    ~GrappConf ();

    void save_config ();
    void load_config (int secs_in_day_, int secs_in_week_);
    void dont_save_project () { m_dont_save_project = true; }

    // Accessors
    const string& get_proj_name    () const { return m_proj_name;    }
    const string& get_proj_path    () const { return m_path_name;    }
    const string& get_filesel_path () const { return m_filesel_path; }

    void set_proj_name    (const string& fname_);
    void set_filesel_path (const string& fspath_);

    /// Handling card duplicates
    bool get_remove_duplicates () const  { return m_remove_dups; }
    void set_remove_duplicates (bool b_) { m_remove_dups = b_;   }

    /// Configure size of the history list
    int  get_history_size () const { return m_history_size; }
    void set_history_size (int v_) { m_history_size = v_;   }

    /// Record paths to Deck files as relative?
    bool with_relpaths () const  { return m_with_relative_paths; }
    void with_relpaths (bool b_) { m_with_relative_paths = b_;   }

    /** Returns an expiration date that should be assigned to a 
	 *	card as it is placed in CardDeck # idx_. 
	 *
	 *  @param idx_ CardDeck number.
	 *	@return Card expiration time.
	 */
    ASSA::TimeVal get_expiration (int idx_);

    /** Access to the scheduling settings by index.
     */
    string get_sched_review_days  (int idx_) { return m_sched [idx_].days  (); }
    string get_sched_review_weeks (int idx_) { return m_sched [idx_].weeks (); }
    string get_sched_review_hours (int idx_) { return m_sched [idx_].hours (); }
    string get_sched_review_mins  (int idx_) { return m_sched [idx_].mins  (); }

    void set_sched_review_days  (int idx_, const string& s_);
    void set_sched_review_weeks (int idx_, const string& s_);
    void set_sched_review_hours (int idx_, const string& s_);
    void set_sched_review_mins  (int idx_, const string& s_);

	long secs_in_day () const { return m_secs_in_day; }

    /// Return timestamp when the program was started.
    ASSA::TimeVal get_load_time () const { return m_load_time; }

    /// Get Real User Name
    const char* get_user_name () const;

    /** Check Deck side recorded. If Deck is not found, return FRONT
     */
    SideSelection get_flipside_history (const string& fname_);
    void set_flipside_history (const string& fname_, SideSelection side_);

	/** Text colors
	 */
	TextColorsDB& text_colors_db () { return m_text_colors_db; }

    /** Text fonts settings
     */
	FontsDB& fonts_db () { return m_fonts_db; }

	/** Text alignment controls
	 */
	AppearanceDB& app_db () { return m_appearance; }

	/** Card AutoFill database is not archived, still hosted here
	 */
	AutoFillDB& autofill_db () { return m_autofill_db; }

    /** Windows geometry
     */
    Gdk::Rectangle get_main_window_geometry () { return m_mwin_geometry;    }
    Gdk::Rectangle get_deck_player_geometry () { return m_dplyr_geometry;   }
    Gdk::Rectangle get_card_view_geometry   () { return m_crdview_geometry; }

    void set_main_window_geometry (int width_, int height_);
    void set_deck_player_geometry (int width_, int height_);
    void set_card_view_geometry   (int width_, int height_);

    /** MainWindow position on a desktop
     */
    void set_win_position (int  x_, int  y_)  { m_root_x = x_; m_root_y = y_; }
    void get_win_position (int& x_, int& y_)  { x_ = m_root_x; y_ = m_root_y; }

    /** Aspect ratio of the DeckPlayer window
     */
    bool keep_deck_player_aspect () { return m_dplyr_keep_aspect; }
    void keep_deck_player_aspect (bool b_) { m_dplyr_keep_aspect = b_; }

    Gdk::Rectangle&  get_deck_player_aspect (){ return m_dplyr_aspect; }
    void set_deck_player_aspect (int width_, int height_);

    /** Command to use to play sound bites.
     */
    const char* get_snd_player_cmd   () const;
    const char* get_snd_player_args  () const;
    const char* get_snd_archive_path () const;

    void set_snd_player_cmd   (const string& s_) { m_snd_player_cmd   = s_; }
    void set_snd_player_args  (const string& s_) { m_snd_player_args  = s_; }
    void set_snd_archive_path (const string& s_) { m_snd_archive_path = s_; }

    /** History-related functions
     */
    string        add_document_history    ();
    void          remove_document_history (const string& fullpath_);
    const DHList& get_document_history    () const { return m_history; }

    /** Dump object's state
     */
    void dump () const;
    void dump_document_history () const;

    /** If true, ignore key_press events in DeckPlayer. This allows
	 *  IM with conflicting key strokes to be still functional.
	 */
    bool disable_key_shortcuts () const  { return m_disable_key_shortcuts; }
    void disable_key_shortcuts (bool b_) { m_disable_key_shortcuts = b_;   }

    /** Auto-play sound bite for each card.
     */
    bool auto_pronounce () const  { return m_auto_pronounce; }
    void auto_pronounce (bool b_) { m_auto_pronounce = b_;   }

	/** Show/Hide Verify Answer control of DeckPlayer dialog.
	 */
	bool show_va_control () const { return m_show_va_control; }
	void show_va_control (bool b_) { m_show_va_control = b_; }

	/** Show/Hide Statistics Information control of DeckPlayer dialog.
	 */
	bool show_si_control () const { return m_show_si_control; }
	void show_si_control (bool b_) { m_show_si_control = b_; }

	/** Show/Hide DeckPlayer's sidebar navigation controls
	 */
	bool show_sidebar () const { return m_show_sidebar_control; }
	void show_sidebar (bool b_) { m_show_sidebar_control = b_; }

	/** DeckPlayer's sidebar navigation controls location
	 */
	SidebarLocation sidebar_location () const { return m_sidebar_location; }
	void sidebar_location (SidebarLocation l_) { m_sidebar_location = l_; }

	/** Return Sidebar width in pixels. This option can only
	 *  be set manually in the configuration file.
	 */
	int sidebar_width () const { return m_sidebar_width; }

private:
    GrappConf (const GrappConf&);	         // copy constructor
    GrappConf& operator= (const GrappConf&); // copy assignment: cleanup & copy

    typedef std::pair<string, SideSelection> sidesel_t;
    typedef std::vector<sidesel_t> flipside_t;
    typedef flipside_t::iterator flipside_iter_t;

    void    str_to_geometry (const string& src_,
							 Gdk::Rectangle& allocation_);
    const char* geometry_to_str (const	Gdk::Rectangle& allocation_);

private:
    string m_proj_name;			// Project name
    string m_path_name;			// Path to configuration file
    string m_filesel_path;		// Path of the last FileSelection dialog
    bool   m_dont_save_project;	        // Don't save project file

	/** Text colors database is the data model.
	 */
	TextColorsDB m_text_colors_db;

	/** Fonts database is the data model.
	 */
	FontsDB m_fonts_db;

	/** Text appearance database is the data model.
	 */
	AppearanceDB m_appearance;

	/** Card AutoFill database is the data model.
	 */
	AutoFillDB m_autofill_db;

	/** All things geometry
	 */
    Gdk::Rectangle  m_mwin_geometry;
    Gdk::Rectangle  m_dplyr_geometry;
    Gdk::Rectangle  m_crdview_geometry;

    bool m_dplyr_keep_aspect;
    Gdk::Rectangle  m_dplyr_aspect; 

	/** Sound-related settings
	 */
    string m_snd_player_cmd;	        // Sound player string
    string m_snd_player_args;           // Sound player arguments formatted 
                                        // string.
    string m_snd_archive_path;		// Path to dictionary sound archive
    string m_user_name;			// Real user name from /etc/passwd

    DHList         m_history;	        // CardFile history list.
    int            m_history_size;      // Maximum size of the history list.

    ASSA::IniFile* m_config;

    flipside_t     m_flipside_history;	// Which side to show for each Deck.
    bool           m_remove_dups;       // Purge CardDeck of duplicate VCards
    bool           m_with_relative_paths; // Record relative paths to the Decks.

    ASSA::TimeVal  m_load_time;	        // Time the program has been loaded
    SchedReview    m_sched [CARDBOXES_MIN];	// Review schedule
    long           m_secs_in_day;       // Seconds in a day
    long           m_secs_in_week;      // Seconds in a week

    int            m_root_x;	        // X-coordinate of MainWindow on desktop
    int            m_root_y;            // Y-coordinate of MainWindow on desktop

    bool           m_disable_key_shortcuts; // No keyboard navigation
    bool           m_auto_pronounce;    // Auto-pronounce each card

	/** Change appearance of DeckPlayer dialog.
	 *  These settings are global.
	 */
	bool           m_show_va_control;  // Verify Answer control
	bool           m_show_si_control;  // Statistics Info control

	bool           m_show_sidebar_control;  // Show sidebar controls

    SidebarLocation m_sidebar_location;
    int             m_sidebar_width;
};

// Global locator

#define CONFIG GrappConf::get_instance()

//------------------------------------------------------------------------------
// Inlines
//------------------------------------------------------------------------------
//

/** Windows geometry
 */
inline void 
GrappConf::set_main_window_geometry (int width_, int height_)
{
    DL((GEOM,"set_main_window_geometry: w=%d, h=%d\n", width_, height_));
    m_mwin_geometry.set_width  (width_);
    m_mwin_geometry.set_height (height_);
}

inline void 
GrappConf::set_deck_player_geometry (int width_, int height_)
{
    DL((GEOM,"set_deck_player_geometry: w=%d, h=%d\n", width_, height_));
    m_dplyr_geometry.set_width  (width_);
    m_dplyr_geometry.set_height (height_);
}

inline void 
GrappConf::set_card_view_geometry (int width_, int height_)
{
    DL((GEOM,"set_cardview_geometry: w=%d, h=%d\n", width_, height_));
    m_crdview_geometry.set_width  (width_);
    m_crdview_geometry.set_height (height_);
}

/** Aspect ratio of the DeckPlayer window
 */
inline void 
GrappConf::set_deck_player_aspect (int width_, int height_)
{
    m_dplyr_aspect.set_width  (width_);
    m_dplyr_aspect.set_height (height_);
}

/** Sounds playback
 */
inline const char* 
GrappConf::get_snd_player_cmd () const 
{ 
    return m_snd_player_cmd.c_str ();
}

inline const char* 
GrappConf::get_snd_player_args() const 
{ 
    return m_snd_player_args.c_str();
}

inline const char* 
GrappConf::get_snd_archive_path () const 
{ 
    return m_snd_archive_path.c_str (); 
}

inline const char* 
GrappConf::get_user_name () const 
{ 
    return m_user_name.c_str (); 
}

inline ASSA::TimeVal 
GrappConf::get_expiration (int idx_)
{
    return (m_sched [idx_].m_tv);
}

inline void 
GrappConf::set_sched_review_days  (int idx_, const string& s_) 
{ 
    m_sched [idx_].days (s_); 
}

inline void 
GrappConf::set_sched_review_weeks (int idx_, const string& s_) 
{ 
    m_sched [idx_].weeks (s_); 
}

inline void 
GrappConf::set_sched_review_hours (int idx_, const string& s_) 
{ 
    m_sched [idx_].hours (s_); 
}

inline void 
GrappConf::set_sched_review_mins (int idx_, const string& s_) 
{ 
    m_sched [idx_].mins (s_); 
}
    
#endif /* CONFIG_H */
