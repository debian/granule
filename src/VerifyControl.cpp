// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: VerifyControl.cpp,v 1.9 2008/08/03 03:56:41 vlg Exp $
//------------------------------------------------------------------------------
//                            VerifyControl.cpp
//------------------------------------------------------------------------------
//  Copyright (c) 2008 by Vladislav Grinchenko 
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date   : Sun Jan  6 2008
//
//------------------------------------------------------------------------------

#include <gtkmm/box.h>
#include <gtkmm/buttonbox.h>
#include <gtkmm/main.h>

using sigc::bind;
using sigc::mem_fun;

#ifdef IS_HILDON
#   include <hildonmm.h>
#endif

#include "Granule-main.h"
#include "Granule.h"
#include "MainWindow.h"
#include "Deck.h"
#include "DeckPlayer.h"
#include "VerifyControl.h"

#include "Intern.h"

static const gchar* states [] = { "VC_START", "VC_ARMED", "VC_VALIDATED" };

/*******************************************************************************
 *  TestLineEntry Member Functions                                             *
 *******************************************************************************
 */
TestLineEntry::
TestLineEntry ()
 	:
	Gtk::HBox (0, 0),
 	m_adjustment (1, 0, 10)		/* (default value, lower, upper) */
{
	m_spin_entry.set_width_chars (2);
	m_spin_entry.set_max_length  (2);
	m_spin_entry.set_adjustment  (m_adjustment);
	m_spin_entry.unset_flags (Gtk::CAN_FOCUS);

	pack_start (m_spin_entry, Gtk::PACK_SHRINK);

	m_spin_entry.signal_value_changed ().connect (
		mem_fun (this, &TestLineEntry::on_entry_changed));
}

/*******************************************************************************
 *                       VerifyControl Member Functions                        *
 *                                                                             *
 * Widget composition:                                                         *
 * ==================                                                          *
 *   Table                                                                     *
 *     +--hbox1                                                                *
 *     |    +--Entry      (m_entry)       <-- modify_base/text                 *
 *     |    +--Button     (m_button)                                           *
 *     |    +--Label      (blank1)                                             *
 *     |                                                                       *
 *     +--hbox2                                                                *
 *          +--EventBox   (m_report_box)                                       *
 *          |    +--Label (m_report)                                           *
 *          +--Label      (blank2)                                             *
 *                                                                             *
 *******************************************************************************
 */
VerifyControl::
VerifyControl (DeckPlayer& parent_) 
	: 
	Gtk::Table  (2, 2, false),
	m_parent    (parent_),
	m_state     (VC_START),
	m_red       ("red"),
	m_green     ("MediumSeaGreen"),
	m_white     ("white"),
	m_black     ("black"),
	m_test_line (NULL),
	m_line_num  (1)
	
{
    trace_with_mask("VerifyControl::VerifyControl", GUITRACE);

	Gtk::Table* table = this;

	m_report = Gtk::manage (new Gtk::Label);

	m_report->set_alignment  (0, 0.5);
	m_report->set_padding    (0,0);
	m_report->set_justify    (Gtk::JUSTIFY_FILL);
	m_report->set_line_wrap  (false);
	m_report->set_use_markup (false);
	m_report->set_selectable (false);
	m_report->set_width_chars (11);
#if !defined (IS_PDA)
	m_report->set_size_request (80, -1); // width, height
#endif
	m_report->set_text (clabels [EMPTY]);
	m_report->set_sensitive (false);

	m_report_box = Gtk::manage (new Gtk::EventBox);
	
    m_entry = Gtk::manage (new Gtk::Entry);
    m_entry->set_flags       (Gtk::CAN_FOCUS);
    m_entry->set_visibility  (true);
    m_entry->set_editable    (true);
    m_entry->set_max_length  (0);
    m_entry->set_text        ("");
    m_entry->set_has_frame   (true);
    m_entry->set_width_chars (20);
    m_entry->set_activates_default (false);
	m_entry->modify_font     (CONFIG->fonts_db ().get_input_font ());

	for (int i = 0; i < LABELS_SZ; i++) {
		m_label [i] = Gtk::manage (new Gtk::Label (clabels [i]));
		m_label [i]->set_alignment  (0, 0.5);
		m_label [i]->set_padding    (0,0);
		m_label [i]->set_justify    (Gtk::JUSTIFY_FILL);
		m_label [i]->set_line_wrap  (false);
		m_label [i]->set_use_markup (false);
		m_label [i]->set_selectable (false);
	}
	m_button = Gtk::manage  (new Gtk::Button);
    m_button->set_flags     (Gtk::CAN_FOCUS);
    m_button->set_relief    (Gtk::RELIEF_NORMAL);
	m_button->set_label     (clabels [CHECK_ANSWER]);
	m_button->set_sensitive (false);

	Gtk::HBox* hbox1  = manage (new Gtk::HBox (false, 0));
	Gtk::HBox* hbox2  = manage (new Gtk::HBox (false, 0));

	Gtk::Label* blank1 = manage (new Gtk::Label);
	Gtk::Label* blank2 = manage (new Gtk::Label);

	blank1->set_text ("  ");
	blank2->set_text ("  ");

	m_report_box->add  (*m_report);

	hbox1->pack_start (*m_entry,  Gtk::PACK_EXPAND_WIDGET, 3);
	hbox1->pack_start (*m_button, Gtk::PACK_SHRINK, 2);

#if defined(IS_DESKTOP) || defined (IS_WIN32)
	hbox1->pack_start (*blank1,   Gtk::PACK_SHRINK, 0);
#endif

	hbox2->pack_start (*m_report_box, Gtk::PACK_EXPAND_WIDGET, 0);

#if defined(IS_DESKTOP) || defined (IS_WIN32)
	hbox2->pack_start (*blank2,       Gtk::PACK_SHRINK, 0);
#endif

	table->set_row_spacings (0);
	table->set_col_spacings (2);

	table->attach (*hbox1, 0, 1, 0, 1, 
				   Gtk::EXPAND|Gtk::SHRINK|Gtk::FILL, 
				   Gtk::FILL, 
				   0, 0);

	table->attach (*hbox2, 1, 2, 0, 1, 
				   Gtk::EXPAND|Gtk::FILL, 
				   Gtk::FILL, 
				   0, 0);

	m_entry->signal_changed ().connect (
		mem_fun (*this, &VerifyControl::on_entry_changed));

    m_button->signal_clicked ().connect (
        mem_fun (m_parent, &DeckPlayer::on_verify_clicked));
}

void
VerifyControl::
repaint () 
{ 
	m_entry->modify_font (CONFIG->fonts_db ().get_input_font ()); 
	modify_text_colors ();
}

void
VerifyControl::
set_focus_on_entry () 
{ 
    trace_with_mask("VerifyControl::set_focus_on_entry", KEYIN);

	m_entry->grab_focus ();
}

/**
 * Here is the tricky part. The answer_ can be marked up with
 * Pango markup - so strip it before comparing. Strip all well-known
 * prefixes as well and reduce multiple white spaces to a single one.
 *
 * The data member m_was_equal keeps the answer of the comparison for later
 * retrieval by the interested parties.
 */
void
VerifyControl::
compare (const ustring& answer_)
{
    trace_with_mask("VerifyControl::compare", GUITRACE);

	gchar* answer_expected = NULL;				// expected
	gchar* user_answered   = NULL;				// given by the user

	gchar* p = NULL;
	int count = 0;
	Glib::ustring token;
	Glib::ustring::value_type end_of_line ('\n');

	std::vector<gchar*> vec;
	std::vector<gchar*>::iterator jter;
	ustring::const_iterator iter;

	/** Assume the worst.
	 */
	m_was_equal = false;

	/** Change button label.
	 */
	m_button->set_label (clabels [NEXT]);

	/** Prepare the answer given by the user.
	 */
	user_answered = Granule::strip_pango_markup (m_entry->get_text ().c_str ());
	DL ((VERIFY,"strip_pango_markup() returned 0x%X.\n", user_answered));

	if (user_answered == NULL) {
		DL ((VERIFY,"Answer provided by the user was EMPTY!\n"));
		goto done;
	}

	Granule::remove_common_prefixes (user_answered);
	DL ((VERIFY,"remove_common_prefixes () returned 0x%X.\n", user_answered));

    /** Treat the card as multiline (even if it is not).
	 */
	iter = answer_.begin ();
	while (iter != answer_.end ()) 
	{
		if (*iter == end_of_line) {
			answer_expected = Granule::strip_pango_markup (token.c_str ());
			Granule::remove_common_prefixes (answer_expected);
			if (answer_expected == NULL) {
				DL ((VERIFY,"Exception: answer_expected = NULL! (line=%d)\n",
					 __LINE__));
				goto done;
			}
			DL ((VERIFY,"vec[%d].push_back(%s)\n", count, answer_expected));
			vec.push_back (answer_expected);
			count++;
			token = "";
		}
		else {
			token += *iter;
		}
		iter++;
	}

	DL ((VERIFY,"Multiline card text processed.\n"));

	/** First (and the only) or last token
	 */
	if (token.size ()) {
		answer_expected = Granule::strip_pango_markup (token.c_str ());
		Granule::remove_common_prefixes (answer_expected);
		if (answer_expected == NULL) {
			DL ((VERIFY,"Exception: answer_expected = NULL! (line=%d)\n",
				 __LINE__));
			goto done;
		}
		DL ((VERIFY,"vec[0].push_back(%s)\n", answer_expected));
		vec.push_back (answer_expected);
	}

	DL ((VERIFY,"m_entry = \"%s\"\n", 
		 (user_answered == NULL ? "NULL" : user_answered)));
	DL ((VERIFY,"answer  = \"%s\"\n", 
		 (answer_expected == NULL ? "NULL" : answer_expected)));

	/** If selector is '0', we compare the answer given by the user
	 *  against all lines of the card.
	 */
	if (m_line_num == 0) {
		jter = vec.begin ();
		while (jter != vec.end ()) {
			if (!::strcmp (user_answered, (*jter))) {
				m_was_equal = true;
				answer_expected = *jter;
				break;
			}
			jter++;
		}
	}
	else {
		if (vec.size () >= m_line_num) 
		{
			answer_expected = vec [m_line_num-1];

			if (answer_expected == NULL) {
				DL ((VERIFY,"Exception: answer_expected = NULL! (line=%d)\n",
					 __LINE__));
				goto done;
			}

			if (!::strcmp (answer_expected, user_answered)) {
				m_was_equal = true;
			}
		}
	}

	/** 
	 * Try to highlight the mismatches visually.
	 * Remember, you are comparing UTF-8 strings and simple
	 * gchar* arithmetic won't work.
	 */
	if (!m_was_equal) 
	{
		Glib::ustring lhs (answer_expected);
		Glib::ustring rhs (user_answered);

		for (size_t idx = 0; idx < lhs.size () && idx < rhs.size (); idx++) 
		{
			if (lhs [idx] != rhs [idx]) {
				m_entry->select_region (idx, -1);
				break;
			}
		}
	}

  done:
	if (m_was_equal) {
		m_report_box->modify_fg (Gtk::STATE_INSENSITIVE, m_green);
		m_report->modify_fg     (Gtk::STATE_INSENSITIVE, m_green);
		m_report->set_label     (clabels [CORRECT]);
	}
	else {
		m_report_box->modify_fg (Gtk::STATE_INSENSITIVE, m_red);
		m_report->modify_fg     (Gtk::STATE_INSENSITIVE, m_red);
		m_report->set_label     (clabels [INCORRECT]);
	}

	m_entry->set_editable (false);
	m_parent.flip_card ();
	set_state (VC_VALIDATED);

	/** Clean up
	 */
	if (user_answered) {
		g_free (user_answered);
	}

	jter = vec.begin ();
	while (jter != vec.end ()) 
	{
		p = *jter;
		g_free (*jter);
		jter++;
	}
}

void
VerifyControl::
on_entry_changed ()
{
    trace_with_mask("VerifyControl::on_entry_changed", KEYIN);

	m_button->set_sensitive (true);

	if (get_state () != VC_ARMED     &&
		m_entry->get_text () != " "  && 
		m_entry->get_text ().size ()) 
	{
		m_parent.disable_answer_controls ();
		set_state (VC_ARMED);
		Glib::ustring str ("<span size=\"medium\">");
		str += m_entry->get_text ();
		str += "</span>";
		m_entry->get_layout ()->set_markup (str);
		DL ((KEYIN,"Gtk::Entry markup enabled\n"));
	}
}

void
VerifyControl::
clear ()
{
	set_state (VC_START);

	m_report->set_text (clabels [EMPTY]);
	m_report->set_sensitive (false);
	m_report_box->modify_fg   (Gtk::STATE_INSENSITIVE, m_black);

	m_entry->set_text ("");
	m_entry->set_editable (true);

	m_button->set_label (clabels [CHECK_ANSWER]);
	m_button->set_sensitive (false);

	set_focus_on_entry ();
	m_parent.enable_answer_controls ();
}

VCState 
VerifyControl::
get_state () const
{
//	DL ((GRAPP,"current state = %s\n", states [m_state]));
	return (m_state);
}
void 
VerifyControl::
set_state (VCState state_)
{
	if (m_state != state_) {
		DL ((GRAPP,"change of state: %s -> %s\n", 
			 states [m_state], states [state_]));
		m_state = state_;
	}
}

/** @todo Enable when Gtkmm 2.12 becomes available.
 *
 *  For the color change to work properly, the cursor must
 *  be set to the text color as well. But, Entry::modify_cursor() 
 *  appears only starting in Gtk+ 2.12.
 */
void
VerifyControl::
modify_text_colors ()
{		
	Deck* deckp = dynamic_cast<Deck*>(m_parent.get_deck ());

	if (deckp != NULL && deckp->with_custom_appearance ()) 
	{
// 		m_entry->modify_base (Gtk::STATE_NORMAL, 
// 							  deckp->text_colors_db ().get_text_bg_color ());

// 		m_entry->modify_text (Gtk::STATE_NORMAL, 
// 							  deckp->text_colors_db ().get_text_fg_color ());

		m_report_box->modify_bg (Gtk::STATE_NORMAL, 
							  deckp->text_colors_db ().get_frame_color ());
	}
	else {
// 		m_entry->modify_base (Gtk::STATE_NORMAL, 
// 							  CONFIG->text_colors_db ().get_text_bg_color ());

// 		m_entry->modify_text (Gtk::STATE_NORMAL, 
// 							  CONFIG->text_colors_db ().get_text_fg_color ());

		m_report_box->modify_bg (Gtk::STATE_NORMAL, 
							 MAINWIN->get_style ()->get_bg (Gtk::STATE_NORMAL));
	}
}

void
VerifyControl::
attach_testline_entry (TestLineEntry* test_line_) 
{ 
    trace_with_mask("VerifyControl::attach_testline_entry", GUITRACE);

	if (test_line_ != NULL) {
		m_test_line = test_line_; 
		m_test_line->signal_entry_changed ().connect (
			sigc::mem_fun (*this, &VerifyControl::on_test_line_changed));
	}
}

void
VerifyControl::
on_test_line_changed ()
{
    trace_with_mask("VerifyControl::on_test_line_changed", GUITRACE);

	if (m_test_line != NULL) 
	{
		m_line_num = m_test_line->get_value_as_int ();
		DL ((GRAPP,"Testing line #%d\n", m_line_num));
	}
}
