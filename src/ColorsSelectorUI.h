// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: ColorsSelectorUI.h,v 1.1 2007/12/02 00:31:24 vlg Exp $
//------------------------------------------------------------------------------
//                            ColorsSelectorUI.h
//------------------------------------------------------------------------------
//  Copyright (c) 2007 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date   : Thu Oct 4 2007
//
//------------------------------------------------------------------------------
#ifndef COLORS_SELECTOR_UI_H
#define COLORS_SELECTOR_UI_H

#include <vector>
#include <string>

#include <gtkmm/frame.h>
#include <gtkmm/alignment.h>
#include <gtkmm/box.h>
#include <gtkmm/entry.h>
#include <gtkmm/table.h>
#include <gtkmm/colorbutton.h>

#include "Granule-main.h"

/**-----------------------------------------------------------------------------
 *  Compound Color Entry
 *
 *  /-------\/---------------------\/----------------\
 *  | Label || Entry               || <Color> Button |
 *  \-------/\---------------------/\----------------/
 *------------------------------------------------------------------------------
 */
class ColorEntry 
{
public:
    Gtk::Label*        m_label;
    Gtk::Entry*        m_entry;		// Font entry
    Gtk::ColorButton*  m_button;	// Font Dialog selection button
};

/**-----------------------------------------------------------------------------
 *  ColorsSelectorUI - the View of Text Colors MVC pattern.
 *  The widget is embeddable.
 *------------------------------------------------------------------------------
 */
class ColorsSelectorUI : public Gtk::Frame
{
public:
	ColorsSelectorUI ();

	ColorEntry& color_entry (TextColorType type_) { return m_centry [type_]; }

	void set_text (TextColorType type_, const Gdk::Color& value_);
	void set_text (TextColorType type_, const Glib::ustring& value_);

	Glib::ustring get_text (TextColorType type_) const;

	/// Callback fired when ColorEntry button is clicked
	void on_select_clicked (int idx_);

	/// Callback fired when TextEntry is changed
	void on_entry_changed (int idx_);

	/// Enable/disable element. By default, all are enabled (active).
	void set_active (TextColorType type_, bool val_ = true);

private:
	Glib::ustring convert_color_to_str (const Gdk::Color& color_);

private:
    ColorEntry   m_centry [COLOR_ENTRY_SZ];
	Gtk::Table*  m_color_table;
};

/**-----------------------------------------------------------------------------
 *  Implementation
 *------------------------------------------------------------------------------
 */


inline void
ColorsSelectorUI::
set_text (TextColorType type_, const Glib::ustring& value_)
{
	m_centry [type_].m_entry->set_text (value_);
}

inline Glib::ustring
ColorsSelectorUI::
get_text (TextColorType type_) const
{
	return m_centry [type_].m_entry->get_text ();
}

#endif /* COLORS_SELECTOR_UI_H */
