// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: CardBoxPrefsWindow.cpp,v 1.8 2008/01/06 16:54:09 vlg Exp $
//------------------------------------------------------------------------------
//                            CardBoxPrefsWindow.h
//------------------------------------------------------------------------------
//  Copyright (c) 2007 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date   : Tue Dec 18 2007
//
//------------------------------------------------------------------------------

#include "GrappConf.h"
#include "CardBoxPrefsWindow.h"
#include "ScheduleDB.h"

CardBoxPrefsWindow::
CardBoxPrefsWindow (Gtk::Widget& parent_) 
	:
    PropertyBox ("CardBox Preferences"),
	m_property_changed (false),
	m_capacity_changed (false),
	m_efactor_changed (false)
{
    trace_with_mask("CardBoxPrefsWindow::CardBoxPrefsWindow",GUITRACE);

#ifdef IS_HILDON
	set_size_request (672, -1);
#else
	set_size_request (580, 480);
	set_transient_for (dynamic_cast<Gtk::Window&>(parent_));
#endif

	/*= Show all */
	m_scheduling_pref.show_all ();
	
	/*= Pack all */
	append_page (m_scheduling_pref, "Schedule");

	/*= Hook up callbacks */
	m_scheduling_pref.signal_changed ().connect (
		sigc::mem_fun (*this, &CardBoxPrefsWindow::on_property_changed_cb));
}

bool
CardBoxPrefsWindow::
run ()
{
    trace_with_mask("CardBoxPrefsWindow::run",GUITRACE);

    m_property_changed = false;
	m_capacity_changed = false;
	m_efactor_changed = false;

	m_apply_button->set_sensitive (false);

    show ();
    Gtk::Main::run ();

    return m_property_changed;
}

void
CardBoxPrefsWindow::
load_from_config (ScheduleDB& sched_db_)
{
    trace_with_mask("CardBoxPrefsWindow::load_from_config",GUITRACE);
	DL ((DECK,"sched_db_.capacity = %d\n", sched_db_.capacity ()));

	m_scheduling_pref.clear_view ();

	sched_db_.update_view (m_scheduling_pref);

	m_old_capacity = m_scheduling_pref.capacity ();
	m_old_efactor  = m_scheduling_pref.capacity ();
}

void
CardBoxPrefsWindow::
save_to_config (ScheduleDB& sched_db_)
{
    trace_with_mask("CardBoxPrefsWindow::save_to_config",GUITRACE);

	sched_db_.fetch_from_view (m_scheduling_pref);
}
