// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: DeckView.cpp,v 1.71 2008/07/21 03:04:40 vlg Exp $
//------------------------------------------------------------------------------
//                            DeckView.cpp
//------------------------------------------------------------------------------
//  Copyright (c) Vladislav Grinchenko 
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date   : Fri Feb  6 23:37:11 EST 2004
//
//------------------------------------------------------------------------------

#include <sstream>
#include <gtkmm/messagedialog.h>
#include <gtkmm/stock.h>
#include <gtkmm/buttonbox.h>

#include <assa/AutoPtr.h>

#include "Granule-main.h"
#include "GrappConf.h"
#include "Granule.h"
#include "MainWindow.h"
#include "Card.h"
#include "CardView.h"
#include "CardDeck.h"
#include "Deck.h"
#include "DeckView.h"
#include "DeckInfo.h"
#include "ButtonWithImageLabel.h"
#include "CardAutoFill.h"

#include "Intern.h"

using sigc::bind;
using sigc::mem_fun;

#define DV_SETUP_LABEL(l) \
	(l)->set_alignment  (0.5,0.5); \
	(l)->set_padding    (0,0); \
	(l)->set_justify    (Gtk::JUSTIFY_LEFT); \
	(l)->set_line_wrap  (false); \
	(l)->set_use_markup (false); \
	(l)->set_selectable (false); 

//------------------------------------------------------------------------------
// DeckView implementation
//------------------------------------------------------------------------------
DeckView::
DeckView (VDeck& deck_, int selected_row_) 
		:
	m_label_count      (manage (new Gtk::Label ("0 cards"))),
	m_deck             (deck_),
	m_row_idx          (selected_row_),
	m_modified         (false)
{
	trace_with_mask("DeckView::DeckView", GUITRACE);

	/********************************
	 * Setup dialog attributes
	 ********************************/
	set_title("Deck View");

	/**
	 * Not a dialog any more - it is a Bin (AppView) instead.
	 * We handle packaging ourselves and mute all attributes of a 
	 * Dialog everywhere down the line.
	 */
    m_vbox = manage (new Gtk::VBox (false, 2));
	add (*m_vbox);

#ifdef IS_HILDON
	set_size_request (672, 396);
#else
	set_modal (true);
#ifdef IS_PDA
	set_resizable(false);
	Gdk::Geometry dp_geometry =	{ 240, 320,	240, 320, 240, 320,	-1, -1,	-1, -1};
	set_geometry_hints (*this, dp_geometry, 
				Gdk::HINT_MIN_SIZE | Gdk::HINT_MAX_SIZE | Gdk::HINT_BASE_SIZE);
#else
	set_resizable(true);
	set_size_request (400, 370);	// width; height 
	set_border_width (2);
#endif // (!IS_PDA)
#endif // (!IS_HILDON)

	/** Add <Close> button to the Gtk::Dialog
	 */
	m_close_button = manage (new ButtonWithImageLabel (
								 _("<u>C</u>lose"), Gtk::Stock::CLOSE));
	m_close_button->set_alt_accelerator_key (GDK_c, get_accel_group ());
	m_close_button->set_flags  (Gtk::CAN_FOCUS);
	m_close_button->set_relief (Gtk::RELIEF_NORMAL);

	m_close_button->signal_clicked ().connect (
		mem_fun (*this, &DeckView::on_close_clicked));

 	signal_delete_event ().connect (
 		sigc::mem_fun (*this, &DeckView::on_delete_window_clicked));

	/** 
	 * For the top edit buttons bar, create an ActionGroup.
	 */
	m_actgroup = Gtk::ActionGroup::create ("Actions");

	m_actgroup->add (Gtk::Action::create ("Add", 
										  Gtk::Stock::ADD,
										  _("_Add"),
										  "Add new card"),
					 mem_fun (*this, &DeckView::on_add_clicked));

	m_actgroup->add (Gtk::Action::create ("Edit", 
										  Gtk::Stock::DND,
										  _("_Edit"),
										  "Edit selected card"),
					 mem_fun (*this, &DeckView::on_edit_clicked));

	m_actgroup->add (Gtk::Action::create ("AutoFill", 
										  Gtk::Stock::CONVERT,
										  _("Auto _Fill"),
										  "AutoFill Card fields"),
					 mem_fun (*this, &DeckView::on_autofill_clicked));

	m_actgroup->add (Gtk::Action::create ("Delete", 
#ifdef IS_WIN32
										  Gtk::Stock::CUT,
#else
										  Gtk::Stock::DELETE,
#endif
										  _("_Delete"),
										  "Delete selected card"),
					 mem_fun (*this, &DeckView::on_delete_clicked));

	m_actgroup->add (Gtk::Action::create ("Go_Up", 
										  Gtk::Stock::GO_UP,
										  _("Move Up"),
										  "Move selecte card up"),
					 mem_fun (*this, &DeckView::on_card_up_clicked));

	m_actgroup->add (Gtk::Action::create ("Go_Down", 
										  Gtk::Stock::GO_DOWN,
										  _("Move Down"),
										  "Move selected card down"),
					 mem_fun (*this, &DeckView::on_card_down_clicked));

	m_actgroup->add (Gtk::Action::create ("Add_Box1", 
										  Gtk::Stock::UNDO,
										  _("Add To CardBox 1"),
										  "Add selected card to Box1"),
					 mem_fun (*this, &DeckView::on_add_card_to_cardbox));

	m_actgroup->add (Gtk::Action::create ("Deck_Info", 
										  Gtk::Stock::PREFERENCES,
										  _("_Preferences"),
										  "Deck preferences"),
					 mem_fun (*this, &DeckView::on_more_clicked));

	m_actgroup->add (Gtk::Action::create ("Save_Deck", 
										  Gtk::Stock::SAVE,
										  _("_Save"),
										  "Save deck"),
					 mem_fun (*this, &DeckView::on_save_deck_clicked));

    m_uimanager = Gtk::UIManager::create ();
    m_uimanager->insert_action_group (m_actgroup);

#ifndef IS_HILDON
	Gtk::Window* this_win = dynamic_cast<Gtk::Window*> (this);
    this_win->add_accel_group (m_uimanager->get_accel_group ());
#endif

	/********************************
	 * Setup toolbar                
	 ********************************/
	Glib::ustring ui_info =
		"<ui>"
		"  <toolbar name='DV_Controls'>"
		"     <toolitem action='Add'/>"
		"     <toolitem action='Edit'/>"
		"     <toolitem action='AutoFill'/>"
		"     <toolitem action='Delete'/>"
		"     <toolitem action='Go_Up'/>"
		"     <toolitem action='Go_Down'/>"
		"     <toolitem action='Add_Box1'/>"
		"     <toolitem action='Deck_Info'/>"
		"     <toolitem action='Save_Deck'/>"
		"  </toolbar>"
		"</ui>";

#ifdef GLIBMM_EXCEPTIONS_ENABLED
	try {
		m_uimanager->add_ui_from_string (ui_info);
    }
    catch (const Glib::Error& ex_) {
		DL((GRAPP,"Building menus failed (%s)\n", ex_.what ().c_str ()));
    }
#else
	std::auto_ptr<Glib::Error> err;
	m_uimanager->add_ui_from_string (ui_info, err);
	if (err.get ()) {
		DL((GRAPP,"Building menus failed (%s)\n", err->what ().c_str ()));
	}
#endif

	m_add_button       = (m_uimanager->get_widget ("/DV_Controls/Add"));
	m_edit_button      = (m_uimanager->get_widget ("/DV_Controls/Edit"));
	m_autofill_button  = (m_uimanager->get_widget ("/DV_Controls/AutoFill"));
	m_delete_button    = (m_uimanager->get_widget ("/DV_Controls/Delete"));
	m_card_up_button   = (m_uimanager->get_widget ("/DV_Controls/Go_Up"));
	m_card_down_button = (m_uimanager->get_widget ("/DV_Controls/Go_Down"));
	m_add_card_to_cardbox = (m_uimanager->get_widget ("/DV_Controls/Add_Box1"));
	m_more_button      = (m_uimanager->get_widget ("/DV_Controls/Deck_Info"));
	m_save_deck_button = (m_uimanager->get_widget ("/DV_Controls/Save_Deck"));

    Gtk::Widget* controls = m_uimanager->get_widget ("/DV_Controls");
    Gtk::Toolbar* ctrl_toolbar = dynamic_cast<Gtk::Toolbar*> (controls);

    ctrl_toolbar->set_tooltips (true);
	ctrl_toolbar->set_show_arrow ();
    ctrl_toolbar->set_orientation (Gtk::ORIENTATION_HORIZONTAL);
    ctrl_toolbar->set_toolbar_style (Gtk::TOOLBAR_ICONS);

	m_vbox->pack_start (*ctrl_toolbar, Gtk::PACK_SHRINK, 0);

	/**************************
	 * List View 
	 **************************/
	m_scrollwin = manage (new Gtk::ScrolledWindow);
	m_scrollwin->set_shadow_type (Gtk::SHADOW_ETCHED_OUT);
	m_scrollwin->set_policy (Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);

	Glib::ustring qstr;
	Gtk::TreeModel::Row row;
	m_list_store_ref = Gtk::ListStore::create (m_columns);
	VDeck::cardlist_iterator iter = m_deck.begin ();

	while (iter != m_deck.end ()) 
	{
		row = *m_list_store_ref->append ();
		qstr = Granule::trim_multiline ((*iter)->get_question ());

		if ((*iter)->get_reversed ()) {
			row [m_columns.m_front_row] = 
				"<span foreground=\"red\">" + qstr + "</span>";
		}
		else {
			row [m_columns.m_front_row] = qstr;
		}

		row [m_columns.m_back_row ] = 
			Granule::trim_multiline ((*iter)->get_answer ());
		row [m_columns.m_card     ] = *iter;
		iter++;
	}

	/** Make both Front and Back columns sortable.
	 */
	m_list_store_ref->set_sort_func (
		0, (mem_fun (*this, &DeckView::on_sort_compare_front)));

	m_list_store_ref->set_sort_func (
		1, (mem_fun (*this, &DeckView::on_sort_compare_back)));

	/******************************
	 * Create TreeView
	 ******************************
	 */
	DL((GRAPP,"Create TreeView\n"));
	m_tree_view.set_model (m_list_store_ref);
	m_tree_view.set_rules_hint ();
	m_tree_view.set_headers_clickable (true);
	m_tree_view.set_headers_visible (true);

	Gtk::TreeView::Column* column;
	Gtk::CellRendererText* renderer;

	/******************************
	 * Front: Column
	 ******************************
	 */
	column = Gtk::manage (new Gtk::TreeView::Column ("Front"));
	m_tree_view.append_column (*column);
	renderer = Gtk::manage (new Gtk::CellRendererText);

	column->pack_start (*renderer, false);
#ifdef GLIBMM_PROPERTIES_ENABLED
	column->add_attribute (renderer->property_markup (), 
						   m_columns.m_front_row);
#else
	column->add_attribute (*renderer, "markup", m_columns.m_front_row);
#endif

	column->set_sizing (Gtk::TREE_VIEW_COLUMN_FIXED);
	column->set_resizable ();
#ifdef IS_PDA
	column->set_fixed_width (125);
#else
	column->set_fixed_width (250);
#endif

	/******************************
	 * Enable column sorting
	 ******************************
	 */
	column->set_sort_column (m_columns.m_front_row);
	column->set_sort_indicator (true);
	column->signal_clicked ().connect (
		mem_fun (*this, &DeckView::on_column_sorted));

	/******************************
	 * Back: Column
	 ******************************
	 */
	column = Gtk::manage (new Gtk::TreeView::Column ("Back"));
	m_tree_view.append_column (*column);
	renderer = Gtk::manage (new Gtk::CellRendererText);

	column->pack_start (*renderer, false);

#ifdef GLIBMM_PROPERTIES_ENABLED
	column->add_attribute (renderer->property_markup (), 
						   m_columns.m_back_row);
#else
	column->add_attribute (*renderer, "markup", m_columns.m_back_row);
#endif

	column->set_sizing (Gtk::TREE_VIEW_COLUMN_FIXED);
	column->set_fixed_width (140);

	/****************************
	 * Enable column sorting
	 ****************************
	 */
	column->set_sort_column (m_columns.m_back_row);
	column->set_sort_indicator (true);
	column->signal_clicked ().connect (
		mem_fun (*this, &DeckView::on_column_sorted));

	m_tree_sel_ref = m_tree_view.get_selection ();
	m_tree_sel_ref->set_mode (Gtk::SELECTION_MULTIPLE);
	m_tree_sel_ref->signal_changed ().connect (
		mem_fun (*this, &DeckView::on_card_selected));

	/**
	 * If the second argument is 'false', it tells Gtk+ to call 
	 * on_key_pressed() BEFORE keypress event handling. 
	 * Returning 'true' from on_key_pressed() stops further event processing.
	 */
	m_tree_view.add_events (Gdk::BUTTON_PRESS_MASK|Gdk::KEY_PRESS_MASK);

	m_tree_view.signal_button_press_event ().connect (
		sigc::mem_fun (*this, &DeckView::on_button_press_event), false);

	m_tree_view.signal_key_press_event ().connect (
		sigc::mem_fun (*this, &DeckView::on_key_tapped), false);

	m_scrollwin->add (m_tree_view);

	/*****************************
	 * Pack the cards list
	 *****************************
	 */
	Gtk::HBox* h_viewbox = manage (new Gtk::HBox(false, 0));
	h_viewbox->pack_start (*m_scrollwin, Gtk::PACK_EXPAND_WIDGET, 0);
	m_vbox->pack_start (*h_viewbox);

	/***********************************
	 *  h_infobox holds:
	 *  -------------------------
	 *      [Total:] [XXX cards] 
	 ***********************************
	 */
	Gtk::Label* label_total = manage (new Gtk::Label ("Total: "));
	Gtk::HBox* h_infobox    = manage (new Gtk::HBox(false, 0));

	DV_SETUP_LABEL(label_total);
	DV_SETUP_LABEL(m_label_count);

	h_infobox->pack_start (*label_total,   Gtk::PACK_SHRINK, 9);
	h_infobox->pack_start (*m_label_count, Gtk::PACK_SHRINK, 0);

	m_vbox->pack_start (*h_infobox, Gtk::PACK_SHRINK, 4);

	/**
	 * Add separator and [Close] button in the ButtonBox
	 */
	Gtk::HSeparator* hseparator = manage (new Gtk::HSeparator);
	m_vbox->pack_start (*hseparator, false, true, 0);

	Gtk::HButtonBox*  hbuttonbox = manage (new Gtk::HButtonBox);
	hbuttonbox->set_spacing (6);
	hbuttonbox->set_border_width (6);

	Gtk::Label* expander = manage (new Gtk::Label (""));
	hbuttonbox->pack_start (*expander, Gtk::PACK_EXPAND_WIDGET);
	hbuttonbox->pack_end (*m_close_button, Gtk::PACK_SHRINK);

	m_vbox->pack_start (*hbuttonbox, Gtk::PACK_SHRINK);

	set_cards_count ();
	show_all ();

	/** 
	 * Select the same row (card) that the DeckPlayer has selected,
	 * and scroll to it. 
	 *
	 * NOTE: There are two stunning points worth noting here.
	 *
	 *       First, upon executing show_all(), Gtkmm internals set the
	 *       selection to row # 0 and then emmit *changed* signal!
	 *       Bottom line - if you want to select a row in c'tor, do 
	 *       your selection AFTER show_all(). 
	 *
	 *       Secondly, select (iter) doesn't work, but select (row) does.
	 */
	DL((APP,"m_row_idx = %d\n", m_row_idx));

	if (m_row_idx >= 0) {
		m_row_idx = selected_row_;
		children_t children = m_list_store_ref->children();
		chiter_t iter = children [m_row_idx];
		row = *iter;
		m_tree_sel_ref->select (row);
	}
	set_sensitivity ();
	set_win_name ();

#if !defined(IS_HILDON) && !defined (IS_PDA)
	set_icon_from_file (GRAPPDATDIR "/pixmaps/deckview_32x32.png");
#endif

	m_tree_view.grab_focus ();
}

DeckView::
~DeckView ()
{
	trace_with_mask("DeckView::~DeckView", GUITRACE);
}

void
DeckView::
set_cards_count ()
{
	std::ostringstream os;

	os << m_deck.size () << " cards.";
	m_label_count->set_text (os.str ());
}

//==============================================================================
//                              Callbacks
//------------------------------------------------------------------------------

gint
DeckView::
run (Gtk::Window& parent_)
{
    trace_with_mask("DeckView::run",GUITRACE);

	GRANULE->move_child_to_parent_position (parent_, *this);

	show ();
	present ();
	gtk_main ();

	GRANULE->move_parent_to_child_position (parent_, *this);

	return (Gtk::RESPONSE_NONE);
}

void
DeckView::
on_close_clicked ()  
{
	trace_with_mask("DeckView::on_close_clicked", GUITRACE);

	hide ();
	gtk_main_quit ();
}

void
DeckView::
on_column_sorted ()
{
	trace_with_mask("DeckView::on_column_sorted", GUITRACE);

	/** Swapping elements works only with unsorted stores!
	 */
	m_card_up_button  ->set_sensitive (false);
	m_card_down_button->set_sensitive (false);

	on_card_selected ();
}

void
DeckView::
on_card_selected ()
{
	trace_with_mask("DeckView::on_card_selected", GUITRACE);

	pathlist_t pathlist = m_tree_sel_ref->get_selected_rows ();
	pathlist_iterator_t iter = pathlist.begin ();

	if (iter == pathlist.end ()) {
		return;
	}

	m_row_idx = *((*iter).begin ());
	DL((GRAPP,"Row selected : %d\n", m_row_idx));
}

void
DeckView::
add_new_card (Card* card_)
{
	trace_with_mask("DeckView::add_new_card", GUITRACE);

	DL ((GRAPP,"Adding new card id = %d\n", card_->get_id ()));
	m_deck.push_back (card_);

	Gtk::TreeModel::Row row = *m_list_store_ref->append ();

	row [m_columns.m_front_row] = 
		Granule::trim_multiline (card_->get_question ());
	row [m_columns.m_back_row ] = 
		Granule::trim_multiline (card_->get_answer ());
	row [m_columns.m_card     ] = card_;

	m_tree_sel_ref->unselect_all ();
	m_tree_sel_ref->select (row);

	set_cards_count ();
	autoscroll_down ();

	m_modified = true;
}

void 
DeckView::
on_add_clicked ()
{
	trace_with_mask("DeckView::on_add_clicked", GUITRACE);

	/** It is safe to cast VDeck& to Deck& here because we can only 
	 *  perform add/edit operation with a real Deck.
	 */
	CardView cview (new Card (dynamic_cast<Deck&>(m_deck)));

	int ret = cview.run_in_add_mode (*this, CONFIG->autofill_db ());

	if (ret == Gtk::RESPONSE_OK) {
		DL((GRAPP,"Card accepted\n"));
		return;
	}
	
	if (ret == Gtk::RESPONSE_CANCEL) {
		DL((GRAPP,"Card cancelled\n"));
	}
	else if (ret == Gtk::RESPONSE_REJECT) {
		DL((GRAPP,"Text entry syntax error (wrong markup?)\n"));
	}
	else {						
		DL((GRAPP|ASSA::ASSAERR, "Unexpected response = %d, "
			"see /path/to/gtkmm/dialog.h for details.\n", ret));
	}
}

void 
DeckView::
on_edit_clicked ()
{
	trace_with_mask("DeckView::on_edit_clicked", GUITRACE);

	if (m_row_idx < 0) {
		Gtk::MessageDialog em ("The deck is empty!",Gtk::MESSAGE_ERROR);
		em.run ();
		return;
	}
	DL ((GRAPP,"m_row_idx = %d\n", m_row_idx));
	Gtk::TreeModel::Row row = *m_list_store_ref->children ()[m_row_idx];
	VCard* card_iter = row [m_columns.m_card];

	if (card_iter == NULL) {
		DL ((GRAPP, "About to crash : card_iter = NULL!\n"));
	}

	CardView cview (card_iter);
	int ret = cview.run_in_edit_mode (*this);

	if (ret == Gtk::RESPONSE_OK) {
		row [m_columns.m_front_row] = Granule::trim_multiline (
			card_iter->get_question());

		row [m_columns.m_back_row ] = Granule::trim_multiline (
			card_iter->get_answer  ());
	}
}

void 
DeckView::
on_autofill_clicked ()
{
	trace_with_mask("DeckView::on_autofill_clicked", GUITRACE);

	CardAutoFill card_autofill_dialog;

	CONFIG->autofill_db ().update_view (card_autofill_dialog);

	int ret = card_autofill_dialog.run (*this);

	if (ret == Gtk::RESPONSE_CANCEL) {
		DL((GRAPP,"CANCEL clicked.\n"));
		return;
	}

	CONFIG->autofill_db ().fetch_from_view (card_autofill_dialog);

	if (ret == Gtk::RESPONSE_OK) {
		DL((GRAPP,"OK clicked.\n"));
	}
	else if (ret == Gtk::RESPONSE_APPLY) {
		DL((GRAPP,"Apply clicked.\n"));
		if (CONFIG->autofill_db ().is_enabled ()) {
			int count = 1;
			VDeck::cardlist_iterator iter = m_deck.begin ();
			while (iter != m_deck.end ()) {
				Card* card = dynamic_cast<Card*> (*iter);
				CONFIG->autofill_db ().apply_patterns (card, count);
				iter++;
				count++;
			}
			// TODO: Do I need to update the DeckView ???
		}
	}
	else {						
		DL((GRAPP|ASSA::ASSAERR, "Unexpected response = %d, "
			"see /path/to/gtkmm/dialog.h for details.\n", ret));
	}
}

void 
DeckView::
on_delete_clicked ()
{
	trace_with_mask("DeckView::on_delete_clicked", GUITRACE);

	if (m_row_idx < 0) {
		Gtk::MessageDialog em ("The deck is empty!",Gtk::MESSAGE_ERROR);
		em.run ();
		return;
	}

	/** Get a list of all selected rows. Convert rows into TreePaths.
	 *  Convert further TreePaths into TreeRowReferences.
	 */
	pathlist_t pathlist = m_tree_sel_ref->get_selected_rows ();

	if (pathlist.size () == 0) {
		Gtk::MessageDialog em ("Nothing is selected",Gtk::MESSAGE_ERROR);
		em.run ();
		return;
	}

	{
		Gtk::MessageDialog qm (
			_("Are you shure you want\nto delete selected card(s)?"),
			false,
			Gtk::MESSAGE_QUESTION, 
			Gtk::BUTTONS_NONE,
			true);
		qm.add_button (Gtk::Stock::NO,  Gtk::RESPONSE_NO);
		qm.add_button (Gtk::Stock::YES, Gtk::RESPONSE_YES);

		if (qm.run () == Gtk::RESPONSE_NO) {
			return;
		}
	}

	/** Convert references to paths
	 */
	std::vector<Gtk::TreeRowReference> selected_refs;
	pathlist_iterator_t iter = pathlist.begin ();
	while (iter != pathlist.end ()) 
	{
		selected_refs.push_back(Gtk::TreeRowReference(m_list_store_ref, *iter));
		iter++;
	}

	VCard* vcardp;
	vector<VCard*> vcards_list;
	vector<VCard*>::iterator vcards_iter;

	Gtk::TreeModel::iterator dead_row_iter;
	std::vector<Gtk::TreeRowReference>::reverse_iterator riter;

	/** Delete from the model first using paths (safe).
	 */
	riter = selected_refs.rbegin ();
	while (riter != selected_refs.rend ()) 
	{
		dead_row_iter = m_list_store_ref->get_iter (riter->get_path ());
		Gtk::TreeModel::Row row = *dead_row_iter;
		vcardp = row [m_columns.m_card];
		vcards_iter = std::find (vcards_list.begin (), 
								 vcards_list.end (), 
								 vcardp);
		if (vcards_iter == vcards_list.end ()) {
			DL ((GRAPP,"Adding '%s' to vcards_list\n", 
				 vcardp->get_question ().c_str ()));
			vcards_list.push_back (vcardp);
		}

		m_list_store_ref->erase (dead_row_iter);

		riter++;
	}

	/** Delete from the CardBox
	 */
	vcards_iter = vcards_list.begin ();
	while (vcards_iter != vcards_list.end ()) {
		DL ((GRAPP,"Erasing '%s'\n", 
			 (*vcards_iter)->get_question ().c_str ()));
		m_deck.erase (*vcards_iter);
		vcards_iter++;
	}

	/** Reposition selected row and update counters.
	 */
	if (m_row_idx == m_deck.size ()) {
		m_row_idx--;
	}

	if (m_row_idx >= 0) {
		children_t children = m_list_store_ref->children();
		chiter_t chiter = children [m_row_idx];
		Gtk::TreeModel::Row row = *chiter;
		m_tree_sel_ref->select (row);
		set_cards_count ();
		autoscroll_down ();
	}

	Deck* real_deck = NULL;
	if ((real_deck = dynamic_cast<Deck*>(&m_deck)) != NULL) {
		real_deck->mark_as_modified (); 
	}

	m_modified = true;
}

void 
DeckView::
on_more_clicked ()
{
	trace_with_mask("DeckView::on_more_clicked", GUITRACE);

	DeckInfo deck_info (*this, dynamic_cast<Deck&>(m_deck));

	GRANULE->move_child_to_parent_position (*this, deck_info);
	deck_info.run ();
	GRANULE->move_parent_to_child_position (*this, deck_info);

	m_repaint_player = deck_info.appearance_changed ();
}

void
DeckView::
autoscroll_up ()
{
    trace_with_mask("DeckView::autoscroll_up",GUITRACE);
                                                                                
    Gtk::Adjustment* adj = m_scrollwin->get_vadjustment ();
                                                                                
    double pg_inc = adj->get_page_increment ();
    double value  = adj->get_value ();
    double prh    = adj->get_upper () / m_list_store_ref->children().size ();

    if (((m_row_idx + 1) * prh - value) < prh) {
        adj->set_value (value - pg_inc/2);
    }
}

void
DeckView::
autoscroll_down ()
{
    trace_with_mask("DeckView::autoscroll_down",GUITRACE);

    Gtk::Adjustment* adj = m_scrollwin->get_vadjustment ();

	double tmp;
    double pg_inc    = adj->get_page_increment ();
    double value     = adj->get_value ();
    double prh       = adj->get_upper () / m_list_store_ref->children().size ();
    double pg_sz     = adj->get_page_size ();
	double last_page = adj->get_upper () - pg_sz;
                                                                                
    if (((m_row_idx + 1) * prh - value) > pg_sz) 
	{
		if ((tmp = value + pg_inc/2) > last_page) {
			adj->set_value (last_page);
		}
		else {
			adj->set_value (tmp);
		}
    }
}

void
DeckView::
on_card_up_clicked ()
{
    trace_with_mask("DeckView::on_card_up_clicked",GUITRACE);

	if (m_row_idx < 0) {
		Gtk::MessageDialog em ("The deck is empty!",Gtk::MESSAGE_ERROR);
		em.run ();
		return;
	}

	if (m_row_idx == 0) {
		return;
	}

	children_t children = m_list_store_ref->children();
	chiter_t a = children [m_row_idx];	

	Gtk::TreeModel::Row row = *a;
	VCard* vcard = row [m_columns.m_card];

	/** Swap adjacent VCard elements in the Deck
	 */
	Deck& deck = dynamic_cast<Deck&> (m_deck); // might throw an exception
	deck.swap_with_prev (vcard);

	/** Update ListStore
	 */
	chiter_t b = children [--m_row_idx];
	m_list_store_ref->iter_swap (a, b);
	autoscroll_up ();
	m_modified = true;
}
	
void
DeckView::
on_card_down_clicked ()
{
    trace_with_mask("DeckView::on_card_down_clicked",GUITRACE);

	if (m_row_idx < 0) {
		Gtk::MessageDialog em ("The deck is empty!",Gtk::MESSAGE_ERROR);
		em.run ();
		return;
	}

	if (m_row_idx == m_deck.size ()-1) {
		return;
	}
	children_t children = m_list_store_ref->children();
	chiter_t a = children [m_row_idx];	

	Gtk::TreeModel::Row row = *a;
	VCard* vcard = row [m_columns.m_card];

	/** Swap adjacent VCard elements in the Deck
	 */
	Deck& deck = dynamic_cast<Deck&> (m_deck);     // might throw an exception
	deck.swap_with_next (vcard);                  

	/** Update ListStore
	 */
	chiter_t b = children [++m_row_idx];
	m_list_store_ref->iter_swap (a, b);
	autoscroll_down ();
	m_modified = true;
}

void
DeckView::
on_add_card_to_cardbox ()
{
    trace_with_mask("DeckView::on_add_card_to_cardbox",GUITRACE);

	if (m_row_idx < 0) {
		Gtk::MessageDialog em ("The deck is empty!",Gtk::MESSAGE_ERROR);
		em.run ();
		return;
	}

	if (m_deck.get_name ().find ("Untitled") != std::string::npos) {
		Gtk::MessageDialog em1 ("Operation is not permitted!\n"
								"Save the 'Untitled' Deck first.",
								Gtk::MESSAGE_ERROR);
		em1.run ();
		return;
	}

	int row_idx;

	VDeck::cardlist_iterator card_iter;
	cardlist_t card_list;
	pathlist_t pathlist = m_tree_sel_ref->get_selected_rows ();

	if (pathlist.size () == 0) {
		Gtk::MessageDialog em ("Nothing is selected",Gtk::MESSAGE_ERROR);
		em.run ();
		return;
	}

	pathlist_iterator_t iter = pathlist.begin ();
	while (iter != pathlist.end ()) 
	{
		row_idx = *((*iter).begin ());
		card_iter = m_deck.begin () + row_idx;
		card_list.push_back (new CardRef (**card_iter, 
										  dynamic_cast<Deck&>(m_deck),
										  CARDBOX->sched_db ().efactor ()));
		iter++;
	}

	CARDBOX->add_cards_to_box (card_list);
	m_modified = true;
}

/**
 * Sort Front column of words.
 *
 * The parts of the text may be marked up with Pango markup language.
 * Remove markup. Also, remove 'to ', 'a ', 'an ' prefixes.
 * Then compare the first word.
*/
int 
DeckView::
sort_compare_impl (const Gtk::TreeModel::iterator& a_, 
				   const Gtk::TreeModel::iterator& b_,
				   int column_)
{
    trace_with_mask("DeckView::sort_compare_impl",GUITRACE);

	Glib::ustring a;
	Glib::ustring b;

	const Gtk::TreeModel::Row row_a = *a_;
	const Gtk::TreeModel::Row row_b = *b_;

	gchar* txt_a;
	gchar* txt_b;
	int    ret = 0;

	txt_a = txt_b = NULL;

//	DL ((GRAPP, "Sorting %s column\n", (column_ == 0 ? "Front" : "Back")));

	if (column_ == 0) {
		a = row_a [m_columns.m_front_row];
		b = row_b [m_columns.m_front_row];
	}
	else {
		a = row_a [m_columns.m_back_row];
		b = row_b [m_columns.m_back_row];
	}

	/** 
	 * We need a hold on the pointers to deallocate memory.
	 */
	txt_a = Granule::strip_pango_markup (a.c_str ());
	txt_b = Granule::strip_pango_markup (b.c_str ());

	Granule::remove_common_prefixes (txt_a);
	Granule::remove_common_prefixes (txt_b);

	if (txt_a != NULL || txt_b != NULL) {
		ret = strcmp (txt_a, txt_b);
		g_free (txt_a);
		g_free (txt_b);
	}

	return ret;
}

/**
 * @return true - top other handlers from being invoked for the event.
 */
bool
DeckView::
on_delete_window_clicked (GdkEventAny* /*event_*/)
{
    trace_with_mask("DeckView::on_delete_window_clicked",GUITRACE);

    on_close_clicked ();
    return true;  
}

//==============================================================================
//                              Utilities
//------------------------------------------------------------------------------
void
DeckView::
set_sensitivity ()
{
	if (dynamic_cast<Deck*>(&m_deck) != NULL) {
		return;
	}

	m_add_button          ->set_sensitive (false);
	m_more_button         ->set_sensitive (false);
	m_card_up_button      ->set_sensitive (false);
	m_card_down_button    ->set_sensitive (false);
	m_add_card_to_cardbox ->set_sensitive (false);
}

void
DeckView::
set_win_name ()
{
	string title = m_deck.get_name ();
	string::size_type idx = title.rfind (G_DIR_SEPARATOR);
    if (idx != string::npos) {
        title.replace (0, idx+1, "");
    }
	set_title ("Deck View : " + title);
}

void
DeckView::
on_save_deck_clicked ()
{
	if (m_row_idx < 0) {
		Gtk::MessageDialog em ("The deck is empty!",Gtk::MESSAGE_ERROR);
		em.run ();
		return;
	}

	DECKMGR->save_deck_cb ();
}

/** 
 * Called when button is pressed on any of the rows.
 * We filter out everything except for the double-click
 * which brings the CardView dialog to edit selected entry.
 *
 * The rest of the events are handled by the default event
 *  handler of TreeView afterwards.
 */
bool
DeckView::
on_button_press_event (GdkEventButton* event_)
{
	trace_with_mask("DeckView::on_button_press_event", GUITRACE);

	DL ((GRAPP,"Key pressed: 0x%x\n", event_->type));

	if (event_->type == GDK_2BUTTON_PRESS || event_->type == GDK_Return)
	{
		on_edit_clicked ();
	}
#ifdef IS_HILDON
	else if (event_->type == GDK_Escape) // Hildon round button (Cancel,Close)
	{
		on_edit_clicked ();
	}
#endif

	return false;
}

/**
 * Return 'true' if keytap event has been handled.
 */
bool
DeckView::
on_key_tapped (GdkEventKey* event_)
{
	trace_with_mask("DeckView::on_key_tapped", GUITRACE);
	bool ret = false;

//	DL ((GRAPP,"Key event: 0x%x (KEY_PRESS=0x%x, ALT_MASK=0x%x\n", 
//		 event_->type, Gdk::KEY_PRESS, Gdk::MOD1_MASK));
//	DL ((GRAPP,"Key pressed: 0x%x\n", event_->keyval));

	if (event_->type == GDK_KEY_PRESS) {
		switch (event_->keyval) {
		case GDK_Return:
			on_edit_clicked ();
			ret = true;
			break;
		
		case GDK_Delete:
			on_delete_clicked ();
			ret = true;
			break;
		}

		if (event_->state & Gdk::MOD1_MASK) 
		{
			ret = true;
			switch (event_->keyval) 
			{
			case GDK_a:	on_add_clicked ();       break;
			case GDK_e:	on_edit_clicked ();	     break;
			case GDK_d:	on_delete_clicked ();    break;
			case GDK_f:	on_autofill_clicked ();  break;
			case GDK_p:	on_more_clicked ();      break;
			case GDK_s:	on_save_deck_clicked (); break;
				
			default: ret = false;
			}
		}
	}

	return ret;
}

