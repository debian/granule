// -*- c++ -*-
//------------------------------------------------------------------------------
//                            FontsSelectorUI.cpp
//------------------------------------------------------------------------------
// $Id: FontsSelectorUI.cpp,v 1.1 2007/12/02 00:31:24 vlg Exp $
//------------------------------------------------------------------------------
//  Copyright (c) 2007 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date   : Sat Oct 6 2007
//
//------------------------------------------------------------------------------
#include <gtkmm/button.h>
#include <gtkmm/buttonbox.h>
#include <gtkmm/label.h>
#include <gtkmm/frame.h>
#include <gtkmm/box.h>
#include <gtkmm/alignment.h>
#include <gtkmm/fontselection.h>

#include "FontsSelectorUI.h"

#include "Intern.h"             // i18n macros

using sigc::mem_fun;

/**-----------------------------------------------------------------------------
 *	Some useful macros
 **-----------------------------------------------------------------------------
 */
//------------------------------------------------------------------------------
static const char* flabels [] = 
{
    _("Question font: "),
    _("Answer font: "),
    _("Example font: "),
	_("Input font: "),
	_("Application font: ")
};

static void set_fonts_frame (Gtk::Frame* f_, Gtk::Label* l_, Gtk::Table* w_)
{
	f_->set_border_width (4); 
	f_->set_shadow_type  (Gtk::SHADOW_ETCHED_IN);
	f_->set_label_align  (Gtk::ALIGN_TOP, Gtk::ALIGN_CENTER);
	f_->set_label_widget (*l_);
	f_->add              (*w_);
}

static void set_fonts_label (Gtk::Label* l_)
{
	l_->set_padding    (0,0);
	l_->set_line_wrap  (false);
	l_->set_use_markup (true);
}

/**-----------------------------------------------------------------------------
 *	FontsSelectorUI
 *
 *  The widget encapsulates text font selection controls.
 *  All fonts in the application can be altered from their default theme values.
 *
 *     ---------------
 * +---| frame_label |------------------^-------------------------+<--m_frame
 * |   ---------------                  | (6pts)                  |
 * |                                    v-------------------------|
 * | +---------+--------------------------------+----------------+<-- FontEntry
 * | | label   |       entry                    | button         ||
 * | +---------+--------------------------------+----------------+|
 * | | label   |       entry                    | button         ||
 * | +---------+--------------------------------+----------------+|
 * | | label   |       entry                    | button         ||
 * | +---------+--------------------------------+----------------+|
 * | | label   |       entry                    | button         ||
 * | +---------+--------------------------------+----------------+|
 * | | label   |       entry                    | button         ||
 * | +---------+--------------------------------+----------------+|
 * +--------------------------------------------------------------+
 *
 **-----------------------------------------------------------------------------
 */
FontsSelectorUI::
FontsSelectorUI ()
{
	Gtk::Frame* fonts_frame = this;

	m_font_table = Gtk::manage (new Gtk::Table (FONT_ENTRY_SZ, 3, false));

	for (int i = 0; i < FONT_ENTRY_SZ; i++) 
	{
	    m_fentry [i].m_label  = Gtk::manage (new Gtk::Label (flabels[i]));
	    m_fentry [i].m_entry  = Gtk::manage (new Gtk::Entry);
	    m_fentry [i].m_button = Gtk::manage (new Gtk::Button ("Select"));

	    m_fentry [i].m_label->set_alignment(Gtk::ALIGN_RIGHT,Gtk::ALIGN_CENTER);
	    m_fentry [i].m_label->set_padding    (0,0);
	    m_fentry [i].m_label->set_justify    (Gtk::JUSTIFY_LEFT);
	    m_fentry [i].m_label->set_line_wrap  (false);
	    m_fentry [i].m_label->set_use_markup (false);
	    m_fentry [i].m_label->set_selectable (false);

	    m_fentry [i].m_entry->set_flags (Gtk::CAN_FOCUS);
	    m_fentry [i].m_entry->set_visibility (true);
	    m_fentry [i].m_entry->set_editable   (true);
	    m_fentry [i].m_entry->set_max_length (0);
	    m_fentry [i].m_entry->set_text       ("");
	    m_fentry [i].m_entry->set_has_frame  (true);
	    m_fentry [i].m_entry->set_activates_default (false);
#ifdef IS_HILDON
		m_fentry [i].m_entry->set_width_chars (26);
#endif

	    m_fentry [i].m_button->set_flags(Gtk::CAN_FOCUS);

		m_font_table->attach (*m_fentry [i].m_label, 
							  0, 1, i, i+1,
							  Gtk::FILL, 
							  Gtk::AttachOptions (), 2, 2);

		m_font_table->attach (*m_fentry [i].m_entry, 
							  1, 2, i, i+1,
#ifdef IS_HILDON
							  Gtk::FILL,
#else
							  Gtk::EXPAND|Gtk::FILL,
#endif
							  Gtk::AttachOptions(), 2, 2);

		m_font_table->attach (*m_fentry [i].m_button, 
							  2, 3, i, i+1,
							  Gtk::FILL, 
							  Gtk::AttachOptions (), 2, 2);
	}

	Gtk::Label* fonts_label = Gtk::manage(new Gtk::Label("<b>Fonts</b>"));
	set_fonts_label(fonts_label);

	/** Pack all together
	 */
	set_fonts_frame (fonts_frame, fonts_label, m_font_table);

	/** Set callbacks
	 */

	/** Callbacks are set by the controller
	 */
	for (int i = 0; i < FONT_ENTRY_SZ; i++) 
	{
	    m_fentry [i].m_button->signal_clicked().connect (
			sigc::bind<int>(mem_fun (*this, 
							   &FontsSelectorUI::on_select_clicked), i));
	}
}

void
FontsSelectorUI::
on_select_clicked (int idx_)
{
	trace_with_mask("FontsSelectorUI::on_select_clicked",GUITRACE);

	Gtk::FontSelectionDialog sfd ("Select Text Font");

	sfd.set_font_name (get_text (TextFontType (idx_)));

	if (sfd.run () != Gtk::RESPONSE_OK) {
		return;
	}

	set_text (TextFontType (idx_), sfd.get_font_name ());

	DL((GRAPP,"Text font changed : \"%s\"\n", 
		get_text (TextFontType (idx_)).c_str ()));
}

void
FontsSelectorUI::
set_active (TextFontType type_, bool val_)
{
	m_fentry [type_].m_label->set_sensitive (val_);
	m_fentry [type_].m_entry->set_sensitive (val_);
	m_fentry [type_].m_button->set_sensitive (val_);
}
