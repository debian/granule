// -*- c++ -*-
//------------------------------------------------------------------------------
//                              CardView.h
//------------------------------------------------------------------------------
// $Id: CardView.h,v 1.32 2008/06/02 03:03:53 vlg Exp $
//
//  Copyright (c) Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//
//  Date: Feb 15, 2004
//------------------------------------------------------------------------------
#ifndef CARD_VIEW_H
#define CARD_VIEW_H

#include <glibmm/ustring.h>
#include <gtkmm/textview.h>
#include <gtkmm/entry.h>
#include <gtkmm/dialog.h>

class Gtk::Button;
class Gtk::TextView;
class Gtk::Entry;

#ifdef IS_HILDON
#    include <hildonmm.h>
#endif

#include "Granule-main.h"
#include "CardViewHelpDialog.h"

class VCard;
class DeckView;
class EditControls;
class AutoFillDB;
class ButtonWithImageLabel;

enum { 
	TV_FRONT, 
	TV_BACK, 
	TV_EXAMPLE, 
	TV_ASF, 
	TV_ASB, 
	TV_IMG, 
	TV_EIMG, 
	TV_SZ 
};

/** @class CardView
 *
 *  View and edit card.
 *
 */

#ifdef IS_HILDON
class CardView : public Hildon::Window
#else
class CardView : public Gtk::Window
#endif
{
public:
	/** Create dialog to view and/or edit an existing card.
	 *  If card_ is NULL, create dialog to add new card(s). 
	 *  User might add more then one card at once without closing the dialog.
	 */
	CardView (VCard* card_ = NULL);

	~CardView ();

	/** Simulate re-entrant event loop. Also, hide/show parent
	 *  window and reposition accordingly.
	 *
	 *  @param disable_cancel_ if true, disables cancel button.
	 */
	int run_in_edit_mode (Gtk::Window& parent_, bool disable_cancel_ = false);

	/** This version is called by DeckView to add a new card
	 *  (or multiple cards without closing the dialog)
	 */
	int run_in_add_mode (DeckView& deckview_, AutoFillDB& autofill_db_);

	/** We block [x] delete action and reroute it to <Close>
	 */
	bool on_delete_window_clicked (GdkEventAny* event_);

	/** Remember window size when resized.
	 */
	void size_allocate_cb (Gtk::Allocation& size_);

private:
	virtual bool on_key_press_event (GdkEventKey* event_);

	void on_add_clicked       ();
	void on_addnclose_clicked ();
	void on_help_clicked      ();
	void on_cancel_clicked    ();

	/** @param type_ This parameter tells which button (TV_IMG orTV_EIMG)
	 *               was pressed.
	 */
	void on_browse_img_clicked (int type_);

#ifdef IS_HILDON
	void hildon_im_close_cb ();
#endif

	void clear_dialog ();

	void take_snapshot ();
	bool text_modified ();

	void refresh_cards_count (DeckView& deckview_);
	
	void apply_patterns (AutoFillDB& autofill_db_, int seqnum_);

private:
	VCard* m_card;
	bool   m_syntax_error;
	bool   m_keep_open;
	int    m_response;
	
	// -*- GUI components -*-
	//
	ButtonWithImageLabel* m_cancel_button;
	ButtonWithImageLabel* m_add_n_close_button;
	ButtonWithImageLabel* m_add_button;

	Gtk::Button*    m_count_button;	          // functions as a label
	ButtonWithImageLabel* m_help_button;

	Gtk::Button*    m_browse_img_button;  // Select Front Image 
	Gtk::Button*    m_browse_example_img_button;  // Select Example Image 

	Gtk::TextView*  m_front_text;
	Gtk::TextView*  m_img_text;	    // Image path 

	Gtk::TextView*  m_back_text;

	Gtk::TextView*  m_example_text;
	Gtk::TextView*  m_example_img_text;	    // Image path 

	Gtk::TextView*  m_asf_text;	    // Alt.Spell Front
	Gtk::TextView*  m_asb_text;	    // Alt.Spell Back

#ifdef IS_HILDON
	/** Temporal fix - in hildon, Gtk::TextView doesn't allocate 
	 *  default buffer.
	 */ 
	Glib::RefPtr<Gtk::TextBuffer> m_textbuf_ref [TV_SZ];
#endif

	// -*- Snapshot to detect true changes -*-
	//
	Glib::ustring   m_snapshot_front;
	Glib::ustring   m_snapshot_back;
	Glib::ustring   m_snapshot_example;
	Glib::ustring   m_snapshot_asf;
	Glib::ustring   m_snapshot_asb;
	Glib::ustring   m_snapshot_img;
	Glib::ustring   m_snapshot_example_img;

	EditControls*       m_edit_controls;
	CardViewHelpDialog* m_help_dialog;
	int                 m_cards_count; // tied up to m_count_button
};



inline
CardView::
~CardView ()
{
	if (m_help_dialog != NULL)  {
		delete m_help_dialog;
		m_help_dialog = NULL;
	}
}

#endif /* CARD_VIEW_H */
