// -*- c++ -*-
//------------------------------------------------------------------------------
//                              Card.cpp
//------------------------------------------------------------------------------
// $Id: Card.cpp,v 1.8 2007/12/26 03:32:23 vlg Exp $
//
//  Copyright (c) 2004 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//
// Date: Jan 22, 2004
//------------------------------------------------------------------------------

#include <assa/TimeVal.h>
using ASSA::TimeVal;

#include "Card.h"
#include "Deck.h"
#include "ScheduleDB.h"

Card::
Card (Deck& container_) : m_container (container_)
{ 
	trace_with_mask("Card::Card",DECK); 
	
	mark_clean ();

	TimeVal t (TimeVal::gettimeofday ());
	m_id = t.sec ();
	DL ((DECK,"create card id = %d\n", m_id));
}

Card::
~Card ()
{
	trace_with_mask ("Card::~Card", DECK);
	DL((DECK, "destroying card id = \"%d\"\n", get_id ()));
}

ustring
Card::
get_deck_path () const
{
	return m_container.get_deck_path (); 
}

PathMode
Card::
image_path_mode () const
{
	return m_container.pics_path_mode ();
}

void
Card::
dump () const
{
	DL((DECK, "id       = \"%d\"\n", get_id ()));
	DL((DECK, "question = \"%s\"\n", get_question ().c_str ()));
	DL((DECK, "answer   = \"%s\"\n", get_answer ().c_str ()));
	DL((DECK, "example  = \"%s\"\n", get_example ().c_str ()));
	DL((DECK, "dirty?   = \"%s\"\n", (m_dirty ? "yes" : "no")));
}
