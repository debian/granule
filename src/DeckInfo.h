// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: DeckInfo.h,v 1.17 2008/06/02 03:03:54 vlg Exp $
//------------------------------------------------------------------------------
//                            DeckInfo.h
//------------------------------------------------------------------------------
//  Copyright (c) Vladislav Grinchenko 
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date   : Mon Feb 16 22:08:26 EST 2004
//
//------------------------------------------------------------------------------
#ifndef DECK_INFO_H
#define DECK_INFO_H

#ifdef HAVE_CONFIG_H
#    include "config.h"
#endif

#include <gtkmm/dialog.h>
#include <gtkmm/checkbutton.h>
#include <gtkmm/box.h>

#ifdef IS_HILDON
#    include <hildonmm.h>
#endif

class Gtk::Entry;

class Deck;
class AppearancePrefs;
class TextAlignmentsUI;
class FontsSelectorUI;
class ColorsSelectorUI;
class ButtonWithImageLabel;

/*==============================================================================
 * Class DeckInfo (Deck Properties)
 *==============================================================================
 */
#ifdef IS_HILDON
class DeckInfo : public Hildon::Window
#else
class DeckInfo : public Gtk::Window
#endif
{  
public:
	DeckInfo (Gtk::Window& parent_, Deck& deck_);

	void on_enable_snd_clicked ();
	void on_enable_rel_snd_path_clicked ();
	void on_enable_app_clicked ();

	bool appearance_changed () const { return m_appearance_changed; }
	void font_changed_cb    () { m_appearance_changed = true; }
	void color_changed_cb   () { m_appearance_changed = true; }

	int run ();

private:
	void on_cancel_clicked ();
	void on_apply_clicked  ();
	void on_browse_clicked ();

	bool on_delete_window_clicked (GdkEventAny* event_);

	Gtk::VBox* get_vbox () { return m_vbox; }
	Gtk::HButtonBox* get_action_area () { return m_action_area; }

private:
	void load_appearance ();
	void save_appearance ();

	void set_question_font (const std::string& v_);
	std::string get_question_font () const;

private:
	Gtk::VBox*       m_vbox;
	Gtk::HButtonBox* m_action_area;

	Deck& m_deck;

	ButtonWithImageLabel* m_cancel_button;
	ButtonWithImageLabel* m_apply_button;

	/** Origin
	 */
	Gtk::Entry*  m_author_entry;
	Gtk::Entry*  m_desc_entry;
	Gtk::Entry*  m_snd_path_entry;

	/** Custom sound preferences
	 */
	Gtk::Button*      m_snd_browse_button;
	Gtk::CheckButton* m_enable_alt_snd;
	Gtk::CheckButton* m_enable_rel_snd_path;

	/** Custom image path preference
	 */
	Gtk::CheckButton* m_enable_rel_pics_path;

	/** Custom appearance
	 */
	Gtk::CheckButton* m_enable_custom_app;
	Gtk::VBox*        m_inner_app_vbox;

	FontsSelectorUI*   m_fonts_widget;
	ColorsSelectorUI*  m_colors_widget;

	TextAlignmentsUI* m_front_widget;
	TextAlignmentsUI* m_back_widget;
	TextAlignmentsUI* m_example_widget;

	int  m_response;
	bool m_appearance_changed;
};

#endif
