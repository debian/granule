// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: EditControls.cpp,v 1.14 2008/05/24 22:58:51 vlg Exp $
//------------------------------------------------------------------------------
//                            EditControls.cpp
//------------------------------------------------------------------------------
//  Copyright (c) 2005,2006 by Vladislav Grinchenko 
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date   : Nov 13 2005
//
//------------------------------------------------------------------------------

#include <sstream>

#include <gtkmm/stock.h>
#include <gtkmm/iconfactory.h>

#include "Granule-main.h"
#include "Granule.h"
#include "EditControls.h"
#include "InputDialog.h"

#include "Intern.h"

using Gtk::Image;

Gtk::StockID EditControls::BOLD        ("bold");
Gtk::StockID EditControls::ITALIC      ("italic");
Gtk::StockID EditControls::UNDERLINE   ("underline");
Gtk::StockID EditControls::MONOSPACE   ("monospace");
Gtk::StockID EditControls::SUBSCRIPT   ("subscript");
Gtk::StockID EditControls::SUPERSCRIPT ("superscript");
Gtk::StockID EditControls::BIG         ("big");
Gtk::StockID EditControls::SMALL       ("small");
Gtk::StockID EditControls::REMOVETAGS  ("remove_tags");
Gtk::StockID EditControls::PASTE       ("paste");
Gtk::StockID EditControls::NORMALIZE   ("normalize");
Gtk::StockID EditControls::ADDINDEX    ("add_index");

/**-----------------------------------------------------------------------------
 */
static void
add_icon_to_factory (Glib::RefPtr<Gtk::IconFactory>& ifactory_,
					 Glib::RefPtr<Gdk::Pixbuf>& pixbuf_,
					 Gtk::StockID& id_)
{
    Gtk::IconSet    iset;
    Gtk::IconSource isource;

    isource.set_pixbuf (pixbuf_);
    isource.set_size   (Gtk::ICON_SIZE_BUTTON);
    iset.add_source    (isource);
    ifactory_->add     (id_, iset);
    Gtk::Stock::add    (Gtk::StockItem (id_, id_.get_string ()));
}

/**-----------------------------------------------------------------------------
 */
EditControls::
EditControls (Glib::RefPtr<Gtk::TextBuffer> tb_ref_)
	: 
	Gtk::HBox (false, 0), 
	m_textbuf (tb_ref_),
	m_index_count (1)
{
	trace_with_mask("EditControls::EditControls", GUITRACE);

	Gtk::Button* button;
	Glib::RefPtr<Gdk::Pixbuf> pixbuf_ref;
	Glib::RefPtr<Gtk::IconFactory> ifactory = Gtk::IconFactory::create ();

	pixbuf_ref = Granule::create_from_file (
		GRAPPDATDIR "pixmaps/text_bold.png");
	add_icon_to_factory (ifactory, pixbuf_ref, EditControls::BOLD);
	m_icon_list.push_back (icon_t (EditControls::BOLD,
								   manage (new Image (pixbuf_ref)),
								   "Apply bold tag",
								   "<control>b"));
	
	pixbuf_ref = Granule::create_from_file (
		GRAPPDATDIR "pixmaps/text_italic.png");
	add_icon_to_factory (ifactory, pixbuf_ref, EditControls::ITALIC);
	m_icon_list.push_back (icon_t (EditControls::ITALIC,
								   manage (new Image (pixbuf_ref)),
								   "Apply italic tag",
								   "<control>i"));

	pixbuf_ref = Granule::create_from_file (
		GRAPPDATDIR "pixmaps/text_underlined.png");
	add_icon_to_factory (ifactory, pixbuf_ref, EditControls::UNDERLINE);
	m_icon_list.push_back (icon_t (EditControls::UNDERLINE,
								   manage (new Image (pixbuf_ref)),
								   "Apply underline tag",
								   "<control>u"));

	pixbuf_ref = Granule::create_from_file (
		GRAPPDATDIR "pixmaps/text_monospaced.png");
	add_icon_to_factory (ifactory, pixbuf_ref, EditControls::MONOSPACE);
	m_icon_list.push_back (icon_t (EditControls::MONOSPACE,
								   manage (new Image (pixbuf_ref)),
								   "Apply monospace tag",
								   "<control>t"));

	pixbuf_ref = Granule::create_from_file (
		GRAPPDATDIR "pixmaps/text_subscript.png");
	add_icon_to_factory (ifactory, pixbuf_ref, EditControls::SUBSCRIPT);
	m_icon_list.push_back (icon_t (EditControls::SUBSCRIPT,
								   manage (new Image (pixbuf_ref)),
								   "Apply subscript tag",
								   "<control>l"));

	pixbuf_ref = Granule::create_from_file (
		GRAPPDATDIR "pixmaps/text_superscript.png");
	add_icon_to_factory (ifactory, pixbuf_ref, EditControls::SUPERSCRIPT);
	m_icon_list.push_back (icon_t (EditControls::SUPERSCRIPT,
								   manage (new Image (pixbuf_ref)),
								   "Apply superscript tag",
								   "<control>k"));

	pixbuf_ref = Granule::create_from_file (
		GRAPPDATDIR "pixmaps/text_big.png");
	add_icon_to_factory (ifactory, pixbuf_ref, EditControls::BIG);
	m_icon_list.push_back (icon_t (EditControls::BIG,
								   manage (new Image (pixbuf_ref)),
								   "Make selection bigger",
								   "<control>m"));

	pixbuf_ref = Granule::create_from_file (
		GRAPPDATDIR "pixmaps/text_small.png");
	add_icon_to_factory (ifactory, pixbuf_ref, EditControls::SMALL);
	m_icon_list.push_back (icon_t (EditControls::SMALL,
								   manage (new Image (pixbuf_ref)),
								   "Make selection smaller",
								   "<control>s"));

	pixbuf_ref = Granule::create_from_file (
		GRAPPDATDIR "pixmaps/text_remove_tags.png");
	add_icon_to_factory (ifactory, pixbuf_ref, EditControls::REMOVETAGS);
	m_icon_list.push_back (icon_t (EditControls::REMOVETAGS,
								   manage (new Image (pixbuf_ref)),
								   "Remove all tags",
								   "<control>x"));

	pixbuf_ref = Granule::create_from_file (
		GRAPPDATDIR "pixmaps/text_paste.png");
	add_icon_to_factory (ifactory, pixbuf_ref, EditControls::PASTE);
	m_icon_list.push_back (icon_t (EditControls::PASTE,
								   manage (new Image (pixbuf_ref)),
								   "Paste from clipboard",
								   "<control>p"));

	pixbuf_ref = Granule::create_from_file (
		GRAPPDATDIR "pixmaps/text_normalize.png");
	add_icon_to_factory (ifactory, pixbuf_ref, EditControls::NORMALIZE);
	m_icon_list.push_back (icon_t (EditControls::NORMALIZE,
								   manage (new Image (pixbuf_ref)),
								   "Remove extra spaces and newlines",
								   "<control>n"));

	pixbuf_ref = Granule::create_from_file (
		GRAPPDATDIR "pixmaps/text_addindex.png");
	add_icon_to_factory (ifactory, pixbuf_ref, EditControls::ADDINDEX);
	m_icon_list.push_back (icon_t (EditControls::ADDINDEX,
								   manage (new Image (pixbuf_ref)),
								   "Add reference index",
								   "<control>r"));

	ifactory->add_default ();

	/** Setup toolbar of buttons. This is a requirement
	 *  imposed on us by broken implementation of the 
	 *  virtual keyboard in maemo as well as portrait
	 *  geometry considerations for OpenHand PDAs.
	 */
	m_actgroup = Gtk::ActionGroup::create ();
	
	/** unset_flags () takes buttons out of the dialog's focus chain.
		There is no other way it can be achieved with set_focus_chain()
		in CardView dialog. I tried it and it doesn't work.
	*/
	icon_list_t::iterator iter = m_icon_list.begin ();
	while (iter != m_icon_list.end ()) 
	{
		button = manage (new Gtk::Button ());
		button->unset_flags (Gtk::CAN_FOCUS);
		button->add (*((*iter).get_image ()));

		m_actgroup->add (Gtk::Action::create ((*iter).get_name (),
											  (*iter).get_stock_id (),
											  (*iter).get_name (),
											  (*iter).get_tooltip ()),
						 Gtk::AccelKey ((*iter).get_accel_key ()),
						 bind<string>(mem_fun (*this, 
										 &EditControls::on_edit_button_clicked),
									  (*iter).get_name ()));
		iter++;
	}

    m_uimanager = Gtk::UIManager::create ();
    m_uimanager->insert_action_group (m_actgroup);

	try {
		Glib::ustring ui_info = 
			"<ui>"
			" <toolbar name='EditControls'>"
			"     <toolitem action='paste'/>"
			"     <toolitem action='remove_tags'/>"
			"     <toolitem action='small'/>"
			"     <toolitem action='superscript'/>"
			"     <toolitem action='subscript'/>"
			"     <toolitem action='bold'/>"
			"     <toolitem action='italic'/>"
			"     <toolitem action='monospace'/>"
			"     <toolitem action='big'/>"
			"     <toolitem action='normalize'/>"
			"     <toolitem action='add_index'/>"
			"  </toolbar>"
			"</ui>";

#ifdef GLIBMM_EXCEPTIONS_ENABLED
		m_uimanager->add_ui_from_string (ui_info);
#else
		std::auto_ptr<Glib::Error> err;
		m_uimanager->add_ui_from_string (ui_info, err);
#endif
    }
    catch (const Glib::Error& ex_) {
		DL((GRAPP,"Building menues failed (%s)\n", ex_.what ().c_str ()));
    }
	Gtk::Widget* controls = m_uimanager->get_widget ("/EditControls");
    Gtk::Toolbar* ctrl_toolbar = dynamic_cast<Gtk::Toolbar*> (controls);

    ctrl_toolbar->set_orientation   (Gtk::ORIENTATION_HORIZONTAL);
    ctrl_toolbar->set_toolbar_style (Gtk::TOOLBAR_ICONS);
    ctrl_toolbar->set_tooltips      (true);

	/** Pack all up
	 */
	this->pack_start (*ctrl_toolbar, Gtk::PACK_EXPAND_WIDGET, 0);
	
	/** See if we can defeat default theme of Hildon
	 */
#ifdef IS_HILDON
#ifdef NICE_TRY
	Gdk::Color box_bgcolor ("grey");
	Glib::RefPtr<Gtk::Style> style = get_style ();
	style->set_base (Gtk::STATE_NORMAL, box_bgcolor);
	style->set_bg   (Gtk::STATE_NORMAL, box_bgcolor);
	style->set_fg   (Gtk::STATE_NORMAL, box_bgcolor);
	set_style (style);
#endif
#endif
}

void
EditControls::
on_edit_button_clicked (const string& action_)
{
	trace_with_mask("EditControls::on_edit_button_clicked",GUITRACE);

	DL ((GRAPP,"Processing <%s> tag request.\n", action_.c_str ()));

	if (action_ == "normalize") {
		normalize_text ();
		return;
	}

	Gtk::TextBuffer::iterator iter;
	Gtk::TextBuffer::iterator start_iter;
	Gtk::TextBuffer::iterator end_iter;

	/** Iterators are invalidated between text changes.
		But marks are not. They are placed in the place where
		the text used to be it it were deleted. 
		Comparing the marks, however, turned out to be useless - 
		you ought to compares iterators only.
	*/
	if (!m_textbuf->get_selection_bounds (start_iter, end_iter)) 
	{
		DL ((GRAPP,"No selection is made.\n"));
		if (action_ == "paste") {
			Glib::RefPtr<Gtk::Clipboard> ref_clipboard = Gtk::Clipboard::get ();
			ref_clipboard->request_text (
				sigc::mem_fun(*this,&EditControls::on_clipboard_text_received));
		}
		return;
	}

	/** We got a valid selection - mark it now. We cannot use
		predefined selection marks "insert" and "selection_bound"
		because if the user made the selection by dragging pointer
		in the opposite direction, we would get iterators in reverse
		and Gtkmm doesn't allow for iterators comparison.
	 */
	Glib::RefPtr<Gtk::TextBuffer::Mark> mb;
	Glib::RefPtr<Gtk::TextBuffer::Mark> me;

	mb = m_textbuf->create_mark ("start_selection", start_iter);
	me = m_textbuf->create_mark ("end_selection",   end_iter);
	iter = start_iter;

	DL ((GRAPP,"selection: from='%c' to='%c'\n", 
		 display_unichar_at (iter),
		 display_unichar_at (me->get_iter ())));

	if (action_ == "bold") {
		m_textbuf->insert (iter, "<b>");
		m_textbuf->insert (me->get_iter (), "</b>");
	}
	else if (action_ == "italic") {
		m_textbuf->insert (iter, "<i>");
		m_textbuf->insert (me->get_iter (), "</i>");
	}
	else if (action_ == "underline") {
		m_textbuf->insert (iter, "<u>");
		m_textbuf->insert (me->get_iter (), "</u>");
	}
	else if (action_ == "monospace") {
		m_textbuf->insert (iter, "<tt>");
		m_textbuf->insert (me->get_iter (), "</tt>");
	}
	else if (action_ == "subscript") {
		m_textbuf->insert (iter, "<sub>");
		m_textbuf->insert (me->get_iter (), "</sub>");
	}
	else if (action_ == "superscript") {
		m_textbuf->insert (iter, "<sup>");
		m_textbuf->insert (me->get_iter (), "</sup>");
	}
	else if (action_ == "big") {
		m_textbuf->insert (iter, "<big>");
		m_textbuf->insert (me->get_iter (), "</big>");
	}
	else if (action_ == "small") {
		m_textbuf->insert (iter, "<small>");
		m_textbuf->insert (me->get_iter (), "</small>");
	}
	else if (action_ == "remove_tags") {
		remove_tags (mb, me);
	}
	else if (action_ == "add_index") {
		add_index (mb, me);
	}
	else {
		DL ((GRAPP,"Unexpected action!\n"));
	}
	m_textbuf->place_cursor (m_textbuf->begin ());

	m_textbuf->delete_mark (mb);
	m_textbuf->delete_mark (me);
}

/** 
 * We remove all complete individual tags. The incomplete tags
 * are ignored. The tag context is also ignored - we don't match
 * open/close tags at all.
 */
void 
EditControls::
remove_tags (Glib::RefPtr<Gtk::TextBuffer::Mark> begin_mark_,
			 Glib::RefPtr<Gtk::TextBuffer::Mark> end_mark_)
{
    trace_with_mask("EditControls::remove_tags",GUITRACE);

	static const gunichar open_brace = '<';
	static const gunichar close_brace = '>';

	Glib::RefPtr<Gtk::TextBuffer::Mark> start_mark;
	Gtk::TextBuffer::iterator next_ch = begin_mark_->get_iter ();
	Gtk::TextBuffer::iterator last_ch = end_mark_->get_iter ();

	DL ((GRAPP,"scanning from='%c' to='%c'\n", 
		 display_unichar_at (next_ch),
		 display_unichar_at (last_ch)));
	
	while (next_ch && next_ch != last_ch) 
	{
		if (next_ch.get_char () == open_brace) 
		{
			start_mark = m_textbuf->create_mark ("start", next_ch);
			next_ch++;

			DL ((GRAPP,"outer: next_ch='%c'\n", display_unichar_at (next_ch)));

			while (next_ch && next_ch != last_ch) {
				if (next_ch.get_char () == close_brace) {
					DL ((GRAPP,"found tag - erasing from '%c' to '%c' ...\n",
						 display_unichar_at (start_mark->get_iter ()),
						 display_unichar_at (next_ch)));

					next_ch++;
					m_textbuf->erase (start_mark->get_iter (), next_ch);

					next_ch = start_mark->get_iter ();
					last_ch = end_mark_->get_iter ();
					break;
				}
				next_ch++;

				DL ((GRAPP,"inner: next_ch='%c'\n",
					 display_unichar_at(next_ch)));
			}
			m_textbuf->delete_mark (start_mark);
		}
		if (next_ch.get_char () != open_brace) {
			next_ch++;
		}
		DL ((GRAPP,"outer: next_ch='%c'\n", display_unichar_at (next_ch)));
	}
}

/** 
 * Replace all newline characters with white spaces, and then
 * remove extra adjacent white spaces. This comes especially
 * handy when you copy-n-paste text from preformatted source
 * which has text evenly distributed on each line (like a book).
 */
void 
EditControls::
normalize_text ()
{
    trace_with_mask("EditControls::normalize_text",GUITRACE);

	static const gunichar newline = '\n';
	static const gunichar space = ' ';

	bool inside_wsp = false;
	Glib::ustring dest1;
	Glib::ustring dest2;
	Gtk::TextBuffer::iterator iter;
	gunichar ch;

	/** First pass - replace newlines
	 */
	iter = m_textbuf->begin ();
	while (iter != m_textbuf->end ()) {
		ch = iter.get_char ();
		if (ch == newline) {
			dest1 += space;
		}
		else {
			dest1 += ch;
		}
		iter++;
	}
	DL ((GRAPP,"dest1 =\"%s\"\n", dest1.c_str ()));

	/** Second pass - replace extra adjancent white spaces.
	 */
	for (int idx = 0; idx < dest1.length (); idx++) 
	{
		if (dest1 [idx] != space) {
			dest2 += dest1 [idx];
			if (inside_wsp) {	      /* left whitespace block */
				inside_wsp = false;
			}
		}
		else {
			if (!inside_wsp) {        /* entered whitespace block */
				dest2 += dest1 [idx];
				inside_wsp = true;
			}
		}
	}

	DL ((GRAPP,"dest2 =\"%s\"\n", dest2.c_str ()));
	m_textbuf->set_text (dest2);
}

/** 
 * Execute canned operation: if selection is being made,
 * ask the user for the index number, and then
 * surround selected box with 
 *
 *   <i>selected_block</i><sup>index</sup>
 *
 * Also, add the reference at the end of the text:
 * 
 *   [index] <i>selected_block</i> -
 */
void 
EditControls::
add_index (Glib::RefPtr<Gtk::TextBuffer::Mark> begin_mark_,
		   Glib::RefPtr<Gtk::TextBuffer::Mark> end_mark_)
{
    trace_with_mask("EditControls::add_index",GUITRACE);

	std::ostringstream oss;
	Glib::ustring selection;
	Glib::ustring s;
	Glib::ustring idx_val;

	oss << m_index_count;
	InputDialog ask_index (_("Enter index number:"), oss.str ());
	oss.str ("");

	if (ask_index.run () == Gtk::RESPONSE_CANCEL) {
		DL ((GRAPP, "action cancelled\n"));
		return;
	}
	idx_val = ask_index.get_entry_value ();	

	selection = m_textbuf->get_slice (begin_mark_->get_iter (),
									  end_mark_->get_iter ());

	m_textbuf->insert (begin_mark_->get_iter (), "<i>");
	s = "</i><sup>" + idx_val + "</sup>";
	m_textbuf->insert (end_mark_->get_iter (), s);

	s = "\n[" + idx_val + "] <i>" + selection + "</i> - ";
	m_textbuf->insert (m_textbuf->end (), s);

	m_index_count = ::atoi (idx_val.c_str ());
	m_index_count++;
}

/** 
 * Clipboard calls back with the text whether it is available or
 * not (immediately).
 */
void
EditControls::
on_clipboard_text_received (const Glib::ustring& text_)
{
    trace_with_mask("EditControls::on_clipboard_text_received",GUITRACE);

	if (text_.size () == 0) {
		return;
	}

	Glib::RefPtr<Gtk::TextBuffer::Mark> insert_mark;
	insert_mark = m_textbuf->get_insert ();
	m_textbuf->insert (insert_mark->get_iter (), text_);
}


/** 
 * Cancel out current selection only when focus is shifted
 * from one entry field to another.
 */
void
EditControls::
on_focus_changed (Gtk::TextView* text_view_)
{
    trace_with_mask("EditControls::on_focus_changed",GUITRACE);

	if (m_textbuf != text_view_->get_buffer ()) {
		m_textbuf->place_cursor (m_textbuf->begin ()); // cancel old select
		m_textbuf = text_view_->get_buffer ();
#ifdef IS_HILDON
		m_start_mark.clear ();
		m_end_mark.clear ();
#endif
	}
}


/**
 *
 */
#ifdef IS_HILDON
void
EditControls::
on_mark_set (const Gtk::TextBuffer::iterator& iter_,
			 const Glib::RefPtr<Gtk::TextBuffer::Mark>& mark_)
{
	Glib::RefPtr<Gtk::TextBuffer::Mark> start;
	Glib::RefPtr<Gtk::TextBuffer::Mark> end;
	
	start = m_textbuf->get_insert ();
	end   = m_textbuf->get_selection_bound ();

	if (start == mark_ || end == mark_) { // Selection changed
		if (start != end) {
			m_start_mark = start;
			m_end_mark   = end;
		}
	}
}

#endif
