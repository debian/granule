// -*- c++ -*-
//------------------------------------------------------------------------------
//                              TopMenuBar.h
//------------------------------------------------------------------------------
// $Id: TopMenuBar.h,v 1.19 2008/06/16 02:36:53 vlg Exp $
//------------------------------------------------------------------------------
//  Copyright (c) 2004,2006-2007 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version
//  2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------
// 01/03/2004 VLG  Created
//------------------------------------------------------------------------------
#ifndef TOP_MENU_BAR_H
#define TOP_MENU_BAR_H

#include <vector>
using std::vector;

#include <gtkmm/menu.h>
#include <gtkmm/menubar.h>
#include <gtkmm/toolbar.h>

#include "Granule-main.h"

class MainWindow;
class DeckManager;

using namespace Gtk;
using namespace Menu_Helpers;

/**
 * @class TopMenuBar
 *
 * This class holds the top menues.
 */
class TopMenuBar : public sigc::trackable
{
public:
    TopMenuBar (MainWindow& mw_, DeckManager& dm_);
    ~TopMenuBar ();
    
    void     create ();
	bool     is_created () const { return m_created; }

    MenuBar* get_menu_bar ();

    void     add_history ();
    void     refill_history_list ();
    
    void set_sensitivity (MWState state_);
    void set_deck_sensitivity (bool is_deck_selected_, size_t num_open_decks_);

#ifdef IS_HILDON
	void create_hildon_menu ();
#endif

private:			// no cloning
    TopMenuBar (const TopMenuBar&);        
    TopMenuBar& operator= (const TopMenuBar&);

private:	
	MainWindow&  m_mw;			// These are needed because of c'tor's
	DeckManager& m_dm;			// circular dependency

	bool         m_created; 
    bool         m_loaded;	// MainWin to let us know when list is loaded

    MenuBar*     m_menu_bar;

    Menu*        m_file_menu;
	Menu*        m_cardfile_menu;
	Menu*        m_deck_menu;
    Menu*        m_play_menu;
	Menu*        m_add_to_box_menu;
	Menu*        m_export_menu;
    Menu*        m_open_recent_menu;

	// -*- [File] items -*-
	//
    MenuItem*    m_preferences;
    MenuItem*    m_exit;

	// -*- [CardFile] items -*-
	//
    MenuItem*    m_new;
    MenuItem*    m_open;
    MenuItem*    m_open_recent;
    MenuItem*    m_save;
    MenuItem*    m_save_as;
    MenuItem*    m_export;
    MenuItem*    m_close;
    MenuItem*    m_play_in;
	MenuItem*    m_cb_preferences;  // CardBox Preferences

	// -*- [Deck] items -*-
	//
    MenuItem*    m_deck_new;
    MenuItem*    m_deck_open;
    MenuItem*    m_deck_insert;
    MenuItem*    m_deck_import;
    MenuItem*    m_deck_export;
    MenuItem*    m_deck_save;
    MenuItem*    m_deck_save_as;
    MenuItem*    m_deck_save_all;
    MenuItem*    m_deck_play;
	MenuItem*    m_deck_unselect;
	MenuItem*    m_add_to_box;
};

inline
TopMenuBar::
TopMenuBar (MainWindow& mw_, DeckManager& dm_) :
	m_mw (mw_),
	m_dm (dm_),
	m_created (false),
	m_file_menu (NULL), 
	m_new (NULL),           
	m_open (NULL),      	m_open_recent (NULL),   
	m_save (NULL),          m_save_as (NULL), 
	m_export (NULL),        m_close (NULL),
	m_play_in (NULL),
	m_deck_new (NULL),      m_deck_open (NULL), 
	m_deck_insert (NULL),   m_deck_import (NULL),   
	m_deck_export (NULL),
	m_deck_save (NULL),     m_deck_save_as (NULL), 
	m_deck_save_all (NULL), m_deck_play (NULL),
	m_deck_unselect (NULL), m_add_to_box (NULL)
{
    trace("TopMenuBar::TopMenuBar");
}

inline
TopMenuBar::
~TopMenuBar ()
{
    trace("TopMenuBar::~TopMenuBar");
}

#endif /* TOP_MENU_BAR_H */
