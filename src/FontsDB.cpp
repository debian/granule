// -*- c++ -*-
//------------------------------------------------------------------------------
//                            FontsDB.cpp
//------------------------------------------------------------------------------
// $Id: FontsDB.cpp,v 1.1 2007/12/02 00:31:24 vlg Exp $
//------------------------------------------------------------------------------
//  Copyright (c) 2007 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date   : Mon Sep 17 2007
//
//------------------------------------------------------------------------------

#include "FontsDB.h"

#include "Granule.h"
#include "FontsSelectorUI.h"

using ASSA::IniFile;

/**-----------------------------------------------------------------------------
 *	Implementation of FontsDB Model.
 **-----------------------------------------------------------------------------
 */
void
FontsDB::
init_global_defaults ()
{
	set_question_font ("Sans 14");
	set_answer_font   ("Sans 14");
	set_example_font  ("Sans 14");
	set_input_font    ("Sans 12");

/**  If fonts description is missing, you need to find out the default
 *	 font of the active theme. Maemo default theme has overblown fonts,
 *	 so we scale them down to fit windows with 800x480 resolution.
 *
 *   Glib::RefPtr<Gtk::Style> style = m_example.get_style ();
 *   Pango::FontDescription default_font = style->get_font ();
 *   set_app_font (default_font.to_string ());
 *
 **/
#ifdef IS_HILDON
	set_app_font ("Bitstream Vera Sans 12");
#endif
}

void
FontsDB::
init_deck_defaults ()
{
	set_question_font ("Sans 14");
	set_answer_font   ("Sans 14");
	set_example_font  ("Sans 14");
	set_input_font    ("Sans 12");

#ifdef IS_HILDON
	set_app_font ("Bitstream Vera Sans 12");
#endif
}

/**********************************************
 * Load fonts configuration settings from XML *
 **********************************************/
void
FontsDB::
load_from_xml_config (xmlDocPtr parser_, std::string& error_msg_)
{
	xmlChar* result;

	/*****************
	 * Question font *
	 *****************/
	result = Granule::get_node_by_xpath (parser_, 
									 "/deck/appearance/fonts/@question_font");
	if (result == NULL) {
		return;
	}
	set_question_font ((const char*) result);
	xmlFree (result);

	/*****************
	 * Answer font *
	 *****************/
	result = Granule::get_node_by_xpath (parser_, 
									 "/deck/appearance/fonts/@answer_font");
	if (result == NULL) {
		return;
	}
	set_answer_font ((const char*) result);
	xmlFree (result);

	/*****************
	 * Example font  *
	 *****************/
	result = Granule::get_node_by_xpath (parser_, 
									 "/deck/appearance/fonts/@example_font");
	if (result == NULL) {
		return;
	}
	set_example_font ((const char*) result);
	xmlFree (result);

	/*****************
	 * Input font    *
	 *****************/
	result = Granule::get_node_by_xpath (parser_, 
									 "/deck/appearance/fonts/@input_font");
	if (result == NULL) {
		return;
	}
	set_input_font ((const char*) result);
	xmlFree (result);

	/*****************
	 * App font      *
	 *****************/
	result = Granule::get_node_by_xpath (parser_, 
									 "/deck/appearance/fonts/@app_font");
	if (result == NULL) {
		return;
	}
	set_app_font ((const char*) result);
	xmlFree (result);
}

void
FontsDB::
save_to_xml_config (xmlTextWriterPtr writer_)
{
	int ret = 0;

	// Start <fonts> section
	//
	ret = xmlTextWriterStartElement (writer_, BAD_CAST "fonts");

	ret = xmlTextWriterWriteAttribute (writer_, BAD_CAST "question_font",
						   BAD_CAST m_question_font_desc.to_string ().c_str ());

	ret = xmlTextWriterWriteAttribute (writer_, BAD_CAST "answer_font",
						   BAD_CAST m_answer_font_desc.to_string ().c_str ());

	ret = xmlTextWriterWriteAttribute (writer_, BAD_CAST "example_font",
						   BAD_CAST m_example_font_desc.to_string ().c_str ());

	ret = xmlTextWriterWriteAttribute (writer_, BAD_CAST "input_font",
						   BAD_CAST m_input_font_desc.to_string ().c_str ());

	ret = xmlTextWriterWriteAttribute (writer_, BAD_CAST "app_font",
						   BAD_CAST m_app_font_desc.to_string ().c_str ());

	// Close </fonts>
	//
	ret = xmlTextWriterEndElement (writer_);	// @fonts
}

/***************************************************
 * Load fonts configuration settings from INI file *
 ***************************************************
 *
 * [Default]
 *    ...
 *    question_font_desc="Sans 14"
 *    answer_font_desc="Sans 14"
 *    example_font_desc="Sans 14"
 *    input_font_desc="Sans 14"
 *    app_font_desc="Bitstream Vera Sans 12"
 *    ...
 *
 ***************************************************
 */
void
FontsDB::
load_from_ini_config (ASSA::IniFile* inifile_)
{
	Glib::ustring ustr;

	if (inifile_ == NULL) {
		return;
	}
    ustr = inifile_->get_value ("Default", "question_font_desc");
    if (! ustr.empty ()) {
		set_question_font (ustr);
    }

    ustr = inifile_->get_value ("Default", "answer_font_desc");
    if (! ustr.empty ()) {
		set_answer_font (ustr);
    }

    ustr = inifile_->get_value ("Default", "example_font_desc");
    if (! ustr.empty ()) {
		set_example_font (ustr);
    }

    ustr = inifile_->get_value ("Default", "input_font_desc");
    if (! ustr.empty ()) {
		set_input_font (ustr);
    }

    ustr = inifile_->get_value ("Default", "app_font_desc");
    if (! ustr.empty ()) {
		set_app_font (ustr);
    }
}

void
FontsDB::
save_to_ini_config (ASSA::IniFile* inifile_)
{
	Glib::ustring us;

    us = m_question_font_desc.to_string ();
    inifile_->set_pair("Default", IniFile::tuple_type("question_font_desc",us));

    us = m_answer_font_desc.to_string ();
    inifile_->set_pair("Default", IniFile::tuple_type("answer_font_desc", us));

    us = m_example_font_desc.to_string ();
    inifile_->set_pair("Default", IniFile::tuple_type("example_font_desc", us));

    us = m_input_font_desc.to_string ();
    inifile_->set_pair("Default", IniFile::tuple_type("input_font_desc", us));

    us = m_app_font_desc.to_string ();
    inifile_->set_pair("Default", IniFile::tuple_type("app_font_desc", us));
}

/** Update View with data from the Model
 */
void
FontsDB::
update_view (FontsSelectorUI*& view_)
{
	view_->set_text (QFONT, get_question_font());
	view_->set_text (AFONT, get_answer_font  ());
	view_->set_text (EFONT, get_example_font ());
	view_->set_text (IFONT, get_input_font   ());
	view_->set_text (PFONT, get_app_font     ());
}

/** Store data from View to the Model.
 */
void
FontsDB::
fetch_from_view (FontsSelectorUI*& view_)
{
	set_question_font (view_->get_text (QFONT));
	set_answer_font   (view_->get_text (AFONT));
	set_example_font  (view_->get_text (EFONT));
	set_input_font    (view_->get_text (IFONT));
	set_app_font      (view_->get_text (PFONT));
}
