// -*- c++ -*-
//------------------------------------------------------------------------------
//                         CardViewHelpDialog.h
//------------------------------------------------------------------------------
// $Id: CardViewHelpDialog.h,v 1.8 2008/02/04 04:02:20 vlg Exp $
//
//  Copyright (c) 2005 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//
//  Date: Jun 8, 2005
//------------------------------------------------------------------------------

#ifndef CARD_VIEW_HELP_DIALOG_H
#define CARD_VIEW_HELP_DIALOG_H

#include <gtkmm/dialog.h>
#include <gtkmm/button.h>
#include <gtkmm/textview.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/buttonbox.h>
#include "Granule-main.h"


#ifdef IS_HILDON
#    include <hildonmm.h>
#endif

#ifdef IS_HILDON
class CardViewHelpDialog : public Hildon::Window
#else
class CardViewHelpDialog : public Gtk::Window
#endif
{  
public:
	CardViewHelpDialog (Gtk::Window& parent_);

private:
	void create_tags (Glib::RefPtr<Gtk::TextBuffer>& ref_buffer_);
	void insert_text (Glib::RefPtr<Gtk::TextBuffer>& ref_buffer_);

	void on_close_clicked () { hide (); }

	virtual bool on_delete_event(GdkEventAny* any_);

private:
	Gtk::Window*          m_dialog;
	Gtk::VBox             m_vbox;

	Gtk::Button*          m_close_button;
	Gtk::TextView*        m_textview;
	Gtk::ScrolledWindow*  m_scrolledwindow;

#ifdef IS_HILDON
	Glib::RefPtr<Gtk::TextBuffer> m_textbuf_ref;
#endif
};

inline bool
CardViewHelpDialog::
on_delete_event (GdkEventAny* any_)	
{
	hide ();
	return true;
}

#endif
