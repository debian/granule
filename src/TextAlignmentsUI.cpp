// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: TextAlignmentsUI.cpp,v 1.2 2008/08/16 03:13:09 vlg Exp $
//------------------------------------------------------------------------------
//                            TextAlignmentsUI.cpp
//------------------------------------------------------------------------------
//  Copyright (c) Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date   : Wed Nov 1 2006
//
//------------------------------------------------------------------------------
#include <gtkmm/button.h>
#include <gtkmm/buttonbox.h>
#include <gtkmm/label.h>
#include <gtkmm/frame.h>
#include <gtkmm/box.h>
#include <gtkmm/alignment.h>

#include "TextAlignmentsUI.h"

#include "Intern.h"             // i18n macros

using sigc::mem_fun;

/**-----------------------------------------------------------------------------
 *	Some useful macros
 **-----------------------------------------------------------------------------
 */
static void TA_SET_LABEL (Gtk::Label* l_, bool m_)
{
	l_->set_alignment   (0.5,0.5); 
	l_->set_padding     (5,0);     
	l_->set_justify     (Gtk::JUSTIFY_LEFT); 
	l_->set_line_wrap   (false);   
	l_->set_use_markup  (m_);       
	l_->set_selectable  (false);   
#ifndef IS_HILDON
#if (GTKMM_MAJOR_VERSION == 2 && GTKMM_MINOR_VERSION >= 6)
	l_->set_ellipsize   (Pango::ELLIPSIZE_NONE); 
	l_->set_width_chars (-1);      
	l_->set_angle       (0);       
	l_->set_single_line_mode (false); 
#endif
#endif
}

static void TA_SET_FRAME (Gtk::Frame*     f_, 
						  Gtk::Label*     l_, 
						  Gtk::Alignment* a_,
						  Gtk::ShadowType etching_)
{
	f_->set_border_width (4); 
	f_->set_shadow_type  (etching_);
	f_->set_label_align  (Gtk::ALIGN_TOP, Gtk::ALIGN_CENTER);
	f_->set_label_widget (*l_); 
	f_->add              (*a_);
}

static void TA_SET_ENTRY (Gtk::Entry* e_)
{
	e_->set_flags       (Gtk::CAN_FOCUS);
	e_->set_visibility  (true);
	e_->set_editable    (true);
	e_->set_max_length  (0);
	e_->set_has_frame   (true);
	e_->set_width_chars (4);
	e_->set_activates_default (false);
}

/**-----------------------------------------------------------------------------
 *	TextAlignmentsUI
 *
 *  The widget encapsulates text label alignment controls.
 *  It consists of the x and y alignments and paragraph justification.
 *
 *     ---------------
 * +---| frame_label |------------------^-------------------------+<--m_frame
 * |   ---------------                  | (6pts)                  |
 * | +----------------------------------v------------------------+<--alignment
 * | |+---------------------------------------------------------+||
 * | ||    ---------------                                      |<---vbox
 * | ||+---| block_label |--------------^----------------------+|||
 * | |||   ---------------              | (6pts)               |<----block_frame
 * | |||+-------------------------------v---------------------+||||
 * | ||||+---------+---------------+---------+---------------+|||||
 * | ||||| x_label | x_align_combo | y_label | y_align_combo |<--text_block_hbox
 * | ||||+---------+---------------+---------+---------------+|||||
 * | |||+-----------------------------------------------------+<-block_alignment
 * | ||+-------------------------------------------------------+|||
 * | |+---------------------------------------------------------+||
 * | +-----------------------------------------------------------+|
 * +--------------------------------------------------------------+
 *
 **-----------------------------------------------------------------------------
 */
TextAlignmentsUI::
TextAlignmentsUI (const char*   title_,
				  value_list_t& x_align_list_,
				  value_list_t& y_align_list_,
				  value_list_t& paragraph_list_,
				  value_list_t& img_position_list_,
				  bool          show_img_position_)
{
	Gtk::TreeModel::Row row;
	vlist_citer_t citer;

	/*------------------------------------
	 * Front Frame
	 *------------------------------------
	 */
	Gtk::Label*     frame_label;
	Gtk::Alignment* alignment;
	Gtk::VBox*      vbox;

	/*-- Text Block --*/
	Gtk::Frame*     block_frame;
	Gtk::Alignment* block_alignment;
	Gtk::Label*     block_label;

	Gtk::HBox*      text_block_hbox;
	Gtk::Label*     x_label;
	/*              m_x_align_combo; */
	Gtk::Label*     y_label;
	/*              m_y_align_combo; */

	/*-- Multiline (Paragraph) Text --*/
	Gtk::Frame*     paragraph_frame;
	Gtk::Alignment* paragraph_alignment;
	Gtk::Label*     paragraph_label;

	Gtk::HBox*      paragraph_hbox;
	Gtk::Label*     paragraph_label2;
	/*              m_paragraph_combo; */

	/*-- Padding Block --*/
	Gtk::Frame*     padding_frame;
	Gtk::Alignment* padding_alignment;
	Gtk::Label*     padding_label;

	Gtk::HBox*      padding_block_hbox;
	Gtk::Label*     x_padding_label;
	/*              m_x_padding_entry; */
	Gtk::Label*     y_padding_label;
	/*              m_y_padding_entry; */

	/*-- Image Aligment --*/
	Gtk::Frame*     img_position_frame;
	Gtk::Alignment* img_position_alignment;
	Gtk::Label*     img_position_label;

	Gtk::HBox*      img_position_hbox;
	Gtk::Label*     img_position_label2;
	/*              m_img_position_combo; */

	/**************************************
	 *
	 * Front Alignment
	 *   
	 **************************************/

	m_frame     = Gtk::manage (new Gtk::Frame());
	frame_label = Gtk::manage (new Gtk::Label(_(title_)));
	alignment   = Gtk::manage (new Gtk::Alignment(0.5, 0.5, 1, 1));
	vbox        = Gtk::manage (new Gtk::VBox(false, 0));

	/*-- Text Block --*/
	block_frame     = Gtk::manage (new Gtk::Frame());
	block_alignment = Gtk::manage (new Gtk::Alignment(0.5, 0.5, 1, 1));
	block_label     = Gtk::manage (new Gtk::Label(_("<b>Text Block</b>")));

	text_block_hbox = Gtk::manage (new Gtk::HBox(false, 0));

	/** [x:] <Choice>
	 */
	x_label         = Gtk::manage (new Gtk::Label(_("x:")));
	m_x_align_combo = Gtk::manage (new Gtk::ComboBox());

	m_falign_x_tree_model  = Gtk::ListStore::create (m_falign_x_col);
	m_x_align_combo->set_model (m_falign_x_tree_model);

	citer = x_align_list_.begin ();
	while (citer != x_align_list_.end ()) {
		row = *(m_falign_x_tree_model->append ());
		row [m_falign_x_col.m_value] = *citer;
		citer++;
	}

	m_x_align_combo->pack_start (m_falign_x_col.m_value);
	m_x_align_combo->set_active (0);

	/** [y:] <Choice>
	 */
	y_label         = Gtk::manage (new Gtk::Label(_("y:")));
	m_y_align_combo = Gtk::manage (new Gtk::ComboBox());

	m_falign_y_tree_model = Gtk::ListStore::create (m_falign_y_col);
	m_y_align_combo->set_model (m_falign_y_tree_model);

	citer = y_align_list_.begin ();
	while (citer != y_align_list_.end ()) {
		row = *(m_falign_y_tree_model->append ());
		row [m_falign_y_col.m_value] = *citer;
		citer++;
	}

	m_y_align_combo->pack_start (m_falign_y_col.m_value);
	m_y_align_combo->set_active (0);

	/**-- Multiline Text Block --
	 */
	paragraph_frame = Gtk::manage (new Gtk::Frame());
	paragraph_alignment = Gtk::manage (new Gtk::Alignment(0.5,0.5,1,1));
	paragraph_label = Gtk::manage (new Gtk::Label(_("<b>Multiline Text</b>")));
		

	/** [Justification:] <Choice>
	 */
	paragraph_hbox    = Gtk::manage (new Gtk::HBox(false, 0));
	paragraph_label2  = Gtk::manage (new Gtk::Label(_("Justification: ")));
	m_paragraph_combo = Gtk::manage (new Gtk::ComboBox());

	m_justify_tree_model = Gtk::ListStore::create (m_justify_col);
	m_paragraph_combo->set_model (m_justify_tree_model);

	citer = paragraph_list_.begin ();
	while (citer != paragraph_list_.end ()) {
		row = *(m_justify_tree_model->append ());
		row [m_justify_col.m_value] = *citer;
		citer++;
	}

	m_paragraph_combo->pack_start (m_justify_col.m_value);
	m_paragraph_combo->set_active (0);

	/**-- Image Alignment --
	 */
	img_position_frame = Gtk::manage (new Gtk::Frame());
	img_position_alignment = Gtk::manage (new Gtk::Alignment(0.5,0.5,1,1));
	img_position_label = Gtk::manage (new Gtk::Label(_("<b>Picture Alignment</b>")));

	/** [Justification:] <Choice>
	 */
	img_position_hbox    = Gtk::manage (new Gtk::HBox(false, 0));
	img_position_label2  = Gtk::manage (new Gtk::Label(_("Position: ")));
	m_img_position_combo = Gtk::manage (new Gtk::ComboBox());

	m_img_position_tree_model = Gtk::ListStore::create (m_img_position_col);
	m_img_position_combo->set_model (m_img_position_tree_model);

	citer = img_position_list_.begin ();
	while (citer != img_position_list_.end ()) {
		row = *(m_img_position_tree_model->append ());
		row [m_img_position_col.m_value] = *citer;
		citer++;
	}

	m_img_position_combo->pack_start (m_img_position_col.m_value);
	m_img_position_combo->set_active (0);

	/*-- Padding Block --
	 */
	padding_frame     = Gtk::manage (new Gtk::Frame());
	padding_alignment = Gtk::manage (new Gtk::Alignment(0.5, 0.5, 1, 1));
	padding_label     = Gtk::manage (new Gtk::Label(_("<b>Text Padding</b>")));

	padding_block_hbox = Gtk::manage (new Gtk::HBox(false, 0));

	/** [x:] <Choice>
	 */
	x_padding_label   = Gtk::manage (new Gtk::Label(_("x:")));
	m_x_padding_entry = Gtk::manage (new Gtk::Entry ());

	/* TODO: set initial value */

	/** [y:] <Choice>
	 */
	y_padding_label   = Gtk::manage (new Gtk::Label(_("y:")));
	m_y_padding_entry = Gtk::manage (new Gtk::Entry ());

	/* TODO: set initial value */

	/*-- Pack Front Text Block --*/

	TA_SET_LABEL(x_label,false);
	TA_SET_LABEL(y_label,false);

	text_block_hbox->pack_start (*x_label, 	       Gtk::PACK_SHRINK, 6);
	text_block_hbox->pack_start (*m_x_align_combo, Gtk::PACK_SHRINK, 0);
	text_block_hbox->pack_start (*y_label, 		   Gtk::PACK_SHRINK, 6);
	text_block_hbox->pack_start (*m_y_align_combo, Gtk::PACK_SHRINK, 0);

	block_alignment->set_border_width (6);
	block_alignment->add (*text_block_hbox);

	TA_SET_LABEL (x_padding_label,false);
	TA_SET_LABEL (y_padding_label,false);

	TA_SET_ENTRY (m_x_padding_entry);
	TA_SET_ENTRY (m_y_padding_entry);

	padding_block_hbox->pack_start (*x_padding_label,   Gtk::PACK_SHRINK, 6);
	padding_block_hbox->pack_start (*m_x_padding_entry, Gtk::PACK_SHRINK, 0);
	padding_block_hbox->pack_start (*y_padding_label,   Gtk::PACK_SHRINK, 6);
	padding_block_hbox->pack_start (*m_y_padding_entry, Gtk::PACK_SHRINK, 0);

	padding_alignment->set_border_width (6);
	padding_alignment->add (*padding_block_hbox);

	TA_SET_LABEL(block_label,true);
	TA_SET_FRAME (block_frame, 
				  block_label, 
				  block_alignment, 
				  Gtk::SHADOW_NONE);

	TA_SET_LABEL(padding_label,true);
	TA_SET_FRAME (padding_frame, 
				  padding_label, 
				  padding_alignment, 
				  Gtk::SHADOW_NONE);

	/*-- Pack Paragraph Justification Block --*/

	TA_SET_LABEL(paragraph_label2,false);
	paragraph_hbox->set_border_width(3);

	paragraph_hbox->pack_start (*paragraph_label2,  Gtk::PACK_SHRINK, 0);
	paragraph_hbox->pack_start (*m_paragraph_combo, Gtk::PACK_SHRINK, 0);

	TA_SET_LABEL(paragraph_label,true);

	paragraph_alignment->set_border_width(6);
	paragraph_alignment->add (*paragraph_hbox);

	TA_SET_FRAME (paragraph_frame,
				  paragraph_label,
				  paragraph_alignment,
				  Gtk::SHADOW_NONE);

	/*-- Pack Image Position Block --*/

	TA_SET_LABEL(img_position_label2,false);
	img_position_hbox->set_border_width(3);

	img_position_hbox->pack_start (*img_position_label2,  Gtk::PACK_SHRINK, 0);
	img_position_hbox->pack_start (*m_img_position_combo, Gtk::PACK_SHRINK, 0);

	TA_SET_LABEL(img_position_label,true);

	img_position_alignment->set_border_width(6);
	img_position_alignment->add (*img_position_hbox);

	TA_SET_FRAME (img_position_frame,
				  img_position_label,
				  img_position_alignment,
				  Gtk::SHADOW_NONE);

	/*-- Put all components together --*/

	vbox->set_border_width (3);

	vbox->pack_start (*block_frame,     Gtk::PACK_SHRINK, 0);
	vbox->pack_start (*paragraph_frame, Gtk::PACK_SHRINK, 0);
	vbox->pack_start (*padding_frame,   Gtk::PACK_SHRINK, 0);

	if (show_img_position_) {
		vbox->pack_start (*img_position_frame, Gtk::PACK_SHRINK, 0);
	}

	alignment->add (*vbox);

	TA_SET_LABEL (frame_label,true);
	TA_SET_FRAME (m_frame,
				  frame_label,
				  alignment,
				  Gtk::SHADOW_ETCHED_IN);
}

TextAlignmentsUI::value_list_t 
TextAlignmentsUI::
make_align_x_list ()
{
	TextAlignmentsUI::value_list_t result;

	result.push_back ("center");
	result.push_back ("left");
	result.push_back ("right");	

	return result;
}

TextAlignmentsUI::value_list_t 
TextAlignmentsUI::
make_align_y_list ()
{
	TextAlignmentsUI::value_list_t result;

	result.push_back ("center");
	result.push_back ("top");
	result.push_back ("bottom");

	return result;
}

TextAlignmentsUI::value_list_t
TextAlignmentsUI::
make_paragraph_list ()
{
	TextAlignmentsUI::value_list_t result;

	result.push_back ("center");
	result.push_back ("left");
	result.push_back ("right");
	result.push_back ("fill");

	return result;
}

TextAlignmentsUI::value_list_t
TextAlignmentsUI::
make_img_position_list ()
{
	TextAlignmentsUI::value_list_t result;

	result.push_back ("top");
	result.push_back ("bottom");

	return result;
}
