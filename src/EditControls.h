// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: EditControls.h,v 1.8 2008/05/24 22:58:51 vlg Exp $
//------------------------------------------------------------------------------
//                            EditControls.h
//------------------------------------------------------------------------------
//  Copyright (c) 2005,2006 by Vladislav Grinchenko 
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date   : Nov 13 2005
//
//------------------------------------------------------------------------------
#ifndef EDIT_CONTROLS_H
#define EDIT_CONTROLS_H

#include <gtkmm/box.h>
#include <gtkmm/textview.h>
#include <gdkmm/pixbuf.h>
#include <gtkmm/image.h>
#include <gtkmm/clipboard.h>
#include <gtkmm/uimanager.h>
#include <gtkmm/toolbar.h>

using sigc::bind;
using sigc::mem_fun;

#include <vector>
#include <string>

using std::vector;
using std::string;

/**=============================================================================
 ** MyIconInfo holds all information pertinent to the icon.
 **=============================================================================
 */
class MyIconInfo
{
public:
	MyIconInfo (Gtk::StockID sid_, 
				Gtk::Image* imgp_, 
				const char* tooltip_,
				const Glib::ustring& accel_key_ = "<control>z");

	Gtk::StockID  get_stock_id () const { return m_stock_id;               }
	Glib::ustring get_name     () const { return m_stock_id.get_string (); }
	Gtk::Image*   get_image    () const { return m_image_ref;              }
	Glib::ustring get_tooltip  () const { return m_tooltip;                }
	Glib::ustring get_accel_key () const { return m_accel_key; }

private:
	Gtk::StockID   m_stock_id;
	Gtk::Image*    m_image_ref;
	Glib::ustring  m_tooltip;
	Glib::ustring  m_accel_key;	     // accelerator key
};

inline
MyIconInfo::
MyIconInfo (Gtk::StockID sid_, Gtk::Image* imgp_, const char* tooltip_,
			const Glib::ustring& accel_key_)
	:
	m_stock_id (sid_), m_image_ref (imgp_), m_tooltip (tooltip_),
	m_accel_key (accel_key_)
{ 
	/* no-op */
}

/**=============================================================================
 ** EditControls is a collection of markup edit buttons that are embedded 
 ** in CardView for quick editing the text in the input fields.
 **=============================================================================
 */
class EditControls :  public Gtk::HBox
{
public:
	EditControls (Glib::RefPtr<Gtk::TextBuffer> tb_ref_);

	void on_focus_changed (Gtk::TextView* text_view_);
	void on_clipboard_text_received (const Glib::ustring& text_);

#ifdef IS_HILDON
	void on_mark_set (const Gtk::TextBuffer::iterator& iter_,
					  const Glib::RefPtr<Gtk::TextBuffer::Mark>& mark_);
#endif

private:
	void on_edit_button_clicked (const string& action_);

	void remove_tags (Glib::RefPtr<Gtk::TextBuffer::Mark> begin_mark_,
					  Glib::RefPtr<Gtk::TextBuffer::Mark> end_mark_);

	void add_index   (Glib::RefPtr<Gtk::TextBuffer::Mark> begin_mark_,
					  Glib::RefPtr<Gtk::TextBuffer::Mark> end_mark_);

	void normalize_text ();

	static gunichar display_unichar_at (Gtk::TextBuffer::iterator iter_);

private:
	static Gtk::StockID BOLD;
	static Gtk::StockID ITALIC;
	static Gtk::StockID UNDERLINE;
	static Gtk::StockID MONOSPACE;
	static Gtk::StockID SUBSCRIPT;
	static Gtk::StockID SUPERSCRIPT;
	static Gtk::StockID BIG;
	static Gtk::StockID SMALL;
	static Gtk::StockID REMOVETAGS;
	static Gtk::StockID PASTE;
	static Gtk::StockID NORMALIZE;
	static Gtk::StockID ADDINDEX;

private:
	typedef MyIconInfo icon_t;
	typedef vector<icon_t> icon_list_t;

	icon_list_t m_icon_list;

	Glib::RefPtr<Gtk::TextBuffer>  m_textbuf;
	Glib::RefPtr<Gtk::UIManager>   m_uimanager;
	Glib::RefPtr<Gtk::ActionGroup> m_actgroup;

	u_int m_index_count;

#ifdef IS_HILDON
	Glib::RefPtr<Gtk::TextBuffer::Mark> m_start_mark;
	Glib::RefPtr<Gtk::TextBuffer::Mark> m_end_mark;
#endif
};


inline gunichar
EditControls::
display_unichar_at (Gtk::TextBuffer::iterator iter_)
{
	gunichar c;
	return ((c = iter_.get_char ()) ? c : ' ');
}

#endif /* EDIT_CONTROLS_H */
