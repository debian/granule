// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: AutoFillDB.cpp,v 1.3 2007/12/02 00:31:24 vlg Exp $
//------------------------------------------------------------------------------
//                            FontsDB.cpp
//------------------------------------------------------------------------------
//  Copyright (c) Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date   : Sun Oct 28 2007
//
//------------------------------------------------------------------------------

#include "AutoFillDB.h"

#include "Granule.h"
#include "CardAutoFill.h"
#include "Card.h"

/**-----------------------------------------------------------------------------
 *	Implementation of AutoFillDB Model.
 **-----------------------------------------------------------------------------
 */
void
AutoFillDB::
init_global_defaults ()
{
	set_enabled (false);
}


/** Update View with data from the Model
 */
void
AutoFillDB::
update_view (CardAutoFill& view_)
{
	view_.set_text (ASF,  get_asf_pattern  ());
	view_.set_text (BACK, get_back_pattern ());
	view_.set_text (ASB,  get_asb_pattern  ());

	view_.mark_enabled (is_enabled ());
}

/** Store data from View to the Model.
 */
void
AutoFillDB::
fetch_from_view (CardAutoFill& view_)
{
	set_asf_pattern  (view_.get_text (ASF) );
	set_back_pattern (view_.get_text (BACK));
	set_asb_pattern  (view_.get_text (ASB) );

	set_enabled (view_.is_enabled ());
}

/** If it is not a real card, don't even bother.
 *  Only non-empty patterns are applied.
 */
void
AutoFillDB::
apply_patterns (Card* card_, int seqnum_)
{
	static char buffer [1024];

	if (!is_enabled ()) {
		return;
	}

	Glib::ustring pattern;

	pattern = get_asf_pattern ();
	if (pattern.size ()) {
		snprintf (buffer, sizeof(buffer)-1, pattern.c_str (), seqnum_);
		card_->set_alt_spelling (ASF, buffer);
	}

	pattern = get_back_pattern ();
	if (pattern.size ()) {
		snprintf (buffer, sizeof(buffer)-1, pattern.c_str (), seqnum_);
		card_->set_answer (buffer);
	}

	pattern = get_asb_pattern ();
	if (pattern.size ()) {
		snprintf (buffer, sizeof(buffer)-1, pattern.c_str (), seqnum_);
		card_->set_alt_spelling (ASB, buffer);
	}
}

