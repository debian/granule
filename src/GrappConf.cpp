// -*- c++ -*-
//------------------------------------------------------------------------------
//                              GrappConf.cpp
//------------------------------------------------------------------------------
// $Id: GrappConf.cpp,v 1.22 2008/08/19 02:52:46 vlg Exp $
//------------------------------------------------------------------------------
//  Copyright (c) 2004-2006 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version
//  2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------
// 01/03/2004 VLG  Created
//------------------------------------------------------------------------------

#ifdef HAVE_CONFIG_H
#   include "config.h"
#endif

#include <limits.h>				// PATH_MAX
#include <string.h>				// strcpy(3)
#include <stdio.h>				// sprintf(3)
#include <sys/types.h>			// stat(3)
#include <sys/stat.h>			// stat(3)

#include <sstream>
using namespace std;

#include <glib.h>
#include <glib/gstdio.h>

#include <gtkmm/dialog.h>
#include <assa/IniFile.h>

#include "GrappConf.h"
#include "Granule.h"

using namespace ASSA;
using ASSA::Utils::split_pair;

ASSA_DECL_SINGLETON(GrappConf);

/******************************************************************************/
void 
SchedReview::
set_delay (const ASSA::TimeVal& load_time_,
		   int secs_in_day_, 
		   int secs_in_week_)
{
	ASSA::TimeVal hhmm (::atoi (m_hours.c_str ()) * 360 +
						::atoi (m_mins.c_str ()) * 60);
	m_tv = load_time_ + 
		ASSA::TimeVal (::atoi (m_days.c_str  ()) * secs_in_day_) +
		ASSA::TimeVal (::atoi (m_weeks.c_str ()) * secs_in_week_) +
		hhmm;
}

/******************************************************************************/
void 
SchedReview::
dump (int idx_)
{
	DL((GRAPP,"Deck %d sched = %s/%s/%s/%s (%ld secs : %s)\n", idx_,
		m_weeks.c_str (), m_days.c_str (), m_hours.c_str (), m_mins.c_str (), 
		m_tv.sec (), m_tv.fmtString ("%c").c_str ()));
}

/******************************************************************************/
GrappConf::
GrappConf () 
	:
    m_proj_name           ("untitled.cdf"),
    m_dont_save_project   (false),
	m_mwin_geometry       (0, 0, 430, 350),
	m_dplyr_geometry      (0, 0, 583, 350),
	m_crdview_geometry    (0, 0, 643, 386),
	m_dplyr_keep_aspect   (true),
	m_dplyr_aspect        (0, 0, 5, 3),
	m_snd_player_cmd      ("sox"),
	m_snd_player_args     ("%s -q -t ossdsp -v 1.7  -r 48000 /dev/dsp"),
	m_config              (NULL),
	m_remove_dups         (true),
	m_with_relative_paths (false),
	m_load_time           (ASSA::TimeVal::gettimeofday ()),
	m_root_x              (0),
    m_root_y              (0),
	m_disable_key_shortcuts (false),
	m_auto_pronounce       (false),
	m_show_va_control      (true),
	m_show_si_control      (true),
	m_show_sidebar_control (true),
	m_sidebar_location (SIDEBAR_LEFT),
    m_sidebar_width (SIDEBAR_MIN_WIDTH)
{
    trace_with_mask("GrappConf::GrappConf",GUITRACE);

    /** Always start with the current directory as pathname
	 */
    m_path_name = GRANULE->get_current_working_dir ();

	text_colors_db ().init_global_defaults ();
	fonts_db ().init_global_defaults ();
	app_db ().init_global_defaults ();

	/** Get Real User name. Retrieved from /etc/passwd, it comes in a format
		'John Doe,,301-JOH-NDOE'.
	 */
#ifdef GRAPP_DEPRICATED
#ifdef HAVE_CUSERID
	char* login_name = ::cuserid (NULL);
	if (login_name != NULL) {
		struct passwd* pw_info = ::getpwnam (login_name);
		if (pw_info != NULL) {
			m_user_name = pw_info->pw_gecos; // Not required by POSIX 1.01
			string::size_type idx = m_user_name.find (',');
			if (idx != string::npos) {
				m_user_name.replace (idx, m_user_name.size (), "");
			}
			DL((GRAPP,"Real User Name: \"%s\"\n", m_user_name.c_str ()));
		}
	}
#endif
#endif

	m_user_name = Glib::get_real_name ();
	if (m_user_name.length () == 0) {
		m_user_name = "John Doe";
	}
}

/******************************************************************************/
GrappConf::
~GrappConf ()
{
    trace_with_mask("GrappConf::~GrappConf",GUITRACE);
    dump ();

	if (m_config) {
		delete m_config;
	}
}

/******************************************************************************/
void
GrappConf::
dump () const
{
    trace_with_mask("GrappConf::dump",GUITRACE);

    DL((TRACE,"proj_name : \"%s\"\n", m_proj_name.c_str ()));
    DL((TRACE,"path_name : \"%s\"\n", m_path_name.c_str ()));

    dump_document_history ();
}

/******************************************************************************/
void
GrappConf::
dump_document_history () const
{
    if (m_history.size () == 0) {
		DL((APP,"Document history is empty\n"));
		return;
    }
    DL((APP,"=== Document history ===\n"));
    DHList_CIter cit = m_history.begin ();
    u_int idx = 0;
    while (cit != m_history.end ()) {
		DL((APP,"[%d] \"%s\"\n", idx++, (*cit).c_str ()));
		cit++;
    }
    DL((APP,"====== End history =====\n"));
}

/*******************************************************************************
 * Add the most recently visited project to the history list.
 * Keep up to the last five files only. 
 * 
 * RETURN: the file path if file was successfully added to the list;
 *         an empty string otherwise.
 */
string
GrappConf::
add_document_history ()
{
    trace_with_mask("GrappConf::add_document_history",GUITRACE);

    string fullpath;
	int hsz = 0;

    if (m_path_name.size () == 0) {
		fullpath = m_proj_name;
    }
    else {
		fullpath = m_path_name + G_DIR_SEPARATOR_S + m_proj_name;
    }

	if (fullpath != UNKNOWN_FNAME) {
		if ((hsz = m_history.size ()) > 0) {
			for (int idx = 0; idx < hsz; idx++) {
				if (m_history [idx] == fullpath) {
					m_history.erase (m_history.begin () + idx);
					break;
				}
			}
		}

		m_history.push_front (fullpath);
		if (m_history.size () > (u_int)m_history_size) {
			m_history.pop_back ();
		} 
	}
	dump_document_history ();
    return fullpath;
}

/*******************************************************************************
 * Remove project from document history list.
 * This happens when user tries to open project that doesn't exist
 * any longer.
 */
void
GrappConf::
remove_document_history (const string& fullpath_)
{
    trace_with_mask("GrappConf::remove_document_history",GUITRACE);

	size_t hsz;

    if ((hsz = m_history.size ()) == 0) {
		return;
	}

	for (u_int idx = 0; idx < hsz; idx++) 
	{
		if (m_history [idx] == fullpath_) {
			m_history.erase (m_history.begin () + idx);
			break;
		}
	}
}

/*******************************************************************************
 * Load configuration from ~/.granule
 *
 * History list can hold up to 5 elements.
 * Keys (history?) are not important.
 *
 * [History]
 *    0=/path/to/carddeck_0.gnl
 *    1=/path/to/carddeck_1.gnl
 *      ...
 *    4=/path/to/carddeck_4.gnl
 *
 * [FlipSide]
 *    front0=/path/to/Deck_One.xml
 *    back1=/path/to/Deck_Two.xml
 *    front2=/path/to/Deck_Three.xml
 *      ...
 *    {front|back}<index>=/path/to/file
 *
 * [Scheduling]  
 *    1=7/0/0/0
 *    2=0/1/0/0
 *    3=0/2/0/0
 *    4=0/3/0/0
 *    5=0/4/0/0
 *
 * [Default]
 *    filesel_path=/path/to/
 *
 *    text_bgcolor="white"
 *    text_fgcolor="black"
 *    sep_color="pink"
 *
 *    question_font_desc="Sans 14"
 *    answer_font_desc="Sans 14"
 *    example_font_desc="Sans 14"
 *    input_font_desc="Sans 14"
 *    app_font_desc="Bitstream Vera Sans 12"
 *
 *    front_x_alignment="center"
 *    front_y_alignment="center"
 *    front_x_padding="4"
 *    front_y_padding="0"
 *    front_justification="center"
 *
 *    back_x_alignment="center"
 *    back_y_alignment="center"
 *    back_x_padding="0"
 *    back_y_padding="0"
 *    back_justification="center"
 *
 *    example_x_alignment="center"
 *    example_y_alignment="center"
 *    example_x_padding="2"
 *    example_y_padding="0"
 *    example_justification="left"
 *
 *    mainwin_geometry=430x350
 *    cardview_geometry_height=643x386
 *    deckplayer_geometry=583x350
 *    deckplayer_aspect_height=3x5
 *    deckplayer_keep_aspect=true
 *
 *    show_verify_answer_control=true
 *    show_stats_info_control=true
 *
 *    snd_player_cmd="sox"
 *    snd_player_agrs="%s -t ossdsp -v 1.7  -r 48000 /dev/dsp"
 *    snd_archive_path=/usr/share/WyabdcRealPeopleTTS/
 *    auto_pronounce=false
 *
 *    remove_duplicates=true
 *    with_relative_paths=false
 *    history_size=5
 *
 *    root_x=120
 *    root_y=240
 */
void
GrappConf::
load_config (int secs_in_day_, int secs_in_week_)
{
    trace_with_mask("GrappConf::load_config",GUITRACE);

    Glib::ustring ustr;
    string s;
    int idx;
    IniFile::const_config_iterator section;

    if (m_config == NULL) {
		s = GRANULE->get_config_file ();
		if (s.size () == 0 || s == "none") {
			s = GRANULE->get_default_config_file ();
		}
		m_config = new IniFile (s);
		if (m_config->load () < 0) {
			DL((GRAPP, "Failed to load configuration file!\n"));
			return;
		}
		DL ((GRAPP,"Loaded configuration from \"%s\"\n", s.c_str ()));
		s = "";
    }
    /** Load [History] section
     */
    section = m_config->find_section ("History");
    if (section != m_config->sect_end ()) {
		DL((APP,"Scanning [History] section ...\n"));
		IniFile::const_tuple_iterator k = (*section).second.begin ();
		while (k != (*section).second.end ()) {
			m_history.push_back ((*k).second);
			k++;
		}
		DL((APP,"... loaded total %d history items.\n", m_history.size ()));
		dump_document_history ();
    }
    else {
		DL((APP,"Section [History] is not in ~/.granule\n"));
    }

    /** Load [FlipSide] section: front(first) = path(second).
     *  We store this info, however, path first to ease the lookup.
     */
    SideSelection ss;
    section = m_config->find_section ("FlipSide");
    DL((APP,"Scanning [FlipSide] section ...\n"));

    if (section != m_config->sect_end ()) {
		IniFile::const_tuple_iterator k = (*section).second.begin ();
		while (k != (*section).second.end ()) {
			if (Glib::file_test ((*k).second, Glib::FILE_TEST_IS_REGULAR)) {
				ss = ((*k).first.find ("front") == 0 ? FRONT : BACK);
				m_flipside_history.push_back (sidesel_t((*k).second, ss));
				DL((APP,"Found %s = %s\n", (*k).first.c_str (), 
					(*k).second.c_str ()));
			}
			k++;
		}
		DL((APP,"... loaded total %d flipside items.\n", 
			m_flipside_history.size ()));
    } 
    else {
		DL((APP,"Section [FlipSide] was not found in \"~/.granule\".\n"));
    }

    /** 
	 * Load [Scheduling] section. 
	 * We have to detect and convert old format:
	 *
	 *   old:  days/weeks
	 *   new:  weeks/days/hours/minutes
     */
    section = m_config->find_section ("Scheduling");

    if (section != m_config->sect_end ()) {
		DL((APP,"Scanning [History] section ...\n"));
		IniFile::const_tuple_iterator k = (*section).second.begin ();
		string lhs;
		string rhs;

		while (k != (*section).second.end ()) { 
			idx = ::atoi ((*k).first.c_str ()) - 1;
			split_pair ((*k).second, '/',
						m_sched [idx].m_days, m_sched [idx].m_weeks);

			if (split_pair (m_sched [idx].m_weeks, '/', lhs, rhs) == 0) {
				m_sched [idx].m_weeks = m_sched [idx].m_days;
				m_sched [idx].m_days  = lhs;
				m_sched [idx].m_hours = rhs;
				split_pair (m_sched [idx].m_hours, '/', lhs, rhs);
				m_sched [idx].m_hours = lhs;
				m_sched [idx].m_mins  = rhs;
			}
			m_sched [idx].set_delay (m_load_time, secs_in_day_, secs_in_week_);
			m_sched [idx].dump (idx);
			k++;
		}
    }

	/** 
	 * Set secs in day/week.
	 */
	m_secs_in_day  = secs_in_day_;
	m_secs_in_week = secs_in_week_;

    /************************************************************
	 *                 Load [Default] section                   *
     ************************************************************/

	/** Default configuration file shipped with Granule comes
	 *  with an empty path. This caused critical assertion with FileChooser:
	 *
	 *     "Gtk-CRITICAL **: gtk_file_system_unix_get_folder: 
	 *                       assertion `g_path_is_absolute (filename)' failed"
	 *
	 *  If empty or non-existent, we initialize it with 
	 *  the current working directory.
	 */
    m_filesel_path = m_config->get_value ("Default", "filesel_path");

	if (m_filesel_path.length () == 0 ||
		Glib::path_is_absolute (m_filesel_path) == FALSE)
	{
		m_filesel_path = Glib::get_current_dir ();
	}

    m_snd_player_cmd  = m_config->get_value ("Default", "snd_player_cmd");
    m_snd_player_args = m_config->get_value ("Default", "snd_player_args");

    if (m_config->get_value ("Default", "remove_duplicates") == "false") {
		m_remove_dups = false;
    }
    m_snd_archive_path = m_config->get_value ("Default", "snd_archive_path");
	m_history_size = ::atoi (m_config->get_value ("Default", 
												  "history_size").c_str ());
	if (m_history_size == 0) {	// conversion failed
		m_history_size = 5;
	}

	if (m_config->get_value ("Default", "with_relative_paths") == "true") {
		m_with_relative_paths = true;
	}

    DL((APP,"m_filesel_path     = \"%s\"\n", m_filesel_path.c_str ()));
    DL((APP,"m_font_desc        = \"%s\"\n", ustr.c_str ()));
    DL((APP,"m_snd_player_cmd   = \"%s\"\n", m_snd_player_cmd.c_str ()));
    DL((APP,"m_snd_player_args  = \"%s\"\n", m_snd_player_args.c_str ()));
    DL((APP,"m_snd_archive_path = \"%s\"\n", m_snd_archive_path.c_str ()));
    DL((APP,"m_relative_paths   = \"%s\"\n", 
		(m_with_relative_paths ? "true" : "false")));

	// Load text colors
	m_text_colors_db.load_from_ini_config (m_config);

	// Text fonts
	m_fonts_db.load_from_ini_config (m_config);

	// Load all appearance settings
	m_appearance.load_from_ini_config (m_config);

	/** Load geometry
	 */
    s = m_config->get_value ("Default", "mainwin_geometry");
    if (! s.empty ()) {
		str_to_geometry (s, m_mwin_geometry);
    }

    s = m_config->get_value ("Default", "cardview_geometry");
    if (! s.empty ()) {
		str_to_geometry (s, m_crdview_geometry);
    }

    s = m_config->get_value ("Default", "deckplayer_geometry");
    if (! s.empty ()) {
		str_to_geometry (s, m_dplyr_geometry);
    }

    if (m_config->get_value ("Default", "deckplayer_keep_aspect") == "false") {
		m_dplyr_keep_aspect = false;
    }
	else {
		m_dplyr_keep_aspect = true;
	}

    s = m_config->get_value ("Default", "deckplayer_aspect");
    if (! s.empty ()) {
		str_to_geometry (s, m_dplyr_aspect);
		if (m_dplyr_keep_aspect) { // enforce aspect based on height.
			m_dplyr_geometry.set_width (
				m_dplyr_geometry.get_height () * m_dplyr_aspect.get_width () /
				m_dplyr_aspect.get_height ());
		}
    }

    if (m_config->get_value ("Default", 
							 "show_verify_answer_control") == "false") {
		m_show_va_control = false;
    }
	else {
		m_show_va_control = true;
	}

    if (m_config->get_value ("Default", 
							 "show_stats_info_control") == "false") {
		m_show_si_control = false;
    }
	else {
		m_show_si_control = true;
	}

    if (m_config->get_value ("Default", 
							 "show_sidebar_control") == "false") {
		m_show_sidebar_control = false;
    }
	else {
		m_show_sidebar_control = true;
	}

    s = m_config->get_value ("Default", "sidebar_location");
    if (! s.empty ()) {
		m_sidebar_location = (s == "left" ? SIDEBAR_LEFT : SIDEBAR_RIGHT);
	}

	s = m_config->get_value ("Default", "sidebar_width");
    if (! s.empty ()) {
		m_sidebar_width = ::atoi (s.c_str ());
		if (m_sidebar_width < SIDEBAR_MIN_WIDTH ||
			m_sidebar_width > SIDEBAR_MAX_WIDTH)
			{
				m_sidebar_width = SIDEBAR_MIN_WIDTH;
			}
	}

	m_root_x = ::atoi (m_config->get_value ("Default", "root_x").c_str ());
    m_root_y = ::atoi (m_config->get_value ("Default", "root_y").c_str ());

    if (m_config->get_value ("Default", "disable_key_shortcuts") == "true") {
		m_disable_key_shortcuts = true;
    }

    if (m_config->get_value ("Default", "auto_pronounce") == "true") {
		m_auto_pronounce = true;
    }
}

/******************************************************************************
 * Save configuration to the user's configuration file, ~/.granule.           *
 ******************************************************************************/
void
GrappConf::
save_config ()
{
    trace_with_mask("GrappConf::save_config",GUITRACE);

	string s;
	const char* fname = NULL;
	char buf[32];

    if (m_config == NULL) {
		s = GRANULE->get_config_file ();
		if (s.size () == 0) {
			s = GRANULE->get_default_config_file ();
		}
		m_config = new IniFile (s);
		s = "";
		if (m_config->load () < 0) {
			DL((APP,"Failed to load config file!\n"));
			return;
		}
    }
    m_config->drop_section ("History");
    m_config->add_section  ("History");

    m_config->drop_section ("Default");
    m_config->add_section  ("Default");

    m_config->drop_section ("FlipSide");
    m_config->add_section  ("FlipSide");

    m_config->drop_section ("Scheduling");
    m_config->add_section  ("Scheduling");

    int position = 0;
    ostringstream ckey;

    /** Save document history
     */
    if (m_history.size () != 0) {
		DHList_CIter cit = m_history.begin ();
		while (cit != m_history.end ()) {
			fname = (*cit).c_str ();
			if (!g_file_test (fname, G_FILE_TEST_IS_REGULAR)) {
				cit++;
				continue;
			}
			ckey.str ("");
			ckey  << position;
			DL((GRAPP,"Saving history[%d] %s=%s>\n", position,
				ckey.str ().c_str (), (*cit).c_str ()));
			m_config->set_pair ("History", 
								IniFile::tuple_type (ckey.str (), (*cit)));
			cit++, position++;
		}
    }
    /** Save Scheduling configuration
     */
    for (int j = 0; j < CARDBOXES_MIN; j++) {
		ckey.str ("");
		ckey << (j + 1);
		m_config->set_pair ("Scheduling", 
							IniFile::tuple_type (ckey.str (),
     						 m_sched [j].m_weeks + '/' + 
							 m_sched [j].m_days  + '/' +
 							 m_sched [j].m_hours + '/' +
 							 m_sched [j].m_mins));
    }

    /**------------------------------------------------------------------**
	 * Flip the side so that next time it would be the back of the Deck.  *
     **------------------------------------------------------------------**/
    DL((GRAPP,"Saving [FlipSide] section\n"));

    flipside_iter_t iter = m_flipside_history.begin ();
    position = 0;

    while (iter != m_flipside_history.end ()) {
		fname = (*iter).first.c_str ();
		if (!g_file_test (fname, G_FILE_TEST_IS_REGULAR)) {
			iter++;
			continue;
		}
		ckey.str ("");
		ckey  << ((*iter).second == FRONT ? "front_" : "back_") << position;
		DL((GRAPP,"%s = %s\n", ckey.str ().c_str (), fname));

		m_config->set_pair ("FlipSide", IniFile::tuple_type (ckey.str (),
															 (*iter).first));
		iter++, position++;
    }

    m_config->set_pair ("Default", 
						IniFile::tuple_type("filesel_path", m_filesel_path));
	// Text colors
	m_text_colors_db.save_to_ini_config (m_config);

    // Text Fonts
	m_fonts_db.save_to_ini_config (m_config);

	// Text alignment
	m_appearance.save_to_ini_config (m_config);

    /**------------------------------------------------------------------** 
	 * Geometry                                                           *
     **------------------------------------------------------------------**/
    m_config->set_pair ("Default", 
						IniFile::tuple_type("mainwin_geometry",	
											geometry_to_str(m_mwin_geometry)));
    m_config->set_pair ("Default", 
						IniFile::tuple_type("cardview_geometry",
										geometry_to_str(m_crdview_geometry)));
    m_config->set_pair ("Default", 
						IniFile::tuple_type("deckplayer_geometry",
											geometry_to_str(m_dplyr_geometry)));

    m_config->set_pair ("Default", IniFile::tuple_type("deckplayer_keep_aspect",
									 (m_dplyr_keep_aspect ? "true" : "false")));
    m_config->set_pair ("Default", 
						IniFile::tuple_type("deckplayer_aspect",
											geometry_to_str (m_dplyr_aspect)));

    m_config->set_pair ("Default", 
						IniFile::tuple_type("show_verify_answer_control",
									   (m_show_va_control ? "true" : "false")));

    m_config->set_pair ("Default", 
						IniFile::tuple_type("show_stats_info_control",
									 (m_show_si_control ? "true" : "false")));
    m_config->set_pair ("Default", 
						IniFile::tuple_type("show_sidebar_control",
								 (m_show_sidebar_control ? "true" : "false")));

    m_config->set_pair ("Default", 
						IniFile::tuple_type("sidebar_location",
								(m_sidebar_location == SIDEBAR_LEFT ?
								 "left" : "right")));

	snprintf (buf, 32, "%d", m_sidebar_width);
    m_config->set_pair ("Default", 
						IniFile::tuple_type("sidebar_width", buf));

    /**------------------------------------------------------------------** 
	 * Sound playback                                                     *
     **------------------------------------------------------------------**/
    m_config->set_pair ("Default", IniFile::tuple_type("snd_player_cmd", 
													   m_snd_player_cmd));
    m_config->set_pair ("Default", IniFile::tuple_type("snd_player_args", 
													   m_snd_player_args));
    m_config->set_pair ("Default", IniFile::tuple_type("remove_duplicates",
										   (m_remove_dups ? "true" : "false")));
    m_config->set_pair ("Default", IniFile::tuple_type("snd_archive_path", 
													   m_snd_archive_path));
	m_config->set_pair ("Default", IniFile::tuple_type ("with_relative_paths",
								   (m_with_relative_paths ? "true" : "false")));
	sprintf (buf, "%d", m_history_size);
	m_config->set_pair ("Default", IniFile::tuple_type ("history_size", buf));

    sprintf (buf, "%d", m_root_x);
    m_config->set_pair ("Default", IniFile::tuple_type ("root_x", buf));
    sprintf (buf, "%d", m_root_y);
    m_config->set_pair ("Default", IniFile::tuple_type ("root_y", buf));

	m_config->set_pair ("Default", IniFile::tuple_type ("disable_key_shortcuts",
								(m_disable_key_shortcuts ? "true":"false")));

	m_config->set_pair ("Default", IniFile::tuple_type ("auto_pronounce",
								(m_auto_pronounce ? "true":"false")));

    /** Commit changes to the disk file
     */
    m_config->sync ();
}

/******************************************************************************/
void 
GrappConf::
set_filesel_path (const string& fspath_) 
{ 
	string::size_type idx;
	idx = fspath_.rfind (G_DIR_SEPARATOR);

	if (idx != string::npos) {
		m_filesel_path = fspath_.substr (0, idx+1);
	}
	else {
		m_filesel_path = fspath_;
	}
}

/******************************************************************************/
SideSelection 
GrappConf::
get_flipside_history (const string& fname_)
{
    trace_with_mask("GrappConf::get_flipside_history",GUITRACE);

	if (fname_.find ("Untitled") != string::npos) {
		return (FRONT);
	}
	flipside_iter_t iter = m_flipside_history.begin ();
	while (iter != m_flipside_history.end ()) {
		if ((*iter).first == fname_) {
			return ((*iter).second);
		}
		iter++;
	}
	m_flipside_history.push_back (sidesel_t (fname_, FRONT));
	DL((APP,"Added new deck info \"%s\"\n", fname_.c_str ()));
	DL((APP,"%d elements in m_flipside_history\n", m_flipside_history.size ()));
	return (FRONT);
}

/******************************************************************************/
void
GrappConf::
set_flipside_history (const string& fname_, SideSelection side_)
{
    trace_with_mask("GrappConf::set_flipside_history",GUITRACE);

	flipside_iter_t iter = m_flipside_history.begin ();
	while (iter != m_flipside_history.end ()) {
		if ((*iter).first == fname_) {
			(*iter).second = side_;
			break;
		}
		iter++;
	}
	if (iter == m_flipside_history.end ()) {
		m_flipside_history.push_back (sidesel_t (fname_, side_));
		DL((APP,"Added new deck info \"%s\"\n", fname_.c_str ()));
		DL((APP,"%d elements in m_flipside_history\n", 
			m_flipside_history.size ()));	
	}
}

void
GrappConf::
set_proj_name (const string& fname_)
{
    m_proj_name = GRANULE->get_basename (fname_.c_str ());
    m_path_name = GRANULE->get_dirname  (fname_.c_str ());

	DL ((GRAPP, "New project name: \"%s\"\n", m_proj_name.c_str ()));
	DL ((GRAPP, "New project path: \"%s\"\n", m_path_name.c_str ()));
}

/*******************************************************************************
	Convert "HEIGHTxWIDTH" from string to Gtk::Allocation.
*/
void
GrappConf::
str_to_geometry (const string& src_, Gdk::Rectangle& aspect_)
{
    trace_with_mask("GrappConf::str_to_geometry",GUITRACE);

	int height;
	int width;

	sscanf (src_.c_str (), "%dx%d", &height, &width);
	aspect_.set_height (height);
	aspect_.set_width  (width);

	DL((GEOM,"aspect : h=%d, w=%d\n",
		aspect_.get_height (),aspect_.get_width ()));
}

/*******************************************************************************
	Convert Gtk::Allocation to string "HEIGHTxWIDTH".
*/
const char* 
GrappConf::
geometry_to_str (const Gdk::Rectangle& aspect_)
{
    static char buf [26];
    sprintf (buf, "%dx%d", aspect_.get_height (), aspect_.get_width  ());
    return buf;
}

