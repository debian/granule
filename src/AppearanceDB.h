// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: AppearanceDB.h,v 1.5 2008/08/16 03:13:08 vlg Exp $
//------------------------------------------------------------------------------
//                            AppearanceDB.h
//------------------------------------------------------------------------------
//  Copyright (c) 2007 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date   : Mon Sep 17 2007
//
//------------------------------------------------------------------------------

#ifndef APPEAR_DB_H
#define APPEAR_DB_H

#include "Granule-main.h"

#include <string>
using std::string;

#include <libxml/tree.h>		// xmlDocPtr
#include <libxml/xmlwriter.h>	// xmlTextWriterPtr

#include <assa/IniFile.h>

class TextAlignmentsUI;

/**-----------------------------------------------------------------------------
 *	AppearanceDB is the Model for the Appearance Preferences.
 *  It it a persistent storage manager of data.
 **-----------------------------------------------------------------------------
 */
class AppearanceDB
{  
public:
    AppearanceDB () { init_global_defaults (); }

	void init_deck_defaults ();
	void init_global_defaults ();

	/** Persisten storage with XML (Deck) file.
	 */
    void load_from_xml_config  (xmlDocPtr parser_, string& error_msg_);
    void save_to_xml_config    (xmlTextWriterPtr writer);

	/** Persistent storage with INI file (application config).
	 */
    void load_from_ini_config  (ASSA::IniFile* ini_filep_);
    void save_to_ini_config    (ASSA::IniFile* ini_filep_);

	/** Text alignment controls
	 */
	void x_alignment   (SideSelection s_, const string& v_);
	void y_alignment   (SideSelection s_, const string& v_);
	void x_padding     (SideSelection s_, const string& v_);
	void y_padding     (SideSelection s_, const string& v_);
	void justification (SideSelection s_, const string& v_);
	void img_position  (SideSelection s_, const string& v_);

	string x_alignment   (SideSelection s_) const;
	string y_alignment   (SideSelection s_) const;
	string x_padding     (SideSelection s_) const;
	string y_padding     (SideSelection s_) const;
	string justification (SideSelection s_) const;
	string img_position  (SideSelection s_) const;

	/// Update View with data from Model
	void update_view (SideSelection s_, TextAlignmentsUI*& view_);

	/// Store View values in Model
	void fetch_from_view (SideSelection s_, TextAlignmentsUI*& view_);

private:
	string    m_front_x_alignment;
	string    m_front_y_alignment; 
	string    m_front_x_padding;
	string    m_front_y_padding; 
	string    m_front_justification; 
	string    m_front_img_position;

	string    m_back_x_alignment;
	string    m_back_y_alignment; 
	string    m_back_x_padding;
	string    m_back_y_padding; 
	string    m_back_justification; 

	string    m_example_x_alignment;
	string    m_example_y_alignment; 
	string    m_example_x_padding;
	string    m_example_y_padding; 
	string    m_example_justification; 
	string    m_example_img_position;
};

/**-------------------------
 **** Retrieve a property **
 **-------------------------
 */
inline string 
AppearanceDB::x_alignment (SideSelection s_) const 
{ 
	if      (s_ == FRONT) { return m_front_x_alignment;   }
	else if (s_ == BACK)  { return m_back_x_alignment;    }
	else    /* EXAMPLE */ { return m_example_x_alignment; }
}

inline string
AppearanceDB::y_alignment (SideSelection s_) const
{ 
	if      (s_ == FRONT) { return m_front_y_alignment;   }
	else if (s_ == BACK)  { return m_back_y_alignment;    }
	else    /* EXAMPLE */ { return m_example_y_alignment; }
}

inline string 
AppearanceDB::x_padding (SideSelection s_) const 
{ 
	if      (s_ == FRONT) { return m_front_x_padding;   }
	else if (s_ == BACK)  { return m_back_x_padding;    }
	else    /* EXAMPLE */ { return m_example_x_padding; }
}

inline string
AppearanceDB::y_padding (SideSelection s_) const
{ 
	if      (s_ == FRONT) { return m_front_y_padding;   }
	else if (s_ == BACK)  { return m_back_y_padding;    }
	else    /* EXAMPLE */ { return m_example_y_padding; }
}

inline string
AppearanceDB::justification (SideSelection s_) const 
{ 
	if      (s_ == FRONT) { return m_front_justification;   }
	else if (s_ == BACK)  { return m_back_justification;    }
	else    /* EXAMPLE */ { return m_example_justification; }
}

inline string
AppearanceDB::img_position (SideSelection s_) const 
{ 
	if      (s_ == FRONT) { return m_front_img_position;   }
	else if (s_ == BACK)  { return "top";                  }
	else    /* EXAMPLE */ { return m_example_img_position; }
}

/**--------------------
 **** Set a property **
 **--------------------
 */
inline void
AppearanceDB::x_alignment (SideSelection s_, const string& v_)
{ 
	if      (s_ == FRONT) { m_front_x_alignment   = v_; }
	else if (s_ == BACK)  { m_back_x_alignment    = v_; }
	else    /* EXAMPLE */ { m_example_x_alignment = v_; }
}

inline void
AppearanceDB::y_alignment (SideSelection s_, const string& v_)
{ 
	if      (s_ == FRONT) { m_front_y_alignment   = v_; }
	else if (s_ == BACK)  { m_back_y_alignment    = v_; }
	else    /* EXAMPLE */ { m_example_y_alignment = v_; }
}

inline void
AppearanceDB::x_padding (SideSelection s_, const string& v_)
{ 
	if      (s_ == FRONT) { m_front_x_padding   = v_; }
	else if (s_ == BACK)  { m_back_x_padding    = v_; }
	else    /* EXAMPLE */ { m_example_x_padding = v_; }
}

inline void
AppearanceDB::y_padding (SideSelection s_, const string& v_)
{ 
	if      (s_ == FRONT) { m_front_y_padding   = v_; }
	else if (s_ == BACK)  { m_back_y_padding    = v_; }
	else    /* EXAMPLE */ { m_example_y_padding = v_; }
}

inline void
AppearanceDB::justification (SideSelection s_, const string& v_)
{ 
	if      (s_ == FRONT) { m_front_justification   = v_; }
	else if (s_ == BACK)  { m_back_justification    = v_; }
	else    /* EXAMPLE */ { m_example_justification = v_; }
}

inline void
AppearanceDB::img_position (SideSelection s_, const string& v_)
{ 
	if      (s_ == FRONT) { m_front_img_position = v_;   }
	else if (s_ == BACK)  { /* no-op */                  }
	else    /* EXAMPLE */ { m_example_img_position = v_; }
}

#endif /* APPEAR_DB */
