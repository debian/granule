// -*- c++ -*-
//------------------------------------------------------------------------------
//                        FolderChooseDialog.h
//------------------------------------------------------------------------------
// $Id: FolderChooseDialog.h,v 1.1 2007/12/02 00:31:24 vlg Exp $
//------------------------------------------------------------------------------
//  Copyright (c) 2006 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version
//  2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------
#ifndef FOLDER_CHOOSE_DIALOG_H
#define FOLDER_CHOOSE_DIALOG_H

#include <gtkmm/filechooserdialog.h>
#include <gtkmm/fileselection.h>

#include "Granule-main.h"
#include "Granule.h"

class FolderChooseDialog
{
public:
	FolderChooseDialog (const Glib::ustring& title_, Gtk::Widget* parent_);

	~FolderChooseDialog ();

	Glib::ustring get_filename     () const;
	void          set_current_folder (const Glib::ustring& name_);

	int  run  ();
	void show ();
	void hide ();

private:

#ifdef OBSOLETE
	Gtk::FileSelection*     m_dialog;
#else
	Gtk::FileChooserDialog* m_dialog;
#endif
};

//------------------------------------------------------------------------------
// Inline functions
//------------------------------------------------------------------------------
inline
FolderChooseDialog::
~FolderChooseDialog ()
{
	if (m_dialog) {
		delete m_dialog;
		m_dialog = NULL;
	}
}

inline Glib::ustring 
FolderChooseDialog::
get_filename () const
{
	return (m_dialog->get_filename ());
}

inline void
FolderChooseDialog::
set_current_folder (const Glib::ustring& dirname_)
{
#ifdef OBSOLETE
	m_dialog->set_filename (dirname_);
#else
	m_dialog->set_current_folder (dirname_);
#endif
}

inline int
FolderChooseDialog::
run ()
{
	return (m_dialog->run ());
}
	
inline void
FolderChooseDialog::
hide ()
{
	m_dialog->hide ();
}

inline void
FolderChooseDialog::
show ()
{
	m_dialog->show ();
}



#endif /* FOLDER_CHOOSE_DIALOG_H */
