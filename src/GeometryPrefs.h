// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: GeometryPrefs.h,v 1.4 2008/08/16 03:13:09 vlg Exp $
//------------------------------------------------------------------------------
//                            GeometryPrefs.h
//------------------------------------------------------------------------------
//  Copyright (c) Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date   : Wed Sep 22 2004
//
//------------------------------------------------------------------------------

#ifndef _GEOMETRY_PREFS_H
#define _GEOMETRY_PREFS_H

#include <gtkmm/entry.h>
#include <gtkmm/button.h>
#include <gtkmm/box.h>
#include <gtkmm/checkbutton.h>
#include <gtkmm/label.h>
#include <gtkmm/spinbutton.h>

class PrefWindow;

/**-----------------------------------------------------------------------------
  GeometryBox holds one row of geometry definition in 
  the Geometry Tab of the Property Window.
--------------------------------------------------------------------------------
*/
class GeometryBox 
{  
public:
	GeometryBox (const char* win_name_);

	void pack (int idx_, Gtk::Table& table_);

	Gtk::SpinButton* get_wspin_button () { return m_wspin_button; }
	Gtk::SpinButton* get_hspin_button () { return m_hspin_button; }

	void set_values (const Gdk::Rectangle& geom_);
	void get_values (int& width_, int& height_);

private:
	Gtk::Label*      m_label;

	Gtk::VBox*       m_height_vbox;
	Gtk::Label*      m_hlabel;
	Gtk::SpinButton* m_hspin_button;

	Gtk::VBox*       m_width_vbox;
	Gtk::Label*      m_wlabel;
	Gtk::SpinButton* m_wspin_button;
};

/**-----------------------------------------------------------------------------
	AspectBox holds the option which when enables, enforces
	predefined aspect ratio on DeckPlayer dialog.
--------------------------------------------------------------------------------
*/
class AspectBox : public Gtk::HBox
{
public:
	AspectBox ();

	Gtk::CheckButton* get_checkbox () { return m_keep_aspect; }

	Gtk::Entry* get_hentry () { return m_hentry; }
	Gtk::Entry* get_wentry () { return m_wentry; }

	void set_value (bool is_on_, Gdk::Rectangle& aspect_);
	void get_value (bool& is_on_, int& width_, int& height_);

private:
    Gtk::CheckButton* m_keep_aspect;
	Gtk::Entry*       m_hentry;	
	Gtk::Label*       m_xlabel;
	Gtk::Entry*       m_wentry;
};

/**-----------------------------------------------------------------------------
	GeometryPrefs holds all Geometry preferences for all 
	frequently-used windows.
--------------------------------------------------------------------------------
*/
class GeometryPrefs : public Gtk::VBox
{  
public:
    GeometryPrefs (PrefWindow& parent_);

    void changed_cb        (int measurement_);
    void load_from_config  ();
    void save_to_config    ();

protected:
	static const int  WIN_ENTRY_SZ = 3;

    PrefWindow&   m_pref_window;
    GeometryBox*  m_geometry [WIN_ENTRY_SZ];	
	AspectBox*    m_aspect;
	bool          m_change_in_progress;	// Block off DeckPlayer geom. change
	Gtk::Table*   m_table;
};

#endif
