// -*- c++ -*-
//------------------------------------------------------------------------------
//                              CardRef.h
//------------------------------------------------------------------------------
// $Id
//
// Date: Apr 24 2004
//------------------------------------------------------------------------------
#ifndef CARD_REF_H
#define CARD_REF_H

#include <assa/TimeVal.h>
using ASSA::TimeVal;

#include "Granule-main.h"
#include "Card.h"
#include "Deck.h"
#include "VCard.h"
#include "GrappConf.h"

class CardRef : public VCard 
{
public:
	CardRef (VCard& card_, Deck& deck_, float efactor_);

	virtual void set_question (const ustring& q_) { m_card.set_question (q_); }
	virtual void set_image_path (const ustring& f_) { 
		m_card.set_image_path (f_); 
	}

	virtual void set_answer   (const ustring& a_) { m_card.set_answer   (a_); }

	virtual void set_example  (const ustring& e_) { m_card.set_example  (e_); }
	virtual void set_example_image_path (const ustring& f_) { 
		m_card.set_example_image_path (f_); 
	}
	virtual ustring get_question   () const { return m_card.get_question   (); }
	virtual ustring get_image_path () const { return m_card.get_image_path (); }
	virtual ustring get_deck_path  () const { return m_card.get_deck_path  (); }
	virtual ustring get_answer     () const { return m_card.get_answer     (); }
	virtual ustring get_example    () const { return m_card.get_example    (); }
	virtual ustring get_example_image_path () const 
		{ return m_card.get_example_image_path (); }

	virtual PathMode image_path_mode() const { return m_card.image_path_mode();}

	virtual void    set_alt_spelling (SideSelection side_, const ustring& a_);
	virtual ustring get_alt_spelling (SideSelection side_) const;

	virtual long get_id () const { return m_card.get_id (); }

	virtual void reschedule (AnswerQuality answer_quality_);
	virtual int  get_interval () const { return m_interval; }

	/// Set absolute expiration date.
	virtual void set_expiration (const ASSA::TimeVal& e_);

	/// Get expiration date.
	virtual ASSA::TimeVal get_expiration () const { return m_exp_date; }

	/// Set expiration date expressed in days from today.
	void set_expiration_in_days (int days_);

	bool   is_expired ();
	string get_expiration_str ();

	virtual string get_success_ratio (string& ok_bad_ratio_) const;

	Deck& get_deck () { return m_deck; }
	Card& get_card () const { return m_card; }

	float get_efactor () const { return m_efactor; }
	void  set_efactor (float ef_) { m_efactor = ef_; }

	virtual void dump () const;

	/// Dump CardRef-specific scheduling part of data
	void dump_schedule (const char* message_) const;

	/** Save card to the XML document.
	 */
	int save_to_xml_config   (xmlTextWriterPtr writer_,
							  const string& proj_path_);

	/** Load SM-2 related part of the card information.
	 */
	int load_from_xml_config (const xmlDocPtr writer_,
							  int deck_, int card_);
protected:
	virtual bool less_then_expiration (const VCard& rhs_);
	virtual bool equal_expiration (const VCard& rhs_);

private:
	template<class T> 
	int write_xml_element (xmlTextWriterPtr writer_,
						   const char* tag_,
						   const char* format_,
						   T value_);

	template<class T> 
	int write_xml_attribute (xmlTextWriterPtr writer_,
							 const char* tag_,
							 const char* format_,
							 T value_);

private:
	Card& m_card;
	Deck& m_deck;

    /** The expiration date of the card expressed in seconds.
	 */
	long m_exp_date;		

	/** Card's E-Factor. This value differs for each card
	 *  based on the quality of answers.
	 */
	float m_efactor;

	/// Current interval in days
	int m_interval;

	/// Repetitin counter captures how many times card has been reviewed.
	int m_repetition;
	
	/** Statistical distribution of answers. 
	 *  For now, GUI only allows either Correct=5 or Incorrect=0.
	 *  Following SM-2 algorithm description:
	 *  <ul>
	 *    <li>5 : perfect response</li>
	 *    <li>4 : correct response after hesitation</li>
	 *    <li>3 : correct response recalled with serious difficulty</li>
	 *    <li>2 : incorrect response; where the correct one seemed easy to 
	 *            recall</li>
	 *    <li>1 : incorrect response; the correct one remembered</li>
	 *    <li>0 : complete blackout</li>
	 *  </ul>
	 *  Answers in the range 0-3 have diminishing effect on E-Factor.
	 *  Answer of 4 has neutral effect on E-Factor.
	 *  Answer of 5 has gaining effect on E-Factor.
	 */
	int m_statistics [CARD_STATS_SZ];
};


inline void 
CardRef::set_alt_spelling (SideSelection side_, const ustring& a_) 
{ 
	m_card.set_alt_spelling  (side_, a_); 
}

inline ustring
CardRef::get_alt_spelling (SideSelection side_) const 
{ 
	return m_card.get_alt_spelling (side_); 
}

inline bool 
CardRef::less_then_expiration (const VCard& rhs_)
{
	return (get_expiration () < rhs_.get_expiration ());
}

inline bool 
CardRef::equal_expiration (const VCard& rhs_)
{
	return (get_expiration () == rhs_.get_expiration ());
}


#endif /* CARD_REF_H */
