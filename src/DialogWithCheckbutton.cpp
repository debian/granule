// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: DialogWithCheckbutton.cpp,v 1.1 2007/12/02 00:31:24 vlg Exp $
//------------------------------------------------------------------------------
//                            DialogWithCheckbutton.cpp
//------------------------------------------------------------------------------
//  Copyright (c) 2006 by Vladislav Grinchenko 
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date   : Sat Aug 26 19:20:12 EDT 2006
//
//------------------------------------------------------------------------------

#include <gtkmm/frame.h>
#include <gtkmm/stock.h>

#include "DialogWithCheckbutton.h"
#include "GeneralPref.h"		           /* useful macros */
#include "Intern.h"

DialogWithCheckbutton::
DialogWithCheckbutton (const Glib::ustring& message_, 
						 const Glib::ustring& checkbutton_message_, 
						 bool activate_checkbutton_,
						 bool use_markup_)
	: 
	Gtk::MessageDialog (message_, 
						use_markup_,             /* markup message? */
						Gtk::MESSAGE_QUESTION, 
						Gtk::BUTTONS_NONE,
						true)                    /* modal dialog    */
{
	add_button (Gtk::Stock::NO,  Gtk::RESPONSE_NO );
	add_button (Gtk::Stock::YES, Gtk::RESPONSE_YES);

	Gtk::Frame*       msg_frame = Gtk::manage(new Gtk::Frame());
	Gtk::Label*       empty_label;

	m_checkbutton =Gtk::manage (new Gtk::CheckButton (_(checkbutton_message_)));

	MAKE_EMPTY_LABEL(empty_label);
	SET_CHECKBUTTON(m_checkbutton);
	SET_FRAME(msg_frame, empty_label, m_checkbutton);

	m_checkbutton->set_active (activate_checkbutton_);
	get_vbox ()->pack_start (*msg_frame, Gtk::PACK_SHRINK, 0);
	show_all_children ();
}

