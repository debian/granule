// -*- c++ -*-
//------------------------------------------------------------------------------
//                               FileSaveDialog.cpp
//------------------------------------------------------------------------------
// $Id: FileSaveDialog.cpp,v 1.4 2008/08/19 02:52:46 vlg Exp $
//------------------------------------------------------------------------------
//  Copyright (c) 2006 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version
//  2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------

#include "GrappConf.h"
#include "FileSaveDialog.h"

FileSaveDialog::
FileSaveDialog (const Glib::ustring& title_,
				Gtk::Widget*         parent_,
				const Glib::ustring& filter_name_,
				const Glib::ustring& filter_pattern_)
{
#ifdef OBSOLETE

	m_dialog = new Gtk::FileSelection (title_);
	m_dialog->set_filename (CONFIG->get_filesel_path ());

#else
    m_dialog = new Gtk::FileChooserDialog (title_, 
										   Gtk::FILE_CHOOSER_ACTION_SAVE);
/**
 * Causes nasty dialog jitter bug on Nokia N8xx platform.
 *
 *	int counter = 0;
 *	Granule::hide_fcd_gtk_labels (GTK_CONTAINER (m_dialog->gobj ()), counter);
 */
    m_dialog->add_button (Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
    m_dialog->add_button (Gtk::Stock::SAVE,   Gtk::RESPONSE_OK);

    m_dialog->set_current_folder (CONFIG->get_filesel_path ());
    m_dialog->set_local_only ();

	if (filter_name_.length () > 0) 
	{
		Gtk::FileFilter filter;
		filter.set_name (filter_name_);
		filter.add_pattern (filter_pattern_);

		m_dialog->set_filter (filter);
	}
#endif

#ifdef IS_HILDON
//    m_dialog->set_transient_for (*HILDONAPPWIN);
    m_dialog->set_transient_for ((Gtk::Window&) *parent_);
#else
    m_dialog->set_transient_for ((Gtk::Window&) *parent_);
#endif
}

