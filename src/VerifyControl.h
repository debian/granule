// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: VerifyControl.h,v 1.6 2008/08/03 03:56:41 vlg Exp $
//------------------------------------------------------------------------------
//                            VerifyControl.h
//------------------------------------------------------------------------------
//  Copyright (c) 2008 Vladislav Grinchenko 
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date   : Sun Jan  6 2008
//
//------------------------------------------------------------------------------
#ifndef VERIFY_CONTROL_H
#define VERIFY_CONTROL_H

#ifdef HAVE_CONFIG_H
#    include "config.h"
#endif

#include <gdkmm/pixbuf.h>
#include <gdkmm/image.h>
#include <gtkmm/messagedialog.h>
#include <gtkmm/notebook.h>
#include <gtkmm/label.h>
#include <gtkmm/textview.h>
#include <gtkmm/uimanager.h>
#include <gtkmm/table.h>

#ifdef IS_HILDON
#include <hildonmm.h>
#endif

#include "GrappConf.h"
#include "VDeck.h"

#include "Intern.h"

class DeckView;
class DeckPlayer;
class Gtk::ProgressBar;
class Gtk::EventBox;

static const 
Glib::ustring clabels [] = 
{ 
	N_("Check"), 
	N_("Next "), 
	N_("Correct  "), 
	N_("Incorrect"), 
	"               "
};

enum { 
	CHECK_ANSWER, 
	NEXT, 
	CORRECT, 
	INCORRECT, 
	EMPTY 
};

/**
 * External events such as control buttons click
 * also cause switching between states.
 */
typedef enum {
	VC_START,					// Waiting for input
	VC_ARMED,					// User entered text into VC entry
	VC_VALIDATED				// Entry text validated
} VCState;

/**===========================================================================**
 *                          Class TestLineEntry                                *
 *-----------------------------------------------------------------------------*
 *   Class TestLineEntry encapsulates check line selection widget.             *
 *   We accept only numerical entires in the valid range of [0; 10]            *
 *                                                                             *
 **===========================================================================**
 */
class TestLineEntry : public Gtk::HBox
{
public:
	typedef sigc::signal<void> signal_changed_type;

public:
	TestLineEntry ();

	int get_value_as_int () const;

	/** 'Changed' signal is emitted any time the user changes
	 *  the value of the entry.
	 */
	signal_changed_type signal_entry_changed () { return m_signal_changed; }

private:
	/** Check the entry for correctness, and if correct,
	 *  emit our custom 'signal_changed'.
	 */
	void on_entry_changed ();

private:
 	Gtk::Adjustment m_adjustment;
 	Gtk::SpinButton m_spin_entry;

	/// Custom entry changed signal.
	signal_changed_type  m_signal_changed;
};

inline int
TestLineEntry::
get_value_as_int () const
{
	return (m_spin_entry.get_value_as_int ());
}

inline void
TestLineEntry::
on_entry_changed ()
{
	/** Notify all subscribers of the update
	 */
	m_signal_changed.emit ();
}

/**===========================================================================**
 *                            Class VerifyControl                              *
 *-----------------------------------------------------------------------------*
 *   Class VerifyControl encapsulates the user answer verification control     *
 *   of the DeckPlayer dialog. This element is optionally shown                *
 *   in the dialog between the notebook and the control toolbar.               *
 *                                                                             *
 **===========================================================================**
 */
class VerifyControl : public Gtk::Table
{
public:
	VerifyControl (DeckPlayer& parent_);

	void    compare (const Glib::ustring& answer_);
	bool    matched () const { return m_was_equal; }
	void    clear   ();

	VCState get_state () const;
	void    set_state (VCState state_);

	void    attach_testline_entry (TestLineEntry* test_line_);
	void    repaint ();

	void    set_focus_on_entry ();
	bool    entry_has_focus    () const { return m_entry->has_focus (); }

	/** If colors have been altered with DeckInfo dialog,
	 *  then modify accordingly.
	 */
	void    modify_text_colors ();

private:
	void    on_entry_changed ();
	void    on_test_line_changed ();

private:
	static const int LABELS_SZ = 4;

	DeckPlayer&      m_parent;

	Gtk::Label*      m_report;		      // Correct, Incorrect
	Gtk::EventBox*   m_report_box;        // Box to hold report label
	Gtk::Entry*      m_entry;
	Gtk::Button*     m_button;	          // 'Check answer', 'Next'
	Gtk::Label*      m_label [LABELS_SZ];

	bool             m_was_equal;         // Keeps result of last compare() op.
	VCState          m_state;

	Gdk::Color       m_red;
	Gdk::Color       m_green;
	Gdk::Color       m_white;
	Gdk::Color       m_black;

	TestLineEntry*   m_test_line;         // Which line to compare for answer.
	size_t           m_line_num;          // Number of line to test
};


#endif /* VERIFY_CONTROL_H */
