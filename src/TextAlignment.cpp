// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: TextAlignment.cpp,v 1.10 2008/08/16 03:13:09 vlg Exp $
//------------------------------------------------------------------------------
//                            TextAlignment.cpp
//------------------------------------------------------------------------------
//  Copyright (c) 2006 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date   : Wed Oct 25 20:49:21 EDT 2006
//
//------------------------------------------------------------------------------

#include "Granule-main.h"
#include "TextAlignment.h"

#include "Intern.h"             // i18n macros

using sigc::mem_fun;

/**-----------------------------------------------------------------------------
 *	TextAlignment
 **-----------------------------------------------------------------------------
 */
TextAlignment::
TextAlignment (PrefWindow& grandpa_) 
	: 
	Gtk::VBox (false, 0),
	m_pref_window (grandpa_)
{
	trace_with_mask("TextAlignment::TextAlignment",GUITRACE);

	m_vbox = this;

	TextAlignmentsUI::value_list_t align_x_list;
	TextAlignmentsUI::value_list_t align_y_list;
	TextAlignmentsUI::value_list_t paragraph_list;
	TextAlignmentsUI::value_list_t img_position_list;

	align_x_list      = TextAlignmentsUI::make_align_x_list      ();
	align_y_list      = TextAlignmentsUI::make_align_y_list      ();
	paragraph_list    = TextAlignmentsUI::make_paragraph_list    ();
	img_position_list = TextAlignmentsUI::make_img_position_list ();

	m_front_widget = new TextAlignmentsUI ("<b>Front Text Alignment</b>",
										   align_x_list,
										   align_y_list,
										   paragraph_list,
										   img_position_list);

	m_back_widget = new TextAlignmentsUI ("<b>Back Text Alignment</b>",
										  align_x_list,
										  align_y_list,
										  paragraph_list,
										  img_position_list, false);

	m_example_widget = new TextAlignmentsUI ("<b>Example Text Alignment</b>",
											 align_x_list,
											 align_y_list,
											 paragraph_list,
											 img_position_list);
	/** Pack all together
	 */
	m_vbox->pack_start (*(m_front_widget->frame   ()), Gtk::PACK_SHRINK, 4);
	m_vbox->pack_start (*(m_back_widget->frame    ()), Gtk::PACK_SHRINK, 4);
	m_vbox->pack_start (*(m_example_widget->frame ()), Gtk::PACK_SHRINK, 4);

	/** Setup callbacks
	 */
	/// Front
	m_front_widget->x_align_combo ()->signal_changed ().connect (
		mem_fun (m_pref_window, &PrefWindow::changed_cb));

	m_front_widget->y_align_combo ()->signal_changed ().connect (
		mem_fun (m_pref_window, &PrefWindow::changed_cb));

	m_front_widget->x_padding_entry ()->signal_changed ().connect (
		mem_fun (m_pref_window, &PrefWindow::changed_cb));

	m_front_widget->y_padding_entry ()->signal_changed ().connect (
		mem_fun (m_pref_window, &PrefWindow::changed_cb));

	m_front_widget->paragraph_combo ()->signal_changed ().connect (
		mem_fun (m_pref_window, &PrefWindow::changed_cb));

	m_front_widget->img_position_combo ()->signal_changed ().connect (
		mem_fun (m_pref_window, &PrefWindow::changed_cb));

	/// Back
	m_back_widget->x_align_combo ()->signal_changed ().connect (
		mem_fun (m_pref_window, &PrefWindow::changed_cb));

	m_back_widget->y_align_combo ()->signal_changed ().connect (
		mem_fun (m_pref_window, &PrefWindow::changed_cb));

	m_back_widget->x_padding_entry ()->signal_changed ().connect (
		mem_fun (m_pref_window, &PrefWindow::changed_cb));

	m_back_widget->y_padding_entry ()->signal_changed ().connect (
		mem_fun (m_pref_window, &PrefWindow::changed_cb));

	m_back_widget->paragraph_combo ()->signal_changed ().connect (
		mem_fun (m_pref_window, &PrefWindow::changed_cb));

	/// Example
	m_example_widget->x_align_combo ()->signal_changed ().connect (
		mem_fun (m_pref_window, &PrefWindow::changed_cb));

	m_example_widget->y_align_combo ()->signal_changed ().connect (
		mem_fun (m_pref_window, &PrefWindow::changed_cb));

	m_example_widget->x_padding_entry ()->signal_changed ().connect (
		mem_fun (m_pref_window, &PrefWindow::changed_cb));

	m_example_widget->y_padding_entry ()->signal_changed ().connect (
		mem_fun (m_pref_window, &PrefWindow::changed_cb));

	m_example_widget->paragraph_combo ()->signal_changed ().connect (
		mem_fun (m_pref_window, &PrefWindow::changed_cb));

	m_example_widget->img_position_combo ()->signal_changed ().connect (
		mem_fun (m_pref_window, &PrefWindow::changed_cb));

	show_all_children ();
}

void
TextAlignment::
load_appearance (AppearanceDB& app_db_)
{
	app_db_.update_view (FRONT,   m_front_widget  );
	app_db_.update_view (BACK,    m_back_widget   );
	app_db_.update_view (EXAMPLE, m_example_widget);
}

void
TextAlignment::
save_appearance (AppearanceDB& app_db_)
{
	app_db_.fetch_from_view (FRONT,   m_front_widget  );
	app_db_.fetch_from_view (BACK,    m_back_widget   );
	app_db_.fetch_from_view (EXAMPLE, m_example_widget);
}
