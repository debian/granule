// -*- c++ -*-
//------------------------------------------------------------------------------
//                              CardAutoFill.h
//------------------------------------------------------------------------------
// $Id: CardAutoFill.h,v 1.6 2008/06/02 03:03:53 vlg Exp $
//
//  Copyright (c) Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//
//  Date: Oct 24, 2007
//------------------------------------------------------------------------------
#ifndef CARD_AUTO_FILL_H
#define CARD_AUTO_FILL_H

#include <glibmm/ustring.h>
#include <gtkmm/label.h>
#include <gtkmm/entry.h>
#include <gtkmm/dialog.h>
#include <gtkmm/checkbutton.h>
#include <gtkmm/frame.h>

class Gtk::Button;
class Gtk::TextView;
class Gtk::Entry;

#ifdef IS_HILDON
#    include <hildonmm.h>
#endif

#include "Granule-main.h"

class ButtonWithImageLabel;

//------------------------------------------------------------------------------
// Class CardAutoFill
//------------------------------------------------------------------------------
#ifdef IS_HILDON
class CardAutoFill : public Hildon::Window
#else
class CardAutoFill : public Gtk::Window
#endif
{
public:
	/** Create dialog that allows a user to specify
	 *  auto-fill parameters.
	 */
	CardAutoFill ();

	/** Simulate re-entrant event loop. 
	 */
	int run (Gtk::Window& parent_);

	void set_text (SideSelection, const Glib::ustring& pattern_);
	Glib::ustring get_text (SideSelection) const; 

	void mark_enabled (bool v_);
	bool is_enabled () const { return m_enable_chkbutton.get_active (); }

private:
	void on_ok_clicked     ();
	void on_apply_clicked  ();
	void on_cancel_clicked ();
	void on_enable_check_clicked ();
	void enable_input (bool v_ = true);

	/** We block [x] delete action and reroute it to <Close>
	 */
	bool on_delete_window_clicked (GdkEventAny* event_);

private:
	int m_response;
	
	// -*- GUI components -*-
	//
	ButtonWithImageLabel*  m_cancel_button;
	ButtonWithImageLabel*  m_apply_button;
	ButtonWithImageLabel*  m_ok_button;

	Gtk::Entry   m_asf_entry;	    // Alt.Spell Front
	Gtk::Entry   m_back_entry;	    // Back of the card
	Gtk::Entry   m_asb_entry;	    // Alt.Spell Back

	Gtk::CheckButton m_enable_chkbutton;
	Gtk::VBox    m_container;
	Gtk::Table   m_table;		// Table holds text entries
	Gtk::Frame   m_frame;
};

inline void
CardAutoFill::
mark_enabled (bool v_)
{
	m_enable_chkbutton.set_active (v_);
	enable_input (v_);
}

inline void
CardAutoFill::
set_text (SideSelection side_, const Glib::ustring& pattern_)
{
	if      (side_ == ASF ) { m_asf_entry.set_text  (pattern_); }
	else if (side_ == BACK) { m_back_entry.set_text (pattern_); }
	else if (side_ == ASB ) { m_asb_entry.set_text  (pattern_);	}
}

inline Glib::ustring
CardAutoFill::
get_text (SideSelection side_) const
{
	if      (side_ == ASF ) { return m_asf_entry.get_text  (); }
	else if (side_ == BACK) { return m_back_entry.get_text (); }
	else if (side_ == ASB ) { return m_asb_entry.get_text  (); }
}

#endif /* CARD_AUTO_FILL_H */
