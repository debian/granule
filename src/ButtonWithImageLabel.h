// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: ButtonWithImageLabel.h,v 1.2 2008/01/11 03:52:40 vlg Exp $
//------------------------------------------------------------------------------
//                      ButtonWithImageLabel.h
//------------------------------------------------------------------------------
//  Copyright (c) 2004 by Vladislav Grinchenko 
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date : Sat Apr 3 12:59:14 EST 2004
//
//------------------------------------------------------------------------------
#ifndef BUTTON_WITH_IMAGE_LABEL_H
#define BUTTON_WITH_IMAGE_LABEL_H

#include <string>
using std::string;

#include <gtkmm/stock.h>

#include <gtkmm/button.h>
#include <gtkmm/alignment.h>
#include <gtkmm/box.h>
#include <gtkmm/label.h>
#include <gtkmm/accelgroup.h>

/** @class ButtonWithImageLabel
 *
 *  This class extends Gtk::Button and packes an icon along with button label. 
 *
 *  The icon size can be one of the standard icon sizes: 
 *
 *     ICON_SIZE_MENU, ICON_SIZE_SMALL_TOOLBAR, ICON_SIZE_LARGE_TOOLBAR, 
 *     ICON_SIZE_BUTTON, ICON_SIZE_DND, ICON_SIZE_DIALOG)
 *
 *  or custom sized via Gtk::IconSize(int size_).
 */
class ButtonWithImageLabel : public Gtk::Button
{
public:
	ButtonWithImageLabel (const string& label_,
						  const Gtk::StockID& id_,
						  Gtk::IconSize icon_size_ = Gtk::ICON_SIZE_BUTTON);

	ButtonWithImageLabel (const string& label_,
						  const string& id_,
						  Gtk::IconSize icon_size_ = Gtk::ICON_SIZE_BUTTON);

	/** 
	 *  The @accel_key_ is the acceleration key that is activated with
	 *  <Alt> function key. The key value can be looked up in <gdk/gdkkeysyms.h>
	 *  GDK_a stands for '<Alt>a', and GDK_Return for '<Alt><Enter>'.
	 *
	 *  The @accel_group_ is the Window accelerator group that hosts
	 *  the ButtonWithImageLabel. You can get to it with 
	 *  window->get_accel_group() function call.
	 */
	void set_alt_accelerator_key (guint accel_key_, 
						  const Glib::RefPtr<Gtk::AccelGroup>& accel_group_);

	/** 
	 *  The @accel_key_ is the acceleration key that is activated with
	 *  <Ctrl> function key. The key value can be looked up in 
	 *  <gdk/gdkkeysyms.h> GDK_a stands for '<ctrl>a', 
	 *  and GDK_Return for '<ctrl><Enter>'.
	 *
	 *  The @accel_group_ is the Window accelerator group that hosts
	 *  the ButtonWithImageLabel. You can get to it with 
	 *  window->get_accel_group() function call.
	 */
	void set_ctrl_accelerator_key (guint accel_key_, 
						  const Glib::RefPtr<Gtk::AccelGroup>& accel_group_);

private:
	void bil_setup ();
	void pack_components ();

private:
	Gtk::Alignment* m_alignment;
	Gtk::HBox*      m_hbox;

	Gtk::Image* m_image;
	Gtk::Label* m_label;
};

#endif /* BUTTON_WITH_IMAGE_LABEL_H */
