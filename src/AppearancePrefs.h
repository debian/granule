// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: AppearancePrefs.h,v 1.6 2008/08/16 03:13:08 vlg Exp $
//------------------------------------------------------------------------------
//                            AppearancePrefs.h
//------------------------------------------------------------------------------
//  Copyright (c) 2006 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date   : Sun Sep 3 2006
//
//------------------------------------------------------------------------------

#ifndef APPEAR_PREFS_H
#define APPEAR_PREFS_H

#include <gtkmm/entry.h>
#include <gtkmm/button.h>
#include <gtkmm/box.h>
#include <gtkmm/checkbutton.h>
#include <gtkmm/label.h>
#include <gtkmm/table.h>

class PrefWindow;
class TextAlignment;
class FontsSelectorUI;
class ColorsSelectorUI;

/**-----------------------------------------------------------------------------
 *	General Preferences tab
 **-----------------------------------------------------------------------------
 */
class AppearancePrefs : public Gtk::VBox
{  
public:
    AppearancePrefs (PrefWindow& parent_);

    void changed_cb        ();
    void load_from_config  ();
    void save_to_config    ();

protected:
    PrefWindow&        m_pref_window;

	FontsSelectorUI*   m_fonts_selector;
	ColorsSelectorUI*  m_colors_selector;
	TextAlignment*     m_text_alignment;
};

/**-----------------------------------------------------------------------------
 *	Some useful macros
 **-----------------------------------------------------------------------------
 */
#define SET_FRAME(f,l,w) \
	f->set_shadow_type  (Gtk::SHADOW_ETCHED_IN); \
	f->set_border_width (4); \
	f->set_label_align  (1, 1); \
	f->set_label_widget (*l); \
	f->add (*w);

#define MAKE_EMPTY_LABEL(l) \
	l = Gtk::manage(new Gtk::Label("")); \
	l->set_alignment  (0.5,0.5); \
	l->set_padding    (0,0); \
	l->set_justify    (Gtk::JUSTIFY_LEFT); \
	l->set_line_wrap  (false); \
	l->set_use_markup (false); \
	l->set_selectable (false);

#define SET_CHECKBUTTON(b) \
	b->set_flags  (Gtk::CAN_FOCUS); \
	b->set_relief (Gtk::RELIEF_NORMAL); \
	b->set_mode   (true); \
    b->set_active (false); \
	b->set_border_width (4); \
	b->set_alignment (0.1, 0);

#endif /* APPEAR_PREFS */
