// -*- c++ -*-
//------------------------------------------------------------------------------
//                             PropertyBox.h
//------------------------------------------------------------------------------
// $Id: PropertyBox.h,v 1.4 2008/06/02 03:03:54 vlg Exp $
//------------------------------------------------------------------------------
//  Copyright (c) 2004,2006 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version
//  2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------
#ifndef PROPERTY_BOX_H
#define PROPERTY_BOX_H

#include <gtkmm/dialog.h>
#include <gtkmm/notebook.h>

#ifdef IS_HILDON
#    include <hildonmm.h>
#endif

#include "Granule-main.h"
#include "Granule.h"

#include "ButtonWithImageLabel.h"

/*==============================================================================
 * Class PropertyBox
 *==============================================================================
 */
#ifdef IS_HILDON
class PropertyBox : public Hildon::Window
#else
class PropertyBox : public Gtk::Dialog
#endif
{
public:
	PropertyBox (const std::string& title_);
	~PropertyBox ();

	/** 
	 * When a setting has changed, application code needs to invoke 
	 * this function to make Ok button sensitive.
	 */
	void changed ();

	/**
	 * Append a new page
	 *
	 * @param child_ Widget to add to the new page
	 * @param tab_ Name of the page
	 */
	gint append_page (Gtk::Widget& child_, const std::string& tab_);

	/**
	 * Called in response to pressing Apply button if any changes
	 * were reported prior by the derived class.
	 */
	virtual void apply_impl () = 0;

	/**
	 * Called in response to pressing Close button.
	 */
	virtual void close_impl () = 0;

protected:
	Gtk::Notebook* m_notebook;

	ButtonWithImageLabel* m_close_button;
	ButtonWithImageLabel* m_apply_button;

private:
	void on_apply_clicked ();
	void on_close_clicked ();
	bool on_delete_window_clicked (GdkEventAny* event_);

private:
	bool m_property_changed;	// Reflects <Apply> button status only.

#ifdef IS_HILDON
	Gtk::VBox* get_vbox () { return m_vbox; }
	Gtk::HButtonBox* get_action_area () { return m_action_area; }

	Gtk::VBox*       m_vbox;
	Gtk::HButtonBox* m_action_area;
#endif
};

inline
PropertyBox::
~PropertyBox ()
{
	trace_with_mask("PropertyBox::~PropertyBox",GUITRACE);
}


#endif /* PROPERTY_BOX_H */
