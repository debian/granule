// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: TextAlignment.h,v 1.8 2008/08/16 03:13:09 vlg Exp $
//------------------------------------------------------------------------------
//                            TextAlignment.h
//------------------------------------------------------------------------------
//  Copyright (c) Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date   : Wed Oct 25 20:49:21 EDT 2006
//
//------------------------------------------------------------------------------
#ifndef TEXT_ALIGNMENT_H
#define TEXT_ALIGNMENT_H

#include "PrefWindow.h"
#include "TextAlignmentsUI.h"

class TextAlignment : public Gtk::VBox
{
public:
	TextAlignment (PrefWindow& grandpa_);

	/** Sync appearance settings with GrappConf persistent storage.
	 */
    void load_appearance (AppearanceDB& app_db_);
    void save_appearance (AppearanceDB& app_db_);

private:
	PrefWindow&       m_pref_window;
	Gtk::VBox*        m_vbox;			/* parent */

	TextAlignmentsUI* m_front_widget;
	TextAlignmentsUI* m_back_widget;
	TextAlignmentsUI* m_example_widget;
};

#endif /* TEXT_ALIGNMENT_H */
