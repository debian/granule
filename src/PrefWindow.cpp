// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: PrefWindow.cpp,v 1.17 2008/06/02 03:03:54 vlg Exp $
//------------------------------------------------------------------------------
//                            PrefWindow.h
//------------------------------------------------------------------------------
//  Copyright (c) Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date   : Sun May 30 21:09:23 EDT 2004
//
//------------------------------------------------------------------------------

#include "GrappConf.h"
#include "PrefWindow.h"

PrefWindow::
PrefWindow (Gtk::Widget& parent_) 
	:
    PropertyBox      ("Preferences"),
	m_general_pref     (*this),
	m_appearance_pref  (*this),
	m_scheduling_pref  (*this),
	m_sound_pref       (*this),
	m_geometry_pref    (*this),
	m_ignore_callbacks (true),
	m_changed          (false)
{
    trace_with_mask("PrefWindow::PrefWindow",GUITRACE);

#ifdef IS_HILDON
	set_size_request (672, -1);
#else
	set_size_request (580, 380);
	set_transient_for (dynamic_cast<Gtk::Window&>(parent_));
#endif

	/** Show all
	 */
	m_general_pref.show_all    ();
	m_appearance_pref.show_all ();
	m_scheduling_pref.show_all ();
	m_sound_pref.show_all      ();
	m_geometry_pref.show_all   ();
	
	/** Pack all
	 */
	append_page (m_general_pref,    "General"   );
	append_page (m_appearance_pref, "Appearance");
	append_page (m_sound_pref,      "Sound"     );
	append_page (m_geometry_pref,   "Geometry"  );
}

bool
PrefWindow::
run ()
{
    trace_with_mask("PrefWindow::run",GUITRACE);

	m_ignore_callbacks = true;	
    m_changed = false;

	/** Fill values of all pages with fresh values from Config
	 */
	m_general_pref.   load_from_config ();
	m_appearance_pref.load_from_config ();
	m_sound_pref.     load_from_config ();
	m_geometry_pref.  load_from_config ();
	
	m_ignore_callbacks = false;
	m_apply_button->set_sensitive (false);

	show ();
    present ();
    gtk_main ();

    return m_changed;
}

void 
PrefWindow::
apply_impl ()
{
    trace_with_mask("PrefWindow::apply_impl",GUITRACE);

	m_general_pref.   save_to_config ();
	m_appearance_pref.save_to_config ();
	m_sound_pref.     save_to_config ();
	m_geometry_pref.  save_to_config ();
}

void
PrefWindow::
stop ()
{
    trace_with_mask("PrefWindow::stop",GUITRACE);

    hide ();
    Gtk::Main::quit ();
}

void 
PrefWindow::
changed_cb ()
{
    trace_with_mask("PrefWindow::changed_cb",GUITRACE);

	m_changed = true;
	DL ((GRAPP,"m_changed set to true\n"));

	if (!m_ignore_callbacks) {
		changed ();				// Enable <Apply> button
	}
}

void 
PrefWindow::
close_impl ()
{
    trace_with_mask("PrefWindow::close_impl",GUITRACE);
    stop ();
}
