// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: Intern.h,v 1.3 2005/06/17 03:09:32 vlg Exp $
//------------------------------------------------------------------------------
//                            Intern.h
// 
//    Copied verbatim from /usr/share/gettext/gettext.h
//------------------------------------------------------------------------------
#ifndef INTERN_H
#define INTERN_H

/*
 * Standard gettext macros.
 */
#ifdef ENABLE_NLS
#  include <libintl.h>
#  undef _
#  define _(String) dgettext (GETTEXT_PACKAGE, String)
#  ifdef gettext_noop
#    define N_(String) gettext_noop (String)
#  else
#    define N_(String) (String)
#  endif
#else
#  undef  textdomain
#  undef  gettext
#  undef  dgettext
#  undef  dcgettext
#  undef  bindtextdomain

#  define textdomain(String) (String)
#  define gettext(String) (String)
#  define dgettext(Domain,Message) (Message)
#  define dcgettext(Domain,Message,Type) (Message)
#  define bindtextdomain(Domain,Directory) (Domain)
#  define _(String) (String)
#  define N_(String) (String)
#endif


#endif /* INTERN_H */
