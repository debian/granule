// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: AppearanceDB.cpp,v 1.7 2008/08/19 02:52:46 vlg Exp $
//------------------------------------------------------------------------------
//                            AppearanceDB.cpp
//------------------------------------------------------------------------------
//  Copyright (c) 2007 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date   : Mon Sep 17 2007
//
//------------------------------------------------------------------------------

#include "AppearanceDB.h"

#include "Granule.h"
#include "TextAlignmentsUI.h"

using ASSA::IniFile;

/**
 * Global appearance settings assume most common flashcards
 * usage pattern of Word-Word-ExampleText combination.
 */
void
AppearanceDB::
init_global_defaults ()
{
	/**- FRONT --**/
	m_front_x_alignment     = "center";
	m_front_y_alignment     = "center";
	m_front_justification   = "center";

	m_front_x_padding       = "4";
	m_front_y_padding       = "0";

	m_front_img_position    = "top";

	/**- BACK --**/
	m_back_x_alignment      = "center";
	m_back_y_alignment      = "center";
	m_back_justification    = "center";

	m_back_x_padding        = "1";
	m_back_y_padding        = "1";

	/**- EXAMPLE --**/
	m_example_x_alignment   = "left";
	m_example_y_alignment   = "top";
	m_example_justification = "left";

	m_example_x_padding     = "2";
	m_example_y_padding     = "0";

	m_example_img_position  = "top";
}

/**
 * The Deck appearance settings are designed for displaying
 * text paragraphs Text-CardNumber-Text in the 'slide show' mode 
 * (full-screen) with enlarged text fonts set to Sans 24 or higher.
 */
void
AppearanceDB::
init_deck_defaults ()
{
	/**- FRONT --**/
	m_front_x_alignment     = "left";
	m_front_y_alignment     = "top";
	m_front_justification   = "left";

	m_front_x_padding       = "15";
	m_front_y_padding       = "15";

	m_front_img_position    = "top";

	/**- BACK --**/
	m_back_x_alignment      = "center";
	m_back_y_alignment      = "center";
	m_back_justification    = "left";

	m_back_x_padding        = "0";
	m_back_y_padding        = "0";

	/**- EXAMPLE --**/
	m_example_x_alignment   = "left";
	m_example_y_alignment   = "top";
	m_example_justification = "left";

	m_example_x_padding     = "8";
	m_example_y_padding     = "8";

	m_example_img_position  = "top";
}

/**=========================================================================**
 * Load text appearance settings from XML file                               *
 **=========================================================================**/
void
AppearanceDB::
load_from_xml_config (xmlDocPtr parser_, std::string& error_msg_)
{
	xmlChar* result;

	/*********
	 * Front *
	 *********/
	result = Granule::get_node_by_xpath (parser_, 
									 "/deck/appearance/front_app/@x_alignment");
	if (result == NULL) {
		return;
	}
	x_alignment (FRONT, (const char*) result);
	xmlFree (result);

	result = Granule::get_node_by_xpath (parser_, 
									 "/deck/appearance/front_app/@y_alignment");
	if (result == NULL) {
		return;
	}
	y_alignment (FRONT, (const char*) result);
	xmlFree (result);

	result = Granule::get_node_by_xpath (parser_, 
									 "/deck/appearance/front_app/@x_padding");
	if (result == NULL) {
		return;
	}
	x_padding (FRONT, (const char*) result);
	xmlFree (result);

	result = Granule::get_node_by_xpath (parser_, 
									 "/deck/appearance/front_app/@y_padding");
	if (result == NULL) {
		return;
	}
	y_padding (FRONT, (const char*) result);
	xmlFree (result);

	result = Granule::get_node_by_xpath (parser_, 
								 "/deck/appearance/front_app/@justification");
	if (result == NULL) {
		return;
	}
	justification (FRONT, (const char*) result);
	xmlFree (result);

	result = Granule::get_node_by_xpath (parser_, 
								 "/deck/appearance/front_app/@img_position");
	if (result == NULL) {
		return;
	}
	img_position (FRONT, (const char*) result);
	xmlFree (result);

	/********
	 * Back *
	 ****** */
	result = Granule::get_node_by_xpath (parser_, 
									 "/deck/appearance/back_app/@x_alignment");
	if (result == NULL) {
		return;
	}
	x_alignment (BACK, (const char*) result);
	xmlFree (result);

	result = Granule::get_node_by_xpath (parser_, 
									 "/deck/appearance/back_app/@y_alignment");
	if (result == NULL) {
		return;
	}
	y_alignment (BACK, (const char*) result);
	xmlFree (result);

	result = Granule::get_node_by_xpath (parser_, 
									 "/deck/appearance/back_app/@x_padding");
	if (result == NULL) {
		return;
	}
	x_padding (BACK, (const char*) result);
	xmlFree (result);

	result = Granule::get_node_by_xpath (parser_, 
									 "/deck/appearance/back_app/@y_padding");
	if (result == NULL) {
		return;
	}
	y_padding (BACK, (const char*) result);
	xmlFree (result);

	result = Granule::get_node_by_xpath (parser_, 
								 "/deck/appearance/back_app/@justification");
	if (result == NULL) {
		return;
	}
	justification (BACK, (const char*) result);
	xmlFree (result);

	/***********
	 * Example *
	 ***********/
	result = Granule::get_node_by_xpath (parser_, 
								 "/deck/appearance/example_app/@x_alignment");
	if (result == NULL) {
		return;
	}
	x_alignment (EXAMPLE, (const char*) result);
	xmlFree (result);

	result = Granule::get_node_by_xpath (parser_, 
								 "/deck/appearance/example_app/@y_alignment");
	if (result == NULL) {
		return;
	}
	y_alignment (EXAMPLE, (const char*) result);
	xmlFree (result);

	result = Granule::get_node_by_xpath (parser_, 
								 "/deck/appearance/example_app/@x_padding");
	if (result == NULL) {
		return;
	}
	x_padding (EXAMPLE, (const char*) result);
	xmlFree (result);

	result = Granule::get_node_by_xpath (parser_, 
								 "/deck/appearance/example_app/@y_padding");
	if (result == NULL) {
		return;
	}
	y_padding (EXAMPLE, (const char*) result);
	xmlFree (result);

	result = Granule::get_node_by_xpath (parser_, 
								 "/deck/appearance/example_app/@justification");
	if (result == NULL) {
		return;
	}

	justification (EXAMPLE, (const char*) result);
	xmlFree (result);

	result = Granule::get_node_by_xpath (parser_, 
								 "/deck/appearance/example_app/@img_position");
	if (result == NULL) {
		return;
	}

	img_position (EXAMPLE, (const char*) result);
	xmlFree (result);
}

void
AppearanceDB::
save_to_xml_config (xmlTextWriterPtr writer_)
{
	int ret = 0;

    //--------------------------------------------------------------------------
	ret = xmlTextWriterStartElement (writer_, BAD_CAST "front_app");
	ret = xmlTextWriterWriteAttribute (writer_, BAD_CAST "x_alignment",
									   BAD_CAST m_front_x_alignment.c_str ());
	ret = xmlTextWriterWriteAttribute (writer_, BAD_CAST "y_alignment",
									   BAD_CAST m_front_y_alignment.c_str ());
	ret = xmlTextWriterWriteAttribute (writer_, BAD_CAST "x_padding",
									   BAD_CAST m_front_x_padding.c_str ());
	ret = xmlTextWriterWriteAttribute (writer_, BAD_CAST "y_padding",
									   BAD_CAST m_front_y_padding.c_str ());
	ret = xmlTextWriterWriteAttribute (writer_, BAD_CAST "justification",
									   BAD_CAST m_front_justification.c_str ());
	ret = xmlTextWriterWriteAttribute (writer_, BAD_CAST "img_position",
									   BAD_CAST m_front_img_position.c_str ());
	ret = xmlTextWriterEndElement (writer_);	// @front_app

    //--------------------------------------------------------------------------
	ret = xmlTextWriterStartElement (writer_, BAD_CAST "back_app");
	ret = xmlTextWriterWriteAttribute (writer_, BAD_CAST "x_alignment",
									   BAD_CAST m_back_x_alignment.c_str ());
	ret = xmlTextWriterWriteAttribute (writer_, BAD_CAST "y_alignment",
									   BAD_CAST m_back_y_alignment.c_str ());
	ret = xmlTextWriterWriteAttribute (writer_, BAD_CAST "x_padding",
									   BAD_CAST m_back_x_padding.c_str ());
	ret = xmlTextWriterWriteAttribute (writer_, BAD_CAST "y_padding",
									   BAD_CAST m_back_y_padding.c_str ());
	ret = xmlTextWriterWriteAttribute (writer_, BAD_CAST "justification",
									   BAD_CAST m_back_justification.c_str ());
	ret = xmlTextWriterEndElement (writer_);	// @back_app

    //--------------------------------------------------------------------------
	ret = xmlTextWriterStartElement (writer_, BAD_CAST "example_app");
	ret = xmlTextWriterWriteAttribute (writer_, BAD_CAST "x_alignment",
									   BAD_CAST m_example_x_alignment.c_str ());
	ret = xmlTextWriterWriteAttribute (writer_, BAD_CAST "y_alignment",
									   BAD_CAST m_example_y_alignment.c_str ());
	ret = xmlTextWriterWriteAttribute (writer_, BAD_CAST "x_padding",
									   BAD_CAST m_example_x_padding.c_str ());
	ret = xmlTextWriterWriteAttribute (writer_, BAD_CAST "y_padding",
									   BAD_CAST m_example_y_padding.c_str ());
	ret = xmlTextWriterWriteAttribute (writer_, BAD_CAST "justification",
									 BAD_CAST m_example_justification.c_str());
	ret = xmlTextWriterWriteAttribute (writer_, BAD_CAST "img_position",
									 BAD_CAST m_example_img_position.c_str());
	ret = xmlTextWriterEndElement (writer_);	// @example_app
}

/**=========================================================================**
 * Load text appearance settings from INI file                               *
 **=========================================================================**/
void
AppearanceDB::
load_from_ini_config (ASSA::IniFile* ini_filep_)
{
	std::string s;

	if (ini_filep_ == NULL) {
		return;
	}

	/**-- FRONT --**/
    s = ini_filep_->get_value ("Default", "front_x_alignment");
    if (! s.empty ()) {
		m_front_x_alignment = s;
    }

    s = ini_filep_->get_value ("Default", "front_y_alignment");
    if (! s.empty ()) {
		m_front_y_alignment = s;
    }

    s = ini_filep_->get_value ("Default", "front_x_padding");
    if (! s.empty ()) {
		m_front_x_padding = s;
    }

    s = ini_filep_->get_value ("Default", "front_y_padding");
    if (! s.empty ()) {
		m_front_y_padding = s;
    }

    s = ini_filep_->get_value ("Default", "front_justification");
    if (! s.empty ()) {
		m_front_justification = s;
    }

    s = ini_filep_->get_value ("Default", "front_img_position");
    if (! s.empty ()) {
		m_front_img_position = s;
    }

	/**-- BACK --**/
    s = ini_filep_->get_value ("Default", "back_x_alignment");
    if (! s.empty ()) {
		m_back_x_alignment = s;
    }

    s = ini_filep_->get_value ("Default", "back_y_alignment");
    if (! s.empty ()) {
		m_back_y_alignment = s;
    }

    s = ini_filep_->get_value ("Default", "back_x_padding");
    if (! s.empty ()) {
		m_back_x_padding = s;
    }

    s = ini_filep_->get_value ("Default", "back_y_padding");
    if (! s.empty ()) {
		m_back_y_padding = s;
    }

    s = ini_filep_->get_value ("Default", "back_justification");
    if (! s.empty ()) {
		m_back_justification = s;
    }

	/**-- EXAMPLE --**/
    s = ini_filep_->get_value ("Default", "example_x_alignment");
    if (! s.empty ()) {
		m_example_x_alignment = s;
    }

    s = ini_filep_->get_value ("Default", "example_y_alignment");
    if (! s.empty ()) {
		m_example_y_alignment = s;
    }

    s = ini_filep_->get_value ("Default", "example_x_padding");
    if (! s.empty ()) {
		m_example_x_padding = s;
    }

    s = ini_filep_->get_value ("Default", "example_y_padding");
    if (! s.empty ()) {
		m_example_y_padding = s;
    }

    s = ini_filep_->get_value ("Default", "example_justification");
    if (! s.empty ()) {
		m_example_justification = s;
    }

    s = ini_filep_->get_value ("Default", "example_img_position");
    if (! s.empty ()) {
		m_example_img_position = s;
    }
}

void
AppearanceDB::
save_to_ini_config (ASSA::IniFile* ini_filep_)
{
	/**-- FRONT --**/
    ini_filep_->set_pair ("Default", 
						  IniFile::tuple_type("front_x_alignment",	
											  m_front_x_alignment));
    ini_filep_->set_pair ("Default", 
						  IniFile::tuple_type("front_y_alignment",	
											  m_front_y_alignment));
    ini_filep_->set_pair ("Default", 
						  IniFile::tuple_type("front_x_padding",	
											  m_front_x_padding));
    ini_filep_->set_pair ("Default", 
						  IniFile::tuple_type("front_y_padding",	
											  m_front_y_padding));
    ini_filep_->set_pair ("Default", 
						  IniFile::tuple_type("front_justification",	
											  m_front_justification));
    ini_filep_->set_pair ("Default", 
						  IniFile::tuple_type("front_img_position",
											  m_front_img_position));
    /**-- BACK --**/
    ini_filep_->set_pair ("Default", 
						  IniFile::tuple_type("back_x_alignment",	
											  m_back_x_alignment));
    ini_filep_->set_pair ("Default", 
						  IniFile::tuple_type("back_y_alignment",	
											  m_back_y_alignment));
    ini_filep_->set_pair ("Default", 
						  IniFile::tuple_type("back_x_padding",	
											  m_back_x_padding));
    ini_filep_->set_pair ("Default", 
						  IniFile::tuple_type("back_y_padding",	
											  m_back_y_padding));
    ini_filep_->set_pair ("Default", 
						  IniFile::tuple_type("back_justification",	
											  m_back_justification));
	/**-- EXAMPLE --**/
    ini_filep_->set_pair ("Default", 
						  IniFile::tuple_type("example_x_alignment",	
											  m_example_x_alignment));
    ini_filep_->set_pair ("Default", 
						  IniFile::tuple_type("example_y_alignment",	
											  m_example_y_alignment));
    ini_filep_->set_pair ("Default", 
						  IniFile::tuple_type("example_x_padding",	
											  m_example_x_padding));
    ini_filep_->set_pair ("Default", 
						  IniFile::tuple_type("example_y_padding",	
											  m_example_y_padding));
    ini_filep_->set_pair ("Default", 
						  IniFile::tuple_type("example_justification",	
											  m_example_justification));
    ini_filep_->set_pair ("Default", 
						  IniFile::tuple_type("example_img_position",	
											  m_example_img_position));
}

/** From DeckView
 */
void
AppearanceDB::
update_view (SideSelection s_, TextAlignmentsUI*& view_)
{
	int value;
	string b;

	/**------------------------------------------------**/
	b = x_alignment (s_);
	if      (b == "center") { value = TA_X_ALIGN_CENTER; }
	else if (b == "left")   { value = TA_X_ALIGN_LEFT;   }
	else if (b == "right")  { value = TA_X_ALIGN_RIGHT;  }
	else                    { value = TA_X_ALIGN_CENTER; }

	view_->x_align_combo ()->set_active (value);

	b = y_alignment (s_);
	if      (b == "center") { value = TA_Y_ALIGN_CENTER; }
	else if (b == "top")    { value = TA_Y_ALIGN_TOP;    }
	else if (b == "bottom") { value = TA_Y_ALIGN_BOTTOM; }
	else                    { value = TA_Y_ALIGN_CENTER; }

	view_->y_align_combo ()->set_active (value);

	/**------------------------------------------------**/
	b = justification (s_);
	if      (b == "center") { value = TA_JUSTIFY_CENTER; }
	else if (b == "left")   { value = TA_JUSTIFY_LEFT;   }
	else if (b == "right")  { value = TA_JUSTIFY_RIGHT;  }
	else if (b == "fill")   { value = TA_JUSTIFY_FILL;   }
	else                    { value = TA_JUSTIFY_CENTER; }

	view_->paragraph_combo ()->set_active (value);

	/**------------------------------------------------**/
	view_->x_padding_entry ()->set_text (x_padding (s_));
	view_->y_padding_entry ()->set_text (y_padding (s_));

	/**------------------------------------------------**/
	b = img_position (s_);
	if      (b == "top")    { value = TA_IMG_POSITION_TOP;    }
	else if (b == "bottom") { value = TA_IMG_POSITION_BOTTOM; }
	else                    { value = TA_IMG_POSITION_TOP;    }

	view_->img_position_combo ()->set_active (value);
}

void
AppearanceDB::
fetch_from_view (SideSelection s_, TextAlignmentsUI*& view_)
{
	std::string result;
	int value;

	/**-----------------------------------------------------**/
	value = view_->x_align_combo ()->get_active_row_number ();

	switch (value) {
	case TA_X_ALIGN_CENTER:	result = "center"; break;
	case TA_X_ALIGN_LEFT:	result = "left";   break;
	case TA_X_ALIGN_RIGHT:	result = "right";  break;
	default:                result = "center";
	}

	x_alignment (s_, result);

	/**-----------------------------------------------------**/
	value = view_->y_align_combo ()->get_active_row_number ();

	switch (value) {
	case TA_Y_ALIGN_CENTER:	result = "center";	break;
	case TA_Y_ALIGN_TOP:    result = "top";    	break;
	case TA_Y_ALIGN_BOTTOM: result = "bottom"; 	break;
	default:                result = "center";
	}

	y_alignment (s_, result);

	/**-----------------------------------------------------**/
	value = view_->paragraph_combo ()->get_active_row_number ();

	switch (value) {
	case TA_JUSTIFY_CENTER:	result = "center";	break;
	case TA_JUSTIFY_LEFT:	result = "left";	break;
	case TA_JUSTIFY_RIGHT:	result = "right";	break;
	case TA_JUSTIFY_FILL:	result = "fill";	break;
	default:                result = "center";
	}

	justification (s_, result);

	/**-----------------------------------------------------**/
	x_padding (s_, view_->x_padding_entry()->get_text());
	y_padding (s_, view_->y_padding_entry()->get_text());

	/**-----------------------------------------------------**/
	value = view_->img_position_combo ()->get_active_row_number ();

	switch (value) {
	case TA_IMG_POSITION_TOP:	 result = "top";    break;
	case TA_IMG_POSITION_BOTTOM: result = "bottom"; break;
	default:                     result = "top";
	}

	img_position (s_, result);
}
