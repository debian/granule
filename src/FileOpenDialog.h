// -*- c++ -*-
//------------------------------------------------------------------------------
//                               FileOpenDialog.h
//------------------------------------------------------------------------------
// $Id: FileOpenDialog.h,v 1.1 2007/12/02 00:31:24 vlg Exp $
//------------------------------------------------------------------------------
//  Copyright (c) 2006 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version
//  2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------
#ifndef FILE_OPEN_DIALOG_H
#define FILE_OPEN_DIALOG_H

#include <vector>
#include <gtkmm/filechooserdialog.h>
#include <gtkmm/fileselection.h>

#include "Granule-main.h"
#include "Granule.h"

class FileOpenDialog
{
public:
	FileOpenDialog (const Glib::ustring& title_,
					Gtk::Widget*         parent_,
					const Glib::ustring& filter_name_    = "",
					const Glib::ustring& filter_pattern_ = "");

	~FileOpenDialog ();

	/** Sets the current name in the file selector, 
	 * as if entered by the user.
	 */
	void set_current_name (const Glib::ustring& name_);

	/// Return true if path can be set
	bool set_current_folder (const Glib::ustring& path_);

	/** Sets filename as the current filename for the file chooser, 
	 *  by changing to the file's parent folder and actually selecting 
	 *  the file in list.
	 */
	bool set_filename (const Glib::ustring& fname_);

	/// Get the name selected
	Glib::ustring get_filename () const;

	/** For multi-entries
	 */
	void set_select_multiple (bool v_ = true);
	std::vector<Glib::ustring> get_filenames () const;

	int  run  ();
	void show ();
	void hide ();

private:

#ifdef OBSOLETE
	Gtk::FileSelection*     m_dialog;
#else
	Gtk::FileChooserDialog* m_dialog;
#endif
	bool m_multi_choice;		// If true, allows to select multiple items
};

//------------------------------------------------------------------------------
// Inline functions
//------------------------------------------------------------------------------
inline
FileOpenDialog::
~FileOpenDialog ()
{
	if (m_dialog) {
		delete m_dialog;
		m_dialog = NULL;
	}
}

inline bool 
FileOpenDialog::
set_current_folder (const Glib::ustring& path_)
{
	return m_dialog->set_current_folder (path_);

}

inline bool
FileOpenDialog::
set_filename (const Glib::ustring& fname_)
{
	return m_dialog->set_filename (fname_);
}

inline Glib::ustring 
FileOpenDialog::
get_filename () const
{
	return m_dialog->get_filename ();
}

inline void
FileOpenDialog::
set_current_name (const Glib::ustring& name_)
{
#ifdef OBSOLETE
	m_dialog->set_filename (name_);
#else
	m_dialog->set_current_name (name_);
#endif
}

inline void 
FileOpenDialog::
set_select_multiple (bool v_) 
{ 
#ifdef OBSOLETE
	m_dialog->set_select_multiple (v_);
#else
	m_dialog->set_select_multiple (v_);
#endif
}

inline int
FileOpenDialog::
run ()
{
	return (m_dialog->run ());
}
	
inline void
FileOpenDialog::
hide ()
{
	m_dialog->hide ();
}

inline void
FileOpenDialog::
show ()
{
	m_dialog->show ();
}


#endif /* FILE_OPEN_DIALOG_H */
