// -*- c++ -*-
//------------------------------------------------------------------------------
//                            FontsSelectorUI.h
//------------------------------------------------------------------------------
// $Id: FontsSelectorUI.h,v 1.1 2007/12/02 00:31:24 vlg Exp $
//------------------------------------------------------------------------------
//  Copyright (c) 2007 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date   : Thu Oct 4 2007
//
//------------------------------------------------------------------------------
#ifndef FONTS_SELECTOR_H
#define FONTS_SELECTOR_H

#include <vector>
#include <string>

#include <gtkmm/dialog.h>
#include <gtkmm/frame.h>
#include <gtkmm/alignment.h>
#include <gtkmm/box.h>
#include <gtkmm/checkbutton.h>
#include <gtkmm/combobox.h>
#include <gtkmm/entry.h>
#include <gtkmm/liststore.h>
#include <gtkmm/treemodel.h>
#include <gtkmm/table.h>

#include "Granule-main.h"

/**-----------------------------------------------------------------------------
 *  Compound Font Entry
 *
 *  /-------\/---------------------\/-----------------\
 *  | Label || Entry               || <Select> Button |
 *  \-------/\---------------------/\-----------------/
 *------------------------------------------------------------------------------
 */
class FontEntry 
{
public:
    Gtk::Label*   m_label;
    Gtk::Entry*   m_entry;		// Font entry
    Gtk::Button*  m_button;		// Font Dialog selection button
};

/**-----------------------------------------------------------------------------
 *  FontsSelectorUI - the View of Fonts MVC pattern.
 *  The widget is embeddable.
 *------------------------------------------------------------------------------
 */
class FontsSelectorUI : public Gtk::Frame
{
public:
	FontsSelectorUI ();

	FontEntry& font_entry (TextFontType type_) { return m_fentry [type_]; }

	void set_text (TextFontType type_, const Pango::FontDescription& value_);
	void set_text (TextFontType type_, const Glib::ustring& value_);

	Glib::ustring get_text (TextFontType type_) const;

	/// Callback fired when FontEntry button is clicked
	void on_select_clicked (int idx_);

	/// Enable/disable element. By default, all are enabled (active).
	void set_active (TextFontType type_, bool val_ = true);

private:
    FontEntry   m_fentry [FONT_ENTRY_SZ];
	Gtk::Table* m_font_table;
};

/**-----------------------------------------------------------------------------
 *  Implementation
 *------------------------------------------------------------------------------
 */

inline void
FontsSelectorUI::
set_text (TextFontType type_, const Pango::FontDescription& value_)
{
	m_fentry [type_].m_entry->set_text (value_.to_string ());
}

inline void
FontsSelectorUI::
set_text (TextFontType type_, const Glib::ustring& value_)
{
	m_fentry [type_].m_entry->set_text (value_);
}

inline Glib::ustring
FontsSelectorUI::
get_text (TextFontType type_) const
{
	return m_fentry [type_].m_entry->get_text ();
}

#endif /* FONTS_SELECTOR_H */
