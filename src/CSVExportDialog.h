// -*- c++ -*-
//------------------------------------------------------------------------------
//                            CSVExportDialog.h
//------------------------------------------------------------------------------
// $Id: CSVExportDialog.h,v 1.1 2006/11/13 01:05:14 vlg Exp $
//------------------------------------------------------------------------------
//  Copyright (c) 2006 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version
//  2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------
// 11/12/2006 VLG  Created
//------------------------------------------------------------------------------
#ifndef CSV_EXPORT_DIALOG_H
#define CSV_EXPORT_DIALOG_H

#include <gtkmm/dialog.h>
#include <gtkmm/button.h>
#include <gtkmm/checkbutton.h>

#include "Granule-main.h"

class CSVExportDialog : public Gtk::Dialog
{  
public:
	CSVExportDialog();

	/// Do we break on LF or CR+LF?
	bool is_windoz_linebreak () const { 
		return m_windoz_crlf->get_active ();
	}

	/// Return separator selected
	const char* get_separator () const;

	/// Return double separator
	const char* get_double_separator () const;

	/** If true, then replace each pair of adjacent separators with one.
	 */
	bool with_example () const { 
		return m_with_example->get_active (); 
	}

	/** callbacks
	 */
	void on_unix_lf_checked     ();
	void on_windoz_crlf_checked ();

	void on_space_sep_checked  ();
	void on_tab_sep_checked    ();
	void on_colon_sep_checked  ();
	void on_semicolon_sep_checked  ();
	void on_comma_sep_checked  ();

private:
	Gtk::Button*      m_cancel_button;
	Gtk::Button*      m_import_button;

	Gtk::CheckButton* m_unix_lf;
	Gtk::CheckButton* m_windoz_crlf;

	Gtk::CheckButton* m_space_sep;
	Gtk::CheckButton* m_tab_sep;
	Gtk::CheckButton* m_colon_sep;
	Gtk::CheckButton* m_semicolon_sep;
	Gtk::CheckButton* m_comma_sep;
	Gtk::CheckButton* m_with_example;

	CSVSeparator m_sep_selection;
	bool         m_recursive_call;
};

#endif
