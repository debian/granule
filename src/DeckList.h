// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: DeckList.h,v 1.12 2008/01/14 03:41:00 vlg Exp $
//------------------------------------------------------------------------------
//                            DeckList.h
//------------------------------------------------------------------------------
//  Copyright (c) 2004 by Vladislav Grinchenko 
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date   : Mar  1 2004
//
//------------------------------------------------------------------------------
#ifndef DECK_LIST_H
#define DECK_LIST_H

#include <gtkmm/scrolledwindow.h>
#include <gtkmm/treeview.h>
#include <gtkmm/liststore.h>
#include <gtkmm/menu.h>
#include <gtkmm/menubar.h>

#include "Deck.h"

class DeckList : public Gtk::ScrolledWindow
{
public:
	typedef sigc::signal<void> signal_mouse_clicked_type;

public:
	DeckList ();
	void init_popup_menu ();

	~DeckList ();

	typedef Gtk::TreeModel::iterator iterator;

	iterator begin ()       { return m_list_store_ref->children ().begin (); }
	iterator end   (void)   { return m_list_store_ref->children ().end   (); }
	size_t   size  () const { return m_list_store_ref->children ().size  (); }

	void   push_back (Deck* deck_);

	string get_name  (iterator at_iter_) const;
	Deck*  get_deck  (iterator at_iter_);
	Deck*  get_selected () const { return m_selected_deck; }
	DeckEditStatus get_deck_status (iterator at_iter_) const;

	void   unselect ();
	void   save (iterator at_iter);

	bool   deck_needs_renaming (iterator at_iter_) const;
	void   refresh_selected_name ();

	/*< callbacks >*/
	void on_name_selected (void);
	bool on_mouse_click (GdkEventButton* event_);
	bool on_key_tapped (GdkEventKey* event_);

	/*< Singals >*/
	signal_mouse_clicked_type signal_mouse_clicked ();

	/** Callback called when user starts dragging Deck item towards
		a CardDeck icon.
	*/
	void on_drag_begin (const Glib::RefPtr<Gdk::DragContext>& context_);

	/** Callback called when user starts dragging Deck item towards
		a CardDeck icon.
	*/
	void on_drag_data_get (const Glib::RefPtr<Gdk::DragContext>& context_, 
						   Gtk::SelectionData& selection_data_, 
						   guint type_id_, 
						   guint drag_timestamp_);

	/** Return XML filename minus path and extention.
	 */
	static string get_bare_name (const string& filepath_);

private:
	struct ModelColumns : public Gtk::TreeModelColumnRecord
	{
		Gtk::TreeModelColumn<std::string> m_name;
		Gtk::TreeModelColumn<Deck*> m_deck_ref;	// hiddend
		ModelColumns () { add (m_name); add (m_deck_ref); }
	};
	const ModelColumns m_columns;

private:

#ifdef IS_HILDON
	void on_row_activated (const Gtk::TreePath& path_, 
						   Gtk::TreeViewColumn* column_);
#endif

private:
	Gtk::TreeView m_tree_view;

	/// TreeModel
	Glib::RefPtr<Gtk::ListStore> m_list_store_ref; 

    /// Selection in current TreeView
	Glib::RefPtr<Gtk::TreeSelection> m_tree_sel_ref; 

	/// Currently selected deck
	Deck* m_selected_deck;

	/// Popup menu
	Gtk::Menu m_popup_menu;

	signal_mouse_clicked_type m_signal_mouse_clicked;
};

inline
DeckList::
~DeckList ()
{
	trace_with_mask("DeckList::DeckList", GUITRACE);
}

inline DeckEditStatus
DeckList::
get_deck_status (iterator iter_) const
{
	trace_with_mask("DeckList::get_deck_status", GUITRACE);
	DeckList* dl = const_cast<DeckList*> (this);
	return (dl->get_deck (iter_)->get_status ());
}

inline bool
DeckList::
deck_needs_renaming (iterator iter_) const
{
	DeckList* dl = const_cast<DeckList*> (this);
	return (dl->get_deck (iter_)->needs_renaming ());
}

inline void
DeckList::
save (iterator iter_)
{
	trace_with_mask("DeckList::save", GUITRACE);
	DeckList* dl = const_cast<DeckList*> (this);
	dl->get_deck (iter_)->save ();
}

#endif /* DECK_LIST_H */
