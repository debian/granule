// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: CardBoxPrefsWindow.h,v 1.8 2008/02/11 03:36:16 vlg Exp $
//------------------------------------------------------------------------------
//                            CardBoxPrefsWindow.h
//------------------------------------------------------------------------------
//  Copyright (c) 2007 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date   : Tue Dec 18 2007
//
//------------------------------------------------------------------------------
#ifndef CB_PREFS_WINDOW_H
#define CB_PREFS_WINDOW_H

#include <gtkmm/main.h>

#include "Granule-main.h"
#include "GrappConf.h"

#include "PropertyBox.h"

#include "ScheduleUI.h"

// Ubuntu seems to have this one missing
#include <math.h>
extern float fabsf(float x);

class ScheduleDB;

/**
 * @class CardBoxPrefsWindow
 *
 * This class is the Property shell for the Schedule UI View.
 * It provides functionality of a property page.
 */
class CardBoxPrefsWindow : public PropertyBox
{
public:
    CardBoxPrefsWindow (Gtk::Widget& parent_);
    ~CardBoxPrefsWindow();
    
	/** Display the Preferences dialog.
	 *  @return true if Apply button was clicked; and false if Cancel.
	 */
    bool run ();

	/** Configure the View with data from the Model.
	 */
	void load_from_config (ScheduleDB& sched_db_);

	/** Save View changes to the Model.
	 */
	void save_to_config (ScheduleDB& sched_db_);

	/** Both capacity and E-EFactor might have changed.
	 *  Caller can find out if capacity has been altered 
	 *  by calling this function.
	 */
	bool capacity_changed () const { return m_capacity_changed; }

	/** Both capacity and E-EFactor might have changed.
	 *  Caller can find out if capacity has been altered 
	 *  by calling this function.
	 */
	bool efactor_changed () const { return m_efactor_changed; }

private:
    virtual void apply_impl ();
    virtual void close_impl ();

    void on_property_changed_cb ();
    void stop ();

private:
	ScheduleUI m_scheduling_pref;

	bool m_property_changed;
	bool m_capacity_changed;
	bool m_efactor_changed;

	size_t m_old_capacity;
	float  m_old_efactor;
};

inline
CardBoxPrefsWindow::
~CardBoxPrefsWindow()
{
    trace_with_mask("CardBoxPrefsWindow::~CardBoxPrefsWindow",GUITRACE);
}

inline void 
CardBoxPrefsWindow::
on_property_changed_cb ()
{
    trace_with_mask("CardBoxPrefsWindow::on_property_changed_cb",GUITRACE);

	m_property_changed = true;
	changed ();				// Enable <Apply> button
}

inline void 
CardBoxPrefsWindow::
apply_impl ()
{
    trace_with_mask("CardBoxPrefsWindow::apply_impl",GUITRACE);

	if (m_old_capacity != m_scheduling_pref.capacity ()) {
		m_capacity_changed = true;
	}

	if (fabsf (m_old_efactor - m_scheduling_pref.efactor ()) >= 0.01) {
		m_efactor_changed = true;
	}
}

inline void 
CardBoxPrefsWindow::
close_impl ()
{
    trace_with_mask("CardBoxPrefsWindow::close_impl",GUITRACE);
    stop ();
}

inline void
CardBoxPrefsWindow::
stop ()
{
    trace_with_mask("CardBoxPrefsWindow::stop",GUITRACE);

    hide ();
    Gtk::Main::quit ();
}

#endif /* CB_PREFS_WINDOW_H */
