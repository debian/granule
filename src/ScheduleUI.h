// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: ScheduleUI.h,v 1.6 2008/02/22 18:52:45 vlg Exp $
//------------------------------------------------------------------------------
//                          ScheduleUI.h
//------------------------------------------------------------------------------
//  Copyright (c) 2007 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date   : Mon Dec 10 2007
//
//------------------------------------------------------------------------------
#ifndef SCHEDULE_UI_H
#define SCHEDULE_UI_H

#include <gtkmm/spinbutton.h>
#include <gtkmm/adjustment.h>
#include <gtkmm/button.h>
#include <gtkmm/frame.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/treeview.h>
#include <gtkmm/liststore.h>

#include <string>
using std::string;

/** 
 *  @class ScheduleUI
 *
 *  This class is the View for the Schedule DB Data.
 *  The user can experiment with the schedule by changing
 *  the E-Factor and Capacity (number of boxes) and redraw
 *  the schedule with [Refresh] button. Pressing [Refresh]
 *  button emmits the change_signal to notify controller 
 *  of the change.
 */
class ScheduleUI : public Gtk::Frame
{
public:
	typedef guint interval_type;
	typedef sigc::signal<void> signal_changed_type;

	class ViewRowData 
	{
	public:
		ViewRowData () : m_index (0) { /* no-op */ }

		void to_str (string& years_, 
					 string& months_, 
					 string& weeks_, 
					 string& days_);
		
		interval_type interval () const { return m_interval; }

	public:
		int           m_index; 
		interval_type m_interval;
		interval_type m_years;
		interval_type m_months;
		interval_type m_weeks;
		interval_type m_days;
	};

public:
	ScheduleUI ();
	
	void   efactor  (float v_);
	float  efactor  () const;

	void   capacity (size_t v_);
	size_t capacity () const;

	/** 'Changed' signal is emitted any time the user presses
	 *  [Refresh] button and configuration has been changed.
	 */
	signal_changed_type signal_changed ();

	/*= TreeView manipulators
	 */
	typedef Gtk::TreeModel::iterator iterator;

    iterator begin ()       { return m_list_store_ref->children ().begin (); }
    iterator end   (void)   { return m_list_store_ref->children ().end   (); }
    size_t   size  () const { return m_list_store_ref->children ().size  (); }
    void     push_back (ViewRowData& vrow_);

	void     clear_view () { m_list_store_ref->clear (); }

private:
	void on_refresh_clicked ();
	void on_changed_clicked ();

private:
	struct ModelColumns :public Gtk::TreeModelColumnRecord
    {
        Gtk::TreeModelColumn<interval_type> m_index;     /* Cardbox number */
        Gtk::TreeModelColumn<interval_type> m_interval;  /* Total days  */
        Gtk::TreeModelColumn<interval_type> m_years;     /* Full years  */
        Gtk::TreeModelColumn<interval_type> m_months;    /* Full months */
        Gtk::TreeModelColumn<interval_type> m_weeks;     /* Full weeks  */
        Gtk::TreeModelColumn<interval_type> m_days;      /* Full days   */

        ModelColumns () { 
			add (m_index); add (m_interval); add (m_years);
			add (m_months); add (m_weeks); add (m_days);
		}
    };

    const ModelColumns m_columns;

private:
	Gtk::TreeView m_tree_view;

    /// TreeModel
    Glib::RefPtr<Gtk::ListStore> m_list_store_ref;

    /// Selection in current TreeView
    Glib::RefPtr<Gtk::TreeSelection> m_tree_sel_ref;

private:
	Gtk::Frame*      m_frame;

	Gtk::Adjustment* m_ef_spinbutton_adj;
   	Gtk::Adjustment* m_sz_spinbutton_adj;

	Gtk::SpinButton* m_ef_spinbutton;
    Gtk::SpinButton* m_sz_spinbutton;

	/// Clicking 'Refresh' button updates View locally
	Gtk::Button*     m_refresh_button;

	/// Changed signal
	signal_changed_type m_signal_changed;
	
	/// True if any configuration property has been altered.
	bool m_property_modified;
};
	

#endif /* SCHEDULE_UI_H */
