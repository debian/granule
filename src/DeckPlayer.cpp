// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: DeckPlayer.cpp,v 1.158 2008/08/19 02:52:46 vlg Exp $
//------------------------------------------------------------------------------
//                            DeckPlayer.cpp
//------------------------------------------------------------------------------
//  Copyright (c) 2004-2008 by Vladislav Grinchenko 
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date   : Fri Feb  6 23:37:11 EST 2004
//
//------------------------------------------------------------------------------

#include <algorithm>

#include <gtkmm/stock.h>
#include <gtkmm/separator.h>
#include <gtkmm/spinbutton.h>
#include <gtkmm/box.h>
#include <gtkmm/handlebox.h>
#include <gtkmm/buttonbox.h>
#include <gtkmm/radiobutton.h>
#include <gtkmm/frame.h>
#include <gtkmm/toolbar.h>
#include <gtkmm/textattributes.h>
#include <gtkmm/main.h>
#include <gtkmm/progressbar.h>
#include <gtkmm/eventbox.h>
#include <gtkmm/menutoolbutton.h>

using sigc::bind;
using sigc::mem_fun;

#ifdef IS_HILDON
#   include <hildonmm.h>
#endif

#include "ButtonWithImageLabel.h"

#include "Granule-main.h"
#include "Granule.h"
#include "MainWindow.h"
#include "Card.h"
#include "Deck.h"
#include "CardBox.h"
#include "DeckPlayer.h"
#include "DeckView.h"
#include "CardView.h"
#include "VerifyControl.h"

#include "Intern.h"

static const gchar* states [] = { "VC_START", "VC_ARMED", "VC_VALIDATED" };

static const int QUESTION_PADDING_X   =4; // Distance between text and an edge
static const int QUESTION_PADDING_Y   =0; // Distance between text and an edge

static const int EXAMPLE_PADDING_X    =2; // Distance between text and an edge
static const int EXAMPLE_PADDING_Y    =0; // Distance between text and an edge

static const int ANSWER_BOX_SPACING   =0; // How far box is placed from the edge
static const int ANSWER_CHILD_SPACING =0; // Spacing between child widgets

static const bool LARGE_ICONS = true;
static const int  LARGE_ICON_SZ = 72;

#if defined(IS_PDA) && (GTKMM_MAJOR_VERSION == 2 && GTKMM_MINOR_VERSION >= 6)
#  define DP_SHRINK_ICONS ,10,10,false
#else
#  define DP_SHRINK_ICONS
#endif

const string 
DeckPlayer::RATIO_TITLE="<span foreground='brown'>OK ratio:</span> ";
const string 
DeckPlayer::RIGHT_WRONG_TITLE="<span foreground='brown'>OK/Bad:</span> ";
const string 
DeckPlayer::REVIEW_TITLE="<span foreground='brown'>Reviewed/Total:</span> ";

/*******************************************************************************
 *  Utilities                                                                  *
 *******************************************************************************
 */
static int ustring_to_int (const ustring& s_)
{
	return (::atoi (s_.c_str ()));
}


/*******************************************************************************
 *                      SideFlipControl Member Functions                       *
 *******************************************************************************
 */
SideFlipControl::
SideFlipControl (DeckPlayer& parent_)
	: m_parent (parent_)
{
	m_flip_button.set_flags  (Gtk::CAN_FOCUS);
	m_flip_button.set_relief (Gtk::RELIEF_NORMAL);

	m_flip_button.set_label  ("F");
    m_side = FRONT;

	m_flip_button.signal_clicked ().connect (
		mem_fun (*this, &SideFlipControl::on_flip_button_clicked));

	add (m_flip_button);
}

void
SideFlipControl::
set_active (SideSelection s_)
{
	if (s_ != m_side) {
		on_flip_button_clicked ();
	}
}

void
SideFlipControl::
on_flip_button_clicked ()
{
	if (m_side == FRONT) {
		m_flip_button.set_label ("B");
		m_parent.on_radio_selected (BACK);
		m_side = BACK;
	}
	else {
		m_flip_button.set_label ("F");
		m_parent.on_radio_selected (FRONT);
		m_side = FRONT;
	}
}

/*******************************************************************************
 *                         DeckPlayer Member Functions                         *
 *******************************************************************************
 */
Gtk::StockID DeckPlayer::SAY_IT    ("say-it" );
Gtk::StockID DeckPlayer::THUMB_0   ("thumb-0");
Gtk::StockID DeckPlayer::THUMB_1   ("thumb-1");
Gtk::StockID DeckPlayer::THUMB_2   ("thumb-2");
Gtk::StockID DeckPlayer::THUMB_3   ("thumb-3");
Gtk::StockID DeckPlayer::THUMB_4   ("thumb-4");
Gtk::StockID DeckPlayer::THUMB_5   ("thumb-5");

DeckPlayer::
DeckPlayer (VDeck& vdeck_) 
	:
    m_close_button         (manage (new ButtonWithImageLabel (
										_("<u>C</u>lose"), Gtk::Stock::CLOSE))),
    m_selected_side        (FRONT),
    m_notebook             (manage (new Gtk::Notebook)),
    m_play_progress        (manage (new Gtk::ProgressBar)),
	m_front_image          (manage (new Gtk::Image)),
	m_example_image        (manage (new Gtk::Image)),
    m_question_box_width   (0),
    m_answer_box_width     (0),
	m_verify_control       (manage (new VerifyControl (*this))),
    m_deck                 (vdeck_),
    m_deck_iter            (m_deck.end ()),         /* empty deck */
    m_is_lesson_learned    (false),
    m_is_real_deck         (true),
	m_tab_activated        (false),
	m_initialized          (false),
	m_answers_toolbar      (NULL),
	m_bottom_ctrl_toolbar  (NULL),
	m_ctrl_sidebar         (NULL),
	m_ctrl_sidebar_vbox    (NULL),
	m_front_scrollw        (NULL),
	m_back_scrollw         (NULL),
	m_vc_toolbar_button    (NULL),
	m_stats_toolbar_button (NULL),
	m_sbar_toolbar_button  (NULL),
	m_fullscreen_mode      (false)
{
    trace_with_mask("DeckPlayer::DeckPlayer", GUITRACE);

    set_title ("DeckPlayer");

	/**
	 * Outer and inner enclosure
	 */
	m_enclosure_hbox = manage (new Gtk::HBox (false, 0));
	add (*m_enclosure_hbox);

    m_vbox = manage (new Gtk::VBox (false, 0));

#ifdef IS_HILDON
	/** 
	 * hildon-specific
	 */
#elif IS_PDA
    /** 
	 *  Make DeckPlayer expand to fill the whole screen. The screen size
	 *  is 240x320, but oddly enough, the WM still leave a few pixels
	 *  around. BTW, set_size_request() doesn't work - it makes the dialog
	 *  shorter then the MainWindow by the order of the titlebar height.
	 *  And, stay away from setting transient on parent.
	 */
	Gdk::Geometry dp_geometry =	{ 240, 320,	240, 320, 240, 320, -1, -1, -1, -1};
	set_geometry_hints (*this, dp_geometry, 
						Gdk::HINT_MIN_SIZE | 
						Gdk::HINT_MAX_SIZE | 
						Gdk::HINT_BASE_SIZE);

#else  // Desktop

	/** For desktop version we can resize and 
	 *  move the window around freely.
	 */	
	set_modal (true);
	set_transient_for (*MAINWIN);
    set_resizable (true);

	Gdk::Rectangle geom = CONFIG->get_deck_player_geometry ();
	DL ((GRAPP,"Setting size to width=%d, height=%d\n",
		 geom.get_width (), geom.get_height ()));

    /** 
	 *  For desktop only, we optionally reinforce the card's geometry
	 *  ratio as setup in Preferences->Geometry:
	 *
	 *  "The other useful fields are the min_aspect and max_aspect fields; 
	 *   these contain a width/height ratio as a floating point number. 
	 *   If a geometry widget is set, the aspect applies to the geometry 
	 *   widget rather than the entire window. The most common use of these 
	 *   hints is probably to set min_aspect and max_aspect to the same value, 
	 *   thus forcing the window to keep a constant aspect ratio. 
	 */
	if (CONFIG->keep_deck_player_aspect ()) 
		{
			Gdk::Rectangle& rec = CONFIG->get_deck_player_aspect ();
			gdouble aspect = rec.get_width () * 1.0 / rec.get_height ();
			DL ((GRAPP,"Enforce aspect ratio of %5.2f\n", aspect));

			Gdk::Geometry answer_box_geometry =  { 
			geom.get_width(),/*min_width*/ geom.get_height (), /* min_height  */
			10000,	         /* max_width; */	10000,	       /* max_height  */
			-1,	             /* base_width */	-1,	           /* base_height */
			-1,	             /* width_inc  */	-1,	           /* height_inc  */
			aspect,	         /* min_aspect (width/height) */
			aspect	         /* max_aspect (width/height) */
			};
			set_geometry_hints (*this, answer_box_geometry, 
								Gdk::HINT_ASPECT | Gdk::HINT_MIN_SIZE);
		}
	else {
		Gdk::Geometry answer_box_geometry =  { 
			geom.get_width(),/*min_width*/ geom.get_height (), /* min_height  */
			10000,	         /* max_width; */	10000,	       /* max_height  */
			-1,	             /* base_width */	-1,	           /* base_height */
			-1,	             /* width_inc  */	-1,	           /* height_inc  */
			0,	             /* min_aspect (width/height) */
			0	             /* max_aspect (width/height) */
		};
		set_geometry_hints (*this, answer_box_geometry, Gdk::HINT_MIN_SIZE);
	}
#endif	// !(IS_PDA)

    /** Are we dealing with CardDeck?
     */
    if (dynamic_cast<Deck*>(&m_deck) == NULL) {
		m_is_real_deck = false;
		DL((GRAPP,"Deck isn't REAL!\n"));
    }
    else {
		DL((GRAPP,"Yep, this IS a real deck\n"));
    }

	/** Add button accelerators
	 */
	m_close_button->set_alt_accelerator_key      (GDK_c, get_accel_group ());

	/** Attach to notebook page switching signal to clear selection.
	 */
	m_notebook->signal_switch_page ().connect (
		mem_fun (*this, &DeckPlayer::on_notebook_page_flipped));

    /** HBox holds both the notebook and controls/info side panel
     */
    Gtk::HBox* nb_box = manage (new Gtk::HBox (false, 2));
    nb_box->pack_start (*m_notebook, Gtk::PACK_EXPAND_WIDGET, 2);

    /********************
	 * Add the notebook *
	 ********************
     */
    m_notebook->set_tab_pos     (Gtk::POS_BOTTOM);
    m_notebook->set_show_border (true);

    /***************************************************************************
	 *                    Fill up notebook with elements                       *
	 *                                                                         *
	 * COLORS: We defeat the theme settings and set our own colors             *
	 *         according to the configuration file. The foreground color       *
	 *         can be set on the Gtk::Label itself via modify_fg(Gdk::Color).  *
	 *         The background color, however, must be set on the enclosing     *
	 *         container via modify_bg(Gdk::Color). But strangely enough,      *
	 *         if you put Gtk::Label into Gtk::ScrolledWindow which is a       *
	 *         Gtk::Container, it won't work! You need a Gtk::EventBox         *
	 *         sandwitched inbetween (???). Gtk::ScrolledWindow takes its      *
	 *         background color hint from the theme style, period. BTW, same   *
	 *         goes for Gtk::Box as well.                                      *
	 *                                                                         *
	 * For more, read                                                          *
	 *         <http://ometer.com/gtk-colors.html> - Gtk Colors Mini HOWTO     *
	 *                                                                         *
	 ***************************************************************************
     */

	/***************************************************************************
	 * Front of the card composition                                           *
	 *                                                                         *
	 * Question Label:                                                         *
	 * ==============                                                          *
	 *         Notebook                                                        *
	 *           +--Page                                                       *
	 *                +--ScrolledWindow                                        *
	 *                     +--EventBox          <- modify_bg                   *
	 *                          +--VBox                                        *
	 *                               +--Label   <- modify_fg                   *
	 *                               +--Image   (optional)                     *
	 *                                                                         *
     ***************************************************************************
	 */
    m_question_box = manage (new Gtk::VBox (false, ANSWER_BOX_SPACING));
	m_question_box->set_border_width (ANSWER_CHILD_SPACING); 

	m_front.set_alignment  (x_alignment_enum (FRONT),
						    y_alignment_enum (FRONT));
	m_front.set_padding    (x_padding_val (FRONT),
						    y_padding_val (FRONT));
	m_front.set_justify    (justification_enum (FRONT));
    m_front.set_use_markup ();
	m_front.set_line_wrap  ();

	if (image_valignment (FRONT) == TA_IMG_POSITION_TOP) {
		m_question_box->pack_start (*m_front_image, Gtk::PACK_SHRINK, 6);
		m_question_box->pack_start (m_front);
	}
	else {
		m_question_box->pack_start (m_front);
		m_question_box->pack_start (*m_front_image, Gtk::PACK_SHRINK, 6);
	}

 	m_question_evbox = Gtk::manage (new Gtk::EventBox);
	m_question_evbox->add (*m_question_box);

	m_front_scrollw = Gtk::manage (new Gtk::ScrolledWindow);
	m_front_scrollw->set_shadow_type (Gtk::SHADOW_NONE);

#ifdef GLIBMM_PROPERTIES_ENABLED
	m_front_scrollw->
		property_window_placement ().set_value (Gtk::CORNER_TOP_LEFT);
#else
	m_front_scrollw->
		set_property ("window_placement", Gtk::CORNER_TOP_LEFT);
#endif

	m_front_scrollw->set_policy (Gtk::POLICY_NEVER , Gtk::POLICY_AUTOMATIC);
	m_front_scrollw->add (*m_question_evbox);

    m_notebook->pages ().push_back (
		Gtk::Notebook_Helpers::TabElem (*m_front_scrollw, _("Front")));

	/***************************************************************************
	 * Back of the card composition:                                           *
	 * ============================                                            *
	 *  Notebook                                                               *
	 *    +--Page                                                              *
	 *         +--ScrolledWindow                                               *
	 *              +--Viewport                               <- modify_bg     *
	 *                     +--VBox (m_answer_box)                              *
	 *                          +--Label (m_back)             <- modify_fg     *
	 *                          +--EventBox                   <- modify_bg     *
	 *                               +--HSeparator                             *
	 *                          +--Label (m_example)          <- modify_fg     *
	 *                                                                         *
     ***************************************************************************
	 */
    m_answer_box = manage (new Gtk::VBox (false, ANSWER_BOX_SPACING));
	m_answer_box->set_border_width (ANSWER_CHILD_SPACING); 

    /* -Back (the answer)- 
	 */
	m_back.set_alignment  (x_alignment_enum (BACK), y_alignment_enum (BACK));
	m_back.set_padding    (x_padding_val (BACK), y_padding_val (BACK));
	m_back.set_justify    (justification_enum (BACK));
    m_back.set_use_markup ();
    m_back.set_line_wrap  ();

    /* -Separator- 
	 */
    Gtk::HSeparator* separator = manage (new Gtk::HSeparator);

 	m_sep_evbox = Gtk::manage (new Gtk::EventBox);
	m_sep_evbox->add (*separator);

    /* -Example- 
	 */
	m_example.set_alignment  (x_alignment_enum (EXAMPLE),
							  y_alignment_enum (EXAMPLE));
	m_example.set_padding    (x_padding_val (EXAMPLE),
							  y_padding_val (EXAMPLE));
	m_example.set_justify    (justification_enum (EXAMPLE));
    m_example.set_use_markup ();
    m_example.set_line_wrap  ();

	m_answer_box->pack_start (m_back,       Gtk::PACK_SHRINK, 1);
    m_answer_box->pack_start (*m_sep_evbox, Gtk::PACK_SHRINK, 1);

	if (image_valignment (EXAMPLE) == TA_IMG_POSITION_TOP) {
		m_answer_box->pack_start (*m_example_image, Gtk::PACK_SHRINK, 6);
		m_answer_box->pack_start (m_example,        Gtk::PACK_SHRINK, 1); 
	}
	else {
		m_answer_box->pack_start (m_example,        Gtk::PACK_SHRINK, 1); 
		m_answer_box->pack_start (*m_example_image, Gtk::PACK_SHRINK, 6);
	}

    /** Don't change the order - this is part of select-on-focus
	 *  Gtk+ *feature* hunt.
	 */
	m_front.unset_flags   (Gtk::CAN_FOCUS);
	m_back.unset_flags    (Gtk::CAN_FOCUS);
	m_example.unset_flags (Gtk::CAN_FOCUS);

	m_front.show   ();
	m_back.show    ();
	m_example.show ();

#if !defined(IS_HILDON)
    m_front.set_selectable   ();
    m_back.set_selectable    ();
    m_example.set_selectable ();
#endif

	/** You need viewport to get background color set right. 
	 *  Without it the whole Notebook page takes its background style
	 *  from the theme, and then Answer/Example labels would have 
	 *  user-defined colors - yuk!
	 */
	m_page_viewport = Gtk::manage (new Gtk::Viewport (
									   *manage (new Gtk::Adjustment(0,0,1)),
									   *manage (new Gtk::Adjustment(0,0,1))));
	m_page_viewport->set_shadow_type(Gtk::SHADOW_IN);
	m_page_viewport->add (*m_answer_box);

	/** Without horizontal AUTOMATIC policy, dialog's resizing
	 *  by the user doesn't function properly. Instead, the window
	 *  keeps growing in size every time there is more text to fit then
	 *  size allows.
	 */
	m_back_scrollw = Gtk::manage (new Gtk::ScrolledWindow);
	m_back_scrollw->set_flags (Gtk::CAN_FOCUS);
	m_back_scrollw->set_shadow_type (Gtk::SHADOW_NONE);
	m_back_scrollw->set_policy (Gtk::POLICY_NEVER , Gtk::POLICY_AUTOMATIC);

#ifdef GLIBMM_PROPERTIES_ENABLED
	m_back_scrollw->
		property_window_placement ().set_value (Gtk::CORNER_TOP_LEFT);
#else
	m_back_scrollw->set_property ("window_placement", Gtk::CORNER_TOP_LEFT);
#endif
	m_back_scrollw->add (*m_page_viewport);

    m_notebook->pages ().push_back (
		Gtk::Notebook_Helpers::TabElem (*m_back_scrollw, _("Back")));

    /**-----------------------------------------------------------------**
	 *   Load pixmaps and create all custom icons                        *
	 **-----------------------------------------------------------------**/

    Glib::RefPtr<Gtk::IconFactory> icon_factory = Gtk::IconFactory::create ();
	Glib::RefPtr<Gdk::Pixbuf> thumb_xpm;

    m_play_sound_xpm = Granule::create_from_file (GRAPPDATDIR 
												  "/pixmaps/speaker_24.png");
	add_pixmap_to_factory (DeckPlayer::SAY_IT, "Say It!",
						   m_play_sound_xpm, icon_factory);
						   
	thumb_xpm = Granule::create_from_file (GRAPPDATDIR 
										   "/pixmaps/thumb-5-up.png");
	add_pixmap_to_factory (DeckPlayer::THUMB_5, "I know it!",
						   thumb_xpm, icon_factory);

	thumb_xpm = Granule::create_from_file (GRAPPDATDIR 
										   "/pixmaps/thumb-4-hesitant.png");
	add_pixmap_to_factory (DeckPlayer::THUMB_4, "Hesitant",
						   thumb_xpm, icon_factory);

	thumb_xpm = Granule::create_from_file(GRAPPDATDIR 
										  "/pixmaps/thumb-3-difficult.png");
	add_pixmap_to_factory (DeckPlayer::THUMB_3, "Difficult",
						   thumb_xpm, icon_factory);

	thumb_xpm = Granule::create_from_file(GRAPPDATDIR 
										  "/pixmaps/thumb-2-bad-recall.png");
	add_pixmap_to_factory (DeckPlayer::THUMB_2, "No recall",
						   thumb_xpm, icon_factory);

	thumb_xpm = Granule::create_from_file(GRAPPDATDIR 
										  "/pixmaps/thumb-1-bad-partial.png");
	add_pixmap_to_factory (DeckPlayer::THUMB_1, "Partial",
						   thumb_xpm, icon_factory);

	thumb_xpm = Granule::create_from_file (GRAPPDATDIR 
										   "/pixmaps/thumb-0-down.png");
	add_pixmap_to_factory (DeckPlayer::THUMB_0, "No clue",
						   thumb_xpm, icon_factory);

    icon_factory->add_default ();

    /*********************************************************************
	 *                  Horizontal bottom_toolbar controls               *
	 *********************************************************************
     *                                                                   *
	 * Starting with Granule 1.4, the controls are spread horizontally   *
	 * across bottom toolbar. This saves me on desktop space.            *
     *                                                                   *
	 *********************************************************************
     */
    Gtk::HBox* bottom_toolbar = manage (new Gtk::HBox (false, 3));

	/**
	 * Radio buttons
	 *   o Ask front
	 *   o Ask back
	 */
	m_sf_control = manage (new SideFlipControl (*this));

	/** Add Test Line selection spin button
	 *    Test line: [ 1 ]
	 */
	m_test_line_entry = manage (new TestLineEntry ());
	m_verify_control->attach_testline_entry (m_test_line_entry);
	m_verify_control->set_focus_on_entry ();

	/** Fetch out the side history and adjust accordingly
	 */
	m_selected_side = vdeck_.get_side_selection ();
	m_sf_control->set_active (m_selected_side);
	m_test_line_entry->set_sensitive (true);

    /************************************************************************** 
	 * bottom_toolbar buttons (new horizontal starting with v.1.4)            *
	 * ===========================================================            *
	 *                                                                        *
	 * [Spk] [F][<1>][[v]][=>] [:5][:4][:3][:2][:1][:0]  V/T (*) [Close]      *
	 *                |  \____________                                        *
	 *                |--------------|                                        *
	 *                | [I]          |                                        *
     *                | [A]          |                                        *
	 *                |--------------|                                        *
	 *                | [Shuffle]    |                                        *
	 *                | [Edit Deck]  |                                        *
	 *                | [Edit Card]  |                                        *
	 *                |--------------|                                        *
	 *                | {Nav. Ctrls} |                                        *
	 *                ----------------                                        *
	 *                                                                        *
	 *    [F]  - ask {Front,Back} control button                              *
	 *    <1>  - verify line # button                                         *
	 *    [I]  - hide/show Statistics Info toolbar                            *
	 *    [A]  - hide/show VerifyControl toolbar                              *
	 *    V/T  - cards Reviewed/Total                                         *
	 *                                                                        *
     **************************************************************************/

	m_play_sound_button = 
		manage (new ButtonWithImageLabel ("", DeckPlayer::SAY_IT));

	bottom_toolbar->pack_start (*m_play_sound_button, Gtk::PACK_SHRINK);
    bottom_toolbar->pack_start (*m_sf_control,        Gtk::PACK_SHRINK);
    bottom_toolbar->pack_start (*m_test_line_entry,   Gtk::PACK_SHRINK);

    m_play_sound_button->signal_clicked ().connect (
		mem_fun (*this, &DeckPlayer::on_play_sound_clicked));

	/**=========================================================================
	 *  Display statistics and progress in a separate hbox:
	 *
	 *  Success ratio: 67%       Right/Wrong: 6/10    Reviewed/Total: 1/138
	 *  --------------------------------------------------------------------
	 *  (m_success_ratio_label) (m_right_wrong_label) (m_count_label)
	 *           
	 *   [XXXXX_________]    (light / m_play_progress)
	 *
	 *==========================================================================
	 */
	m_stats_info_hbox = manage (new Gtk::HBox (false, 3));

	m_success_ratio_label.set_alignment  (Gtk::ALIGN_LEFT, Gtk::ALIGN_CENTER);
	m_success_ratio_label.set_padding    (0, 0);
	m_success_ratio_label.set_use_markup ();
    m_success_ratio_label.set_justify    (Gtk::JUSTIFY_LEFT);

	m_right_wrong_label.set_alignment    (Gtk::ALIGN_LEFT, Gtk::ALIGN_CENTER);
	m_right_wrong_label.set_padding      (0, 0);
	m_right_wrong_label.set_use_markup   ();
    m_right_wrong_label.set_justify      (Gtk::JUSTIFY_LEFT);

	m_count_label.set_alignment  (Gtk::ALIGN_LEFT, Gtk::ALIGN_CENTER);
	m_count_label.set_padding    (0, 0);
	m_count_label.set_use_markup ();
    m_count_label.set_justify    (Gtk::JUSTIFY_FILL);

	m_count_label_bare.set_alignment  (Gtk::ALIGN_LEFT, Gtk::ALIGN_CENTER);
	m_count_label_bare.set_padding    (0, 0);
	m_count_label_bare.set_use_markup ();
    m_count_label_bare.set_justify    (Gtk::JUSTIFY_FILL);

	m_sched_grey = Granule::create_from_file(
		Granule::find_pixmap_path ("sched_grey.png"));

	m_exp_light = manage (new Gtk::Image);
	m_exp_light->set_alignment (0.5, 0.5);
	m_exp_light->set_padding   (0, 2);
	m_exp_light->set (m_sched_grey);

	Gtk::HBox* progress_hbox = manage (new Gtk::HBox (false, 0));
	progress_hbox->pack_start (*m_play_progress, Gtk::PACK_SHRINK);

	m_stats_info_hbox->pack_end (*progress_hbox,        Gtk::PACK_SHRINK, 3);
	m_stats_info_hbox->pack_end (m_count_label,         Gtk::PACK_SHRINK, 3);
	m_stats_info_hbox->pack_end (m_right_wrong_label,   Gtk::PACK_SHRINK, 3);
	m_stats_info_hbox->pack_end (m_success_ratio_label, Gtk::PACK_SHRINK, 3);

	get_vbox ()->set_homogeneous (false);
	get_vbox ()->set_spacing (0);
	get_vbox ()->pack_start (*nb_box);

    nb_box->show_all ();

    m_actgroup_extra = Gtk::ActionGroup::create ("ExtraControls");

	/*---------------------------------------------------------------------*/
	/*                          Show/Hide Actions                          */
	/*---------------------------------------------------------------------*/
    m_actgroup_extra->
		add (Gtk::ToggleAction::create ("ShowVCToolbar",  
										Gtk::Stock::JUMP_TO, 
										_("Show Answer Toolbar")),
			 Gtk::AccelKey (GDK_e, Gdk::MOD1_MASK),
			 mem_fun (*this, 
					  &DeckPlayer::on_verify_control_checkbutton_clicked));

    m_actgroup_extra->
		add (Gtk::ToggleAction::create ("ShowStatsToolbar",  
										Gtk::Stock::INFO, 
										_("Show Stats Toolbar")),
			 Gtk::AccelKey (GDK_t, Gdk::MOD1_MASK),
			 mem_fun (*this, 
					  &DeckPlayer::on_stats_info_checkbutton_clicked));

    m_actgroup_extra->
		add (Gtk::ToggleAction::create ("ShowSidebar",  
										Gtk::Stock::INFO, 
										_("Show FullScreen Sidebar")),
			 Gtk::AccelKey (GDK_b, Gdk::MOD1_MASK),
			 mem_fun (*this, 
					  &DeckPlayer::on_sidebar_checkbutton_clicked));

    m_actgroup_extra->
		add (Gtk::Action::create ("FlipSidebar",  
								  Gtk::Stock::REDO, 
								  _("Flip FullScreen Sidebar")),
			 Gtk::AccelKey (GDK_f, Gdk::MOD1_MASK),
			 mem_fun (*this, 
					  &DeckPlayer::change_sidebar_location));

    m_actgroup_extra->
		add (Gtk::ToggleAction::create ("GoFullscreen",  
								  Gtk::Stock::FULLSCREEN, 
								  _("Resize to fullscreen")),
			 Gtk::AccelKey (GDK_l, Gdk::MOD1_MASK),
			 mem_fun (*this, 
					  &DeckPlayer::go_to_fullscreen));

	/*---------------------------------------------------------------------*/
	/*                     Cards-related Actions                           */
	/*---------------------------------------------------------------------*/
    m_actgroup_extra->
		add (Gtk::Action::create ("Shuffle",  
								  Gtk::Stock::REFRESH, 
								  _("Shuffle Cards")),
			 Gtk::AccelKey (GDK_s, Gdk::MOD1_MASK),
			 mem_fun (*this, &DeckPlayer::on_shuffle_clicked));

    m_actgroup_extra->
		add (Gtk::Action::create ("EditDeck", 
								  Gtk::Stock::DND_MULTIPLE,
								  _("Edit Deck")),
			 Gtk::AccelKey (GDK_d, Gdk::MOD1_MASK),
			 mem_fun (*this, &DeckPlayer::on_edit_clicked));

    m_actgroup_extra->
		add (Gtk::Action::create ("EditCard", 
								  Gtk::Stock::DND,
								  _("Edit Card")),
			 Gtk::AccelKey (GDK_k, Gdk::MOD1_MASK),
			 bind<bool>(mem_fun (*this, &DeckPlayer::on_vcard_edit_clicked), 
						false));

    m_actgroup_extra->
		add (Gtk::Action::create ("SayIt", 
								  DeckPlayer::SAY_IT,
								  _("Play Sound")),
			 mem_fun (*this, &DeckPlayer::on_play_sound_clicked));

    m_actgroup_extra->
		add (Gtk::Action::create ("Flip", 
								  Gtk::Stock::REDO,
								  _("Flip Card")),
			 mem_fun (*this, &DeckPlayer::flip_card));

    m_actgroup_extra->
		add (Gtk::Action::create ("ScrollUp", 
								  Gtk::Stock::GOTO_TOP,
								  _("ScrollUp")),
			 mem_fun (*this, &DeckPlayer::scroll_up));

    m_actgroup_extra->
		add (Gtk::Action::create ("ScrollDown", 
								  Gtk::Stock::GOTO_BOTTOM,
								  _("ScrollDown")),
			 mem_fun (*this, &DeckPlayer::scroll_down));

    m_actgroup_extra->
		add (Gtk::Action::create ("ChangeLocation", 
								  Gtk::Stock::MEDIA_FORWARD,
								  _("Change Location")),
			 mem_fun (*this, &DeckPlayer::change_sidebar_location));

	/*---------------------------------------------------------------------*/
	/*                      Movements Actions                              */
	/*---------------------------------------------------------------------*/
    m_actgroup_extra->
		add (Gtk::Action::create ("GoFirst", 
								  Gtk::Stock::GOTO_FIRST),
			 mem_fun (*this, &DeckPlayer::go_first_cb));

    m_actgroup_extra->
		add (Gtk::Action::create ("GoLast", 
								  Gtk::Stock::GOTO_LAST),
			 mem_fun (*this, &DeckPlayer::go_last_cb));

    m_actgroup_extra->
		add (Gtk::Action::create ("GoBack", 
								  Gtk::Stock::GO_BACK),
			 mem_fun (*this, &DeckPlayer::go_prev_cb));


    m_actgroup = Gtk::ActionGroup::create ("Controls");

	/**---------------------------------------------------------------------**
	 *                    Visible Quality-of-Answer Actions                  *
	 **---------------------------------------------------------------------**/
    m_actgroup->add (Gtk::Action::create ("GoNext", 
										  Gtk::Stock::GO_FORWARD,
										  _("Go next card"),
										  _("Show next card")),
					 mem_fun (*this, &DeckPlayer::go_next_cb));

	/**--------------------------------------------------------------------**
	 *                Icons that are placed on the button toolbar           *
	 **--------------------------------------------------------------------**/
    m_actgroup->add (Gtk::Action::create ("IKnowIt", 
										  DeckPlayer::THUMB_5,
										  _("I know it"),
										  _("Perfect response")),
					 bind<AnswerQuality>(mem_fun (*this,
												&DeckPlayer::process_answer_cb),
										 ANSWER_OK));

    m_actgroup->add (Gtk::Action::create ("Hesitant", 
										  DeckPlayer::THUMB_4,
										  _("Hesitant response"),
										  _("OK with hesitation")),
					 bind<AnswerQuality>(mem_fun (*this,
												&DeckPlayer::process_answer_cb),
										 ANSWER_OK_HESITANT));

    m_actgroup->add (Gtk::Action::create ("Difficult", 
										  DeckPlayer::THUMB_3,
										  _("Difficult response"),
										  _("OK with difficulty")),
					 bind<AnswerQuality>(mem_fun (*this,
											 &DeckPlayer::process_answer_cb),
										 ANSWER_OK_DIFFICULT));

    m_actgroup->add (Gtk::Action::create ("NoRecall", 
										  DeckPlayer::THUMB_2,
										  _("Easy recall"),
										  _("No, but easily recalled")),
					 bind<AnswerQuality>(mem_fun (*this,
												&DeckPlayer::process_answer_cb),
										 ANSWER_BAD_RECALL));

    m_actgroup->add (Gtk::Action::create ("Partial", 
										  DeckPlayer::THUMB_1,
										  _("Partial recall"),
										  _("No, but can recall")),
					 bind<AnswerQuality>(mem_fun (*this,
												&DeckPlayer::process_answer_cb),
										 ANSWER_BAD_PARTIAL));

    m_actgroup->add (Gtk::Action::create ("NoClue", 
										  DeckPlayer::THUMB_0,
										  _("No clue"),
										  _("Complete blackout")),
					 bind<AnswerQuality>(mem_fun (*this,
												&DeckPlayer::process_answer_cb),
										 ANSWER_BAD_BLACKOUT));

	/**--------------------------------------------------------------------**
	 *                     Create toolbars                                  *
	 **--------------------------------------------------------------------**/
    m_uimanager = Gtk::UIManager::create ();

    m_uimanager->insert_action_group (m_actgroup_extra);
    m_uimanager->insert_action_group (m_actgroup);

#if !defined(IS_HILDON)
    add_accel_group (m_uimanager->get_accel_group ());
#endif

    try {
		Glib::ustring ui_info =
			"<ui>"
			"  <toolbar name='ExtraControls'>"
			"     <toolitem action='ShowVCToolbar'/>"
			"     <toolitem action='ShowStatsToolbar'/>"
			"     <separator name='10'/>"
			"     <toolitem action='GoFullscreen'/>"
			"     <toolitem action='ShowSidebar'/>"
			"     <toolitem action='FlipSidebar'/>"
			"     <separator name='11'/>"
			"     <toolitem action='Shuffle'/>"
			"     <toolitem action='EditDeck'/>"
			"     <toolitem action='EditCard'/>"
			"     <separator name='12'/>"
			"     <toolitem action='GoFirst'/>"
			"     <toolitem action='GoLast'/>"
			"     <toolitem action='GoBack'/>"
			"  </toolbar>"

			"  <toolbar name='Controls'>"
			"     <separator name='20'/>"
			"     <toolitem action='NoClue'/>"
			"     <toolitem action='Partial'/>"
			"     <toolitem action='NoRecall'/>"
			"     <toolitem action='Difficult'/>"
			"     <separator name='21'/>"
			"     <toolitem action='Hesitant'/>"
			"     <toolitem action='IKnowIt'/>"
			"     <separator name='22'/>"
			"     <toolitem action='GoNext'/>"
			"  </toolbar>"
			"</ui>";

#ifdef GLIBMM_EXCEPTIONS_ENABLED
		m_uimanager->add_ui_from_string (ui_info);
#else
		std::auto_ptr<Glib::Error> err;
		m_uimanager->add_ui_from_string (ui_info, err);
#endif

    }
    catch (const Glib::Error& ex_) {
		DL((GRAPP,"Building menues failed (%s)\n", ex_.what ().c_str ()));
    }

	m_vc_toolbar_button = dynamic_cast<Gtk::ToggleToolButton*> (
		m_uimanager->get_widget("/ExtraControls/ShowVCToolbar"));
	Assure_exit (m_vc_toolbar_button);

	m_stats_toolbar_button = dynamic_cast<Gtk::ToggleToolButton*> (
		m_uimanager->get_widget("/ExtraControls/ShowStatsToolbar"));
	Assure_exit (m_stats_toolbar_button);

	m_sbar_toolbar_button = dynamic_cast<Gtk::ToggleToolButton*> (
		m_uimanager->get_widget("/ExtraControls/ShowSidebar"));
	Assure_exit (m_sbar_toolbar_button);

	m_shuffle_button    = m_uimanager->get_widget ("/ExtraControls/Shuffle");
	m_edit_button       = m_uimanager->get_widget ("/ExtraControls/EditDeck");
	m_vcard_edit_button = m_uimanager->get_widget ("/ExtraControls/EditCard");
		
    m_toolbar_go_first = m_uimanager->get_widget ("/ExtraControls/GoFirst");
    m_toolbar_go_last  = m_uimanager->get_widget ("/ExtraControls/GoLast");
    m_toolbar_go_prev  = m_uimanager->get_widget ("/ExtraControls/GoBack");

    m_toolbar_go_next  = m_uimanager->get_widget ("/Controls/GoNext");

	m_toolbar_answers_ok.push_back (
		m_uimanager->get_widget("/Controls/IKnowIt"  ));
	m_toolbar_answers_ok.push_back (
		m_uimanager->get_widget("/Controls/Hesitant" ));

	m_toolbar_answers_bad.push_back (
		m_uimanager->get_widget("/Controls/Difficult"));
	m_toolbar_answers_bad.push_back (
		m_uimanager->get_widget("/Controls/NoRecall" ));
	m_toolbar_answers_bad.push_back (
		m_uimanager->get_widget("/Controls/Partial"  ));
	m_toolbar_answers_bad.push_back (
		m_uimanager->get_widget("/Controls/NoClue"   ));

	/**
	 *  Add input verification control
	 */
	Gtk::HSeparator* hsep = manage (new Gtk::HSeparator);
	get_vbox ()->pack_start (*hsep, Gtk::PACK_SHRINK);
	get_vbox ()->pack_start (*m_verify_control, Gtk::PACK_SHRINK);

	m_verify_control->show_all ();

	/** 
	 * Add Statistics information bar
	 */
	get_vbox ()->pack_start (*m_stats_info_hbox, Gtk::PACK_SHRINK, 4);
	m_stats_info_hbox->show_all ();

    /**------------------------------------------------------------------**
	 * Pack and add controls toolbar and <Close> button                   *
     **------------------------------------------------------------------**/
    Gtk::Widget* controls = m_uimanager->get_widget ("/ExtraControls");

    m_bottom_ctrl_toolbar = dynamic_cast<Gtk::Toolbar*> (controls);
    m_bottom_ctrl_toolbar->set_orientation   (Gtk::ORIENTATION_HORIZONTAL);
    m_bottom_ctrl_toolbar->set_toolbar_style (Gtk::TOOLBAR_ICONS);
    m_bottom_ctrl_toolbar->set_tooltips      (true);

    m_extra_controls_hbox = manage (new Gtk::HBox);
    m_extra_controls_hbox->pack_start (*m_bottom_ctrl_toolbar,
									   Gtk::PACK_SHRINK);

	/**------------------------------------------------------------------** 
	 * Create a vertical bar with huge (72x72) touch screen buttons       *
	 * suitable for full-screen mode for portables (N8x0).                *
	 **------------------------------------------------------------------**/
	Gtk::Button* nav_button;

    m_ctrl_sidebar_vbox = manage (new Gtk::VBox (false, 2));

	/** Play Sound */
    nav_button = make_button_with_image ("/pixmaps/speaker_72x72.png");
	nav_button->signal_clicked ().connect (
		mem_fun (*this, &DeckPlayer::on_play_sound_clicked));
	m_ctrl_sidebar_vbox->pack_start (*nav_button, Gtk::PACK_SHRINK, 2);

	/** Flip */
    nav_button = make_button_with_image ("/pixmaps/flip-card_72x72.png");
	nav_button->signal_clicked ().connect (
		mem_fun (*this, &DeckPlayer::flip_card));
	m_ctrl_sidebar_vbox->pack_start (*nav_button, Gtk::PACK_SHRINK, 2);

	/** Next Card */
	if (real_deck ()) {
		nav_button = make_button_with_image ("/pixmaps/next-card_72x72.png");
		nav_button->signal_clicked ().connect (
			mem_fun (*this, &DeckPlayer::go_next_cb));
		m_ctrl_sidebar_vbox->pack_start (*nav_button, Gtk::PACK_SHRINK, 2);
	}

	/** Asnwers Good/Bad */
	if (! real_deck ()) 
	{
		nav_button = 
			make_button_with_image ("/pixmaps/thumb-4-hesitant_72x72.png");
		nav_button->signal_clicked ().connect (
			bind<AnswerQuality>(mem_fun (*this,
										 &DeckPlayer::process_answer_cb),
								ANSWER_OK_HESITANT));
		m_ctrl_sidebar_vbox->pack_start (*nav_button, Gtk::PACK_SHRINK, 2);
		m_sidebar_answers_ok.push_back (nav_button);

		nav_button = 
			make_button_with_image ("/pixmaps/thumb-0-down_72x72.png");
		nav_button->signal_clicked ().connect (
			bind<AnswerQuality>(mem_fun (*this,
										 &DeckPlayer::process_answer_cb),
								ANSWER_BAD_BLACKOUT));
		m_ctrl_sidebar_vbox->pack_start (*nav_button, Gtk::PACK_SHRINK, 2);
		m_sidebar_answers_bad.push_back (nav_button);
	}

	/** Scroll UP */
    nav_button = make_button_with_image ("/pixmaps/scroll-up_72x72.png");
	nav_button->signal_clicked ().connect (
		mem_fun (*this, &DeckPlayer::scroll_up));
	m_ctrl_sidebar_vbox->pack_start (*nav_button, Gtk::PACK_SHRINK, 2);

	/** Scroll Down */
    nav_button = make_button_with_image ("/pixmaps/scroll-down_72x72.png");
	nav_button->signal_clicked ().connect (
		mem_fun (*this, &DeckPlayer::scroll_down));
	m_ctrl_sidebar_vbox->pack_start (*nav_button, Gtk::PACK_SHRINK, 2);

	/**------------------------------------------------------------------**/

	if (CONFIG->sidebar_location () == SIDEBAR_LEFT) 
	{
		get_enclosure ()->pack_start (*m_ctrl_sidebar_vbox, Gtk::PACK_SHRINK);
		get_enclosure ()->pack_start (*get_vbox (), Gtk::PACK_EXPAND_WIDGET);
	}
	else {
		get_enclosure ()->pack_start (*get_vbox (), Gtk::PACK_EXPAND_WIDGET);
		get_enclosure ()->pack_start (*m_ctrl_sidebar_vbox, Gtk::PACK_SHRINK);
	}

	/**------------------------------------------------------------------**
	 * And Controls group                                                 *
	 **------------------------------------------------------------------**/
    controls = m_uimanager->get_widget ("/Controls");

    m_answers_toolbar = dynamic_cast<Gtk::Toolbar*> (controls);
    m_answers_toolbar->set_orientation   (Gtk::ORIENTATION_HORIZONTAL);
    m_answers_toolbar->set_toolbar_style (Gtk::TOOLBAR_ICONS);
    m_answers_toolbar->set_tooltips      (true);

    m_navigation_controls_hbox = manage (new Gtk::HBox);
    m_navigation_controls_hbox->pack_start (*m_answers_toolbar,
											Gtk::PACK_EXPAND_WIDGET);

	m_nav_ctrl_evbox = manage (new Gtk::EventBox ());
	m_nav_ctrl_evbox->add (*m_navigation_controls_hbox);

	bottom_toolbar->pack_start (*m_extra_controls_hbox, Gtk::PACK_SHRINK);
    bottom_toolbar->pack_start (*m_nav_ctrl_evbox);
	bottom_toolbar->pack_start (m_count_label_bare, Gtk::PACK_SHRINK);
	bottom_toolbar->pack_start (*m_exp_light,       Gtk::PACK_SHRINK);
	bottom_toolbar->pack_end   (*m_close_button,    Gtk::PACK_SHRINK);

	get_vbox ()->pack_start (*bottom_toolbar, Gtk::PACK_SHRINK);

    m_navigation_controls_hbox->show_all ();

	/**------------------------------------------------------------------**
	 * Callbacks                                                          *
	 **------------------------------------------------------------------**/
    m_close_button->signal_clicked ().connect (
		mem_fun (*this, &DeckPlayer::on_close_clicked));

	/**
	 * Setting resize callbacks. The answer box holds 
	 * Back/Separator/Example triplet trapped in the scrollwindow.
	 */
    m_question_box->signal_size_allocate ().connect (
		mem_fun (*this, &DeckPlayer::question_box_size_allocate_cb));

    m_answer_box->signal_size_allocate ().connect (
		mem_fun (*this, &DeckPlayer::answer_box_size_allocate_cb));
	
    signal_size_allocate ().connect (
		mem_fun (*this, &DeckPlayer::size_allocate_cb));

	add_events (Gdk::BUTTON_PRESS_MASK|Gdk::EXPOSURE_MASK);

	/** 
	 *  'false' argument tells Gtk+ to call on_key_pressed() BEFORE
	 *	keypress event handling. Returning 'true' from on_key_pressed()
	 *	will stop event processing.
	 */
	if (!CONFIG->disable_key_shortcuts ()) {
		signal_key_press_event ().connect(
			mem_fun (*this, &DeckPlayer::on_key_pressed), false);
	}

 	signal_delete_event ().connect (
 		sigc::mem_fun (*this, &DeckPlayer::on_delete_window_clicked));
	
	/** Set text colors, font sizes and styles
	 */
	reset_text_colors ();
	reset_text_fonts  ();
	m_verify_control->modify_text_colors ();

	/** For Maemo (Nokia IT), reduce overall font size
	 */
	Pango::FontDescription fd ("Bitstream Vera Sans 8");

	m_shuffle_button    ->modify_font (fd);
	m_edit_button       ->modify_font (fd);
	m_vcard_edit_button ->modify_font (fd);

	/**------------------------------------------------------------------**
	 * Grand finale                                                       *
	 **------------------------------------------------------------------**/
#if !defined(IS_HILDON)
	set_icon_from_file (GRAPPDATDIR "/pixmaps/deckplayer_32x32.png");
#endif

    set_sensitivity ();
	sensitize_controls ();

    m_deck.reset_progress ();
    set_win_name ();
    show_deck ();
    switch_side ();

    show_all_children ();
	hide_sidebar ();

	m_initialized = true;
}

//==============================================================================
// DeckPlayer methods implementation.
//==============================================================================
//
gint
DeckPlayer::
run (Gtk::Window& parent_)
{
    trace_with_mask("DeckPlayer::run",GUITRACE);

#ifdef IS_HILDON
	m_verify_control->set_focus_on_entry ();
#endif

	GRANULE->move_child_to_parent_position (parent_, *this);

	adjust_count ();		
	set_controls_visibility ();	

	present ();
	gtk_main ();

	save_controls_visibility ();
	GRANULE->move_parent_to_child_position (parent_, *this);

	return (Gtk::RESPONSE_NONE);
}

bool
DeckPlayer::
on_delete_window_clicked (GdkEventAny* /* event_ */)
{
    trace_with_mask("DeckPlayer::on_delete_window_clicked",GUITRACE);

	on_close_clicked ();
	return true;
}

void
DeckPlayer::
on_close_clicked ()
{
    trace_with_mask("DeckPlayer::on_close_clicked",GUITRACE);

	gtk_main_quit ();
	//Gtk::Main::quit ();
}

void
DeckPlayer::
show_deck ()
{
    trace_with_mask("DeckPlayer::show_deck",GUITRACE);

    if (m_deck.empty ()) {
		DL((GRAPP,"The deck is empty!\n"));
		return;
    }
    reset ();
}

void
DeckPlayer::
flip_card ()
{
	m_notebook->set_current_page (m_notebook->get_current_page () ? 0 : 1);
	m_verify_control->set_focus_on_entry ();
}

void
DeckPlayer::
reset ()
{
    trace_with_mask("DeckPlayer::reset",GUITRACE);

    m_deck_iter = m_deck.begin ();
	DL ((APP,"m_deck_iter = begin()\n"));
    show_card ();
}

/**
 * Always clear all fields first. Then, try to fill them up.
 *
 * If I were given an invalid markup text, then set_markup()
 * would fail with 
 *
 *         "Gtk-WARNING **: Failed to set label from markup 
 *          due to error parsing markup"
 *
 * I'd like ultimately to catch the error and show it in a warning dialog 
 * to the user, but for now I don't see any way of doing it with GTK+.
 */
void
DeckPlayer::
show_card (bool with_adjust_count_)
{
    trace_with_mask("DeckPlayer::show_card",GUITRACE);

	Glib::ustring pic_path;
	Glib::ustring file_path;

	clear_text ();

    if (m_deck.size () > 0 || m_deck_iter != m_deck.end ())
	{
		while (!markup_is_valid ((*m_deck_iter))) 
		{
			Gtk::MessageDialog em (
				_("The card you are about to see has invalid markup.\n"
				  "To proceed further, first, fix the broken markup.\n"
				  "If unsure about the correct syntax, consult Help."), 
				Gtk::MESSAGE_ERROR);
			em.run ();
			em.hide ();
			on_vcard_edit_clicked (true);
		}

		/** Each VCard points to the Deck it belongs to.
		 */
		try {
			m_front.set_markup ((*m_deck_iter)->get_question ());

			/** Front: See if I have a picture to display.
			 */
			pic_path = (*m_deck_iter)->get_image_path ();

			if (GRANULE->test_file_exists (pic_path, 
										   (*m_deck_iter)->get_deck_path (),
										   file_path))
			{
				m_front_img_pixbuf = Granule::create_from_file (file_path);
				m_front_image->set (m_front_img_pixbuf);
				m_front_image->show ();
			}

			m_back.set_markup ((*m_deck_iter)->get_answer ());

			m_example.set_markup (markup_tildas((*m_deck_iter)->get_example()));

			/** Example: same stuff
			 */
			pic_path = (*m_deck_iter)->get_example_image_path ();

			if (GRANULE->test_file_exists (pic_path, 
										   (*m_deck_iter)->get_deck_path (),
										   file_path))
			{
				m_example_img_pixbuf = Granule::create_from_file (file_path);
				m_example_image->set (m_example_img_pixbuf);
				m_example_image->show ();
			}
			else {
				DL ((GRAPP,"File '%s' doesn't exist!!!\n", file_path.c_str ()));
			}
		}
		catch (const Glib::ConvertError& e) {
			DL ((GRAPP,"Caught exception: \"%s\"\n", e.what ().c_str()));
			switch (e.code ()) {
			case Glib::ConvertError::NO_CONVERSION: 
				DL ((GRAPP,"code: no conversion\n")); 
				break;
			case Glib::ConvertError::ILLEGAL_SEQUENCE:
				DL ((GRAPP,"code: illegal sequence\n")); 
				break;
			case Glib::ConvertError::FAILED:
				DL ((GRAPP,"code: failed\n")); 
				break;
			case Glib::ConvertError::PARTIAL_INPUT:
				DL ((GRAPP,"code: partial input\n")); 
				break;
			case Glib::ConvertError::BAD_URI:
				DL ((GRAPP,"code: bad uri\n"));
				break;
			case Glib::ConvertError::NOT_ABSOLUTE_PATH:
				DL ((GRAPP,"code: not absolute path\n")); 
				break;
			}
		}
		catch (const Glib::Error& e) {
			DL ((GRAPP,"Caught exception: \"%s\"\n",
				 e.what ().c_str()));
		}

		if ((*m_deck_iter)->get_reversed ()) {
			m_notebook->set_current_page (BACK);
		}
	}

	if (with_adjust_count_) {
		adjust_count ();
	}

#if !defined(IS_HILDON)
	/** Calling set_focus_on_entry() causes crash with Maemo 2.0 when
	 *  you open CardFile twice in the row. 
	 *  No clue - investigate later.
     */
	m_verify_control->set_focus_on_entry ();
#endif

}

void
DeckPlayer::
adjust_count ()
{
    trace_with_mask("DeckPlayer::adjust_count",GUITRACE);

	std::ostringstream os;
    string percent_msg;
	string right_wrong_msg;
    float percent_done;

    if (m_deck.empty ()) 
	{
		DL((GRAPP,"The deck is empty!\n"));
		os << RATIO_TITLE << "n/a";
		m_success_ratio_label.set_markup (os.str ());
		os.str ("");

		os << RIGHT_WRONG_TITLE << "0/0";
		m_right_wrong_label.set_markup (os.str ());
		os.str ("");

		os << REVIEW_TITLE << "0/0";
		m_count_label.set_markup (os.str ());

		os.str ("");
		os << "0/0";
		m_count_label_bare.set_markup (os.str ());
		return;
    }

    m_deck.get_progress (percent_done, 
						 percent_msg, 
						 m_deck_iter - m_deck.begin ());

    m_play_progress->set_fraction (percent_done);

	os << REVIEW_TITLE << percent_msg;
    m_count_label.set_markup (os.str ());
	os.str ("");

	os << percent_msg;
    m_count_label_bare.set_markup (os.str ());
	os.str ("");

	/** Show success ratio statistics.
	 */
	if (m_deck_iter != m_deck.end ()) 
	{
		DL ((DECK,"m_deck_iter points to valid iterator\n"));
		os << RATIO_TITLE << (*m_deck_iter)->get_success_ratio(right_wrong_msg);
		m_success_ratio_label.set_markup (os.str ());
		os.str ("");
		
		if (right_wrong_msg.length () == 0) {
			right_wrong_msg = "0/0";
		}
		os << RIGHT_WRONG_TITLE << right_wrong_msg;
		m_right_wrong_label.set_markup (os.str ());
	}
	else {
		DL ((DECK,"m_deck_iter == m_deck.end ()\n"));
	}
}

void
DeckPlayer::
set_sensitivity ()
{
    trace_with_mask("DeckPlayer::set_sensitivity", GUITRACE);

    if (real_deck ()) 
	{
		answers_iterator iter;

		iter = m_toolbar_answers_ok.begin ();
		while (iter != m_toolbar_answers_ok.end ()) {
			(*iter)->set_sensitive (false);
			iter++;
		}

		iter = m_toolbar_answers_bad.begin ();
		while (iter != m_toolbar_answers_bad.end ()) {
			(*iter)->set_sensitive (false);
			iter++;
		}

		iter = m_sidebar_answers_ok.begin ();
		while (iter != m_sidebar_answers_ok.end ()) {
			(*iter)->set_sensitive (false);
			iter++;
		}

		iter = m_sidebar_answers_bad.begin ();
		while (iter != m_sidebar_answers_bad.end ()) {
			(*iter)->set_sensitive (false);
			iter++;
		}
    }
    else 
	{
#if !defined(IS_PDA) 
		m_toolbar_go_first->set_sensitive (false);
		m_toolbar_go_last ->set_sensitive (false);
#endif
		m_toolbar_go_prev ->set_sensitive (false);
		m_toolbar_go_next ->set_sensitive (false);
    }
}

void
DeckPlayer::
enable_good_answer_buttons ()
{
	trace_with_mask("DeckPlayer::enable_good_answer_buttons", GUITRACE);

	if (! real_deck ()) {
		answers_iterator iter;
		iter = m_toolbar_answers_ok.begin ();
		while (iter != m_toolbar_answers_ok.end ()) {
			(*iter)->set_sensitive (true);
			iter++;
		}

		iter = m_sidebar_answers_ok.begin ();
		while (iter != m_sidebar_answers_ok.end ()) {
			(*iter)->set_sensitive (true);
			iter++;
		}
	}
}

void
DeckPlayer::
enable_bad_answer_buttons ()
{
	trace_with_mask("DeckPlayer::enable_bad_answer_buttons", GUITRACE);

	if (! real_deck ()) 
	{
		answers_iterator iter = m_toolbar_answers_bad.begin ();
		while (iter != m_toolbar_answers_bad.end ()) {
			(*iter)->set_sensitive (true);
			iter++;
		}

		iter = m_sidebar_answers_bad.begin ();
		while (iter != m_sidebar_answers_bad.end ()) {
			(*iter)->set_sensitive (true);
			iter++;
		}
	}
}

void
DeckPlayer::
sensitize_controls ()
{
	trace_with_mask("DeckPlayer::sensitize_controls", GUITRACE);

    if (! real_deck ()) {
		return;
	}
	DL ((GRAPP,"Deck is empty\n"));

	if (m_deck.empty ()) 
	{
		m_shuffle_button    ->set_sensitive (false);
		m_vcard_edit_button ->set_sensitive (false);
#if !defined(IS_PDA)
 		m_toolbar_go_first  ->set_sensitive (false);
 		m_toolbar_go_last   ->set_sensitive (false);
#endif
		m_toolbar_go_prev   ->set_sensitive (false);
		m_toolbar_go_next   ->set_sensitive (false);
    }
	else {
		m_shuffle_button    ->set_sensitive (true);
		m_vcard_edit_button ->set_sensitive (true);
#if !defined(IS_PDA)
 		m_toolbar_go_first  ->set_sensitive (true);
 		m_toolbar_go_last   ->set_sensitive (true);
#endif
		m_toolbar_go_prev   ->set_sensitive (true);
		m_toolbar_go_next   ->set_sensitive (true);
	}
}

void 
DeckPlayer::
enable_answer_controls ()
{
	trace_with_mask("DeckPlayer::enable_answer_controls", GUITRACE);

    if (! real_deck ()) 
	{
		answers_iterator iter;
		iter = m_toolbar_answers_ok.begin ();
		while (iter != m_toolbar_answers_ok.end ()) {
			(*iter)->set_sensitive (true);
			iter++;
		}

		iter = m_toolbar_answers_bad.begin ();
		while (iter != m_toolbar_answers_bad.end ()) {
			(*iter)->set_sensitive (true);
			iter++;
		}

		iter = m_sidebar_answers_ok.begin ();
		while (iter != m_sidebar_answers_ok.end ()) {
			(*iter)->set_sensitive (true);
			iter++;
		}

		iter = m_sidebar_answers_bad.begin ();
		while (iter != m_sidebar_answers_bad.end ()) {
			(*iter)->set_sensitive (true);
			iter++;
		}
	}
}

void 
DeckPlayer::
disable_answer_controls ()
{
	trace_with_mask("DeckPlayer::disable_answer_controls", GUITRACE);

	answers_iterator iter;
	
	iter = m_toolbar_answers_ok.begin ();
	while (iter != m_toolbar_answers_ok.end ()) {
		(*iter)->set_sensitive (false);
		iter++;
	}

	iter = m_toolbar_answers_bad.begin ();
	while (iter != m_toolbar_answers_bad.end ()) {
		(*iter)->set_sensitive (false);
		iter++;
	}

	iter = m_sidebar_answers_ok.begin ();
	while (iter != m_sidebar_answers_ok.end ()) {
		(*iter)->set_sensitive (false);
		iter++;
	}

	iter = m_sidebar_answers_bad.begin ();
	while (iter != m_sidebar_answers_bad.end ()) {
		(*iter)->set_sensitive (false);
		iter++;
	}
}

//==============================================================================
//                          Action Buttons callbacks
//==============================================================================
void
DeckPlayer::
go_last_cb ()
{
    if ((m_deck_iter + 1) != m_deck.end ()) {
		m_deck_iter = m_deck.end ();
		m_deck_iter--;
		DL ((APP,"m_deck_iter--\n"));
		show_card ();
    }

    switch_side ();
	m_verify_control->clear ();
}

void
DeckPlayer::
go_first_cb ()
{
    if (m_deck_iter != m_deck.begin ()) {
		m_deck_iter = m_deck.begin ();
		DL ((APP,"m_deck_iter = begin()\n"));
		show_card ();
    }

    switch_side ();
	m_verify_control->clear ();
}

void
DeckPlayer::
go_prev_cb ()
{
    if (m_deck_iter != m_deck.begin ()) {
		m_deck_iter--;
		DL ((APP,"m_deck_iter--\n"));
		show_card ();
    }

    switch_side ();
	m_verify_control->clear ();
}

void
DeckPlayer::
go_next_cb ()
{
    trace_with_mask("DeckPlayer::go_next_cb",GUITRACE);

    if (m_deck_iter != m_deck.end () &&	         // empty deck
		(m_deck_iter + 1) != m_deck.end ())      // at the last element
	{
	    DL((GRAPP,"Go next card=>\n"));
	    m_deck_iter++;
		DL ((APP,"m_deck_iter++\n"));
	    show_card ();
	}
    /** We flip the side only if user has gone (hopefully)
     *  through an entire Deck (for *real* Decks only).
     */
    if (real_deck ()) {
		if (m_deck_iter + 1 == m_deck.end () && !m_is_lesson_learned) {	
			DL((GRAPP,"Reached the last card!\n"));
			CONFIG->set_flipside_history (m_deck.get_name (), 
										  m_selected_side==FRONT ? BACK:FRONT);
			m_is_lesson_learned = true;
		}
    }

    switch_side ();
	m_verify_control->clear ();
}

void
DeckPlayer::
scroll_up ()
{
    trace_with_mask("DeckPlayer::scroll_up",GUITRACE);

	GdkEventKey evt_key;

	::memset (&evt_key, 0, sizeof (evt_key));
	evt_key.keyval = GDK_Page_Up;
	on_key_pressed (&evt_key);
}

void
DeckPlayer::
scroll_down ()
{
    trace_with_mask("DeckPlayer::scroll_down",GUITRACE);

	GdkEventKey evt_key;

	::memset (&evt_key, 0, sizeof (evt_key));
	evt_key.keyval = GDK_Page_Down;
	on_key_pressed (&evt_key);
}

/** 
 * This callback is activated only while learning flashcards in the
 * Card Box. We are always at the beginning of the list, moving
 * elements to the next box or in case of the last box, moving 
 * them to the back of the same list. We proceed in this manner until 
 * all cards in the box are exhausted.
 */
void
DeckPlayer::
process_answer_cb (AnswerQuality answer_quality_)
{
    trace_with_mask("DeckPlayer::process_answer_cb",GUITRACE);

    if (m_deck_iter == m_deck.end ()) {
		DL((GRAPP,"The deck is empty!\n"));
		return;
    }

    CARDBOX->move_card_to_box (m_deck_iter, 
							   m_deck.get_index (),
							   answer_quality_);
    switch_side ();
	m_verify_control->clear ();
}

void
DeckPlayer::
on_radio_selected (SideSelection selected_)
{
    trace_with_mask("DeckPlayer::on_radio_selected",GUITRACE);

    m_selected_side = selected_;
	DL ((GRAPP,"selected = %s\n",(m_selected_side == BACK ? "BACK" : "FRONT")));

	m_deck.set_side_selection (m_selected_side);
    switch_side ();
}

void
DeckPlayer::
on_play_sound_clicked ()
{
    trace_with_mask("DeckPlayer::on_play_sound_clicked",GUITRACE);

    if (m_deck_iter != m_deck.end ()) 
	{
		if (!Deck::audio_playback_active ()) {
			m_deck.play_card (SideSelection (m_notebook->get_current_page ()), 
							  m_deck_iter);
		}
		else {
			DL ((GRAPP,"Already playing an audio clip.\n"));
		}
    }
}

void
DeckPlayer::
on_shuffle_clicked ()
{
    trace_with_mask("DeckPlayer::on_shuffle_clicked",GUITRACE);

    if (real_deck ()) 
	{
		random_shuffle (m_deck.begin (), m_deck.end ());
		Gtk::MessageDialog qm (
			_("Would you like to make cards\norder changes permanent?"),
			false,
			Gtk::MESSAGE_QUESTION, 
			Gtk::BUTTONS_NONE,
			true);
        qm.add_button (Gtk::Stock::NO,  Gtk::RESPONSE_NO);
        qm.add_button (Gtk::Stock::YES, Gtk::RESPONSE_YES);

		if (qm.run () == Gtk::RESPONSE_YES) 
		{
			Deck& real_deck = dynamic_cast<Deck&>(m_deck);
			real_deck.mark_as_modified ();
		}
    }
	/** For CardDeck, shuffle all subsets.
	 */
	else {
		CardDeck& card_deck = dynamic_cast<CardDeck&>(m_deck);
		card_deck.shuffle_subsets ();
	}

	/** Refresh display, but don't adjust progress bar
	 */
    m_deck_iter = m_deck.begin ();
	DL ((APP,"m_deck_iter = begin()\n"));
    show_card (false);
}

void
DeckPlayer::
on_edit_clicked ()
{
    trace_with_mask("DeckPlayer::on_edit_clicked",GUITRACE);
    DL((GRAPP,"Bring up deck viewer dialog\n"));

	bool dv_modified = false;

	int idx = m_deck_iter == m_deck.end() ? -1 : (m_deck_iter - m_deck.begin());

	{
		DeckView dv (m_deck, idx);
		dv.run (*this);
		dv_modified = dv.is_modified ();
		if (dv.appearance_changed ()) {
			repaint ();
		}
	}

    DL((GRAPP,"Deck size after edit = %d cards (modified = %s)\n", 
		m_deck.size (), (dv_modified ? "TRUE" : "FALSE")));

	sensitize_controls ();

    /** 
     * Update viewer - iterators might have become invalid.
     */
    if (m_deck.size () == 0) 	// Last card removed - clean up
	{
		m_deck_iter = m_deck.end ();
		CARDBOX->mark_as_modified ();
		show_card ();
		on_close_clicked ();
    }
    else 
	{
		if (dv_modified) {
			DL ((APP,"The Deck has been modified - go to the beginning\n"));
			CARDBOX->mark_as_modified ();
			show_deck ();
		}
		else {
			DL ((APP,"The Deck is intact - stay where we were\n"));
			show_card (false);
		}
    }

	show ();					// Restore DeckPlayer
	set_controls_visibility ();	
}

void
DeckPlayer::
on_vcard_edit_clicked (bool disable_cancel_)
{
    trace_with_mask("DeckPlayer::on_vcard_edit_clicked",GUITRACE);

	if (m_deck_iter != m_deck.end () || m_deck.size () > 0) 
	{
		VCard* vc = *m_deck_iter;
		CardView cview (vc);

		int ret = cview.run_in_edit_mode (*this, disable_cancel_);
		set_controls_visibility ();	

		if (ret == Gtk::RESPONSE_OK) {
			show_card (false);
		}
	}
	else {
        Gtk::MessageDialog em (_("The deck is empty!"), Gtk::MESSAGE_ERROR);
        em.run ();
        return;
	}
}

void
DeckPlayer::
on_verify_clicked ()
{
    trace_with_mask("DeckPlayer::on_verify_clicked",GUITRACE);

	if (m_verify_control->get_state () == VC_ARMED) 
	{
		m_verify_control->compare (get_answer ());
		if (m_verify_control->matched ()) {
			enable_good_answer_buttons ();
		}
		else {
			enable_bad_answer_buttons ();
		}
		auto_pronounce_card (BACK);
	}
	else if (m_verify_control->get_state () == VC_VALIDATED) 
	{
		if (real_deck ()) {
			go_next_cb ();
		}
		else { /* CardBox Player*/ 
			/**
			 *  TODO: This is a thorny point - I need to let the user
			 *        adjust quality of answer feedback once he clicked
			 *        on 'Check' button. But how?
			 */
			if (m_verify_control->matched ()) {
				process_answer_cb (ANSWER_OK_HESITANT);
			}
			else {
				process_answer_cb (ANSWER_BAD_BLACKOUT);
			}
		}
		m_verify_control->clear ();
	}
}

/**=============================================================================
  Key values can be found in /usr/include/gtk-2.0/gdk/gdkkeysyms.h

  Key bindings:

  Deck mode:
    /------------------------+------------------------.
    |     Action             |        Key(s)          |
    +------------------------+------------------------+
    | Play the sound         | UpArrow                |
    | Previous card          | LeftArrow              |
    | Next card              | RigthArrow, Enter      |
    | Flip the card          | DownArrow, Spacebar    |
    | Iconize windows        | 'u'                    |
    `------------------------+------------------------/

  CardBox mode:
    /------------------------+------------------------.
    |     Action             |        Key(s)          |
    +------------------------+------------------------+
    | Play the sound         | UpArrow                |
    | 'NO' answer            | LeftArrow, 'n'         |
    | 'YES' answer           | RigthArrow, Enter, 'y' |
    | Flip the card          | DownArrow, Spacebar    |
    | Iconize windows        | 'u'                    |
    `------------------------+------------------------/

  Engaging Verification Control (any mode):

    /-------------------------+------------------------.
	| Activate [Check] button | Type in the answer     |
    +-------------------------+------------------------+
	| Check the answer,       | [Check], Enter         |
	| flip the card,          |                        |
	| activate [Next] button  |                        |
    `-------------------------+------------------------/

  GDK_space     0x0020

  GDK_Left      0xFF51
  GDK_Up        0xFF52
  GDK_Right     0xFF53
  GDK_Down      0xFF54

  GDK_KP_Left   0xFF96
  GDK_KP_Up     0xFF97
  GDK_KP_Right  0xFF98
  GDK_KP_Down   0xFF99

  GDK_Tab       0xFF09
  GDK_Return    0xFF0D
  GDK_KP_Enter  0xFF8D
  GDK_F1        0xFFBE
  GDK_F6        0xFFC3
  GDK_F7        0xFFC4
  GDK_F8        0xFFC5

  Clearing selected text was a pure hack.

*===============================================================================
*/

bool 
DeckPlayer::
on_key_pressed (GdkEventKey* evt_)
{
    trace_with_mask("DeckPlayer::on_key_pressed",KEYIN);

	bool result      = true;
	bool entry_armed = false;
	bool pgup        = false;
	bool pgdown      = false;

	Gtk::ScrolledWindow* scrollw = NULL;

#if !defined(IS_HILDON)
	if (evt_->keyval == GDK_F1) {
		MAINWIN->iconify ();
	}
#endif

	if (evt_->keyval == GDK_space || evt_->keyval > 0xFF51) {
		DL ((KEYIN,"Key pressed: 0x%04X, real_deck: %s, state: %s\n", 
			 evt_->keyval, (real_deck () ? "Yes" : "No"),
			 (states [m_verify_control->get_state ()])));
	}
	
	if (evt_->keyval == GDK_space && m_tab_activated) {
		m_tab_activated = false;
		result = false;
		goto done;
	}
	
	if (evt_->keyval == GDK_Tab) {
		m_tab_activated = true;
		result = false;
		goto done;
	}

	if (check_fullscreen_key (evt_->keyval)) {
		result = true;
		goto done;
	}

	/*********************************************
	 * Real Deck                                 *
	 *********************************************/

	if (real_deck ()) 
	{
		DL ((KEYIN,"Real deck processes key event.\n"));

		switch (m_verify_control->get_state ()) {

		case VC_START:
		case VC_VALIDATED:
			if (evt_->keyval == GDK_Right    || 
				evt_->keyval == GDK_KP_Right ||
				evt_->keyval == GDK_Return   || 
				evt_->keyval == GDK_KP_Enter ||
				check_F7_key (evt_->keyval)) 
			{
				go_next_cb ();
			}
			else if (evt_->keyval == GDK_Left    || 
					 evt_->keyval == GDK_KP_Left ||
					 check_F8_key (evt_->keyval))
			{
				go_prev_cb ();
			}
			break;

		case VC_ARMED:
			entry_armed = true;
			if (evt_->keyval == GDK_Return ||
				evt_->keyval == GDK_KP_Enter) 
			{
				m_verify_control->compare (get_answer ());
				auto_pronounce_card (BACK);
				clear_selection ();  
			}
			else if (evt_->keyval == GDK_space) {
				result = false;
				goto done;
			}
			break;
		}
	}
	/*********************************************
	 * CardBox Player                            *
	 *********************************************/

	else {
		DL ((KEYIN,"CardDeck processes key event.\n"));

		switch (m_verify_control->get_state ()) 
		{
		case VC_START:
			if (evt_->keyval == GDK_Right    || 
				evt_->keyval == GDK_KP_Right ||
				check_F7_key (evt_->keyval))
			{
				process_answer_cb (ANSWER_OK_HESITANT);
			}
			else if (evt_->keyval == GDK_Left    || 
					 evt_->keyval == GDK_KP_Left ||
					 check_F8_key (evt_->keyval))
			{
				process_answer_cb (ANSWER_BAD_BLACKOUT);
			}
			break;

		case VC_ARMED:
			entry_armed = true;
			if (evt_->keyval == GDK_Return || 
				evt_->keyval == GDK_KP_Enter) 
			{
				m_verify_control->compare (get_answer ());
				if (m_verify_control->matched ()) {
					enable_good_answer_buttons ();
				}
				else {
					enable_bad_answer_buttons ();
				}
				auto_pronounce_card (BACK);
				clear_selection ();  
			}
			else if (evt_->keyval == GDK_space) {
				result = false;
				goto done;
			}
			break;

		case VC_VALIDATED:
			if (evt_->keyval == GDK_Return || 
				evt_->keyval == GDK_KP_Enter) 
			{
				if (m_verify_control->matched ()) {
					process_answer_cb (ANSWER_OK_HESITANT);
				}
				else {
					process_answer_cb (ANSWER_BAD_BLACKOUT);
				}
				m_verify_control->clear ();
			}
			break;
		}

	} // end

	if (evt_->state & GDK_CONTROL_MASK) {
		if (evt_->keyval == GDK_q || evt_->keyval == GDK_Q)
		{
			pgup = true;
		}
		if (evt_->keyval == GDK_a || evt_->keyval == GDK_A)
		{
			pgdown = true;
		}
	}

	switch (evt_->keyval)
		{
		case GDK_Page_Up:
		case GDK_KP_Page_Up:
			pgup = true;
			break;
			
		case GDK_Page_Down:
		case GDK_KP_Page_Down:
			pgdown = true;
			break;
			
		case GDK_Up:          
		case GDK_KP_Up:
			on_play_sound_clicked ();
			break;

		/** Note: Don't change the order.
		 *        Both 'space' and 'down' keys flip the card.
		 */
		case GDK_space: 
		{ 
			m_tab_activated = false; 
		}
		case GDK_Down:        
		case GDK_KP_Down:
			flip_card ();
			break;
			
		default:
			result = false;
		}

	if (m_notebook->get_current_page () == FRONT) {
		scrollw = m_front_scrollw;
	}
	else {
		scrollw = m_back_scrollw;
	}

	if (pgup || pgdown) {
		Gtk::Adjustment* adj = scrollw->get_vadjustment ();
		gdouble val = adj->get_value ();
		if (pgup) {
			val -= adj->get_step_increment () * 4;
		}
		else {
			val += adj->get_step_increment () * 4;
		}
		val = std::max (val, adj->get_lower ());
		val = std::min (val, adj->get_upper () - adj->get_page_size ());
		adj->set_value (val);
		result = true;
	}
	
  done:
	return result;
}	

void
DeckPlayer::
set_win_name ()
{
    string title = m_deck.get_name ();
    string::size_type idx = title.rfind (G_DIR_SEPARATOR);
    if (idx != string::npos) {
        title.replace (0, idx+1, "");
    }
    set_title ("Deck Player : " + title);
}

/**
   Remember the size of the window if user resized it.
*/
void
DeckPlayer::
size_allocate_cb (Gtk::Allocation& allocation_)
{
    trace_with_mask("DeckPlayer::size_allocate_cb",GEOM);

	DL ((GEOM,"New size change requested: w=%d, h=%d\n", 
		 allocation_.get_width (), allocation_.get_height ()));

	Glib::RefPtr<Gdk::Window> gdk_window = get_window ();
	if (gdk_window) {
		Gdk::WindowState winstate = gdk_window->get_state ();

		if (winstate & Gdk::WINDOW_STATE_MAXIMIZED) {
			DL((GRAPP,"Window is in FULLSCREEN mode - ignore its geometry.\n"));
			return;
		}
	}

	CONFIG->set_deck_player_geometry (allocation_.get_width (),
									  allocation_.get_height ());
}

/** 
	Adjust the size of the 'question' label when size of 
	the enclosing VBox is changed by the user via resize.
*/
void
DeckPlayer::
question_box_size_allocate_cb (Gtk::Allocation& allocation_)
{
    trace_with_mask("DeckPlayer::question_box_size_allocate_cb",GEOM);

    DL((GEOM,"new question_box size (w=%d; h=%d)\n", 
		allocation_.get_width (), allocation_.get_height ()));

    if (m_question_box_width != allocation_.get_width ()) {
		m_front.set_size_request (allocation_.get_width () - 20,  -1);
		m_question_box_width = allocation_.get_width ();
	}
}

/** 
	Adjust the size of the 'example' label when size of 
	the enclosing VBox is changed by the user via resize.
*/
void
DeckPlayer::
answer_box_size_allocate_cb (Gtk::Allocation& allocation_)
{
    trace_with_mask("DeckPlayer::answer_box_size_allocate_cb",GEOM);

    DL((GEOM,"new answer_box size (w=%d; h=%d)\n", 
		allocation_.get_width (), allocation_.get_height ()));

    if (m_answer_box_width != allocation_.get_width ()) {
		m_example.set_size_request (allocation_.get_width () - 20,  -1);
		m_answer_box_width = allocation_.get_width ();
	}
}

void
DeckPlayer::
reset_text_fonts ()
{
   trace_with_mask("DeckPlayer::reset_text_fonts",GUITRACE);

   if (real_deck ()) {
	   Deck* deckp = dynamic_cast<Deck*> (&m_deck);
	   if (deckp->with_custom_appearance ()) 
	   {
		   m_front.modify_font   (deckp->fonts_db ().get_question_font ());
		   m_back.modify_font    (deckp->fonts_db ().get_answer_font   ());
		   m_example.modify_font (deckp->fonts_db ().get_example_font  ());
		   return;
	   }
   }

   m_front.modify_font   (CONFIG->fonts_db ().get_question_font ());
   m_back.modify_font    (CONFIG->fonts_db ().get_answer_font   ());
   m_example.modify_font (CONFIG->fonts_db ().get_example_font  ());
}

void
DeckPlayer::
reset_text_colors ()
{
   trace_with_mask("DeckPlayer::reset_text_colors",GUITRACE);

   if (real_deck ()) {
	   Deck* deckp = dynamic_cast<Deck*> (&m_deck);
	   if (deckp->with_custom_appearance ()) 
	   {
		   m_front.modify_fg (Gtk::STATE_NORMAL, 
							  deckp->text_colors_db ().get_text_fg_color ());
		   m_question_evbox->modify_bg (Gtk::STATE_NORMAL, 
								deckp->text_colors_db ().get_text_bg_color());  

		   m_back.modify_fg (Gtk::STATE_NORMAL, 
							 deckp->text_colors_db ().get_text_fg_color ());

		   m_sep_evbox->modify_bg (Gtk::STATE_NORMAL, 
							   deckp->text_colors_db ().get_separator_color ());

		   m_example.modify_fg (Gtk::STATE_NORMAL, 
								deckp->text_colors_db ().get_text_fg_color ());
		   m_page_viewport->modify_bg (Gtk::STATE_NORMAL, 
							   deckp->text_colors_db ().get_text_bg_color ());

		   m_sf_control->modify_fg (Gtk::STATE_NORMAL, 
								deckp->text_colors_db ().get_text_fg_color ());
		   m_sf_control->modify_bg (Gtk::STATE_NORMAL, 
								deckp->text_colors_db ().get_frame_color ());

		   m_navigation_controls_hbox->modify_bg (Gtk::STATE_NORMAL, 
								  deckp->text_colors_db ().get_frame_color ());
		   m_navigation_controls_hbox->modify_bg (Gtk::STATE_INSENSITIVE, 
								  deckp->text_colors_db ().get_frame_color ());

		   m_nav_ctrl_evbox->modify_bg (Gtk::STATE_NORMAL, 
								deckp->text_colors_db ().get_frame_color ());
		   m_answers_toolbar->modify_bg (Gtk::STATE_NORMAL, 
								 deckp->text_colors_db ().get_frame_color ());

		   /** And the frame itself
			*/
		   modify_fg (Gtk::STATE_NORMAL,
					  deckp->text_colors_db ().get_text_fg_color ());
		   modify_bg (Gtk::STATE_NORMAL,
					  deckp->text_colors_db ().get_frame_color ());
		   return;
	   }
   }

   m_front.modify_fg (Gtk::STATE_NORMAL, 
					  CONFIG->text_colors_db ().get_text_fg_color ());
   m_question_evbox->modify_bg (Gtk::STATE_NORMAL, 
							   CONFIG->text_colors_db ().get_text_bg_color());  
   m_back.modify_fg (Gtk::STATE_NORMAL, 
					 CONFIG->text_colors_db ().get_text_fg_color ());

   m_sep_evbox->modify_bg (Gtk::STATE_NORMAL, 
						   CONFIG->text_colors_db ().get_separator_color ());

   m_example.modify_fg (Gtk::STATE_NORMAL, 
						CONFIG->text_colors_db ().get_text_fg_color ());
   m_page_viewport->modify_bg (Gtk::STATE_NORMAL, 
							   CONFIG->text_colors_db ().get_text_bg_color ());

   /** Restore framing back to the default theme colors
	*/
   Glib::RefPtr<Gtk::Style> style = MAINWIN->get_style ();
   Gdk::Color theme_bg  = style->get_bg (Gtk::STATE_NORMAL);
   Gdk::Color theme_fg  = style->get_fg (Gtk::STATE_NORMAL);
   
   m_sf_control->modify_fg (Gtk::STATE_NORMAL, theme_fg);
   m_sf_control->modify_bg (Gtk::STATE_NORMAL, theme_bg);

   m_navigation_controls_hbox->modify_bg (Gtk::STATE_NORMAL, theme_bg);
   m_navigation_controls_hbox->modify_bg (Gtk::STATE_INSENSITIVE, theme_bg);

   m_nav_ctrl_evbox->modify_bg (Gtk::STATE_NORMAL, theme_bg);
   m_answers_toolbar->modify_bg (Gtk::STATE_NORMAL, theme_bg);

   modify_fg (Gtk::STATE_NORMAL, theme_fg);
   modify_bg (Gtk::STATE_NORMAL, theme_bg);
}

void
DeckPlayer::
repaint ()
{
	trace_with_mask("DeckPlayer::repaint",GUITRACE);

	reset_text_colors ();
	reset_text_fonts ();

	DL ((GRAPP,"Answer padding (x, y) = %d, %d\n",
		 x_padding_val (BACK), y_padding_val (BACK)));

	m_verify_control->repaint ();
	update_geometry ();

	/* Update text alignment
	 */
   	m_front.set_alignment (x_alignment_enum (FRONT),
						   y_alignment_enum (FRONT));
	m_front.set_justify   (justification_enum (FRONT));
	
	m_back.set_alignment (x_alignment_enum (BACK),
						  y_alignment_enum (BACK));
	m_back.set_justify   (justification_enum (BACK));
}

/** Resize only if geometry stored in Configurator differs
 *  from the actual size;
 */
void
DeckPlayer::
update_geometry ()
{
#if !defined(IS_HILDON)
	int actual_height = 0;
	int actual_width  = 0;

	Gdk::Rectangle geom = CONFIG->get_deck_player_geometry ();
	get_size (actual_width, actual_height);

	if (actual_width != geom.get_width () || 
		actual_height != geom.get_height ())
	{
		resize (geom.get_width (), geom.get_height ());
	}
#endif
}

/**
 * Replace all newline characters first.
 * Then remove all leading and following extra spaces.
 * Then shrink all  whitespaces that are left to a single space.
 */
void
DeckPlayer::
shrink_to_single_line (Glib::ustring& txt_)
{
   trace_with_mask("DeckPlayer::shrink_to_single_line",GUITRACE);

   Glib::ustring::size_type idx;

   idx = txt_.find_first_of ("\n");
   if (idx != Glib::ustring::npos) {
	   txt_.replace (idx, txt_.size (), "");
   }

   while ((idx = txt_.find ("  ")) != Glib::ustring::npos) {
	   txt_.replace (idx, 2, " ");
   }

   idx = txt_.find_first_not_of (" \t");
   if (idx != Glib::ustring::npos) {
	   txt_.replace (0, idx, "");
   }
   
   idx = txt_.find_last_not_of (" \t");
   if (idx != Glib::ustring::npos) {
	   txt_.replace (idx + 1, txt_.size (), "");
   }
}


Glib::ustring
DeckPlayer::
markup_tildas (const Glib::ustring& src_)
{
//    trace_with_mask("DeckPlayer::markup_tildas", GUITRACE);

	static Glib::ustring tilda ("<span foreground=\"red\">~</span>");
	Glib::ustring ret;

	if (src_.size () == 0) {
		return ret;
	}

	u_int i;
	u_int b;

	for (i = 0, b = 0; i < src_.size (); i++) {
		if (src_[i] == '~') {
			if (i == 0) {
				ret = tilda;
				b++;
				continue;
			}
			ret += src_.substr (b, i-b) + tilda;
			b = i+1;
		}
	}
	ret += src_.substr (b);
	return ret;
}

bool
DeckPlayer::
markup_is_valid (VCard* card_)
{
	return (Granule::check_markup (card_->get_question ()) &&
			Granule::check_markup (card_->get_answer   ()) &&
			Granule::check_markup (card_->get_example  ()));
}

/** If card we view is reversed, disregard Deck's orientation. 
 *  In this case, each card carries its own *sticky* orientation history
 *  which makes the whole experience of coding and testing maddening!!!
 */
void
DeckPlayer::
switch_side ()
{
    trace_with_mask("DeckPlayer::switch_side", GUITRACE);

	if (m_deck_iter == m_deck.end ()) {
		m_notebook->set_current_page (m_selected_side);
		return;
	}

	if (!real_deck () && (*m_deck_iter)->get_reversed ())
	{
		m_notebook->set_current_page (m_selected_side == FRONT ? BACK : FRONT);
	}
	else {
		m_notebook->set_current_page (m_selected_side);
	}

	auto_pronounce_card (!(*m_deck_iter)->get_reversed () ? FRONT : BACK);
}

/** We want to have card already painted in the window.
 */
void
DeckPlayer::
auto_pronounce_card (SideSelection side_)
{
    trace_with_mask("DeckPlayer::auto_pronounce_card", GUITRACE);

	if (!m_initialized || m_deck_iter == m_deck.end ()) {
		return;
	}

	if (m_selected_side == side_ && CONFIG->auto_pronounce ())
    {
		if (!Deck::audio_playback_active ()) {
			m_deck.play_card (m_selected_side, m_deck_iter);
		}
		else {
			DL ((GRAPP,"Already playing an audio clip.\n"));
		}
	}
}

Glib::ustring
DeckPlayer::
get_answer ()
{
    trace_with_mask("DeckPlayer::get_answer", GUITRACE);

	if ((*m_deck_iter)->get_reversed () || m_selected_side == BACK) {
		return (m_front.get_text ());
	}
	else {
		return (m_back.get_text ());
	}
}

/* Clear selection, but preserve the focus of the Entry.
 */
void
DeckPlayer::
clear_selection ()
{
    trace_with_mask("DeckPlayer::clear_selection", GUITRACE);

	int start = 0;
	int end = 0;
	
	if (m_notebook->get_current_page () == 0) {	 // front
		m_front.get_selection_bounds (start, end);
		if (start != end) {
			m_front.select_region (0, 0);
		}
	}
	else {	// back
		m_back.get_selection_bounds (start, end);
		if (start != end) {
			m_back.select_region (0, 0);
		}
		start = end = 0;
		m_example.get_selection_bounds (start, end);
		if (start != end) {
			m_example.select_region (0, 0);
		}
	}

	m_verify_control->set_focus_on_entry ();
}

void
DeckPlayer::
clear_text ()
{
	m_front.  set_text ("");
	m_back.   set_text ("");
	m_example.set_text ("");

	if (m_front_image != NULL) {
		m_front_image->clear ();
	}
	
	if (m_example_image != NULL) {
		m_example_image->clear ();
	}

	Gtk::Adjustment* adj = m_front_scrollw->get_vadjustment ();
	adj->set_value (adj->get_lower ());

	adj = m_back_scrollw->get_vadjustment ();
	adj->set_value (adj->get_lower ());
}

/******************************************************************************
 *	           Convert string alignment to Gtk::AlignmentEnum                 *
 ******************************************************************************/

Gtk::AlignmentEnum
DeckPlayer::
x_alignment_enum (SideSelection side_) 
{
	Gtk::AlignmentEnum result = Gtk::ALIGN_CENTER;
	std::string val;

	if (m_deck.with_custom_appearance ()) {
		Deck* deckp = dynamic_cast<Deck*> (&m_deck);
		val = deckp->app_db ().x_alignment (side_);
	}
	else {
		val = CONFIG->app_db ().x_alignment (side_);
	}

	if (val == "left") {
		result = Gtk::ALIGN_LEFT;
	}
	else if (val == "right") {
		result = Gtk::ALIGN_RIGHT;
	}

	return (result);
}

Gtk::AlignmentEnum
DeckPlayer::
y_alignment_enum (SideSelection side_) 
{
	Gtk::AlignmentEnum result = Gtk::ALIGN_CENTER;
	std::string val;

	if (m_deck.with_custom_appearance ()) {
		Deck* deckp = dynamic_cast<Deck*> (&m_deck);
		val = deckp->app_db ().y_alignment (side_);
	}
	else {
		val = CONFIG->app_db ().y_alignment (side_);
	}

	if (val == "top") {
		result = Gtk::ALIGN_TOP;
	}
	else if (val == "bottom") {
		result = Gtk::ALIGN_BOTTOM;
	}
	return (result);
}


/*******************************************************************************
 *            Convert string justification to Gtk::Justification
 *******************************************************************************/

Gtk::Justification
DeckPlayer::
justification_enum (SideSelection side_)
{
	Gtk::Justification result = Gtk::JUSTIFY_CENTER;
	std::string val;

	if (m_deck.with_custom_appearance ()) {
		Deck* deckp = dynamic_cast<Deck*> (&m_deck);
		val = deckp->app_db ().justification (side_);
	}
	else {
		val = CONFIG->app_db ().justification (side_);
	}

	if (val == "left") {
		result = Gtk::JUSTIFY_LEFT;
	}
	else if (val == "right") {
		result = Gtk::JUSTIFY_RIGHT;
	}
	else if (val == "fill") {
		result = Gtk::JUSTIFY_FILL;
	}
	return (result);
}

int
DeckPlayer::
x_padding_val (SideSelection side_)
{
	int result;

	if (m_deck.with_custom_appearance ()) {
		Deck* deckp = dynamic_cast<Deck*> (&m_deck);
		result = ustring_to_int (deckp->app_db ().x_padding (side_));
	}
	else {
		result = ustring_to_int (CONFIG->app_db ().x_padding (side_));
	}

	return (result);
}

int
DeckPlayer::
y_padding_val (SideSelection side_)
{
	int result;

	if (m_deck.with_custom_appearance ()) {
		Deck* deckp = dynamic_cast<Deck*> (&m_deck);
		result = ustring_to_int (deckp->app_db ().y_padding (side_));
	}
	else {
		result = ustring_to_int (CONFIG->app_db ().y_padding (side_));
	}

	return (result);
}

int
DeckPlayer::
image_valignment (SideSelection side_)
{
	int valignment = TA_IMG_POSITION_TOP;

	if (real_deck ()) {
		Deck* deckp = dynamic_cast<Deck*> (&m_deck);
		if (deckp->with_custom_appearance ()) {
			if (deckp->app_db ().img_position (side_) == "bottom") {
				return (TA_IMG_POSITION_BOTTOM);
			}
		}
	}

	if (CONFIG->app_db ().img_position (side_) == "bottom") {
		valignment = TA_IMG_POSITION_BOTTOM;
	}

	return (valignment);
}

/*******************************************************************************
 *
 ******************************************************************************/
void
DeckPlayer::
on_verify_control_checkbutton_clicked ()
{
	if (m_verify_control->is_visible ()) 
	{
		m_verify_control->hide ();
	}
	else {
		m_verify_control->show ();
	}
}

void
DeckPlayer::
on_stats_info_checkbutton_clicked ()
{
	if (m_stats_info_hbox->is_visible ()) 
	{
		m_stats_info_hbox->hide ();
	}
	else {
		m_stats_info_hbox->show ();
	}
}

void
DeckPlayer::
on_sidebar_checkbutton_clicked ()
{
	if (!m_fullscreen_mode) {
		return;
	}

	if (m_ctrl_sidebar_vbox->is_visible ()) 
	{
		m_ctrl_sidebar_vbox->hide ();
	}
	else {
		m_ctrl_sidebar_vbox->show ();
	}
}

/**
 * IconSet holds IconSources each of which can specify different
 * state/size of the icon.
 *
 * IconFactory holds a collection of IconSets. They are identified
 * by their unique StockID. Also, a new StockItem with the same ID
 * is added to the Gtk::Stock collection.
 */
void
DeckPlayer::
add_pixmap_to_factory (Gtk::StockID& id_,
					   const Glib::ustring& label_,
					   Glib::RefPtr<Gdk::Pixbuf> pixbuf_,
					   Glib::RefPtr<Gtk::IconFactory> icon_factory_,
					   bool large_toolbar_)
{
    Gtk::IconSet    icon_set;
	Gtk::IconSource icon_source;

	if (large_toolbar_) {
		Gtk::IconSize big_icon_size (72);
//		icon_source.set_size(Gtk::ICON_SIZE_LARGE_TOOLBAR);
		icon_source.set_size (big_icon_size);
//		icon_source.set_size_wildcarded(); /* Icon may be scaled. */
		icon_source.set_pixbuf (pixbuf_);
		icon_set.add_source (icon_source);
	}
	else {
		icon_source.set_size(Gtk::ICON_SIZE_SMALL_TOOLBAR);
		icon_source.set_size_wildcarded(); /* Icon may be scaled. */
		icon_source.set_pixbuf (pixbuf_);
		icon_set.add_source (icon_source);
	}

    icon_factory_->add (id_, icon_set);

    Gtk::Stock::add (Gtk::StockItem (id_, label_));
}

void
DeckPlayer::
on_notebook_page_flipped (GtkNotebookPage* page_, guint /* page_num_ */)
{
    trace_with_mask("DeckPlayer::on_notebook_page_flipped", GUITRACE);

	clear_selection ();
}

void
DeckPlayer::
set_controls_visibility ()
{
    trace_with_mask("DeckPlayer::set_controls_visibility", GUITRACE);

	if (CONFIG->show_va_control ()) {
		m_vc_toolbar_button->set_active (true);
		m_verify_control->show ();
	}
	else {
		m_vc_toolbar_button->set_active (false);
		m_verify_control->hide ();
	}

	if (CONFIG->show_si_control ()) {
		m_stats_toolbar_button->set_active (true);
		m_stats_info_hbox->show ();
	}
	else {
		m_stats_toolbar_button->set_active (false);
		m_stats_info_hbox->hide ();
	}

	if (CONFIG->show_sidebar ()) {
		m_sbar_toolbar_button->set_active (true);
		if (m_fullscreen_mode) {
			show_sidebar ();
		}
		else {
			hide_sidebar ();
		}
	}
	else {
		m_sbar_toolbar_button->set_active (false);
	}
}

void
DeckPlayer::
save_controls_visibility ()
{
    trace_with_mask("DeckPlayer::save_controls_visibility", GUITRACE);

	bool old_state;
	bool new_state;

	old_state = CONFIG->show_va_control ();
	new_state = m_vc_toolbar_button->get_active ();

	if (old_state != new_state) {
		CONFIG->show_va_control (new_state);
	}

	old_state = CONFIG->show_si_control ();
	new_state = m_stats_toolbar_button->get_active ();

	if (old_state != new_state) {
		CONFIG->show_si_control (new_state);
	}

	old_state = CONFIG->show_sidebar ();
	new_state = m_sbar_toolbar_button->get_active ();

	if (old_state != new_state) {
		CONFIG->show_sidebar (new_state);
	}
}

void
DeckPlayer::
go_to_fullscreen ()
{
    trace_with_mask("DeckPlayer::go_to_fullscreen", GUITRACE);

	check_fullscreen_key (GDK_F6);
}

bool
DeckPlayer::
check_fullscreen_key (gint keyval_)
{
    trace_with_mask("DeckPlayer::check_fullscreen_key", GUITRACE);

	if (keyval_ == GDK_F6)
	{
		DL ((GRAPP,"before: m_fullscreen_mode = %s\n", 
			 (m_fullscreen_mode ? "true" : "false")));

		if (!m_fullscreen_mode) 
		{
			if (m_sbar_toolbar_button->get_active ()) {
				show_sidebar ();
			}
			else {
				hide_sidebar ();
			}
			fullscreen ();
			m_fullscreen_mode = true;
		}
		else {
			unfullscreen (); 
			m_fullscreen_mode = false;
			hide_sidebar ();
		}
		return true;
	}

	return false;
}

void
DeckPlayer::
change_sidebar_location ()
{
    trace_with_mask("DeckPlayer::change_sidebar_location", GUITRACE);

	if (CONFIG->sidebar_location () == SIDEBAR_LEFT) { 
		CONFIG->sidebar_location (SIDEBAR_RIGHT);
		m_enclosure_hbox->reorder_child (*m_ctrl_sidebar_vbox, 1);
		DL ((GRAPP,"Moved sidebar to the Right\n"));
	}
	else {
		CONFIG->sidebar_location (SIDEBAR_LEFT);
		DL ((GRAPP,"Moved sidebar to the Left\n"));
		m_enclosure_hbox->reorder_child (*m_ctrl_sidebar_vbox, 0);
	}
}

Gtk::Button*
DeckPlayer::
make_button_with_image (const char* pixmap_path_)
{
	Gtk::Button* nav_button = NULL;
	Gtk::Image*  nav_image = NULL;
	Glib::RefPtr<Gdk::Pixbuf> image_xpm;
	Glib::ustring path (GRAPPDATDIR);

	path += pixmap_path_;

    image_xpm = Granule::create_from_file (path,
										   CONFIG->sidebar_width () - 2,
										   CONFIG->sidebar_width () - 2);
	nav_image  = manage (new Gtk::Image (image_xpm));
	nav_button = manage (new Gtk::Button);
	nav_button->set_image (*nav_image);

	return nav_button;
}
