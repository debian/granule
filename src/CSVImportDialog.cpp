// -*- c++ -*-
//------------------------------------------------------------------------------
//                            CSVImportDialog.cpp
//------------------------------------------------------------------------------
// $Id: CSVImportDialog.cpp,v 1.4 2007/06/18 02:30:43 vlg Exp $
//------------------------------------------------------------------------------
//  Copyright (c) 2005-2006 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version
//  2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------
// 05/14/2005 VLG  Created
//------------------------------------------------------------------------------
#include <gdk/gdkkeysyms.h>
#include <gtkmm/accelgroup.h>
#include <gtkmm/image.h>
#include <gtkmm/label.h>
#include <gtkmm/box.h>
#include <gtkmm/alignment.h>
#include <gtkmm/buttonbox.h>
#include <gtkmm/frame.h>
#include <gtkmm/table.h>

#include "CSVImportDialog.h"
#include "Intern.h"

CSVImportDialog::
CSVImportDialog() : 
	m_sep_selection (SPACE_SEP),
	m_recursive_call (false)
{  
	CSVImportDialog* dialog = this;

	Gtk::Image* image1;
	Gtk::Label* label4;
	Gtk::HBox* hbox3;
	Gtk::Alignment* alignment1;
	Gtk::Image* image2;
	Gtk::Label* label5;
	Gtk::HBox* hbox4;
	Gtk::Alignment* alignment2;
	Gtk::Label* linebrk_label;
	Gtk::HBox* linebrk_hbox;
	Gtk::Label* lb_frame_label;
	Gtk::Frame* linebrk_frame;
	Gtk::Table* sep_table;
	Gtk::VBox* sep_vbox;
	Gtk::Label* sep_frame_label;
	Gtk::Frame* m_sep_frame;
	Gtk::VBox* extra_vbox;


	image1          = Gtk::manage (new Gtk::Image(Gtk::StockID("gtk-cancel"), 
												  Gtk::IconSize(4)));
	label4          = Gtk::manage (new Gtk::Label(_("Cancel")));
	hbox3           = Gtk::manage (new Gtk::HBox(false, 2));
	alignment1      = Gtk::manage (new Gtk::Alignment(0.5, 0.5, 0, 0));
	m_cancel_button = Gtk::manage (new Gtk::Button());
   
	image2          = Gtk::manage (new Gtk::Image(Gtk::StockID("gtk-apply"), 
												  Gtk::IconSize(4)));
	label5          = Gtk::manage (new Gtk::Label(_("Import")));
	hbox4           = Gtk::manage (new Gtk::HBox(false, 2));
	alignment2      = Gtk::manage (new Gtk::Alignment(0.5, 0.5, 0, 0));
	m_import_button = Gtk::manage (new Gtk::Button());
   
	linebrk_label   = Gtk::manage (new Gtk::Label(_("Line Break:")));
	m_unix_lf       = Gtk::manage (new Gtk::CheckButton(_("UNIX (LF)")));
	m_windoz_crlf   = Gtk::manage (new Gtk::CheckButton(_("Windows (CR+LF)")));
   
	linebrk_hbox    = Gtk::manage (new Gtk::HBox(false, 15));
	lb_frame_label  = Gtk::manage (new Gtk::Label(_("")));
	linebrk_frame   = Gtk::manage (new Gtk::Frame());
	m_space_sep     = Gtk::manage (new Gtk::CheckButton(_("Space")));
	m_tab_sep       = Gtk::manage (new Gtk::CheckButton(_("Tab")));
	m_semicolon_sep = Gtk::manage (new Gtk::CheckButton(_("Semicolon (;)")));
	m_colon_sep     = Gtk::manage (new Gtk::CheckButton(_("Colon (:)")));

	m_comma_sep     = Gtk::manage (new Gtk::CheckButton(_("Comma (,)")));
   
	sep_table       = Gtk::manage (new Gtk::Table(2, 3, true));
	sep_vbox        = Gtk::manage (new Gtk::VBox(false, 0));
	sep_frame_label = Gtk::manage (new Gtk::Label(_("Separators")));
	m_sep_frame     = Gtk::manage (new Gtk::Frame());
	m_shrink_seps   = Gtk::manage (new Gtk::CheckButton(
									   _("See two separators as one")));
   
	extra_vbox = Gtk::manage (new Gtk::VBox(false, 0));

	image1->set_alignment(0.5,0.5);
	image1->set_padding(0,0);
	label4->set_alignment(0.5,0.5);
	label4->set_padding(0,0);
	label4->set_justify(Gtk::JUSTIFY_LEFT);
	label4->set_line_wrap(false);
	label4->set_use_markup(false);
	label4->set_selectable(false);
	hbox3->pack_start(*image1, Gtk::PACK_SHRINK, 0);
	hbox3->pack_start(*label4, Gtk::PACK_SHRINK, 0);
	alignment1->add(*hbox3);
	m_cancel_button->set_flags(Gtk::CAN_FOCUS);
	m_cancel_button->set_relief(Gtk::RELIEF_NORMAL);
	m_cancel_button->add(*alignment1);

	image2->set_alignment(0.5,0.5);
	image2->set_padding(0,0);
	label5->set_alignment(0.5,0.5);
	label5->set_padding(0,0);
	label5->set_justify(Gtk::JUSTIFY_LEFT);
	label5->set_line_wrap(false);
	label5->set_use_markup(false);
	label5->set_selectable(false);
	hbox4->pack_start(*image2, Gtk::PACK_SHRINK, 0);
	hbox4->pack_start(*label5, Gtk::PACK_SHRINK, 0);
	alignment2->add(*hbox4);
	m_import_button->set_flags(Gtk::CAN_FOCUS);
	m_import_button->set_relief(Gtk::RELIEF_NORMAL);
	m_import_button->add(*alignment2);
#ifdef GLIBMM_PROPERTIES_ENABLED
	dialog->get_action_area()->property_layout_style().set_value(Gtk::BUTTONBOX_END);
#endif
	linebrk_label->set_alignment(0.5,0.5);
	linebrk_label->set_padding(18,0);
	linebrk_label->set_justify(Gtk::JUSTIFY_LEFT);
	linebrk_label->set_line_wrap(false);
	linebrk_label->set_use_markup(false);
	linebrk_label->set_selectable(false);
	m_unix_lf->set_flags(Gtk::CAN_FOCUS);
	m_unix_lf->set_border_width(5);
	m_unix_lf->set_relief(Gtk::RELIEF_NORMAL);
	m_unix_lf->set_mode(true);
	m_unix_lf->set_active(true);
	m_windoz_crlf->set_flags(Gtk::CAN_FOCUS);
	m_windoz_crlf->set_border_width(5);
	m_windoz_crlf->set_relief(Gtk::RELIEF_NORMAL);
	m_windoz_crlf->set_mode(true);
	m_windoz_crlf->set_active(false);
	linebrk_hbox->set_border_width(2);
	linebrk_hbox->pack_start(*linebrk_label, Gtk::PACK_SHRINK, 0);
	linebrk_hbox->pack_start(*m_unix_lf, Gtk::PACK_SHRINK, 0);
	linebrk_hbox->pack_start(*m_windoz_crlf, Gtk::PACK_SHRINK, 0);
	lb_frame_label->set_alignment(0.5,0.5);
	lb_frame_label->set_padding(0,0);
	lb_frame_label->set_justify(Gtk::JUSTIFY_LEFT);
	lb_frame_label->set_line_wrap(false);
	lb_frame_label->set_use_markup(false);
	lb_frame_label->set_selectable(false);
	linebrk_frame->set_border_width(4);
	linebrk_frame->set_shadow_type(Gtk::SHADOW_ETCHED_IN);
	linebrk_frame->set_label_align(0,0.5);
	linebrk_frame->add(*linebrk_hbox);
	linebrk_frame->set_label_widget(*lb_frame_label);
	m_space_sep->set_flags(Gtk::CAN_FOCUS);
	m_space_sep->set_relief(Gtk::RELIEF_NORMAL);
	m_space_sep->set_mode(true);
	m_space_sep->set_active(true);
	m_tab_sep->set_flags(Gtk::CAN_FOCUS);
	m_tab_sep->set_relief(Gtk::RELIEF_NORMAL);
	m_tab_sep->set_mode(true);
	m_tab_sep->set_active(false);
	m_semicolon_sep->set_flags(Gtk::CAN_FOCUS);
	m_semicolon_sep->set_relief(Gtk::RELIEF_NORMAL);
	m_semicolon_sep->set_mode(true);
	m_semicolon_sep->set_active(false);
	m_colon_sep->set_flags(Gtk::CAN_FOCUS);
	m_colon_sep->set_relief(Gtk::RELIEF_NORMAL);
	m_colon_sep->set_mode(true);
	m_colon_sep->set_active(false);
	m_comma_sep->set_flags(Gtk::CAN_FOCUS);
	m_comma_sep->set_relief(Gtk::RELIEF_NORMAL);
	m_comma_sep->set_mode(true);
	m_comma_sep->set_active(false);

	sep_table->set_row_spacings(4);
	sep_table->set_col_spacings(4);

	sep_table->attach(*m_space_sep,0,1,0,1,
					  Gtk::FILL,Gtk::AttachOptions (), 0,0);
	sep_table->attach(*m_tab_sep, 1,2,0,1,
					  Gtk::FILL, Gtk::AttachOptions (), 0,0);
	sep_table->attach(*m_semicolon_sep, 2,3,0,1, 
					  Gtk::FILL, Gtk::AttachOptions (), 0,0);
	sep_table->attach(*m_colon_sep,0,1,1,2,Gtk::FILL, 
					  Gtk::AttachOptions (), 0,0);
	sep_table->attach(*m_comma_sep,1,2,1,2,Gtk::FILL, 
					  Gtk::AttachOptions (), 0,0);

	sep_vbox->set_border_width(2);
	sep_vbox->pack_start(*sep_table, Gtk::PACK_SHRINK, 3);
	sep_frame_label->set_alignment(0.5,0.5);
	sep_frame_label->set_padding(0,0);
	sep_frame_label->set_justify(Gtk::JUSTIFY_LEFT);
	sep_frame_label->set_line_wrap(false);
	sep_frame_label->set_use_markup(false);
	sep_frame_label->set_selectable(false);
	m_sep_frame->set_border_width(3);
	m_sep_frame->set_shadow_type(Gtk::SHADOW_ETCHED_IN);
	m_sep_frame->set_label_align(0,0.5);
	m_sep_frame->add(*sep_vbox);
	m_sep_frame->set_label_widget(*sep_frame_label);
	m_shrink_seps->set_flags(Gtk::CAN_FOCUS);
	m_shrink_seps->set_border_width(4);
	m_shrink_seps->set_relief(Gtk::RELIEF_NORMAL);
	m_shrink_seps->set_mode(true);
	m_shrink_seps->set_active(false);
	extra_vbox->set_border_width(2);
	extra_vbox->pack_start(*m_shrink_seps, Gtk::PACK_SHRINK, 0);
	dialog->get_vbox()->set_homogeneous(false);
	dialog->get_vbox()->set_spacing(0);
	dialog->get_vbox()->pack_start(*linebrk_frame, Gtk::PACK_SHRINK, 0);
	dialog->get_vbox()->pack_start(*m_sep_frame, Gtk::PACK_SHRINK, 0);
	dialog->get_vbox()->pack_start(*extra_vbox, Gtk::PACK_SHRINK, 0);
	dialog->set_border_width(1);
	dialog->set_title(_("CSV Text Import"));
	dialog->set_modal(true);
#ifdef GLIBMM_PROPERTIES_ENABLED
	dialog->property_window_position().set_value(Gtk::WIN_POS_CENTER_ON_PARENT);
#endif
	dialog->set_resizable(true);
#ifdef GLIBMM_PROPERTIES_ENABLED
	dialog->property_destroy_with_parent().set_value(false);
#endif
	dialog->set_has_separator(true);
	dialog->add_action_widget(*m_cancel_button, Gtk::RESPONSE_CANCEL);
	dialog->add_action_widget(*m_import_button, Gtk::RESPONSE_APPLY);

	m_unix_lf->signal_toggled().connect (
		mem_fun (*this, &CSVImportDialog::on_unix_lf_checked));
	m_windoz_crlf->signal_toggled().connect (
		mem_fun (*this, &CSVImportDialog::on_windoz_crlf_checked));

	m_space_sep->signal_toggled().connect (
		mem_fun (*this, &CSVImportDialog::on_space_sep_checked));
	m_tab_sep->signal_toggled().connect (
		mem_fun (*this, &CSVImportDialog::on_tab_sep_checked));
	m_colon_sep->signal_toggled().connect (
		mem_fun (*this, &CSVImportDialog::on_colon_sep_checked));
	m_semicolon_sep->signal_toggled().connect (
		mem_fun (*this, &CSVImportDialog::on_semicolon_sep_checked));
	m_comma_sep->signal_toggled().connect (
		mem_fun (*this, &CSVImportDialog::on_comma_sep_checked));
		
	dialog->show_all ();
}

void 
CSVImportDialog::
on_unix_lf_checked ()
{
	trace_with_mask("CSVImportDialog::on_unix_lf_checked",GUITRACE);

	if (!m_recursive_call) {
		m_recursive_call = true;
		m_windoz_crlf->set_active (false);
		m_recursive_call = false;
	}
}

void
CSVImportDialog::
on_windoz_crlf_checked ()
{
	trace_with_mask("CSVImportDialog::on_windoz_crlf_checked",GUITRACE);

	if (!m_recursive_call) {
		m_recursive_call = true;
		m_unix_lf->set_active (false);
		m_recursive_call = false;
	}
}

void
CSVImportDialog::
on_space_sep_checked ()
{
	trace_with_mask("CSVImportDialog::on_space_sep_checked",GUITRACE);

	if (!m_recursive_call) {
		m_recursive_call = true;
		m_tab_sep     ->set_active (false);
		m_colon_sep   ->set_active (false);
		m_semicolon_sep   ->set_active (false);
		m_comma_sep   ->set_active (false);
		m_sep_selection = SPACE_SEP;
		m_recursive_call = false;
	}
}

void 
CSVImportDialog::
on_tab_sep_checked ()
{
	trace_with_mask("CSVImportDialog::on_tab_sep_checked",GUITRACE);

	if (!m_recursive_call) {
		m_recursive_call = true;
		m_space_sep     ->set_active (false);
		m_colon_sep     ->set_active (false);
		m_semicolon_sep ->set_active (false);
		m_comma_sep     ->set_active (false);
		m_sep_selection = TAB_SEP;
		m_recursive_call = false;
	}
}

void 
CSVImportDialog::
on_colon_sep_checked ()
{
	trace_with_mask("CSVImportDialog::on_colon_sep_checked",GUITRACE);

	if (!m_recursive_call) {
		m_recursive_call = true;
		m_space_sep     ->set_active (false);
		m_tab_sep       ->set_active (false);
		m_semicolon_sep ->set_active (false);
		m_comma_sep     ->set_active (false);
		m_sep_selection = COLON_SEP;
		m_recursive_call = false;
	}
}
	
void 
CSVImportDialog::
on_semicolon_sep_checked ()
{
	trace_with_mask("CSVImportDialog::on_semicolon_sep_checked",GUITRACE);

	if (!m_recursive_call) {
		m_recursive_call = true;
		m_space_sep     ->set_active (false);
		m_tab_sep       ->set_active (false);
		m_colon_sep ->set_active (false);
		m_comma_sep     ->set_active (false);
		m_sep_selection = SEMICOLON_SEP;
		m_recursive_call = false;
	}
}
	
void 
CSVImportDialog::
on_comma_sep_checked ()
{
	trace_with_mask("CSVImportDialog::",GUITRACE);

	if (!m_recursive_call) {
		m_recursive_call = true;
		m_space_sep     ->set_active (false);
		m_tab_sep       ->set_active (false);
		m_colon_sep     ->set_active (false);
		m_semicolon_sep ->set_active (false);

		m_sep_selection = COMMA_SEP;
		m_recursive_call = false;
	}
}

const char*
CSVImportDialog::
get_separator () const 
{
	char* ret = "";

	switch (m_sep_selection) 
	{
	case SPACE_SEP:     ret = " ";  break; 
	case TAB_SEP:       ret = "\t"; break;
	case COLON_SEP:     ret = ":";  break;
	case SEMICOLON_SEP: ret = ";"; break;
	case COMMA_SEP:     ret = ","; break;
	}
	return ret;
}

const char*
CSVImportDialog::
get_double_separator () const 
{
	char* ret = "";

	switch (m_sep_selection) 
	{
	case SPACE_SEP:     ret = "  ";   break; 
	case TAB_SEP:       ret = "\t\t"; break;
	case COLON_SEP:     ret = "::";   break;
	case SEMICOLON_SEP: ret = ";;";   break;
	case COMMA_SEP:     ret = ",,";   break;
	}
	return ret;
}
