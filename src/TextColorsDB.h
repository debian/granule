// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id
//------------------------------------------------------------------------------
//                            TextColorsDB.h
//------------------------------------------------------------------------------
//  Copyright (c) 2007 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date   : Sun Oct 8 2007
//
//------------------------------------------------------------------------------

#ifndef MY_TEXT_COLORS_DB_H
#define MY_TEXT_COLORS_DB_H

#include "Granule-main.h"

#include <assa/IniFile.h>

#include <string>
#include <libxml/tree.h>		// xmlDocPtr
#include <libxml/xmlwriter.h>	// xmlTextWriterPtr

#include <pangomm/fontdescription.h>
#include <gdkmm/color.h>

class ColorsSelectorUI;

/**-----------------------------------------------------------------------------
 *	TextColorsDB is the Model of the Text Colors Selector MVC pattern.
 *  It it a persistent storage manager of data.
 *  
 *  We keep colors as ustrings in the database and then make Gdk::Colors
 *  out of them as needed. There is no method of converting Gdk::Color
 *  object to its string representation.
 **-----------------------------------------------------------------------------
 */
class TextColorsDB
{  
public:
    TextColorsDB () { init_global_defaults (); }

	void init_global_defaults ();
	void init_deck_defaults   ();

	/** Persisten storage with XML (Deck) file.
	 */
    void load_from_xml_config  (xmlDocPtr parser_, std::string& error_msg_);
    void save_to_xml_config    (xmlTextWriterPtr writer);

	/** Persistent storage with INI file (application config).
	 */
    void load_from_ini_config  (ASSA::IniFile* ini_filep_);
    void save_to_ini_config    (ASSA::IniFile* ini_filep_);

	/** Text alignment controls (From Deck.h)
	 */
    Gdk::Color get_text_fg_color  () { return Gdk::Color (m_text_fg_color);   }
    Gdk::Color get_text_bg_color  () { return Gdk::Color (m_text_bg_color);   }
    Gdk::Color get_separator_color() { return Gdk::Color (m_separator_color); }
    Gdk::Color get_frame_color    () { return Gdk::Color (m_frame_color);     }

    void set_text_fg_color   (const Glib::ustring& c_) { m_text_fg_color = c_; }
    void set_text_bg_color   (const Glib::ustring& c_) { m_text_bg_color = c_; }
    void set_separator_color (const Glib::ustring& c_) { m_separator_color=c_; }
    void set_frame_color     (const Glib::ustring& c_) { m_frame_color = c_; }

	/// Update View with data from Model
	void update_view (ColorsSelectorUI*& view_);

	/// Store View values in Model
	void fetch_from_view (ColorsSelectorUI*& view_);

private:
    Glib::ustring m_text_fg_color;
    Glib::ustring m_text_bg_color;
    Glib::ustring m_separator_color;
    Glib::ustring m_frame_color;
};

#endif /* MY_TEXT_COLORS_DB */
