// -*- c++ -*-
//------------------------------------------------------------------------------
//                            InputDialog.cpp
//------------------------------------------------------------------------------
// $Id: InputDialog.cpp,v 1.1 2007/12/02 00:31:24 vlg Exp $
//------------------------------------------------------------------------------
//  Copyright (c) 2006 by Vladislav Grinchenko 
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date   : Mon Dec 25 15:57:28 EST 2006
//
//------------------------------------------------------------------------------

#include <gtkmm/stock.h>

#include "InputDialog.h"

InputDialog::
InputDialog (const Glib::ustring& message_, const Glib::ustring& value_)
	: 
	Gtk::MessageDialog (message_, 
						true,	               /* markup message? */
						Gtk::MESSAGE_QUESTION, 
						Gtk::BUTTONS_NONE,
						true),	               /* modal dialog    */
	m_local_entry (NULL)
{
	add_button (Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL );
	add_button (Gtk::Stock::OK,     Gtk::RESPONSE_OK);

	m_local_entry =Gtk::manage (new Gtk::Entry ());

	m_local_entry->set_flags             (Gtk::CAN_FOCUS);
    m_local_entry->set_visibility        (true);
    m_local_entry->set_editable          (true);
    m_local_entry->set_has_frame         (true);
    m_local_entry->set_activates_default (true);

    m_local_entry->set_text(value_);

	get_vbox ()->pack_start (*m_local_entry, Gtk::PACK_SHRINK, 0);

	show_all_children ();
}

