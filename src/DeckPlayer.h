// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: DeckPlayer.h,v 1.88 2008/08/16 03:13:09 vlg Exp $
//------------------------------------------------------------------------------
//                            DeckPlayer.h
//------------------------------------------------------------------------------
//  Copyright (c) Vladislav Grinchenko 
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date   : Fri Feb  6 23:37:11 EST 2004
//
//------------------------------------------------------------------------------
#ifndef DECK_PLAYER_H
#define DECK_PLAYER_H

#ifdef HAVE_CONFIG_H
#    include "config.h"
#endif

#include <gdkmm/pixbuf.h>
#include <gdkmm/image.h>
#include <gtkmm/messagedialog.h>
#include <gtkmm/notebook.h>
#include <gtkmm/label.h>
#include <gtkmm/textview.h>
#include <gtkmm/uimanager.h>
#include <gtkmm/table.h>
#include <gtkmm/toggletoolbutton.h>

#ifdef IS_HILDON
#include <hildonmm.h>
#endif

#include "GrappConf.h"
#include "VDeck.h"

#include <vector>

class Gtk::ProgressBar;
class Gtk::EventBox;

class DeckView;
class DeckPlayer;
class TestLineEntry;
class VerifyControl;
class ButtonWithImageLabel;

/**===========================================================================**
 *                                                                             *
 *   Class SideFlipControl encapsulates the side switch radio buttons.         *
 *                                                                             *
 **===========================================================================**
 */
class SideFlipControl : public Gtk::EventBox
{
public:
	SideFlipControl (DeckPlayer& parent_);

	void set_active (SideSelection s_);

private:
	void on_flip_button_clicked ();

private:
	DeckPlayer& m_parent;

	Gtk::Button      m_flip_button;
	SideSelection    m_side;
};

/**===========================================================================**
 *                                                                             *
 *                              Class DeckPlayer                               *
 *                                                                             *
 **===========================================================================**
 */
#ifdef IS_HILDON 
class DeckPlayer : public Hildon::Window
#else
class DeckPlayer : public Gtk::Window
#endif
{
public:
	DeckPlayer (VDeck& vdeck_);

	/** Simulate re-entrant event loop. Also, hide/show parent
	 *  window and reposition accordingly.
	 */
	gint run (Gtk::Window& parent_);

	/** We block [x] delete action and reroute it to <Close>
	 */
	bool on_delete_window_clicked (GdkEventAny* event_);

	/** Called in response to pressing <Close> button
	 */
	void on_close_clicked ();

	/** Called every time VBox that holds 
		the question size of the card is resized.
	*/
	void question_box_size_allocate_cb (Gtk::Allocation& size_);

	/** Called every time VBox that holds 
		the answer size of the card is resized.
	*/
	void answer_box_size_allocate_cb (Gtk::Allocation& size_);

	/** Called every time window is resized.
	*/
	void size_allocate_cb (Gtk::Allocation& size_);

	/** Clear selection on notebook page switch
	 */
	void on_notebook_page_flipped (GtkNotebookPage* page_, guint page_num_);

	/** User pressed <Sound> button to play WAV audio (if available).
	 */
	void on_play_sound_clicked ();

	/** Shuffle the cards in the deck
	 */
	void on_shuffle_clicked ();

	/** User pressed <View/Edit Deck> button to Add/Delete/Edit cards
	 */
	void on_edit_clicked ();

	/** User pressed <Edit Card> button to Edit current card
	 *
	 *  @param disable_cancel_ disables <Cancel> button if set to true.
	 */
	void on_vcard_edit_clicked (bool disable_cancel_ = false);

	// -*- Button Actions -*-
	void go_first_cb ();		// Go to the first card
	void go_prev_cb  ();		// Go back one card
	void go_next_cb  ();		// Go to the next card
	void go_last_cb  ();		// Go to the last card

	// -*- Answer Callbacks -*-
	void process_answer_cb (AnswerQuality qa_);

	// VerifyControl is enabled and user clicked [Check] button
	void on_verify_clicked ();

	// -*- Radio Button Actions -*-
	void on_radio_selected (SideSelection selected_);

	// -*- Catch and process keyboard events -*-
	bool on_key_pressed (GdkEventKey* evt_);

	// Show/Hide VerifyControl toolbar (sticky).
	void on_verify_control_checkbutton_clicked ();

	// Show/Hide StatsInfo toolbar (sticky).
	void on_stats_info_checkbutton_clicked ();

	// Show/Hide Sidebar (sticky).
	void on_sidebar_checkbutton_clicked ();

	/** Get the deck pointer for comparison
	 */
	VDeck* get_deck () const { return &m_deck; }

	/** Is it a virtual deck or the real one.
	 */
	bool real_deck () const { return m_is_real_deck; }

	/** Reset iterator/count to reflect new card(s) added to the deck.
		Also, point iterator to the first card and show it. This method
		should be called each time the VDeck's count is modified (card is
		being moved or deleted).
	*/
	void reset ();

	/** Set expiration light. 
	 */
	void set_light (Glib::RefPtr<Gdk::Pixbuf> l_) { m_exp_light->set (l_); }

	/// Reset geometry from Configurator
	void update_geometry ();

	/** Take into account changed Preferences: adjust geometry
		and/or repaint the selected card.
	*/
	void repaint ();

	/** Clear the text fields of the old or stale text
	 */
	void clear_text ();

	/// Enable answer control buttons ("I know", "Not a clue")
	void enable_answer_controls ();

	/// Disable answer control buttons ("I know", "Not a clue")
	void disable_answer_controls ();

    /// Flip the Card
	void flip_card ();	

	/// Auto-pronounce word on next/flip action
	void auto_pronounce_card (SideSelection side_);

	SideSelection get_selected_side () const { return m_selected_side; }

public:
	/** Remove all newlines and shrink all whitespace.
	*/
	static void shrink_to_single_line (Glib::ustring& txt_);

	/** Extend Stock Icons with application-specific ones.
	 */
	static Gtk::StockID SAY_IT;
	static Gtk::StockID THUMB_0;
	static Gtk::StockID THUMB_1;
	static Gtk::StockID THUMB_2;
	static Gtk::StockID THUMB_3;
	static Gtk::StockID THUMB_4;
	static Gtk::StockID THUMB_5;

private:
	void show_deck       ();	// Show first card of the loaded deck
	void switch_side     ();	// Show the front/back side of the card
	void adjust_count    ();	// Adjust count label: current/total
	void set_sensitivity ();    // De-sensetize inappropriate controls
	void set_win_name    ();    // Set tile name
	void clear_selection ();    // Clear any selected text (annoying)
	void set_controls_visibility ();   // If needed, hide optional controls.
	void save_controls_visibility ();  // Save visibility of controls.

	void enable_good_answer_buttons ();
	void enable_bad_answer_buttons ();

	void scroll_up ();
	void scroll_down ();

	void reset_text_fonts ();
	void reset_text_colors ();

	Gtk::AlignmentEnum x_alignment_enum   (SideSelection s_);
	Gtk::AlignmentEnum y_alignment_enum   (SideSelection s_);
	Gtk::Justification justification_enum (SideSelection s_);

	int x_padding_val (SideSelection s_);
	int y_padding_val (SideSelection s_);

	/** Return image position in relation to text (vertical).
	 *  @return TA_IMG_POSITION_TOP for image above text;
	 *          TA_IMG_POSITION_BOTTOM for image below text;
	 */
	int image_valignment (SideSelection side_);

	/** If deck is empty, paralize controls. Deck can be loaded empty,
	 *  or become empty once the last card is removed. The reverse
	 *  applies as soon as first card is added.
	 *  This only makes sense with real decks.
	 */
	void sensitize_controls ();

    /** Display the card pointed to by the iterator
	 */
	void show_card (bool with_adjust_count_ = true);	

	/** What should be considered an answer?
	 */
	Glib::ustring get_answer ();

	/** Pango-markup tildas in the string
	 */
	static Glib::ustring markup_tildas (const Glib::ustring& src_);

	/** Check markup validity of the text fields of the card_
	 */
	bool markup_is_valid (VCard* card_);

	void go_to_fullscreen ();
	bool check_fullscreen_key (gint keyval_);  // F6 key
	bool check_F7_key (gint keyval_);          // F7 (hildon zoom in)
	bool check_F8_key (gint keyval_);          // F8 (hildon zoom out)

	Gtk::VBox* get_vbox () { return m_vbox; }
	Gtk::HBox* get_enclosure () { return m_enclosure_hbox; }

	/** A utility function similar to one found in EditControls.
	 */
	static void 
	add_pixmap_to_factory (Gtk::StockID& id_,
						   const Glib::ustring& label_,
						   Glib::RefPtr<Gdk::Pixbuf> pixbuf_,
						   Glib::RefPtr<Gtk::IconFactory> ifactory_,
						   bool large_icons_ = false);

	/** Make a 72x72 image button suitable for sidebar controls
	 *  display in fullscreen mode with touchscreen platforms (N8x0).
	 */
	static Gtk::Button* make_button_with_image (const char* pixmap_path_);

	/** Go full-screen and back to normal size.
	 */
	void go_fullscreen_mode ();

	void hide_sidebar () { m_ctrl_sidebar_vbox->hide (); }
	void show_sidebar () { m_ctrl_sidebar_vbox->show (); }
	void change_sidebar_location ();

private:
	static const string RATIO_TITLE;
	static const string REVIEW_TITLE;
	static const string RIGHT_WRONG_TITLE;

private:
	typedef std::vector<Gtk::Widget*> answers_type;
	typedef answers_type::iterator answers_iterator;

	ButtonWithImageLabel* m_play_sound_button;	 // Play the sound
	ButtonWithImageLabel* m_close_button;

	Gtk::Widget*      m_shuffle_button;      // Shuffle Deck
	Gtk::Widget*      m_edit_button;	     // View/Edit Deck
	Gtk::Widget*      m_vcard_edit_button;   // Edit Card

	int               m_match_front_row;     // Which row to select for answer

	Gtk::Label        m_count_label;         // Reviewed/Total
	Gtk::Label        m_count_label_bare;    // R/T (review/total)
	Gtk::Label        m_success_ratio_label; // Success ratio
	Gtk::Label        m_right_wrong_label;   // Right/Wrong answers
	Gtk::Image*       m_exp_light;           // Expiration light
	Glib::RefPtr<Gdk::Pixbuf> m_sched_grey;  // Default color of exp. light

	SideFlipControl*  m_sf_control;
	SideSelection     m_selected_side;

	Gtk::Notebook*    m_notebook;
	Gtk::ProgressBar* m_play_progress;

	Gtk::Label        m_front;
	Gtk::Label        m_back;
	Gtk::Label        m_example;

	Gtk::Image*       m_front_image; // (optional) image for the Front side
	Glib::RefPtr<Gdk::Pixbuf> m_front_img_pixbuf;

	Gtk::Image*       m_example_image; // (optional) image for the Front side
	Glib::RefPtr<Gdk::Pixbuf> m_example_img_pixbuf;

	TestLineEntry*    m_test_line_entry;

	Gtk::VBox*        m_question_box; /* VBox that holds the front of a card */
	int               m_question_box_width;             /* last resize width */
	Gtk::EventBox*    m_question_evbox;

	Gtk::VBox*        m_answer_box;   /* VBox that holds the back of a card  */
	int               m_answer_box_width;               /* last resize width */

	Gtk::EventBox*    m_sep_evbox;
	Gtk::Viewport*    m_page_viewport;

	VerifyControl*    m_verify_control;	// optional answer verification entry 

	Gtk::Widget*      m_toolbar_go_first;
	Gtk::Widget*      m_toolbar_go_prev;
	Gtk::Widget*      m_toolbar_go_next;
	Gtk::Widget*      m_toolbar_go_last;

	answers_type      m_toolbar_answers_ok;
	answers_type      m_toolbar_answers_bad;

	answers_type      m_sidebar_answers_ok;
	answers_type      m_sidebar_answers_bad;

	Glib::RefPtr<Gdk::Pixbuf> m_play_sound_xpm;	// Sound icon

	VDeck&                   m_deck;
	VDeck::cardlist_iterator m_deck_iter; // Position in the Deck

	bool m_is_lesson_learned;	// Have user gone through the deck?
	bool m_is_real_deck;		// Is it a real Deck we are dealing with?

	Glib::RefPtr<Gtk::UIManager>   m_uimanager;
	Glib::RefPtr<Gtk::ActionGroup> m_actgroup;
	Glib::RefPtr<Gtk::ActionGroup> m_actgroup_extra;

	bool m_tab_activated;		// Leave Tab+Spacebar/Enter event to Dialog
	bool m_initialized;	        // Has the object been fully constructed?

	Gtk::HBox*     m_enclosure_hbox;  // The outer box (sidebar + m_vbox)
	Gtk::VBox*     m_vbox;            // Inner box (notebook + controls)

	Gtk::HBox*     m_navigation_controls_hbox;
	Gtk::EventBox* m_nav_ctrl_evbox;

	Gtk::HBox*     m_extra_controls_hbox;
	Gtk::HBox*     m_stats_info_hbox;

	Gtk::Toolbar*  m_answers_toolbar;  /** A collection of answer buttons 
										*  0-5 embedded inside bottom toolbar.*/
	Gtk::Toolbar*  m_bottom_ctrl_toolbar;

 	Gtk::Toolbar*  m_ctrl_sidebar;     /** A full-screen optional sidebar */
	Gtk::VBox*     m_ctrl_sidebar_vbox;

	Gtk::ScrolledWindow* m_front_scrollw;
	Gtk::ScrolledWindow* m_back_scrollw;

    /** Hide/Show VerifyControl toolbar.
	 */
	Gtk::ToggleToolButton* m_vc_toolbar_button; 

    /** Hide/Show statistics toolbar.
	 */
	Gtk::ToggleToolButton* m_stats_toolbar_button; 

    /** Hide/Show vertical sidebar.
	 */
	Gtk::ToggleToolButton* m_sbar_toolbar_button; 

	/** Tracks the state of the fullscreen mode.
	 */
	bool m_fullscreen_mode;

};


/**---------------------------------------------------------------------------**
 * Inline functions                                                            *
 **---------------------------------------------------------------------------**
 */

/**
 * Madness I have to live with just for the sake of nokia 770
 */
inline bool
DeckPlayer::
check_F7_key (gint keyval_)
{
#ifdef IS_HILDON
	return (keyval_ == GDK_F7);
#else
	return false;
#endif
}

inline bool
DeckPlayer::
check_F8_key (gint keyval_)
{
#ifdef IS_HILDON
	return (keyval_ == GDK_F8);
#else
	return false;
#endif
}

#endif /* DECK_PLAYER_H */
