// -*- c++ -*-
//------------------------------------------------------------------------------
//                              FileOpen.h
//------------------------------------------------------------------------------
// $Id: FileOpen.h,v 1.3 2004/02/17 04:10:56 vlg Exp $
//------------------------------------------------------------------------------
#ifndef FILE_OPEN_H
#define FILE_OPEN_H

#include <gtkmm/fileselection.h>

template<class CardBox>
class FileOpen : public Gtk::FileSelection
{
public:
    FileOpen (CardBox* cb_);

protected:
    void ok_cb ();
    void cancel_cb ();

private:
    FileOpen (const FileOpen&);	// cloning is forbidden
    FileOpen& operator= (const FileOpen&);

private:
    CardBox* m_card_box;
};

template<class CardBox>
FileOpen<CardBox>::
FileOpen (CardBox* cb_) : 
	Gtk::FileSelection ("Open Project"),
	m_card_box (cb_) 
{ 
	trace_with_mask("FileOpen::FileOpen",GUITRACE);
		
	/** clicked () is an 'emittable signal' defined in Gtk::Button class
	 */
	get_ok_button ()->
		signal_clicked ().connect (
			slot (*this, &FileOpen<CardBox>::ok_cb));
	
	/** hide() is an 'emittable signal' defined in Gtk::Widget class
	 */
	get_cancel_button ()->
		signal_clicked ().connect (
			slot (*this, &FileOpen<CardBox>::cancel_cb));
	
	/** Set up file filter based on extention
	 */
	complete ("*.xml");
	show_all ();
}

template<class CardBox> void
FileOpen<CardBox>::
ok_cb () 
{
	trace_with_mask("FileOpen::ok_cb",GUITRACE);
	
	m_card_box->load (get_filename ());
	hide ();
}

template<class CardBox> void
FileOpen<CardBox>::
cancel_cb () 
{
	trace_with_mask("FileOpen::cancel_cb",GUITRACE);
	hide ();
}

#endif /* FILE_OPEN_H */
