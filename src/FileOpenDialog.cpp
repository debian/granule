// -*- c++ -*-
//------------------------------------------------------------------------------
//                               FileOpenDialog.cpp
//------------------------------------------------------------------------------
// $Id: FileOpenDialog.cpp,v 1.1 2007/12/02 00:31:24 vlg Exp $
//------------------------------------------------------------------------------
//  Copyright (c) 2006 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version
//  2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------

#include "GrappConf.h"
#include "FileOpenDialog.h"

/**
 * Filenames are always returned in the character set specified by the 
 * G_FILENAME_ENCODING environment variable (local encoding).
 *
 * This means that while you can pass the result of FileChooser::get_filename()
 * to open(2) or fopen(3), you may not be able to directly set it as the text 
 * of a Gtk::Label widget unless you convert it first to UTF-8, which all 
 * gtkmm widgets expect. You should use Glib::filename_to_utf8() to convert 
 * filenames into strings that can be passed to gtkmm widgets.
 *
 * Note:
 *  The gtkmm FileChooser API is broken in that methods return Glib::ustring 
 *  even though the returned string is not necessarily UTF-8 encoded. 
 *  Any FileChooser method that takes or returns a filename (not a URI) should 
 *  have std::string as parameter or return type. Fortunately this mistake 
 *  doesn't prevent you from handling filenames correctly in your application. 
 *  Just pretend that the API uses std::string and call 
 *  Glib::filename_to_utf8() or Glib::filename_from_utf8() as appropriate.
 */
FileOpenDialog::
FileOpenDialog (const Glib::ustring& title_,
				Gtk::Widget*         parent_,
				const Glib::ustring& filter_name_,
				const Glib::ustring& filter_pattern_)
{
#ifdef OBSOLETE

	m_dialog = new Gtk::FileSelection (title_);
	m_dialog->hide_fileop_buttons ();
	m_dialog->set_filename (CONFIG->get_filesel_path ());

#else /* Desktop, nokia770 */

    m_dialog = new Gtk::FileChooserDialog (title_, 
										   Gtk::FILE_CHOOSER_ACTION_OPEN);
	int counter = 0;
	Granule::hide_fcd_gtk_labels (GTK_CONTAINER (m_dialog->gobj ()), counter);

    m_dialog->add_button (Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
    m_dialog->add_button ("Select",           Gtk::RESPONSE_OK);
    m_dialog->set_current_folder (CONFIG->get_filesel_path ());
    m_dialog->set_local_only ();

	if (filter_name_.length () > 0) 
	{
		Gtk::FileFilter filter;
		filter.set_name (filter_name_);
		filter.add_pattern (filter_pattern_);
		m_dialog->set_filter (filter);
	}
#endif

#ifdef IS_HILDON
//    m_dialog->set_transient_for (*HILDONAPPWIN);
    m_dialog->set_transient_for ((Gtk::Window&)*parent_);
#else
    m_dialog->set_transient_for ((Gtk::Window&)*parent_);
#endif

}

std::vector<Glib::ustring>
FileOpenDialog::
get_filenames () const
{
#ifdef OBSOLETE
	// not implemented, but can be via
	//
	// Glib::ArrayHandle<std::string> Gtk::FileSelection::get_selections()
	//
#else
	return (m_dialog->get_filenames ());
#endif
}
	

