// -*- c++ -*-
//------------------------------------------------------------------------------
//                            ScheduleDB.cpp
//------------------------------------------------------------------------------
// $Id: ScheduleDB.cpp,v 1.7 2008/08/19 02:52:46 vlg Exp $
//------------------------------------------------------------------------------
//  Copyright (c) 2007 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date   : Sun Dec  9 2007
//
//------------------------------------------------------------------------------

#include "ScheduleDB.h"
#include "Granule.h"

#include <math.h>
#include <sstream>

using ASSA::IniFile;

/*==============================================================================
 * ScheduleDB static member 
 *==============================================================================
 */
const float ScheduleDB::EF_MIN        =   1.30;
const float ScheduleDB::EF_MAX        =   2.50;

const int   ScheduleDB::BOX_ONE_STEP  =   1;
const int   ScheduleDB::BOX_TWO_STEP  =   6;

const int   ScheduleDB::DAYS_IN_YEAR  = 365;
const int   ScheduleDB::DAYS_IN_MONTH =  30;
const int   ScheduleDB::DAYS_IN_WEEK  =   7;

float
ScheduleDB::
ef_next (float efactor_, int quality_)
{
	float result;

	result = efactor_ + ef_coefficient (quality_);

	/*= Keep EF(n+1) in the valid range.
	 */
	if (fabsf (result - EF_MIN) < 0.01) {
		result = EF_MIN;
	}

	if (fabsf (result - EF_MAX) < 0.01) {
		result = EF_MAX;
	}

	return result;
}

float
ScheduleDB::
ef_coefficient (int quality_) 
{
	float result;

	Assure_exit (quality_ >= 0 && quality_ <= 5);
	result = (0.1 - (5 - quality_) * (0.08 + (5 - quality_) * 0.02));

	if (fabsf (result) < 0.01) {
		result = 0;
	}

	return result;
}

/*==============================================================================
 * ScheduleDB member functions
 *==============================================================================
 */
void
ScheduleDB::
rebuild_schedule ()
{
	trace_with_mask("ScheduleDB::rebuild_schedule",GUITRACE);

	ScheduleUI::interval_type prev = 0;
	ScheduleUI::interval_type next = 0;

	m_box_intervals.clear ();

	m_box_intervals.push_back (m_interval_1);
	m_box_intervals.push_back (m_interval_2);

	prev = m_interval_2;
	for (u_int i = 3; i <= m_capacity; i++) {
		next = (ScheduleUI::interval_type) (prev * m_efactor);
		DL ((ASSA::APP,"step %d, interval : %d\n", i, next));
		m_box_intervals.push_back (next);
		prev = next;
	}
}

bool
ScheduleDB::
set_capacity (size_t new_size_)
{
	trace_with_mask("ScheduleDB::set_capacity",GUITRACE);

	DL ((ASSA::APP,"Requested new capacity = %d\n", new_size_));

	if (new_size_ == m_capacity) {
		DL ((ASSA::APP,"No change in capacity!\n"));
		return true;
	}

	if (new_size_ < CARDBOXES_MIN) {
		DL ((ASSA::APP,"Size requested is smaller then minimum allowable.\n"));
		return false;
	}

	if (new_size_ > CARDBOXES_MAX) {
		DL ((ASSA::APP,"Size requested is greater then maximum allowable.\n"));
		return false;
	}

	m_capacity = new_size_;
	return true;
}

/*= How do you preserve the range?
 */
bool
ScheduleDB::
set_efactor (float efactor_)
{
	trace_with_mask("ScheduleDB::set_efactor",GUITRACE);

	DL ((ASSA::APP,"Requested new efactor = %5.2f\n", efactor_));

	float delta = fabsf (efactor_ - m_efactor);

	if (delta < 0.01) {
		DL ((ASSA::APP,"No change in EFactor!\n"));
		return true;
	}

	if (efactor_ > EF_MAX && delta > 0.01) {
		DL ((ASSA::APP,"EFactor value exceed upper limit!\n"));
		return false;
	}

	if (efactor_ < EF_MIN && delta > 0.01) {
		DL ((ASSA::APP,"EFactor value is less then lower limit!\n"));
		return false;
	}

	m_efactor = efactor_;
	return true;
}

/*=
 * Update View with data from Model
 */
void
ScheduleDB::
update_view (ScheduleUI& view_)
{
	trace_with_mask("ScheduleDB::update_view",GUITRACE);

	ScheduleUI::ViewRowData vrow;
	int index = 0;

	view_.efactor  (m_efactor);
	view_.capacity (m_capacity);

	DL ((DECK,"m_box_intervals.size() = %d\n", m_box_intervals.size ()));

	collection_iter iter = m_box_intervals.begin ();
	while (iter != m_box_intervals.end ()) {
		fill_view_row (index, vrow);
		view_.push_back (vrow);
		index++;
		iter++;
	}
}
	
/*=
 * Store View configuration in Model.
 * The only things we care about are the EFactor and Capacity.
 */
void
ScheduleDB::
fetch_from_view (ScheduleUI& view_)
{
	trace_with_mask("ScheduleDB::fetch_from_view",GUITRACE);

	m_efactor  = view_.efactor ();
	m_capacity = view_.capacity ();
	rebuild_schedule ();
}

bool
ScheduleDB::
format_interval (int index_, 
				 ScheduleUI::interval_type& interval_,
				 string& years_, 
				 string& months_, 
				 string& weeks_, 
				 string& days_)
{
	trace_with_mask("ScheduleDB::format_interval",GUITRACE);

	ScheduleUI::ViewRowData view_row;

	if (!fill_view_row (index_, view_row)) {
		return false;
	}

	view_row.to_str (years_, months_, weeks_, days_);
	interval_ = view_row.interval ();
	DL ((ASSA::APP,"index = %d : interval = %d\n", index_, interval_));

	return true;
}

/*=
 * Fill ViewRowData structure with data to show.
 */
bool
ScheduleDB::
fill_view_row (int index_, ScheduleUI::ViewRowData& vrow_)
{
	trace_with_mask("ScheduleDB::fill_view_row",GUITRACE);

	ScheduleUI::interval_type tmp;

	if ((u_int) index_ >= capacity ()) {
		DL ((ASSA::APP,"CardBox index %d is out of range!\n", index_));
		return false;
	}
	
	vrow_.m_index = index_ + 1;

	tmp = m_box_intervals [index_];
	vrow_.m_interval = tmp;

	DL ((ASSA::APP,"CardBox[%d].interval = %d\n", index_+1, vrow_.m_interval));

	vrow_.m_years = vrow_.m_interval / DAYS_IN_YEAR;
	tmp = vrow_.m_interval - vrow_.m_years * DAYS_IN_YEAR;

	vrow_.m_months = tmp / DAYS_IN_MONTH;
	tmp = tmp - vrow_.m_months * DAYS_IN_MONTH;

	vrow_.m_weeks = tmp / DAYS_IN_WEEK;
	vrow_.m_days = tmp - vrow_.m_weeks * DAYS_IN_WEEK;

	return true;
}

/**
 * To maintain backward compatability, a missing element or its attribute 
 * is a non-fatal event. The caller should log the error(s), 
 * but proceed anyway.
 */
bool
ScheduleDB::
load_from_xml_config (xmlDocPtr parser_, string& error_msg_)
{
	trace_with_mask("ScheduleDB::load_from_xml_config",GUITRACE);

	xmlChar* result;
	size_t   tmp;
	bool     ret = true;
	
	error_msg_ = "";

	result = Granule::get_node_by_xpath (parser_, 
										 "/cardfile/schedule/@efactor");
	if (result == NULL) {
		error_msg_ = "Missing 'efactor' attribute. ";
		ret = false;
	}
	else {
		m_efactor = float (atof ((const char*) result));
	}

	result = Granule::get_node_by_xpath (parser_, 
										 "/cardfile/schedule/@capacity");
	if (result == NULL) {
		error_msg_ += "Missing 'apacity' attribute. ";
		ret = false;
	}
	else {
		tmp = atoi ((const char*) result);

		if (tmp < CARDBOXES_MIN || tmp > CARDBOXES_MAX) {
			error_msg_ += "Attribute 'capacity' is out of valid range. ";
			ret = false;
		}
		else {
			m_capacity = tmp;
		}
	}

	result = Granule::get_node_by_xpath (parser_, 
										 "/cardfile/schedule/@interval_1");
	if (result == NULL) {
		error_msg_ += "Missing 'interval_1' attribute. ";
		ret = false;
	}
	else {
		tmp = atoi ((const char*) result);
		if (tmp < 1 || tmp > 5) {
			error_msg_ += "Attribute 'interval_1' is out of valid range. ";
			ret = false;
		}
		else {
			m_interval_1 = tmp;
		}
	}

	result = Granule::get_node_by_xpath (parser_, 
										 "/cardfile/schedule/@interval_2");
	if (result == NULL) {
		error_msg_ += "Missing 'interval_2' attribute. ";
		ret = false;
	}
	else {
		tmp = atoi ((const char*) result);
		if (tmp < 1 || tmp > 10) {
			error_msg_ += "Attribute 'interval_2' is out of valid range. ";
			ret = false;
		}
		else {
			m_interval_2 = tmp;
		}
	}

	rebuild_schedule ();
	return ret;
}

void
ScheduleDB::
save_to_xml_config (xmlTextWriterPtr writer_)
{
	trace_with_mask("ScheduleDB::save_to_xml_config",GUITRACE);

	char buf [32];
	int ret = 0;

	ret = xmlTextWriterStartElement (writer_, BAD_CAST "schedule");

	snprintf (buf, sizeof(buf), "%.2f", m_efactor);
	ret = xmlTextWriterWriteAttribute (writer_, BAD_CAST "efactor",
									   BAD_CAST buf);

	snprintf (buf, sizeof(buf), "%d", m_capacity);
	ret = xmlTextWriterWriteAttribute (writer_, BAD_CAST "capacity",
									   BAD_CAST buf);

	snprintf (buf, sizeof(buf), "%d", m_interval_1);
	ret = xmlTextWriterWriteAttribute (writer_, BAD_CAST "interval_1",
									   BAD_CAST buf);

	snprintf (buf, sizeof(buf), "%d", m_interval_2);
	ret = xmlTextWriterWriteAttribute (writer_, BAD_CAST "interval_2",
									   BAD_CAST buf);

	ret = xmlTextWriterEndElement (writer_);	// @schedule
}

/**
 * Based on the scheduled repetition interval of the CardRef, 
 * find the box it should be moved to.
 *
 * @param interval_ Rescheduling interval in days.
 * @return The index number of the CardDeck.
 */
int
ScheduleDB::
find_next_box (int interval_)
{
	trace_with_mask("ScheduleDB::find_next_box",GUITRACE);

	int box_index = 0;

	collection_iter iter = m_box_intervals.begin ();
	while (iter != m_box_intervals.end ()) {
		if (*iter >= (u_int) interval_) {
			break;
		}
		iter++;
		box_index++;
	}

	if ((u_int) box_index >= capacity ()) {
		box_index--;
	}

	return box_index;
}
