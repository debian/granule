// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: DeckList.cpp,v 1.27 2008/02/04 04:02:20 vlg Exp $
//------------------------------------------------------------------------------
//                            DeckList.cpp
//------------------------------------------------------------------------------
//  Copyright (c) 2004,2006 by Vladislav Grinchenko 
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date   : Mar  1 2004
//
//------------------------------------------------------------------------------

#include <gdkmm/dragcontext.h>

#include "Granule-main.h"
#include "DeckList.h"
#include "Deck.h"
#include "DeckManager.h"
#include "TopMenuBar.h"
#include "Granule.h"
#include "MainWindow.h"

using sigc::mem_fun;

//------------------------------------------------------------------------------
// Static functions
//
string
DeckList::
get_bare_name (const string& fname_)
{
    string tmp (fname_);
	string::size_type idx = tmp.rfind (G_DIR_SEPARATOR);
	if (idx != string::npos) {
		tmp.replace (0, idx+1, "");
	}
	idx = tmp.rfind ('.');
	if (idx != string::npos) {
		tmp.replace (idx, tmp.size (), "");
	}
	return tmp;
}

//------------------------------------------------------------------------------
// Data members
//
DeckList::
DeckList () : m_selected_deck (NULL)
{
	trace_with_mask("DeckList::DeckList", GUITRACE);	

	set_shadow_type (Gtk::SHADOW_ETCHED_OUT);
	set_policy (Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);

	m_list_store_ref = Gtk::ListStore::create (m_columns);

	m_tree_view.set_model (m_list_store_ref);
	m_tree_view.set_rules_hint ();
	m_tree_view.set_headers_clickable (false);
	m_tree_view.set_headers_visible (true);

	int cnt = m_tree_view.append_column ("Decks", m_columns.m_name);
	Gtk::TreeViewColumn* column = m_tree_view.get_column (cnt - 1);
	column->set_sizing (Gtk::TREE_VIEW_COLUMN_FIXED);
	column->set_resizable ();
	
	/** Enable column sorting
	 */
	column->set_sort_column (m_columns.m_name);
	column->set_sort_indicator (true);

	m_tree_sel_ref = m_tree_view.get_selection ();
	m_tree_sel_ref->set_mode (Gtk::SELECTION_SINGLE);
	m_tree_sel_ref->signal_changed ().connect (
		mem_fun (*this, &DeckList::on_name_selected));

	m_tree_view.add_events (Gdk::BUTTON_PRESS_MASK);

	m_tree_view.signal_button_press_event ().connect (
		mem_fun (*this, &DeckList::on_mouse_click), false);

	m_tree_view.signal_key_press_event ().connect (
		mem_fun (*this, &DeckList::on_key_tapped), false);

#ifdef IS_HILDON
	m_tree_view.signal_row_activated ().connect (
		mem_fun (*this, &DeckList::on_row_activated), false);
#endif

	/** Enable DeckList rows as DND source. Target entry of the source
		and destination must match (i.e. "STRING"->"STRING"). On a related note,
		"text/uri-list" target entry should work on Windows as well.
	 */
	std::list<Gtk::TargetEntry> list_targets;
	list_targets.push_back (Gtk::TargetEntry ("STRING"));
	list_targets.push_back (Gtk::TargetEntry ("text/plain"));

	m_tree_view.enable_model_drag_source (list_targets);

	m_tree_view.signal_drag_data_get ().connect (
		mem_fun (*this, &DeckList::on_drag_data_get));

	m_tree_view.signal_drag_begin ().connect (
		mem_fun (*this, &DeckList::on_drag_begin));

	/** Pack it up.
	 */
	add (m_tree_view);
}

void
DeckList::
init_popup_menu ()
{
	trace_with_mask("DeckList::init_popup_menu", GUITRACE);

	/** Add Popup Menu dialog
	 */
	MenuList& d = m_popup_menu.items ();

	d.push_back (MenuElem ("Insert from Deck",
						   mem_fun (*DECKMGR, &DeckManager::insert_deck_cb)));

	d.push_back (SeparatorElem ()); /*-----------------------------------*/

	d.push_back (MenuElem ("Import from CSV",
						   mem_fun (*DECKMGR, &DeckManager::import_deck_cb)));
	d.push_back (MenuElem ("Export to CSV",
						   mem_fun (*DECKMGR, &DeckManager::export_deck_cb)));

	d.push_back (SeparatorElem ()); /*-----------------------------------*/

	d.push_back (MenuElem ("Save", 
						   mem_fun (*DECKMGR, &DeckManager::save_deck_cb)));
	d.push_back (MenuElem ("Save As",
						   mem_fun (*DECKMGR, &DeckManager::save_as_deck_cb)));

	d.push_back (SeparatorElem ()); /*-----------------------------------*/

	d.push_back (MenuElem ("Play/Edit", 
						   mem_fun (*DECKMGR, &DeckManager::play_deck_cb)));

	/**----------------------------
	 *  [Deck]->[Add2Box] SubMenu
	 */
	Menu* add2box_menu = manage (new Menu ());
	MenuList& a2bp = add2box_menu->items ();

	for (int j = 0; j < CARDBOXES_MIN; j++) {
		std::ostringstream os;
		os << "Card box " << (j + 1);
		a2bp.push_back (MenuElem (os.str (), 
								  sigc::bind<int>(
						mem_fun(*DECKMGR, &DeckManager::add_deck_to_box), j)));
	}
	d.push_back (MenuElem ("Add to ", *add2box_menu));
	/*------------------------------
	 */
	d.push_back (MenuElem ("Unselect",
						   mem_fun (*MAINWIN, &MainWindow::unselect_deck_cb)));
}

void
DeckList::
push_back (Deck* deck_)
{
	trace_with_mask("DeckList::push_back(Deck)", GUITRACE);

	Gtk::TreeModel::iterator iter = m_list_store_ref->append ();
	Gtk::TreeModel::Row row = *iter;
	Glib::ustring fname = Glib::ustring (row [m_columns.m_name]);

	row [m_columns.m_name] = get_bare_name (deck_->get_name ());
	row [m_columns.m_deck_ref] = deck_;
}

string 
DeckList::
get_name (iterator iter_) const
{
	Gtk::TreeModel::Row row = *iter_;
	return row [m_columns.m_name];
}

Deck*
DeckList::
get_deck (iterator iter_)
{
	Gtk::TreeModel::Row row = *iter_;
	Deck* deck = row [m_columns.m_deck_ref];
	return deck;
}

void
DeckList::
on_name_selected (void)
{
	trace_with_mask("DeckList::on_name_selected", GUITRACE);

	Gtk::TreeModel::iterator iter = m_tree_sel_ref->get_selected ();
	if (iter) {
		Gtk::TreeModel::Path path = m_list_store_ref->get_path (iter);
		Gtk::TreeModel::Row row = *iter;
		m_selected_deck = row [m_columns.m_deck_ref];
		TOPMENUBAR->set_deck_sensitivity (true, size ());
	}
	else {
		DL ((GRAPP,"Row selection is empty!\n"));
	}
}

void
DeckList::
unselect ()
{
	trace_with_mask("DeckList::unselect", GUITRACE);

	Gtk::TreeModel::iterator iter = m_tree_sel_ref->get_selected ();
	if (iter) {
		m_tree_sel_ref->unselect (iter);
		m_selected_deck = NULL;
	}
	TOPMENUBAR->set_deck_sensitivity (false, size ());
}

void
DeckList::
refresh_selected_name ()
{
	trace_with_mask("DeckList::refresh_selected_name", GUITRACE);

	Gtk::TreeModel::iterator iter = m_tree_sel_ref->get_selected ();
	if (iter) {
		Gtk::TreeModel::Path path = m_list_store_ref->get_path (iter);
		Gtk::TreeModel::Row row = *iter;

		Deck* deck = row [m_columns.m_deck_ref];
		row [m_columns.m_name] = get_bare_name (deck->get_name ());
	}
}
		

/** Called when user starts dragging selected Deck from DeckList. 
	We replace TreeView's default drag icon with out own 
	which we borrowed from Nautilus. 
	Thanks to Crhis Vine <chris@cvine.freeserve.co.uk> for the hint.
 */
void
DeckList::
on_drag_begin (const Glib::RefPtr<Gdk::DragContext>& context_)
{
	trace_with_mask("DeckList::on_drag_begin", GUITRACE);

	int x_off;
	int y_off;
	int cell_width;
	int cell_height;
	int x_pos; 
	Gdk::Rectangle area;

	Gtk::TreeModel::iterator iter = m_tree_sel_ref->get_selected ();
	if (iter) {
		/** Get the cell height for vertical displacement.
		 */
		m_tree_view.get_column (0)->cell_get_size (area, x_off, y_off,
												   cell_width, cell_height);
		if (cell_height < 0) { 
			cell_height = 0;	
		}

        /* We always want the cursor to be over the icon */
		x_pos = 20;

		Glib::RefPtr<Gdk::Pixbuf> dnd_pixbuf =
			Granule::create_from_file(GRAPPDATDIR "/pixmaps/copy_deck.png");

		context_->set_icon (dnd_pixbuf, x_pos, cell_height/2);
	}
}

/** Also called when user starts dragging selected Deck from DeckList. 
 */
void
DeckList::
on_drag_data_get (const Glib::RefPtr<Gdk::DragContext>& context_, 
				  Gtk::SelectionData& selection_data_, 
				  guint type_id_, 
				  guint drag_timestamp_)
{
	trace_with_mask("DeckList::on_drag_data_get", GUITRACE);

	/** format - is the number of bits required to store one element 
		         of the target type.
        For some reason, passing around a pointer to m_selected_deck doesn't
		work - when it is retrieved by the CardDeck method, it is all garbled.
		So, set() call below is useless (investigate) - however, if we don't
		make this call, on_drop_data_get () will never get called.
	*/

	selection_data_.set (sizeof (Deck**),
						 (const guint8*) &m_selected_deck,
						 sizeof (Deck**));	

	/** Instead, use my own "clipboard" via MainWindow.
	 */
	MAINWIN->set_dnd_source (m_selected_deck);
}

/** 
 * This method is called when button is pressed on any of the rows.
 *
 * We filter out Right-Click to bring up a popup dialog or
 * double-click to bring DeckPlayer window.
 *
 * The rest of the events are handled by the default event
 * handler of TreeView afterwards.
*/
bool
DeckList::
on_mouse_click (GdkEventButton* event_)
{
	trace_with_mask("DeckList::on_mouse_click", GUITRACE);
	bool ret = false;

	if (event_->type == GDK_2BUTTON_PRESS 
#ifdef IS_HILDON
		|| event_->type == GDK_Escape // Hildon round button (Cancel,Close)
#endif
		) 
	{
		DECKMGR->play_deck_cb ();
	}
	else if (event_->type == GDK_BUTTON_PRESS && event_->button == 3) 
	{
		m_popup_menu.popup (event_->button, event_->time);
		/** 
		 * Returning 'true' results in DND behavior !?!
		 */
		ret = true;
	}

#ifdef IS_HILDON
	/** Somehow, the very first deck loaded is not selected
	 */
	on_name_selected ();
#endif

	/* Emmit our [clicked] signal
	 */
	m_signal_mouse_clicked.emit ();

	return ret;
}

DeckList::signal_mouse_clicked_type 
DeckList::
signal_mouse_clicked ()
{
	trace_with_mask("DeckList::signal_mouse_clicked", GUITRACE);
	return m_signal_mouse_clicked;
}

/**
 * Handler [Enter] key.
 *
 */
bool
DeckList::
on_key_tapped (GdkEventKey* event_)
{
	trace_with_mask("DeckList::on_key_tapped", GUITRACE);

	bool ret = false;

	DL ((GRAPP,"Key event: 0x%x\n", event_->type));
	DL ((GRAPP,"Key pressed: 0x%x\n", event_->keyval));

	if (event_->type == GDK_KEY_PRESS && event_->keyval == GDK_Return)
	{
		DECKMGR->play_deck_cb ();
		ret = true;
	}
		
	return ret;
}

/** 
 * Catch Select (round button) press event with a selected row
 */
#ifdef IS_HILDON
void
DeckList::
on_row_activated (const Gtk::TreePath& path_, Gtk::TreeViewColumn* column_)
{
	trace_with_mask("DeckList::on_row_activated", GUITRACE);

	DECKMGR->play_deck_cb ();
}
#endif	// IS_HILDON
