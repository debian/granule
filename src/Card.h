// -*- c++ -*-
//------------------------------------------------------------------------------
//                              Card.h
//------------------------------------------------------------------------------
// $Id: Card.h,v 1.18 2008/05/23 03:01:38 vlg Exp $
//
// Date: Jan 8, 2004
//------------------------------------------------------------------------------
#ifndef CARD_H
#define CARD_H

#include "VCard.h"
#include "Granule-main.h"

class Deck;

class Card : public VCard 
{
public:
	Card (Deck& container_);
	virtual ~Card ();

	virtual PathMode image_path_mode () const;

	virtual void set_question     (const ustring& q_);
	virtual void set_image_path   (const ustring& i_);
	virtual void set_answer       (const ustring& a_);
	virtual void set_example      (const ustring& e_);
	virtual void set_example_image_path   (const ustring& i_);
	virtual void set_alt_spelling (SideSelection side_, const ustring& a_);

	void set_id (long id_);

	virtual ustring get_question     () const { return m_question;     }
	virtual ustring get_deck_path    () const;
	virtual ustring get_image_path   () const { return m_image_path;   }
	virtual ustring get_answer       () const { return m_answer;       }
	virtual ustring get_example      () const { return m_example;      }
	virtual ustring get_example_image_path () const;
	virtual ustring get_alt_spelling (SideSelection side_) const;
	virtual long    get_id           () const { return m_id;           }

	virtual void dump () const;

protected:
	virtual bool less_then_expiration (const VCard& rhs_) { return true; }
	virtual bool equal_expiration     (const VCard& rhs_) { return true; }

private:
	long      m_id;
	Deck&     m_container;		// Reference back to enclosing container

	ustring   m_question;
	ustring   m_answer;
	ustring   m_example;

	ustring   m_front_alt_spelling;	 // (optional)
	ustring   m_back_alt_spelling;   // (optional)

	ustring   m_image_path;		// Relative image path (optional)
	ustring   m_example_image_path;
};

//------------------------------------------------------------------------------
// Implementation
//------------------------------------------------------------------------------
inline ustring 
Card::get_example_image_path () const 
{ 
	return m_example_image_path;
}

inline ustring
Card::get_alt_spelling (SideSelection side_) const 
{ 
	trace_with_mask("Card::get_alt_spelling",GUITRACE);

	ustring ret;
	if (side_ == ASF) {
//		DL ((GRAPP,"Return front.alt.spell = %s\n",
//			 m_front_alt_spelling.c_str ()));
		ret = m_front_alt_spelling;
	}
	else if (side_ == ASB) {
//		DL ((GRAPP,"Return back.alt.spell = %s\n",
//			 m_back_alt_spelling.c_str ()));
		ret = m_back_alt_spelling;
	}
	return ret;
		
#ifdef BLOCK
	return (side_ == ASF ? m_front_alt_spelling : m_back_alt_spelling);
#endif
}

inline void
Card::set_alt_spelling (SideSelection side_, const ustring& a_) 
{ 
//	trace_with_mask("Card::set_alt_spelling",GUITRACE);

	if (side_ == ASF) {
//		DL ((GRAPP,"Set front.alt.spell = %s\n", a_.c_str ()));
		m_front_alt_spelling = a_;
	}
	else if (side_ == ASB) {
//		DL ((GRAPP,"Set back.alt.spell = %s\n", a_.c_str ()));
		m_back_alt_spelling = a_;
	}
	mark_dirty (); 
}

inline void 
Card::set_question (const ustring& q_) 
{ 
	m_question = q_;
	mark_dirty (); 
}

inline void 
Card::set_image_path (const ustring& p_) 
{ 
	m_image_path = p_;
	mark_dirty (); 
}

inline void 
Card::set_example_image_path (const ustring& p_) 
{ 
	m_example_image_path = p_;
	mark_dirty (); 
}

inline void 
Card::set_answer (const ustring& a_) 
{ 
	m_answer = a_;
	mark_dirty (); 
}

inline void
Card::set_example (const ustring& e_) 
{ 
	m_example = e_;
	mark_dirty (); 
}

inline void
Card::set_id (long id_)
{ 
	m_id = id_;
	mark_dirty (); 
}

#endif /* CARD_H */
