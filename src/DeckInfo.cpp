// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: DeckInfo.cpp,v 1.37 2008/08/19 02:52:46 vlg Exp $
//------------------------------------------------------------------------------
//                            DeckInfo.cpp
//------------------------------------------------------------------------------
//  Copyright (c) Vladislav Grinchenko 
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date   : Mon Feb 16 22:08:26 EST 2004
//
//------------------------------------------------------------------------------

#ifdef HAVE_CONFIG_H
#    include "config.h"
#endif

#include <gtkmm/button.h>
#include <gtkmm/buttonbox.h>
#include <gtkmm/label.h>
#include <gtkmm/alignment.h>
#include <gtkmm/entry.h>
#include <gtkmm/table.h>
#include <gtkmm/box.h>
#include <gtkmm/stock.h>
#include <gtkmm/main.h>

#include "Granule-main.h"
#include "Granule.h"
#include "DeckInfo.h"
#include "Deck.h"
#include "GrappConf.h"
#include "FolderChooseDialog.h"

#include "TextAlignment.h"
#include "FontsSelectorUI.h"
#include "ColorsSelectorUI.h"
#include "ButtonWithImageLabel.h"

#include "Intern.h"

//------------------------------------------------------------------------------
// Useful Macros
//------------------------------------------------------------------------------

#define DI_SET_LABEL(l) \
	l->set_alignment  (0.5,0.5); \
	l->set_padding    (0,0); \
	l->set_justify    (Gtk::JUSTIFY_LEFT); \
	l->set_line_wrap  (false); \
	l->set_use_markup (false); \
	l->set_selectable (false);

#define DI_SET_ENTRY(e, t, d)      \
	e->set_flags (Gtk::CAN_FOCUS); \
	e->set_visibility (true);      \
	e->set_editable   (d);         \
	e->set_max_length (0);         \
    e->set_text       (t);         \
	e->set_has_frame  (true);      \
	e->set_activates_default (false);

#define DI_SET_FRAME(f,w) \
	f->set_shadow_type  (Gtk::SHADOW_ETCHED_IN); \
	f->set_border_width (4); \
	f->set_label_align  (1, 1); \
	f->add (*w);


#define DI_SET_CHECKBUTTON(b,a)		\
	b->set_flags  (Gtk::CAN_FOCUS); \
	b->set_relief (Gtk::RELIEF_NORMAL); \
	b->set_mode   (true); \
    b->set_active (a); \
	b->set_border_width (4); \
	b->set_alignment (0.1, 0);

//------------------------------------------------------------------------------
// Class DeckInfo
//------------------------------------------------------------------------------

DeckInfo::
DeckInfo (Gtk::Window& parent_, Deck& deck_) 
	:
	m_deck (deck_),
	m_author_entry   (Gtk::manage (new Gtk::Entry ())),
	m_desc_entry     (Gtk::manage (new Gtk::Entry ())),
	m_snd_path_entry (Gtk::manage (new Gtk::Entry ())),
	m_response (Gtk::RESPONSE_CANCEL),
	m_appearance_changed (false)
{  
	trace_with_mask("DeckInfo::DeckInfo", GUITRACE);

	Gtk::Table*  info_table;

	Gtk::Label*  author_label;
	Gtk::Label*  desc_label;
	Gtk::Label*  snd_label;
	
	Gtk::Alignment* author_alignment;
	Gtk::Alignment* desc_alignment;
	Gtk::Alignment* snd_label_alignment;

	Gtk::HBox*  snd_path_box;
	Gtk::VBox*  snd_vbox;
	Gtk::VBox*  snd_vbox2;
	Gtk::Frame* snd_frame;

	Gtk::VBox*  pic_vbox;
	Gtk::Frame* pic_frame;

	Gtk::Frame* app_frame;
	Gtk::VBox*  app_vbox;

	/** 
	 * Dialog box holds both vbox and action_area
	 */
	Gtk::VBox* dialog_box = manage (new Gtk::VBox (false, 2));
	add (*dialog_box);

    m_vbox = manage (new Gtk::VBox (false, 2));
	dialog_box->pack_start (*m_vbox, Gtk::PACK_EXPAND_WIDGET, 1);

	Gtk::HSeparator* hseparator = manage (new Gtk::HSeparator);
	dialog_box->pack_start (*hseparator, Gtk::PACK_SHRINK, 3);

	m_action_area = manage (new Gtk::HButtonBox);
    m_action_area->set_homogeneous ();
    m_action_area->set_spacing (8);
	m_action_area->set_layout (Gtk::BUTTONBOX_END);

	dialog_box->pack_start (*m_action_area, Gtk::PACK_SHRINK, 1);

	/** Set up the dialog itself
	 */
	set_title (_("Deck Preferences"));
	set_modal (true);

#ifdef IS_PDA
	set_resizable (false);
	Gdk::Geometry dp_geometry =	{ 240, 320,	240, 320, 240, 320,	-1, -1,	-1, -1};
	set_geometry_hints (*this, dp_geometry, 
				Gdk::HINT_MIN_SIZE | Gdk::HINT_MAX_SIZE | Gdk::HINT_BASE_SIZE);
#else

#ifdef IS_HILDON
	Gdk::Geometry dp_geometry =	{ 640, 480, 640, 480, 640, 480, -1, -1, -1, -1};
	set_geometry_hints (*this, dp_geometry, 
				Gdk::HINT_MIN_SIZE | Gdk::HINT_MAX_SIZE | Gdk::HINT_BASE_SIZE);
#else
	set_size_request (575, 548);	// width; height 
#endif	// @IS_HILDON

	set_resizable (true);
	set_transient_for (parent_);

#endif	// @IS_PDA

	/***********************
	 *  Control buttons
	 ***********************/
	m_cancel_button = manage (
		new ButtonWithImageLabel (_("<u>C</u>ancel"), "gtk-cancel"));

	m_apply_button  = manage (
		new ButtonWithImageLabel (_("<u>A</u>pply"), "gtk-apply"));

	m_cancel_button->set_alt_accelerator_key (GDK_c, get_accel_group ());
	m_apply_button ->set_alt_accelerator_key (GDK_a, get_accel_group ());

	/***********************
	 *  Author/Description
	 ***********************/
	info_table       = Gtk::manage (new Gtk::Table (2, 2, false));

	author_label     = Gtk::manage (new Gtk::Label (_("Author: ")));
	author_alignment = Gtk::manage (new Gtk::Alignment (Gtk::ALIGN_LEFT,
														Gtk::ALIGN_CENTER,
														0.2, 0));
	desc_label       = Gtk::manage (new Gtk::Label (_("Description: ")));
	desc_alignment   = Gtk::manage (new Gtk::Alignment (Gtk::ALIGN_LEFT,
														Gtk::ALIGN_CENTER,
														0.2, 0));
	/*************************
	 * Sound Dictionary Path
	 *************************/
	snd_label           = Gtk::manage (new Gtk::Label (_("Path: ")));
	snd_label_alignment = Gtk::manage (new Gtk::Alignment (Gtk::ALIGN_LEFT,
														   Gtk::ALIGN_CENTER,
														   0.2, 0));

	m_snd_browse_button = Gtk::manage (new Gtk::Button (_("Browse")));
	snd_path_box        = Gtk::manage (new Gtk::HBox (false, 2));
    snd_vbox            = Gtk::manage (new Gtk::VBox (false, 2));
	snd_vbox2           = Gtk::manage (new Gtk::VBox (false, 2));
	snd_frame           = Gtk::manage (new Gtk::Frame ());
	m_enable_alt_snd    = Gtk::manage (new Gtk::CheckButton (
									 _("Enable alternative sound dictionary")));
	m_enable_rel_snd_path = Gtk::manage (new Gtk::CheckButton (
									 _("Record sound path as relative")));

	/************************
	 * Picture Path
	 ************************/
	pic_vbox               = Gtk::manage (new Gtk::VBox (false, 2));
	pic_frame              = Gtk::manage (new Gtk::Frame ());
	m_enable_rel_pics_path = Gtk::manage (new Gtk::CheckButton (
									 _("Record picture paths as relative.")));

	/************************
	 * Custom Appearance
	 ************************/

	/** Enable checkbox
	 */
	m_enable_custom_app = Gtk::manage (new Gtk::CheckButton (
										   _("Enable custom appearance")));
	m_inner_app_vbox = Gtk::manage (new Gtk::VBox (false, 2));
	app_vbox         = Gtk::manage (new Gtk::VBox (false, 2));
	app_frame        = Gtk::manage (new Gtk::Frame ());

	/** Fonts
	 */
	m_fonts_widget = Gtk::manage (new FontsSelectorUI ());
	m_fonts_widget->set_active (IFONT, false);
	m_fonts_widget->set_active (PFONT, false);

	/** Colors
	 */
	m_colors_widget = Gtk::manage (new ColorsSelectorUI ());

	/** Text Alignment
	 */
	TextAlignmentsUI::value_list_t align_x_list;
	TextAlignmentsUI::value_list_t align_y_list;
	TextAlignmentsUI::value_list_t paragraph_list;
	TextAlignmentsUI::value_list_t img_position_list;

	align_x_list      = TextAlignmentsUI::make_align_x_list      ();
	align_y_list      = TextAlignmentsUI::make_align_y_list      ();
	paragraph_list    = TextAlignmentsUI::make_paragraph_list    ();
	img_position_list = TextAlignmentsUI::make_img_position_list ();

	m_front_widget=new TextAlignmentsUI("<b>Front Text Alignment</b>",
										align_x_list,
										align_y_list,
										paragraph_list,
										img_position_list);

	m_back_widget=new TextAlignmentsUI("<b>Back Text Alignment</b>",
									   align_x_list,
									   align_y_list,
									   paragraph_list,
									   img_position_list, false);

	m_example_widget=new TextAlignmentsUI("<b>Example Text Alignment</b>",
										  align_x_list,
										  align_y_list,
										  paragraph_list,
										  img_position_list);

	/** Load data from the model
	 */
	load_appearance ();

	m_inner_app_vbox->pack_start (*m_fonts_widget,  Gtk::PACK_SHRINK, 4);
	m_inner_app_vbox->pack_start (*m_colors_widget, Gtk::PACK_SHRINK, 4);

	m_inner_app_vbox->pack_start (*(m_front_widget->frame ()), 
								  Gtk::PACK_SHRINK, 4);
	m_inner_app_vbox->pack_start (*(m_back_widget->frame ()), 
								  Gtk::PACK_SHRINK, 4);
	m_inner_app_vbox->pack_start (*(m_example_widget->frame ()), 
								  Gtk::PACK_SHRINK, 4);

	/*******************
	 * Control buttons
	 *******************/
	m_cancel_button->set_flags  (Gtk::CAN_FOCUS);
	m_cancel_button->set_relief (Gtk::RELIEF_NORMAL);

	m_apply_button->set_flags  (Gtk::CAN_FOCUS);
	m_apply_button->set_relief (Gtk::RELIEF_NORMAL);

#ifdef GLIBMM_PROPERTIES_ENABLED
	get_action_area ()->property_layout_style ().set_value (Gtk::BUTTONBOX_END);
#else
	get_action_area ()->set_property ("layout_style", Gtk::BUTTONBOX_END);
#endif

	DI_SET_LABEL(author_label);
	author_alignment->add (*author_label);

	DI_SET_LABEL(desc_label);
	desc_alignment->add (*desc_label);

	DI_SET_ENTRY(m_author_entry, m_deck.get_author (), true);
	DI_SET_ENTRY(m_desc_entry,   m_deck.get_desc   (), true);

	info_table->set_row_spacings (1);
	info_table->set_col_spacings (1);

	/** Default [xy]options is Gtk::FILL|Gtk::EXPAND
	 */
	info_table->attach (*author_alignment, 0, 1, 0, 1, Gtk::FILL);
	info_table->attach (*desc_alignment,   0, 1, 1, 2, Gtk::SHRINK);
	info_table->attach (*m_author_entry,   1, 2, 0, 1);
	info_table->attach (*m_desc_entry,     1, 2, 1, 2);

	/****************
	 *  Setup sound
	 ****************/
	DI_SET_LABEL(snd_label);
	snd_label_alignment->add (*snd_label);

	std::string snd_path = m_deck.get_snd_path ();

	if (snd_path.length () > 0) 
	{
		DI_SET_CHECKBUTTON(m_enable_alt_snd,true);
		DI_SET_ENTRY(m_snd_path_entry, snd_path, true);
		m_snd_browse_button->set_sensitive (true);
		if (m_deck.snd_path_mode () == PATH_RELATIVE) 
		{
			DI_SET_CHECKBUTTON(m_enable_rel_snd_path, true);
		}
		else {
			DI_SET_CHECKBUTTON(m_enable_rel_snd_path, false);
		}
	}
	else {
		DI_SET_CHECKBUTTON(m_enable_alt_snd,false);
		m_snd_browse_button->set_sensitive (false);
		DI_SET_CHECKBUTTON(m_enable_rel_snd_path,false);
	}

	m_snd_browse_button->set_flags  (Gtk::CAN_FOCUS);
	m_snd_browse_button->set_relief (Gtk::RELIEF_NORMAL);

	Gtk::Table* snd_table = Gtk::manage (new Gtk::Table (2, 2, false));
	Gtk::Label* filler_label = Gtk::manage (new Gtk::Label ("        "));

	/** attach (widget, left, right, top, bottom)
	 */
	snd_table->attach (*filler_label, 0, 1, 0, 1, 
					   Gtk::FILL|Gtk::SHRINK, Gtk::FILL, 0, 0);

	snd_path_box->pack_start (*snd_label_alignment, Gtk::PACK_SHRINK, 6);
	snd_path_box->pack_start (*m_snd_path_entry,    Gtk::PACK_EXPAND_WIDGET, 6);
	snd_path_box->pack_start (*m_snd_browse_button, Gtk::PACK_SHRINK,        2);

	snd_table->attach (*snd_path_box, 1, 2, 0, 1,
					   Gtk::EXPAND|Gtk::FILL, Gtk::FILL, 0, 0);

	filler_label = Gtk::manage (new Gtk::Label ("        "));

	snd_table->attach (*filler_label, 0, 1, 1, 2, 
					   Gtk::FILL|Gtk::SHRINK, Gtk::FILL, 0, 0);

	snd_table->attach (*m_enable_rel_snd_path, 1, 2, 1, 2,
					   Gtk::EXPAND|Gtk::FILL, Gtk::FILL, 0, 6);
	
	snd_vbox->pack_start (*m_enable_alt_snd, Gtk::PACK_EXPAND_WIDGET, 4);
	snd_vbox->pack_start (*snd_table, Gtk::PACK_SHRINK, 4);

	DI_SET_FRAME(snd_frame, snd_vbox);

	/********************
	 * Setup Pic Path   *
	 ********************
	 */
	if (m_deck.pics_path_mode () == PATH_RELATIVE) {
		DI_SET_CHECKBUTTON(m_enable_rel_pics_path, true);
	}
	else {
		DI_SET_CHECKBUTTON(m_enable_rel_pics_path, false);
	}

	pic_vbox->pack_start (*m_enable_rel_pics_path, Gtk::PACK_EXPAND_WIDGET, 4);
	DI_SET_FRAME(pic_frame, pic_vbox);

	/********************
	 * Setup Appearance *
	 ********************
	 */
	bool app_enabled = m_deck.with_custom_appearance ();
	DI_SET_CHECKBUTTON(m_enable_custom_app, app_enabled);
	m_inner_app_vbox->set_sensitive (app_enabled);

	app_vbox->pack_start (*m_enable_custom_app, Gtk::PACK_EXPAND_WIDGET, 4);
	app_vbox->pack_start (*m_inner_app_vbox,    Gtk::PACK_SHRINK,        4);

	DI_SET_FRAME(app_frame, app_vbox);

	/********************
	 * Pack all up      *
	 ********************
	 */
	Gtk::VBox* vbox_all = Gtk::manage (new Gtk::VBox (false, 0));

	vbox_all->pack_start (*info_table, Gtk::PACK_SHRINK, 4);
	vbox_all->pack_start (*snd_frame,  Gtk::PACK_SHRINK, 4);
	vbox_all->pack_start (*pic_frame,  Gtk::PACK_SHRINK, 4);
	vbox_all->pack_start (*app_frame,  Gtk::PACK_SHRINK, 4);

	vbox_all->set_homogeneous (false);
	vbox_all->set_spacing (0);

	Gtk::ScrolledWindow* scrollw = Gtk::manage (new Gtk::ScrolledWindow);
	scrollw->set_flags (Gtk::CAN_FOCUS);
	scrollw->set_shadow_type (Gtk::SHADOW_NONE);
	scrollw->set_policy (Gtk::POLICY_AUTOMATIC , Gtk::POLICY_AUTOMATIC);

#ifdef GLIBMM_PROPERTIES_ENABLED
	scrollw->property_window_placement ().set_value (Gtk::CORNER_TOP_LEFT);
#else
	scrollw->set_property ("window_placement", Gtk::CORNER_TOP_LEFT);
#endif

	scrollw->add (*vbox_all);
	get_vbox ()->pack_start (*scrollw, Gtk::PACK_EXPAND_WIDGET, 0);

	get_action_area ()->pack_start (*m_cancel_button);
	get_action_area ()->pack_start (*m_apply_button);

	/************************
	 * Initialize callbacks *
	 ************************
	 */
 	signal_delete_event ().connect (
 		sigc::mem_fun (*this, &DeckInfo::on_delete_window_clicked));

	m_cancel_button->signal_clicked ().connect (
		sigc::mem_fun (*this, &DeckInfo::on_cancel_clicked));

	m_apply_button->signal_clicked ().connect (
		sigc::mem_fun (*this, &DeckInfo::on_apply_clicked));

	m_snd_browse_button->signal_clicked ().connect (
		sigc::mem_fun (*this, &DeckInfo::on_browse_clicked));

	m_enable_alt_snd->signal_toggled ().connect (
		sigc::mem_fun (*this, &DeckInfo::on_enable_snd_clicked));

	m_enable_rel_snd_path->signal_toggled ().connect (
		sigc::mem_fun (*this, &DeckInfo::on_enable_rel_snd_path_clicked));

	m_enable_custom_app->signal_toggled ().connect (
		sigc::mem_fun (*this, &DeckInfo::on_enable_app_clicked));

	for (int t = 0; t < FONT_ENTRY_SZ; t++) 
	{
	    m_fonts_widget->
			font_entry (TextFontType (t)).m_entry->signal_changed().connect (
				mem_fun (*this, &DeckInfo::font_changed_cb));
	}

	for (int t = 0; t < COLOR_ENTRY_SZ; t++) 
	{
	    m_colors_widget->
			color_entry (TextColorType (t)).m_entry->signal_changed().connect (
				mem_fun (*this, &DeckInfo::color_changed_cb));
	}

	show_all  ();
}

void
DeckInfo::
on_enable_snd_clicked ()
{
	trace_with_mask("DeckInfo::on_enable_snd_clicked", GUITRACE);

	bool b = m_enable_alt_snd->get_active ();
	m_snd_browse_button->set_sensitive (b);
	m_enable_rel_snd_path->set_sensitive (b);
}

void
DeckInfo::
on_enable_app_clicked ()
{
	trace_with_mask("DeckInfo::on_enable_app_clicked", GUITRACE);

	m_inner_app_vbox->set_sensitive (m_enable_custom_app->get_active ());
}

void
DeckInfo::
on_enable_rel_snd_path_clicked ()
{
	trace_with_mask("DeckInfo::on_enable_rel_snd_path_clicked", GUITRACE);

	DL ((GRAPP,"1\n"));
	Glib::ustring path_entry = m_snd_path_entry->get_text ();

	DL ((GRAPP,"2\n"));
	bool enabled = m_enable_rel_snd_path->get_active ();

	if (enabled) {
		DL ((GRAPP,"e-1\n"));
		string relpath = Granule::calculate_relative_path (
			m_deck.get_deck_path (), 
			Granule::filename_from_utf8 (m_snd_path_entry->get_text ()));

		DL ((GRAPP,"e-2\n"));
        m_snd_path_entry->set_text (relpath);
		DL ((GRAPP,"Enable relative snd path '%s'\n", relpath.c_str ()));
	}
	else {
		if (!path_entry.empty ()) {
			DL ((GRAPP,"noob-1\n"));
			string abspath = Granule::calculate_absolute_path (
				m_deck.get_deck_path (), path_entry);
			DL ((GRAPP,"Disable relative snd path.\n"));
			DL ((GRAPP,"New path '%s'\n", abspath.c_str ()));
			m_snd_path_entry->set_text (abspath);
		}
	}
}

/**
 * Open FileChooserDialog and let user select new sound path.
 * If path stored in path_entry is relative, merge it with the Deck's
 * own path to create an absolute path.
 */
void
DeckInfo::
on_browse_clicked ()
{
	trace_with_mask("DeckInfo::on_browse_clicked", GUITRACE);

	FolderChooseDialog f (_("Select Sound Dictionary"), this);
	Glib::ustring path = m_snd_path_entry->get_text ();
	string selection;


	if (!path.empty ()) 
	{
		if (m_enable_rel_snd_path->get_active ()) 
		{
			path = m_deck.get_deck_path () + G_DIR_SEPARATOR_S + path;
		}
		else {
			/** If path type is PATH_ABSOLUTE but the entry 
			 *  has relative path entered by the user (i.e. '.'),
			 *  we double check to make sure it is not.
			 *  If it is not absolute, we try to correct.
			 */
			selection = Granule::filename_from_utf8 (path);
			if (g_path_is_absolute (selection.c_str ())) {
				f.set_current_folder (selection + G_DIR_SEPARATOR_S);
			}
			else {	/* correction */
				string ap = Granule::calculate_absolute_path (
					m_deck.get_deck_path (), selection);
				f.set_current_folder (ap + G_DIR_SEPARATOR_S);
			}
		}
	}

    if (f.run () == Gtk::RESPONSE_OK) 
	{
		selection = Granule::filename_from_utf8 (f.get_filename ());
		if (m_enable_rel_snd_path->get_active ()) 
		{
			string rp = Granule::calculate_relative_path (
				m_deck.get_deck_path (), selection);
			m_snd_path_entry->set_text (rp);
		}
		else {
			m_snd_path_entry->set_text (selection);
		}
    }
}

void
DeckInfo::
on_apply_clicked ()
{
	trace_with_mask("DeckInfo::on_apply_clicked", GUITRACE);

	bool active;
	PathMode pmode;

	if (m_deck.get_author () != m_author_entry->get_text ()) {
		m_deck.set_author (m_author_entry->get_text ());
		m_deck.mark_as_modified ();
	}

	if (m_deck.get_desc () != m_desc_entry->get_text ()) {
		m_deck.set_desc (m_desc_entry->get_text ());
		m_deck.mark_as_modified ();
	}

	DL ((APP,"[%c] enable alt snd path\n", 
		 (m_enable_alt_snd->get_active () ? 'Y' : 'N')));

	if (m_enable_alt_snd->get_active () == false) {
		m_deck.set_snd_path ("");
		m_deck.snd_path_mode (PATH_ABSOLUTE);
		m_deck.mark_as_modified ();
	}
	else {
		active = m_enable_rel_snd_path->get_active ();
		pmode = active ? PATH_RELATIVE : PATH_ABSOLUTE;
		if (m_deck.get_snd_path () != m_snd_path_entry->get_text () ||
			m_deck.snd_path_mode () != pmode)
		{
			m_deck.snd_path_mode (pmode);
			m_deck.set_snd_path (m_snd_path_entry->get_text ());
			m_deck.mark_as_modified ();
		}
	}

	active = m_enable_rel_pics_path->get_active ();
	pmode = active ? PATH_RELATIVE : PATH_ABSOLUTE;

	if (m_deck.pics_path_mode () != pmode) {
		m_deck.pics_path_mode (pmode);
		m_deck.mark_as_modified ();
	}

	if (m_enable_custom_app->get_active () == false) {
		m_deck.set_custom_appearance (false);
	}
	else {
		m_deck.set_custom_appearance ();
		m_deck.mark_as_modified ();
	}

	save_appearance ();
	m_appearance_changed = true;

	m_response = Gtk::RESPONSE_APPLY;
	hide ();
	gtk_main_quit ();
}

/**
 * @return true - top other handlers from being invoked for the event.
 */
bool
DeckInfo::
on_delete_window_clicked (GdkEventAny* /*event_*/)
{
    trace_with_mask("DeckInfo::on_delete_window_clicked",GUITRACE);

	on_cancel_clicked ();
    return true;  
}

void
DeckInfo::
on_cancel_clicked ()
{
	trace_with_mask("DeckInfo::on_cancel_clicked", GUITRACE);

	m_response = Gtk::RESPONSE_CANCEL;
	hide ();
	gtk_main_quit ();
}

/**-----------------------------------------------------------------------------
 ** Alignment widgets.
 **-----------------------------------------------------------------------------
 */
void
DeckInfo::
load_appearance ()
{
	m_deck.fonts_db ().update_view (m_fonts_widget);
	m_deck.text_colors_db ().update_view (m_colors_widget);

	m_deck.app_db ().update_view (FRONT,   m_front_widget  );
	m_deck.app_db ().update_view (BACK,    m_back_widget   );
	m_deck.app_db ().update_view (EXAMPLE, m_example_widget);
}

void
DeckInfo::
save_appearance ()
{
	m_deck.fonts_db ().fetch_from_view (m_fonts_widget);
	m_deck.text_colors_db ().fetch_from_view (m_colors_widget);

	m_deck.app_db ().fetch_from_view (FRONT,   m_front_widget  );
	m_deck.app_db ().fetch_from_view (BACK,    m_back_widget   );
	m_deck.app_db ().fetch_from_view (EXAMPLE, m_example_widget);
}

int
DeckInfo::
run ()
{
	trace_with_mask("DeckInfo::run", GUITRACE);	

	show ();
	present ();
	gtk_main ();

	return m_response;
}
