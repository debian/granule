// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: ScheduleUI.cpp,v 1.6 2008/02/04 04:02:21 vlg Exp $
//------------------------------------------------------------------------------
//                            ScheduleUI.cpp
//------------------------------------------------------------------------------
//  Copyright (c) 2007 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date   : Wed Nov 1 2007
//
//------------------------------------------------------------------------------
#include <gtkmm/button.h>
#include <gtkmm/buttonbox.h>
#include <gtkmm/box.h>
#include <gtkmm/label.h>
#include <gtkmm/alignment.h>
#include <gtkmm/table.h>
#include <gtkmm/scrolledwindow.h>

#include <sstream>

#include "ScheduleUI.h"
#include "ScheduleDB.h"
#include "Granule.h"

#include "Intern.h"             // i18n macros

using sigc::mem_fun;
using sigc::bind;

void 
ScheduleUI::ViewRowData::
to_str (string& years_, 
		string& months_, 
		string& weeks_, 
		string& days_)
{
	std::ostringstream os;

	os << m_years;
	years_ = os.str ();

	os.str ("");
	os << m_months;
	months_ = os.str ();

	os.str ("");
	os << m_weeks;
	weeks_ = os.str ();

	os.str ("");
	os << m_days;
	days_ = os.str ();
}

ScheduleUI::
ScheduleUI () : m_property_modified (false)
{  
	trace_with_mask("ScheduleUI::ScheduleUI",GUITRACE);

	Gtk::Label* ef_label;
	Gtk::Label* capacity_label;
	Gtk::Alignment* frame_alignment;
	Gtk::Table* params_table;
	Gtk::VBox*  vbox_refbutton;
	Gtk::HBox*  hbox_upper;
	Gtk::VBox*  vbox_all;
	Gtk::Label* frame_label;

	ef_label        = Gtk::manage(new Gtk::Label(_("EFactor: ")));
	capacity_label  = Gtk::manage(new Gtk::Label(_("Capacity: ")));
	frame_alignment = Gtk::manage(new Gtk::Alignment(0, 0, 0.92, 0.93));
	vbox_refbutton  = Gtk::manage(new Gtk::VBox(false, 0));
	hbox_upper      = Gtk::manage(new Gtk::HBox(false, 5));
	vbox_all        = Gtk::manage(new Gtk::VBox(false, 6));
	frame_label     = Gtk::manage(new Gtk::Label(""));
	params_table    = Gtk::manage(new Gtk::Table(2, 2, false));

	/*= EFactor adjustment restricts input range [1.3; 2.5]
	 */
	m_ef_spinbutton_adj = Gtk::manage 
		(new Gtk::Adjustment (1.3, 1.3, 2.5, 0.1, 0.5, 10));

	m_ef_spinbutton = Gtk::manage 
		(new Gtk::SpinButton (*m_ef_spinbutton_adj, 1, 2));
   
	/*= Capacity adjustment restricts input range [5; 30]
	 */
	m_sz_spinbutton_adj = Gtk::manage 
		(new Gtk::Adjustment(10, 5, 30, 1, 2, 10));

	m_sz_spinbutton = Gtk::manage
		(new Gtk::SpinButton(*m_sz_spinbutton_adj, 1, 0));
   
	m_refresh_button = Gtk::manage(new Gtk::Button(_("Refresh")));
   
	m_frame = this;

	ef_label->set_alignment(0,0.5);
	ef_label->set_padding(0,0);
	ef_label->set_justify(Gtk::JUSTIFY_LEFT);
	ef_label->set_line_wrap(false);
	ef_label->set_use_markup(false);
	ef_label->set_selectable(false);
	ef_label->set_ellipsize(Pango::ELLIPSIZE_NONE);
	ef_label->set_width_chars(-1);
	ef_label->set_angle(0);
	ef_label->set_single_line_mode(true);

	capacity_label->set_alignment(0,0.5);
	capacity_label->set_padding(0,0);
	capacity_label->set_justify(Gtk::JUSTIFY_LEFT);
	capacity_label->set_line_wrap(false);
	capacity_label->set_use_markup(false);
	capacity_label->set_selectable(false);
	capacity_label->set_ellipsize(Pango::ELLIPSIZE_NONE);
	capacity_label->set_width_chars(-1);
	capacity_label->set_angle(0);
	capacity_label->set_single_line_mode(true);

	m_ef_spinbutton->set_flags(Gtk::CAN_FOCUS);
	m_ef_spinbutton->set_update_policy(Gtk::UPDATE_ALWAYS);
	m_ef_spinbutton->set_numeric(false);
	m_ef_spinbutton->set_digits(2);
	m_ef_spinbutton->set_wrap(false);
	m_sz_spinbutton->set_flags(Gtk::CAN_FOCUS);
	m_sz_spinbutton->set_update_policy(Gtk::UPDATE_ALWAYS);
	m_sz_spinbutton->set_numeric(true);
	m_sz_spinbutton->set_digits(0);
	m_sz_spinbutton->set_wrap(false);

	params_table->set_border_width(7);
	params_table->set_row_spacings(2);
	params_table->set_col_spacings(2);

	params_table->attach(*ef_label, 0, 1, 0, 1, 
						 Gtk::FILL, Gtk::AttachOptions(), 0, 0);
	params_table->attach(*capacity_label, 0, 1, 1, 2, 
						 Gtk::FILL, Gtk::AttachOptions(), 0, 0);
	params_table->attach(*m_ef_spinbutton, 1, 2, 0, 1, 
						 Gtk::FILL, Gtk::AttachOptions(), 0, 0);
	params_table->attach(*m_sz_spinbutton, 1, 2, 1, 2, 
						 Gtk::EXPAND|Gtk::FILL, Gtk::AttachOptions(), 0, 0);

	m_refresh_button->set_flags(Gtk::CAN_FOCUS);
	m_refresh_button->set_relief(Gtk::RELIEF_NORMAL);

	vbox_refbutton->pack_start(*m_refresh_button, Gtk::PACK_EXPAND_PADDING, 0);

	hbox_upper->pack_start(*params_table, Gtk::PACK_SHRINK, 0);
	hbox_upper->pack_start(*vbox_refbutton, Gtk::PACK_SHRINK, 0);

	vbox_all->pack_start(*hbox_upper, Gtk::PACK_SHRINK, 0);

	frame_label->set_alignment(0.5,0.5);
	frame_label->set_padding(0,0);
	frame_label->set_justify(Gtk::JUSTIFY_LEFT);
	frame_label->set_line_wrap(false);
	frame_label->set_use_markup(true);
	frame_label->set_selectable(false);
	frame_label->set_ellipsize(Pango::ELLIPSIZE_NONE);
	frame_label->set_width_chars(-1);
	frame_label->set_angle(0);
	frame_label->set_single_line_mode(false);

	m_frame->set_border_width(1);
	m_frame->set_shadow_type(Gtk::SHADOW_IN);
	m_frame->set_label_align(0,0.5);
	m_frame->set_label_widget(*frame_label);

	/*= Setup the schedule table
	 */
	m_list_store_ref = Gtk::ListStore::create (m_columns);
    m_tree_view.set_model (m_list_store_ref);
    m_tree_view.set_rules_hint ();
	m_tree_view.set_headers_clickable (true);
	m_tree_view.set_headers_visible (true);

	int cnt;
    Gtk::TreeViewColumn* column;

	cnt = m_tree_view.append_column ("Box#", m_columns.m_index);
    column = m_tree_view.get_column (cnt - 1);
    column->set_expand (true);

	cnt = m_tree_view.append_column ("Total Days", m_columns.m_interval);
    column = m_tree_view.get_column (cnt - 1);
    column->set_expand (true);

	cnt = m_tree_view.append_column ("Years", m_columns.m_years);
    column = m_tree_view.get_column (cnt - 1);
    column->set_expand (true);

	cnt = m_tree_view.append_column ("Months", m_columns.m_months);
	column = m_tree_view.get_column (cnt - 1);
    column->set_expand (true);

	cnt = m_tree_view.append_column ("Weeks", m_columns.m_weeks);
    column = m_tree_view.get_column (cnt - 1);
    column->set_expand (true);

	cnt = m_tree_view.append_column ("Days", m_columns.m_days);
    column = m_tree_view.get_column (cnt - 1);
    column->set_expand (true);

	Gtk::ScrolledWindow* scrollw = Gtk::manage (new Gtk::ScrolledWindow);
	scrollw->set_flags       (Gtk::CAN_FOCUS);
    scrollw->set_shadow_type (Gtk::SHADOW_NONE);
    scrollw->set_policy      (Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);

	scrollw->add (m_tree_view);

	vbox_all->pack_start(*scrollw, Gtk::PACK_EXPAND_WIDGET, 6);
	frame_alignment->add (*vbox_all);

	m_frame->add (*frame_alignment);

	/*= Callbacks.
	 */
	m_refresh_button->signal_clicked ().connect (
        mem_fun (*this, &ScheduleUI::on_refresh_clicked));

	m_ef_spinbutton->signal_value_changed ().connect (
        mem_fun (*this, &ScheduleUI::on_changed_clicked));

	m_sz_spinbutton->signal_value_changed ().connect (
        mem_fun (*this, &ScheduleUI::on_changed_clicked));

}

void
ScheduleUI::
efactor (float v_)
{
	trace_with_mask("ScheduleUI::set_efactor",GUITRACE);

	m_ef_spinbutton->set_value (v_);
}

float
ScheduleUI::
efactor () const
{
	trace_with_mask("ScheduleUI::get_efactor",GUITRACE);

	return float (m_ef_spinbutton->get_value ());
}

void
ScheduleUI::
capacity (size_t v_)
{
	trace_with_mask("ScheduleUI::set_capacity",GUITRACE);

	m_sz_spinbutton->set_value (v_);
}

size_t 
ScheduleUI::
capacity () const
{
	trace_with_mask("ScheduleUI::get_capacity",GUITRACE);

	return m_sz_spinbutton->get_value_as_int ();
}

void
ScheduleUI::
push_back (ViewRowData& vrow_)
{
    Gtk::TreeModel::iterator iter = m_list_store_ref->append ();
    Gtk::TreeModel::Row row = *iter;

    row [m_columns.m_index   ] = vrow_.m_index;
    row [m_columns.m_interval] = vrow_.m_interval;
    row [m_columns.m_years   ] = vrow_.m_years;
    row [m_columns.m_months  ] = vrow_.m_months;
    row [m_columns.m_weeks   ] = vrow_.m_weeks;
    row [m_columns.m_days    ] = vrow_.m_days;
}

/**
 * Use temporary ScheduleDB object to let user play with
 * the schedule. The real database is updates only when
 * changes are committed.
 */
void
ScheduleUI::
on_refresh_clicked ()
{
	trace_with_mask("ScheduleUI::on_refresh_clicked",GUITRACE);

	if (m_property_modified == false) {
		DL ((GRAPP,"Configuration hasn't changed.\n"));
		return;
	}

	ScheduleDB tmp_schedb;

	tmp_schedb.set_efactor (efactor ());
	tmp_schedb.set_capacity (capacity ());

	tmp_schedb.rebuild_schedule ();

	m_list_store_ref->clear ();
	tmp_schedb.update_view (*this);

	/*= Emit the change signal for observer(s) to notice the change.
	 */
	m_property_modified = true;
	m_signal_changed.emit ();
}

void
ScheduleUI::
on_changed_clicked ()
{
	trace_with_mask("ScheduleUI::on_changed_clicked",GUITRACE);

	m_property_modified = true;
}

ScheduleUI::signal_changed_type
ScheduleUI::
signal_changed ()
{
	trace_with_mask("ScheduleUI::signal_changed",GUITRACE);
	return m_signal_changed;
}

