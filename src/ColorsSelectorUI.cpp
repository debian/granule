// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: ColorsSelectorUI.cpp,v 1.1 2007/12/02 00:31:24 vlg Exp $
//------------------------------------------------------------------------------
//                            ColorsSelectorUI.cpp
//------------------------------------------------------------------------------
//  Copyright (c) 2007 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date   : Sat Oct 8 2007
//
//------------------------------------------------------------------------------
#include <gtkmm/button.h>
#include <gtkmm/buttonbox.h>
#include <gtkmm/label.h>
#include <gtkmm/frame.h>
#include <gtkmm/box.h>
#include <gtkmm/alignment.h>
#include <gtkmm/fontselection.h>

#include "ColorsSelectorUI.h"

#include "Intern.h"             // i18n macros

using sigc::mem_fun;

/**-----------------------------------------------------------------------------
 *	Some useful macros
 **-----------------------------------------------------------------------------
 */
//------------------------------------------------------------------------------
static const char* clabels[] = {
    _("Text foreground: "),
    _("Text background: "),
    _("Separator color: "),
	_("Frame color: ")
};

static void set_colors_frame (Gtk::Frame* f_, Gtk::Label* l_, Gtk::Table* w_)
{
	f_->set_border_width (4); 
	f_->set_shadow_type  (Gtk::SHADOW_ETCHED_IN);
	f_->set_label_align  (Gtk::ALIGN_TOP, Gtk::ALIGN_CENTER);
	f_->set_label_widget (*l_);
	f_->add              (*w_);
}

static void set_colors_label (Gtk::Label* l_)
{
	l_->set_padding    (0,0);
	l_->set_line_wrap  (false);
	l_->set_use_markup (true);
}

/**-----------------------------------------------------------------------------
 *	ColorsSelectorUI
 *
 *  The widget encapsulates text font selection controls.
 *  All fonts in the application can be altered from their default theme values.
 *
 *     ---------------
 * +---| frame_label |------------------^-------------------------+<--m_frame
 * |   ---------------                  | (6pts)                  |
 * |                                    v-------------------------|
 * | +---------+--------------------------------+----------------+<-- FontEntry
 * | | label   |       entry                    | button         ||
 * | +---------+--------------------------------+----------------+|
 * | | label   |       entry                    | button         ||
 * | +---------+--------------------------------+----------------+|
 * | | label   |       entry                    | button         ||
 * | +---------+--------------------------------+----------------+|
 * | | label   |       entry                    | button         ||
 * | +---------+--------------------------------+----------------+|
 * | | label   |       entry                    | button         ||
 * | +---------+--------------------------------+----------------+|
 * +--------------------------------------------------------------+
 *
 **-----------------------------------------------------------------------------
 */
ColorsSelectorUI::
ColorsSelectorUI ()
{
	Gtk::Frame* colors_frame = this;

	m_color_table = Gtk::manage (new Gtk::Table (COLOR_ENTRY_SZ, 3, false));

	for (int i = 0; i < COLOR_ENTRY_SZ; i++) 
	{
	    m_centry [i].m_label  = Gtk::manage (new Gtk::Label (clabels[i]));
	    m_centry [i].m_entry  = Gtk::manage (new Gtk::Entry);
	    m_centry [i].m_button = Gtk::manage (new Gtk::ColorButton ());

	    m_centry [i].m_label->set_alignment(Gtk::ALIGN_RIGHT,Gtk::ALIGN_CENTER);
	    m_centry [i].m_label->set_padding    (0,0);
	    m_centry [i].m_label->set_justify    (Gtk::JUSTIFY_LEFT);
	    m_centry [i].m_label->set_line_wrap  (false);
	    m_centry [i].m_label->set_use_markup (false);
	    m_centry [i].m_label->set_selectable (false);

	    m_centry [i].m_entry->set_flags (Gtk::CAN_FOCUS);
	    m_centry [i].m_entry->set_visibility (true);
	    m_centry [i].m_entry->set_editable   (true);
	    m_centry [i].m_entry->set_max_length (0);
	    m_centry [i].m_entry->set_text       ("");
	    m_centry [i].m_entry->set_has_frame  (true);
	    m_centry [i].m_entry->set_activates_default (false);
#ifdef IS_HILDON
		m_centry [i].m_entry->set_width_chars (26);
#endif

	    m_centry [i].m_button->set_flags(Gtk::CAN_FOCUS);

		m_color_table->attach (*m_centry [i].m_label, 
							  0, 1, i, i+1,
							  Gtk::FILL, 
							  Gtk::AttachOptions (), 2, 2);

		m_color_table->attach (*m_centry [i].m_entry, 
							  1, 2, i, i+1,
#ifdef IS_HILDON
							  Gtk::FILL,
#else
							  Gtk::EXPAND|Gtk::FILL,
#endif
							  Gtk::AttachOptions(), 2, 2);

		m_color_table->attach (*m_centry [i].m_button, 
							  2, 3, i, i+1,
							  Gtk::FILL, 
							  Gtk::AttachOptions (), 2, 2);
	}

	Gtk::Label* colors_label = Gtk::manage(new Gtk::Label("<b>Colors</b>"));
	set_colors_label(colors_label);

	/** Pack all together
	 */
	set_colors_frame (colors_frame, colors_label, m_color_table);

	/** Set callbacks.
	 *    Clicking on button brings up ColorEntry dialog.
	 *    Modifying an entry updates ColorButton color selection.
	 */
	for (int i = 0; i < COLOR_ENTRY_SZ; i++) 
	{
	    m_centry [i].m_button->signal_color_set ().connect (
			sigc::bind<int>(mem_fun (*this, 
							 &ColorsSelectorUI::on_select_clicked), i));

		m_centry [i].m_entry->signal_changed().connect (
			sigc::bind<int>(mem_fun (*this, 
							 &ColorsSelectorUI::on_entry_changed), i));
	}
}

void
ColorsSelectorUI::
set_active (TextColorType type_, bool val_)
{
	m_centry [type_].m_label->set_sensitive (val_);
	m_centry [type_].m_entry->set_sensitive (val_);
	m_centry [type_].m_button->set_sensitive (val_);
}

/* This is a simple helper function which converts an RGB triplet 
 * (which is part of a GdkColor) into a "#RRGGBB" string.
 */
Glib::ustring
ColorsSelectorUI::
convert_color_to_str (const Gdk::Color& color_)
{
	char color_string [] = "#000000";

	/** GdkColor stores its red, green, and blue as 0..65536 
	 *	instead of 0..256 
	 */
	sprintf (&color_string[1], "%.2X", color_.get_red   () / 256);
	sprintf (&color_string[3], "%.2X", color_.get_green () / 256);
	sprintf (&color_string[5], "%.2X", color_.get_blue  () / 256);

	return (color_string);
}

void
ColorsSelectorUI::
on_select_clicked (int idx_)
{
	trace_with_mask("ColorsSelectorUI::on_select_clicked",GUITRACE);

	const Gdk::Color c = m_centry [idx_].m_button->get_color ();
	set_text (TextColorType (idx_), convert_color_to_str (c));
}

void
ColorsSelectorUI::
on_entry_changed (int idx_)
{
	trace_with_mask("ColorsSelectorUI::on_entry_changed",GUITRACE);

	m_centry [idx_].m_button->set_color (
		Gdk::Color (m_centry [idx_].m_entry->get_text ()));
}

void
ColorsSelectorUI::
set_text (TextColorType type_, const Gdk::Color& color_)
{
	m_centry [type_].m_entry->set_text (convert_color_to_str (color_));
	m_centry [type_].m_button->set_color (color_);
}
