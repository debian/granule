// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: GeometryPrefs.cpp,v 1.16 2008/08/19 02:52:46 vlg Exp $
//------------------------------------------------------------------------------
//                            GeometryPrefs.cpp
//------------------------------------------------------------------------------
//  Copyright (c) 2004,2005 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date   :  Wed Sep 22 2004
//
//------------------------------------------------------------------------------

#include <gtkmm/frame.h>
#include <gtkmm/adjustment.h>
#include <gtkmm/scrolledwindow.h>

#include "Granule-main.h"
#include "GrappConf.h"
#include "PrefWindow.h"
#include "GeometryPrefs.h"

using sigc::mem_fun;
using sigc::bind;

static const char* geom_labels [] = {
    "Main Window :",
    "Card View :",
    "Deck Player :"
};

enum { MW, CV, DP };
enum { WIDTH, HEIGHT };

/**-----------------------------------------------------------------------------
   GeometryBox methods
--------------------------------------------------------------------------------
*/
GeometryBox::
GeometryBox (const char* win_name_)
{  
	Gtk::Label* hidden_label;
	Gtk::Adjustment* spin_button_adj;

	hidden_label    = Gtk::manage (new Gtk::Label (""));
	m_label         = Gtk::manage(new Gtk::Label(win_name_));

	m_width_vbox    = Gtk::manage (new Gtk::VBox (false, 0));
	m_wlabel        = Gtk::manage (new Gtk::Label ("Width:"));
	spin_button_adj = Gtk::manage (new Gtk::Adjustment (1,100,1600,1,10,10));
	m_wspin_button  = Gtk::manage (new Gtk::SpinButton (*spin_button_adj,1,0));

	m_height_vbox   = Gtk::manage (new Gtk::VBox (false, 0));   
	m_hlabel        = Gtk::manage (new Gtk::Label ("Height:"));
	spin_button_adj = Gtk::manage (new Gtk::Adjustment (1,100,1600,1,10,10));
	m_hspin_button  = Gtk::manage (new Gtk::SpinButton (*spin_button_adj,1,0));

	m_label->set_alignment(Gtk::ALIGN_RIGHT, Gtk::ALIGN_CENTER);
	m_label->set_padding(0,0);
	m_label->set_justify(Gtk::JUSTIFY_LEFT);
	m_label->set_line_wrap(false);
	m_label->set_use_markup(false);
	m_label->set_selectable(false);

	m_wlabel->set_alignment(0.5,0.5);
	m_wlabel->set_padding(0,0);
	m_wlabel->set_justify(Gtk::JUSTIFY_LEFT);
	m_wlabel->set_line_wrap(false);
	m_wlabel->set_use_markup(false);
	m_wlabel->set_selectable(false);

	m_wspin_button->set_flags(Gtk::CAN_FOCUS);
	m_wspin_button->set_update_policy(Gtk::UPDATE_ALWAYS);
	m_wspin_button->set_numeric(false);
	m_wspin_button->set_digits(0);
	m_wspin_button->set_wrap(false);

	m_width_vbox->pack_start (*m_wlabel, Gtk::PACK_SHRINK, 0);
	m_width_vbox->pack_start( *m_wspin_button, Gtk::PACK_SHRINK, 0);

	m_hlabel->set_alignment(0.5,0.5);
	m_hlabel->set_padding(0,0);
	m_hlabel->set_justify(Gtk::JUSTIFY_LEFT);
	m_hlabel->set_line_wrap(false);
	m_hlabel->set_use_markup(false);
	m_hlabel->set_selectable(false);

	m_hspin_button->set_flags(Gtk::CAN_FOCUS);
	m_hspin_button->set_update_policy(Gtk::UPDATE_ALWAYS);
	m_hspin_button->set_numeric(false);
	m_hspin_button->set_digits(0);
	m_hspin_button->set_wrap(false);

	m_height_vbox->pack_start(*m_hlabel, Gtk::PACK_SHRINK, 0);
	m_height_vbox->pack_start(*m_hspin_button, Gtk::PACK_SHRINK, 0);
}

void
GeometryBox::
pack (int idx_, Gtk::Table& table_)
{
	table_.attach (*m_label,              
				   0, 1, idx_, idx_+1, 
				   Gtk::FILL, Gtk::AttachOptions (), 
				   2, 2);

	table_.attach (*m_height_vbox,
				   1, 2, idx_, idx_+1, 
				   Gtk::AttachOptions (), 
				   Gtk::AttachOptions (), 
				   2, 2);

	table_.attach (*m_width_vbox, 
				   2, 3, idx_, idx_+1, 
				   Gtk::AttachOptions (), 
				   Gtk::AttachOptions (), 
				   2, 2);
}

void 
GeometryBox::
set_values (const Gdk::Rectangle& geom_)
{
	m_hspin_button->set_value (geom_.get_height());
	m_wspin_button->set_value (geom_.get_width ());
}

void 
GeometryBox::
get_values (int& width_, int& height_)
{
	height_ = int (m_hspin_button->get_value ());
	width_  = int (m_wspin_button->get_value ());
}

/**-----------------------------------------------------------------------------
   AspectBox methods
--------------------------------------------------------------------------------
*/
AspectBox::
AspectBox () :
	Gtk::HBox(false, 0)
{
	m_keep_aspect = Gtk::manage (
		new Gtk::CheckButton("Keep Deck Player aspect ratio at: "));

	m_hentry = Gtk::manage (new Gtk::Entry);
	m_xlabel = Gtk::manage (new Gtk::Label (" x "));
	m_wentry = Gtk::manage (new Gtk::Entry);

	m_keep_aspect->set_flags  (Gtk::CAN_FOCUS);
	m_keep_aspect->set_relief (Gtk::RELIEF_NORMAL);
	m_keep_aspect->set_mode   (true);
	m_keep_aspect->set_active (false);
	m_keep_aspect->set_border_width (4);

	m_hentry->set_flags       (Gtk::CAN_FOCUS);
	m_hentry->set_visibility  (true);
	m_hentry->set_editable    (true);
	m_hentry->set_max_length  (0);
	m_hentry->set_text        ("3");
	m_hentry->set_has_frame   (true);
	m_hentry->set_width_chars (1);
	m_hentry->set_activates_default (false);

	m_xlabel->set_alignment   (0.5,0.44);
	m_xlabel->set_padding     (0,0);
	m_xlabel->set_justify     (Gtk::JUSTIFY_LEFT);
	m_xlabel->set_line_wrap   (false);
	m_xlabel->set_use_markup  (false);
	m_xlabel->set_selectable  (false);

	m_wentry->set_flags       (Gtk::CAN_FOCUS);
	m_wentry->set_visibility  (true);
	m_wentry->set_editable    (true);
	m_wentry->set_max_length  (0);
	m_wentry->set_text        ("5");
	m_wentry->set_has_frame   (true);
	m_wentry->set_width_chars (1);
	m_wentry->set_activates_default (false);

	pack_start (*m_keep_aspect, Gtk::PACK_SHRINK, 0);
	pack_start (*m_hentry,      Gtk::PACK_SHRINK, 0);
	pack_start (*m_xlabel,      Gtk::PACK_SHRINK, 0);
	pack_start (*m_wentry,      Gtk::PACK_SHRINK, 0);

	show_all ();
}

void 
AspectBox::
set_value (bool is_on_, Gdk::Rectangle& aspect_)
{
	char buf [16];
	m_keep_aspect->set_active (is_on_);

	sprintf (buf, "%d", int (aspect_.get_width ()));
	m_wentry->set_text (buf);

	sprintf (buf, "%d", int (aspect_.get_height ()));
	m_hentry->set_text (buf);
}

void 
AspectBox::
get_value (bool& is_on_, int& width_, int& height_)
{
	is_on_ = m_keep_aspect->get_active ();
	sscanf (m_wentry->get_text ().c_str (), "%d", &width_);
	sscanf (m_hentry->get_text ().c_str (), "%d", &height_);
}

/**-----------------------------------------------------------------------------
 **   GeometryPrefs methods
 **-----------------------------------------------------------------------------
 **/
GeometryPrefs::
GeometryPrefs (PrefWindow& pwin_) 
	: 
	Gtk::VBox (false, 0),
	m_pref_window (pwin_),
	m_change_in_progress (false)
{  	
	int i;
	trace_with_mask("GeometryPrefs::GeometryPrefs",GUITRACE);

#ifdef IS_HILDON
	set_border_width(0);
#else
	set_border_width(14);
#endif

	m_table = Gtk::manage (new Gtk::Table (3, 3, false));

	for (i = 0; i < WIN_ENTRY_SZ; i++) {
		m_geometry [i] = new GeometryBox (geom_labels [i]);
		m_geometry [i]->pack (i, *m_table);
	}

	m_aspect = Gtk::manage (new AspectBox);
	m_aspect->set_value (CONFIG->keep_deck_player_aspect (),
						 CONFIG->get_deck_player_aspect  ());

	Gtk::HBox*  hbox          = Gtk::manage (new Gtk::HBox (false, 0));
	Gtk::Frame* all_frame     = Gtk::manage (new Gtk::Frame);
	Gtk::Frame* hbox_frame    = Gtk::manage (new Gtk::Frame);
	Gtk::Frame* options_frame = Gtk::manage (new Gtk::Frame);
	Gtk::Label* hidden_label  = Gtk::manage (new Gtk::Label);

	hidden_label->set_alignment  (0.5,0.5);
	hidden_label->set_padding    (0,0);
	hidden_label->set_justify    (Gtk::JUSTIFY_LEFT);
	hidden_label->set_line_wrap  (false);
	hidden_label->set_use_markup (false);
	hidden_label->set_selectable (false);

	options_frame->set_shadow_type  (Gtk::SHADOW_ETCHED_IN);
	options_frame->set_border_width (4);
	options_frame->set_label_align  (1, 1);
	options_frame->set_label_widget (*hidden_label);

	options_frame->add (*m_aspect);

	hbox_frame->set_shadow_type  (Gtk::SHADOW_ETCHED_IN);
	hbox_frame->set_border_width (4);

	all_frame->set_shadow_type  (Gtk::SHADOW_ETCHED_IN);
	all_frame->set_border_width (4);

	/** Pack everything. 
	 *  The Center the table in the middle with two hidden buttons. 
	 *  Update: left-align GeometryBox.
	 */
	Gtk::Label* post_hidden = manage (new Gtk::Label);

	hbox->pack_start (*m_table,     Gtk::PACK_SHRINK, 4);
	hbox->pack_start (*post_hidden, Gtk::PACK_EXPAND_WIDGET);

	hbox_frame->add (*hbox);

	/** Warning label
	 */
	Gtk::HBox* warning_hbox = Gtk::manage (new Gtk::HBox (false, 0));
	Gtk::Label* warning_label = Gtk::manage (new Gtk::Label);
	warning_label->set_use_markup ();
	warning_label->set_markup (
			   "<span size=\"large\" weight=\"heavy\">NOTE:</span>\n"
			   "      Any changes in Geometry settings\n"
			   "      require immediate application restart.\n"
			   "      To reduce size of a window, change its\n"
			   "      geometry here and restart Granule.");

	warning_hbox->pack_start (*warning_label, Gtk::PACK_SHRINK, 4);

// #if defined(IS_HILDON) || defined(IS_PDA)
// 	/** Both Maemo and PDAs have a narrow screen and thus requires
// 	 * 	smart packing.
// 	 */
	Gtk::ScrolledWindow* scrollw = Gtk::manage (new Gtk::ScrolledWindow);
	scrollw->set_flags (Gtk::CAN_FOCUS);
	scrollw->set_shadow_type (Gtk::SHADOW_NONE);
	scrollw->set_policy (Gtk::POLICY_AUTOMATIC , Gtk::POLICY_ALWAYS);
#ifdef GLIBMM_PROPERTIES_ENABLED
	scrollw->property_window_placement ().set_value (Gtk::CORNER_TOP_LEFT);
#else
	scrollw->set_property ("window_placement", Gtk::CORNER_TOP_LEFT);
#endif

	Gtk::VBox* scrollw_vbox = Gtk::manage (new Gtk::VBox (false, 0));

	scrollw_vbox->pack_start (*warning_hbox,  Gtk::PACK_SHRINK, 4);
	scrollw_vbox->pack_start (*hbox_frame,    Gtk::PACK_SHRINK, 4);
	scrollw_vbox->pack_start (*options_frame, Gtk::PACK_SHRINK, 4);

	all_frame->add (*scrollw_vbox);
	scrollw->add (*all_frame);
	pack_start (*scrollw);

// #else  // Desktop (Linux or win32)
 
// 	pack_start (*warning_hbox,  Gtk::PACK_SHRINK, 4);
// 	pack_start (*hbox_frame,    Gtk::PACK_SHRINK, 0);
// 	pack_start (*options_frame, Gtk::PACK_SHRINK, 0);

// #endif

	/** Set callbacks
	 */
	m_aspect->get_checkbox ()->signal_toggled ().connect (
		mem_fun (m_pref_window, &PrefWindow::changed_cb));

	m_aspect->get_wentry ()->signal_changed ().connect (
		mem_fun (m_pref_window, &PrefWindow::changed_cb));

	m_aspect->get_hentry ()->signal_changed ().connect (
		mem_fun (m_pref_window, &PrefWindow::changed_cb));

	for (i = 0; i < WIN_ENTRY_SZ-1; i++) {
	    m_geometry [i]->get_wspin_button ()->signal_value_changed ().connect (
			mem_fun (m_pref_window, &PrefWindow::changed_cb));

	    m_geometry [i]->get_hspin_button ()->signal_value_changed ().connect (
			mem_fun (m_pref_window, &PrefWindow::changed_cb));
	}

	m_geometry [DP]->get_wspin_button ()->signal_value_changed ().connect (
		bind<int> (mem_fun (*this, &GeometryPrefs::changed_cb), WIDTH));
	
	m_geometry [DP]->get_hspin_button ()->signal_value_changed ().connect (
		bind<int> (mem_fun (*this, &GeometryPrefs::changed_cb), HEIGHT));

	show_all ();
}

/** If 'keep aspect ratio' is enabled, we modify each 
	measurement in accordance with the ratio.
*/
void
GeometryPrefs::
changed_cb (int measurement_) 
{ 
	trace_with_mask("GeometryPrefs::changed_cb",GUITRACE);

	bool active;
	int  width;
	int  height;

	m_aspect->get_value (active, width, height);
	CONFIG->keep_deck_player_aspect (active);

	if (!m_change_in_progress && CONFIG->keep_deck_player_aspect ()) {
		Gdk::Rectangle& aspect = CONFIG->get_deck_player_aspect ();
		m_geometry [DP]->get_values (width, height);

		m_change_in_progress = true;
		if (measurement_ == HEIGHT) {
			width = height * aspect.get_width () / aspect.get_height ();
			m_geometry [DP]->get_wspin_button ()->set_value (width);
		}
		else {
			height = width  * aspect.get_height () / aspect.get_width ();
			m_geometry [DP]->get_hspin_button ()->set_value (height);
		}
		m_change_in_progress = false;
	}

	m_pref_window.changed_cb (); 
}

void
GeometryPrefs::
load_from_config ()
{
	trace_with_mask("GeometryPrefs::load_from_config",GUITRACE);

	Gdk::Rectangle geom;

	DL((GEOM,"Loading MainWindow geometry ...\n"));
	geom = CONFIG->get_main_window_geometry ();
	DL((GEOM,"Geometry changed: (h=%d, w=%d)\n", 
		geom.get_height(), geom.get_width ()));
	m_geometry [MW]->set_values (geom);

	DL((GEOM,"Loading CardView geometry ...\n"));
	geom = CONFIG->get_card_view_geometry ();
	DL((GEOM,"Geometry changed: (h=%d, w=%d)\n", 
		geom.get_height(), geom.get_width ()));
	m_geometry [CV]->set_values (geom);

	DL((GEOM,"Loading DeckPlayer geometry ...\n"));
	geom = CONFIG->get_deck_player_geometry ();
	DL((GEOM,"Geometry changed: (h=%d, w=%d)\n", 
		geom.get_height(), geom.get_width ()));
	m_geometry [DP]->set_values (geom);

	DL((GEOM,"Loading DeckPlayer aspect ratio ...\n"));
	m_aspect->set_value (CONFIG->keep_deck_player_aspect (),
						 CONFIG->get_deck_player_aspect ());
}

void
GeometryPrefs::
save_to_config ()
{
	trace_with_mask("GeometryPrefs::save_to_config",GUITRACE);
	
	bool active;
	int  width;
	int  height;

	m_geometry [MW]->get_values (width, height);
	CONFIG->set_main_window_geometry (width, height);

	m_geometry [CV]->get_values (width, height);
	CONFIG->set_card_view_geometry (width, height);

	m_geometry [DP]->get_values (width, height);
	CONFIG->set_deck_player_geometry (width, height);

	m_aspect->get_value (active, width, height);
	CONFIG->keep_deck_player_aspect (active);
	CONFIG->set_deck_player_aspect (width, height);
}

