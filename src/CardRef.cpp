// -*- c++ -*-
//------------------------------------------------------------------------------
//                              CardRef.cpp
//------------------------------------------------------------------------------
// $Id: CardRef.cpp,v 1.15 2008/05/19 03:39:03 vlg Exp $
//
//  Copyright (c) 2004 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//
// Date: Apr 24 2004
//------------------------------------------------------------------------------
#include <math.h>
#include <sstream>
#include <iostream>
#include <iomanip>

#include <assa/TimeVal.h>

#include "CardRef.h"
#include "Granule.h"
#include "ScheduleDB.h"

static const int SBUF_SZ = 32;
static char sbuf [SBUF_SZ];

CardRef::
CardRef (VCard& vcard_, Deck& deck_, float efactor_) 
	: 
	m_card (dynamic_cast<Card&>(vcard_)), 
	m_deck (deck_),
	m_exp_date (CONFIG->get_load_time ())
{ 
	trace_with_mask("CardRef::CardRef",GUITRACE); 

	m_efactor    = efactor_;
	m_interval   = ScheduleDB::BOX_ONE_STEP;
	m_repetition = 0;

	for (int i = 0; i < CARD_STATS_SZ; i++) {
		m_statistics [i] = 0;
	}
}

int
CardRef::
save_to_xml_config (xmlTextWriterPtr writer_, const string& proj_path_)
{
	int ret = 0;
	std::string abs_fname;
	std::string deck_fname;

	ret = xmlTextWriterStartElement (writer_, BAD_CAST "card");

	ret = xmlTextWriterWriteAttribute (writer_, BAD_CAST "id",
									   BAD_CAST get_id_str ().c_str ());

	if (get_reversed ()) {
		xmlTextWriterWriteAttribute (writer_, BAD_CAST "reverse", BAD_CAST "1");
	}

	abs_fname = get_deck ().get_name ();
	if (CONFIG->with_relpaths ()) {
		deck_fname = GRANULE->calculate_relative_path (proj_path_, abs_fname);
	}
	else {
		deck_fname = abs_fname;
	}

	DL ((GRAPP, "Write %s deck path \"%s\".\n", 
		 (CONFIG->with_relpaths () ? "relative" : "absolute"),
		 deck_fname.c_str ()));

	if (CONFIG->with_relpaths ()) {
		DL ((GRAPP, "Components: proj_path_ = \"%s\", abs_fname = \"%s\"\n",
			 proj_path_.c_str (), abs_fname.c_str ()));
	}

	ret = xmlTextWriterWriteElement (writer_, BAD_CAST "fromdeck",
					 BAD_CAST Granule::locale_to_utf8 (deck_fname).c_str ());

	ret = xmlTextWriterWriteElement (writer_, BAD_CAST "expdate",
					 BAD_CAST get_expiration_str ().c_str ());

	/*= Write out Scheduling stuff.
	 */
	dump_schedule ("saving CardRef schedule");
	ret = write_xml_element (writer_, "efactor",    "%.2f", m_efactor);
	ret = write_xml_element (writer_, "interval",   "%d",   m_interval);
	ret = write_xml_element (writer_, "repetition", "%d",   m_repetition);

	/** Quality of answers statistics element
	 */
	ret = xmlTextWriterStartElement (writer_, BAD_CAST "statistics");

	for (int i = 0; i < CARD_STATS_SZ; i++) 
	{
		snprintf (sbuf, SBUF_SZ, "qs%d", i);
		ret = write_xml_attribute (writer_, sbuf, "%d", m_statistics [i]);
	}	
	ret = xmlTextWriterEndElement (writer_);  /*= End of statistics tag */

	ret = xmlTextWriterEndElement (writer_);  /*= End of card tag */

	return (ret);
}
	
int
CardRef::
load_from_xml_config (const xmlDocPtr parser_, int deck_, int card_)
{
	char*    attrp;
	int      n;
	char     xpath [64];
	xmlChar* result;

	n = sprintf (xpath, "/cardfile/carddeck[%d]/card[%d]/", deck_+1, card_);
	attrp = xpath + n;

	/** Parse E-Factor
	 */
	sprintf (attrp, "efactor");
	result = Granule::get_node_by_xpath (parser_, xpath);
	if (result != NULL) {
		m_efactor = ::atof ((const char*) result);
		xmlFree (result);
	}
	
	/** Parser Interval
	 */
	sprintf (attrp, "interval");
	result = Granule::get_node_by_xpath (parser_, xpath);
	if (result != NULL) {
		m_interval = ::atoi ((const char*) result);
		xmlFree (result);
	}

	/** Parser Repetition
	 */
	sprintf (attrp, "repetition");
	result = Granule::get_node_by_xpath (parser_, xpath);
	if (result != NULL) {
		m_repetition = ::atoi ((const char*) result);
		xmlFree (result);
	}

	/** Parser Statistics
	 */
	n = sprintf (attrp, "statistics/");
	attrp += n;
	for (int i = 0; i < CARD_STATS_SZ; i++) 
	{
		sprintf (attrp, "@qs%d", i);
		result = Granule::get_node_by_xpath (parser_, xpath);
		if (result != NULL) {
			m_statistics [i] = ::atoi ((const char*) result);
			xmlFree (result);
		}
		else {
			DL ((DECK,"Attribute %s of id=%d not found!\n", xpath, get_id ()));
		}
	}

	dump_schedule ("loaded CardRef schedule");
	return 0;
}

void
CardRef::
dump () const
{
	DL((DECK, "========= CardRef =========\n"));
	DL((DECK, "id       = \"%d\"\n", m_card.get_id ()));
	DL((DECK, "question = \"%s\"\n", m_card.get_question ().c_str ()));
	DL((DECK, "answer   = \"%s\"\n", m_card.get_answer ().c_str ()));
	DL((DECK, "example  = \"%s\"\n", m_card.get_example ().c_str ()));
	DL((DECK, "dirty?   = \"%s\"\n", (m_dirty ? "yes" : "no")));
	ASSA::TimeVal t (m_exp_date, 0);
	DL((DECK, "exp_date = %s\n", t.fmtString ("%c").c_str ()));

/*
	DL((DECK, "sel_side = \"%s\"\n", 
		(m_selection_side == FRONT ? "front" : "back")));
*/
}

void 
CardRef::
dump_schedule (const char* message_) const
{
	ASSA::TimeVal t (m_exp_date, 0);

	DL((DECK, "===== %s  ===\n",     message_));
	DL((DECK, "id         = %d\n",   m_card.get_id ()));
	DL((DECK, "expire on  = %s\n",   t.fmtString ("%c").c_str ()));
	DL((DECK, "m_exp_date = %d\n",   m_exp_date));
	DL((DECK, "efactor    = %.2f\n", m_efactor));
	DL((DECK, "interval   = %d\n",   m_interval));
	DL((DECK, "repetition = %d\n",   m_repetition));
	DL((DECK, "stat[Fail] = %d\n",   m_statistics [0]));
	DL((DECK, "stat[Ok]   = %d\n",   m_statistics [5]));
	DL((DECK, "===========================\n"));
}

template<class T> int 
CardRef::
write_xml_element (xmlTextWriterPtr writer_,
				   const char* tag_,
				   const char* format_,
				   T value_)
{
	static char buf [32];
	snprintf (buf, SBUF_SZ, format_, value_);
	return (xmlTextWriterWriteElement (writer_, BAD_CAST tag_, BAD_CAST buf));
}

template<class T> int 
CardRef::
write_xml_attribute (xmlTextWriterPtr writer_,
					 const char* tag_,
					 const char* format_,
					 T value_)
{
	static char buf [32];
	snprintf (buf, SBUF_SZ, format_, value_);
	return (xmlTextWriterWriteAttribute(writer_, BAD_CAST tag_, BAD_CAST buf));
}

void 
CardRef::
set_expiration_in_days (int days_)
{ 
	m_exp_date = CONFIG->get_load_time ().sec () +
		days_ * CONFIG->secs_in_day ();
}

void 
CardRef::
set_expiration (const ASSA::TimeVal& e_) 
{ 
	trace_with_mask("CardRef::set_expiration",DECK);

	m_exp_date = e_.sec (); 

	ASSA::TimeVal exp_tv (m_exp_date, 0);
	DL((DECK, "New exp_date = %s\n", exp_tv.fmtString ("%c").c_str ()));
}

string
CardRef::
get_expiration_str ()
{ 
	std::ostringstream expd;
	expd << m_exp_date;
	return expd.str ();
}

/** 
 * Compares the expiration date of the card with today's date
 * (time when the program started).
 *
 * @return true if card has expired; false if not
 */
bool
CardRef::
is_expired ()
{
	trace_with_mask("CardRef::is_expired",GUITRACE);

	ASSA::TimeVal tlh = get_expiration ();
	ASSA::TimeVal trh = CONFIG->get_load_time ();

	DL((GRAPP,"exp_time (%s) vs.\n", tlh.fmtString ("%c").c_str ()));
	DL((GRAPP,"loaded   (%s)\n",     trh.fmtString ("%c").c_str ()));
	DL((GRAPP,"the card %s\n", 
		(tlh <= trh ? "expired" : "has not expired yet")));

	return (tlh <= trh);
}

/**
 * Adjust E-Factor of the Card base on the quality of answer.
 * Adjust repetition counter.
 * Calculate new repetition interval.
 * Adjust expiration date.
 */
void
CardRef::
reschedule (AnswerQuality answer_quality_)
{
	trace_with_mask("CardRef::reschedule",DECK);
	
	dump_schedule ("Before adjustment");
	DL ((DECK,"answer_quality_ = %d\n", answer_quality_));

	if (answer_quality_ == ANSWER_OK || answer_quality_ == ANSWER_OK_HESITANT)
	{
		if (answer_quality_ == ANSWER_OK) {
			float ef = m_efactor + 0.1;
			if (fabsf (ef - ScheduleDB::EF_MAX) < 0.01) {
				m_efactor = ScheduleDB::EF_MAX;
			}
			else {
				/** For OK_HESITANT, EFactor is not changed.
				 */
				m_efactor = ef;
			}
		}
		
		m_repetition++;
		if (m_repetition == 1) {
			m_interval = ScheduleDB::BOX_TWO_STEP;
		}
		else {
			m_interval = int (m_interval * m_efactor);
		}
	}
	else  /* The rest, including ANSWER_OK_DIFFICULT - keep EFactor */
	{
		m_repetition = 0;
		m_interval = ScheduleDB::BOX_ONE_STEP;
	}
	
	/** Set expiration 
	 */
	DL ((DECK,"Old m_exp_date (in secs) = %ld\n", m_exp_date));

	long secs_in_day = CONFIG->secs_in_day ();
	DL ((DECK,"secs_in_day = %d\n", secs_in_day));

	long interval_in_secs = m_interval * CONFIG->secs_in_day ();
	DL ((DECK,"m_interval = %d (in secs: %d)\n", 
		 m_interval, interval_in_secs));

	long time_now = ASSA::TimeVal::gettimeofday ().sec ();
	DL ((DECK,"time now (in secs) = %d\n", time_now));

	m_exp_date = time_now + interval_in_secs;

	DL ((DECK,"New m_exp_date (in secs) = %ld\n", m_exp_date));

	/** Now, the statistics.
	 */
	m_statistics [answer_quality_]++;

	dump_schedule ("After adjustment");
}

string
CardRef::
get_success_ratio (string& correct_wrong_ratio_) const
{
	trace_with_mask("CardRef::get_success_ratio",DECK);

	static char buffer [32];
	int correct = 0;
	int wrong = 0;
	int total = 0;
	float success_ratio = 0;

	for (int i = 0; i < CARD_STATS_SZ; i++) {
		total += m_statistics [i];
	}

	if (total == 0) {
		return "0%";
	}

	wrong = m_statistics [ANSWER_BAD_BLACKOUT] +
		m_statistics [ANSWER_BAD_PARTIAL] +
		m_statistics [ANSWER_BAD_RECALL];

	correct = m_statistics [ANSWER_OK] +
		m_statistics [ANSWER_OK_HESITANT] +
		m_statistics [ANSWER_OK_DIFFICULT];

	snprintf (buffer, sizeof (buffer), "%d/%d", correct, wrong);
	correct_wrong_ratio_ = buffer;
	
	success_ratio = correct * 100.0 / total;
	snprintf (buffer, sizeof (buffer), "%d%%", int(success_ratio));
	DL ((DECK,"Success ratio: %s (%d/%d)\n", buffer, correct, total));

	return buffer;
}
