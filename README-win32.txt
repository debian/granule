Granule is a flashcards program for learning new words.

This distribution is based on ver. 1.2.1. 

It comes in two flavors: with and without (-bare) 
GTK+ run-time environment. 

If you don't have already GTK+ runtime installed on your machine,
you might want to install both packages separately:

1. GTK+ runtime gtk-win32-2.8.18-rc1 from

   http://gladewin32.sourceforge.net/modules/news/


2. Gtkmm runtime gtkmm-runtime-2.8.8-1 from

   http://www.pcpm.ucl.ac.be/~gustin/win32_ports/index.html


Both runtime packages should be installed in the same folder,
i.e. c:\Program Files\GTK-Runtime.

To start the program, go to 'bin' folder and 
click on 'granule' (Application). You can also create
a desktop shortcut and use granule.ico in the same
folder for shortcut.

Visit project homepage for more details:

http://granule.sourceforge.net/


Vladislav Grinchenko
vlg@users.sourceforge.net

June 9, 2006