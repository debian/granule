##
## $Id: granule.spec.in,v 1.20 2008/09/01 13:59:19 vlg Exp $
##
Summary:    Flashcards program based on Leitner methodology
Name:       granule
Version:    1.4.0-7
Release:    0%{?dist}
License:    GPLv2+
Group:      Applications/Multimedia
URL:        http://granule.sourceforge.net/
Source:     http://dl.sf.net/sourceforge/granule/granule-%{version}.tar.gz 
BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}-root

BuildRequires: gtkmm24-devel >= 2.4.0
BuildRequires: libassa-devel
BuildRequires: libxml2-devel
BuildRequires: desktop-file-utils
BuildRequires: gettext
# Only for the glib-gettextize and intltoolize hack.
BuildRequires: glib2-devel intltool

%description 
granule is a flashcard program that implements Leither cardfile
methodology for learing new words. It features both short-term and
long-term memory training capabilities with scheduling.


%prep
%setup -q


%build
glib-gettextize --copy --force
intltoolize --automake --copy --force
%configure
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT install
%find_lang %{name} --with-gnome

mkdir -p -m0755 ${RPM_BUILD_ROOT}%{_datadir}/granule/pixmaps
mkdir -p -m0755 ${RPM_BUILD_ROOT}%{_datadir}/granule/xml

install -p -m 0644 granule-linux.conf ${RPM_BUILD_ROOT}%{_datadir}/granule/
install -p -m 0644 granule-hildon.conf ${RPM_BUILD_ROOT}%{_datadir}/granule/
install -p -m 0644 granule-pda.conf ${RPM_BUILD_ROOT}%{_datadir}/granule/
install -p -m 0644 pixmaps/*.png ${RPM_BUILD_ROOT}%{_datadir}/granule/pixmaps/
install -p -m 0644 *.dtd ${RPM_BUILD_ROOT}%{_datadir}/granule/xml/

desktop-file-install --vendor ""  \
        --dir ${RPM_BUILD_ROOT}%{_datadir}/applications  \
        --add-category X-Fedora  \
        %{name}.desktop


%clean
%{__rm} -rf ${RPM_BUILD_ROOT}


%files -f %{name}.lang
%defattr(-,root,root, -)
%doc COPYING AUTHORS README ChangeLog NEWS 
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/pixmaps/%{name}.png
%{_datadir}/%{name}/


%changelog
* Sun Aug 31 2008 Vladislav Grinchenko <vlg[AT]users.sourceforge.net> - 1.4.0-6
- Fix config files installation.

* Fri Jul 25 2008 Tom Callaway <tcallawa[AT]redhat.com> - 1.3.0-2
- Fix license tag

* Wed Oct 18 2006 Michael Schwendt <mschwendt[AT]users.sf.net> - 1.2.3-2
- Fix missing BR and remaining spec issues.

* Sat Oct 14 2006 Vladislav Grinchenko <vlg[AT]users.sourceforge.net> - 1.2.3
- Overall makeup to adhere to the RPM packaging guidelines.

* Sun Nov  6 2005 Vladislav Grinchenko <vlg[AT]users.sourceforge.net> 
- Fixed License tag

* Mon Jul  4 2005 Vladislav Grinchenko <vlg[AT]users.sourceforge.net> 
- Changed XML dir.
 
* Thu Jun  3 2004 Vladislav Grinchenko <vlg[AT]users.sourceforge.net> - 1.0.0
- Initial release.

